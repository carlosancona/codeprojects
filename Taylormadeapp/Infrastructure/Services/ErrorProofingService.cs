using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Intercept;

namespace TMGA.Infrastructure.Services
{

    //captures serial numbers, verifies templates, verifies head covers. if it involves matching or capturing serial codes, it's here

    public class ErrorProofingService : IErrorProofingService
    {
        private readonly CoreContext _coreContext;
        private readonly InterceptContext _interceptContext;
        private readonly IInterceptService _interceptService;
        private readonly ILogger<SiteManagementService> _logger;
        private readonly IMapper _mapper;

        public ErrorProofingService(CoreContext coreContext, InterceptContext interceptContext, ILogger<SiteManagementService> logger, IInterceptService interceptService, IMapper mapper)
        {
            _coreContext = coreContext;
            _interceptContext = interceptContext;
            _logger = logger;
            _interceptService = interceptService;
            _mapper = mapper;
        }

        public async Task<List<string>> GetScannedSerialNumbers(string workOrderNumber) =>
            await _coreContext.WorkOrderSerialCodes
                .Where(_ => _.WorkOrder == workOrderNumber)
                .Select(_ => _.ScannedSerialCode)
                .ToListAsync();

        public async Task CheckHeadCover()
        {

        }

        public async Task CaptureSerial()
        {

        }

        public async Task VerifyShaftCutTemplate()
        {

        }
    }
}