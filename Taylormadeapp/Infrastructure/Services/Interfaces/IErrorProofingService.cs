using TMGA.Shared.ViewModels;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TMGA.Infrastructure.Services
{
    public interface IErrorProofingService
    {
        Task<List<string>> GetScannedSerialNumbers(string workOrderNumber);
        Task CheckHeadCover();
        Task CaptureSerial();
        Task VerifyShaftCutTemplate();
    }
}