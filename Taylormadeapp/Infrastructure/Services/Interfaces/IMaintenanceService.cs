using TMGA.Domain.Models.Engine;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;
using System.Collections.Generic;

namespace TMGA.Infrastructure.Services
{
    public interface IMaintenanceService
    {
        Task<List<MaintenanceQuestionViewModel>> GetEligibleMaintenanceQuestions(int siteId);
        Task<bool> AnswerQuestion(AnsweredQuestion answeredQuestion);
         
    }
}