using TMGA.Shared.ViewModels;
using System.Threading.Tasks;

namespace TMGA.Infrastructure.Services
{
    public interface IUserService
    {
        Task<UserViewModel> Login(string id);

        void Logout(string id);
    }
}