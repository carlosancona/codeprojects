using TMGA.Domain.Models.Engine;
using TMGA.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMGA.Domain.Models.Intercept;

namespace TMGA.Infrastructure.Services
{
    public interface ISiteManagementService
    {
        //sites
        Task<SiteViewModel> CreateSite(string siteName);

        Task<SiteViewModel> GetSite(int id);

        Task<List<SiteViewModel>> GetSites();

        //work order scanning for Search.razor
        Task<List<HistoryViewModel>> GetByWO(string workOrderId);

        Task<List<HistoryViewModel>> GetByShipment(string workOrderId);

        //all users
        Task<int> GetTotalOrdersByCell(int cellId);

        Task<List<StationViewModel>> GetTotalOrdersByCell(int cellId, DateTime? startDate, DateTime? endDate);

        Task<List<StationViewModel>> GetTotalOrdersByStation(int stationId);

        Task<List<StationViewModel>> GetTotalOrdersByStation(int stationId, DateTime? startDate, DateTime? endDate);

        //by user
        Task<List<StationViewModel>> GetTotalOrdersByCellByUser(int cellId, int userId);

        Task<List<StationViewModel>> GetTotalOrdersByCellByUser(int cellId, DateTime? startDate, DateTime? endDate);

        Task<List<StationViewModel>> GetTotalOrdersByStationByUser(int stationId);

        Task<CellAndStationTotalsByUserViewModel> GetTotalOrdersByStationByUser(ScannedFromShopFloorViewModel vm);
        //misc
        Task<List<PrinterViewModel>> GetPrinters();

        //exp
        Task<CellAndStationTotalsByUserViewModel> GetCellAndStationTotalsByUser(int siteId, DateTime? from, DateTime? to);

        Task<List<CuringViewModel>> GetCuringStats(ScannedFromShopFloorViewModel vm);

        Task<bool> ToggleStationHold(StationHoldViewModel stationHoldViewModel);

        Task<List<DefectReasonViewModel>> GetEligibleStationDefects(int id);

        Task<bool> LogSerial(SerialCodeCaptureViewModel serialCodeCaptureViewModel);

        Task<bool> CheckSerial(SerialCodeCaptureViewModel serialCodeCaptureViewModel);
        Task<bool> CheckSerialWithoutWorkOrder(SerialCodeCaptureViewModel serialCodeCaptureViewModel);

        Task<List<string>> GetNeededEANs(List<string> skus);

        Task<List<string>> GetHeadCoverUPCs(List<string> skus);

        Task<string> GetComponentType(string workorderid);

        Task<List<WoComponent>> GetHeadCoversAndMisc(string workorderid);

        Task<WoSku> GetWoSku(string workorderid);

        Task<List<WoComponent>> GetHeadComponents(string workorderid);
    }
}