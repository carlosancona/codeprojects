using TMGA.Domain.Models.Engine;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace TMGA.Infrastructure.Services
{
    public interface ICognosService
    {
        Task<string> GenerateCustomPickList(CellQueueViewModel workOrder);

        Task<string> GenerateStockPickList(CellQueueViewModel workOrder);

        string GetPdfSavePath();

        string GetTodaysFolderName();
    }
}