using TMGA.Domain.Models.Wms;
using TMGA.Shared.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TMGA.Infrastructure.Services
{
    public interface IWmsService
    {
        Task<WaveViewModel> GetWaveById(string waveId);
                                                                           
        Task<IEnumerable<LatestWavesViewModel>> GetLatestCustomWaves();

        Task<IEnumerable<LatestWavesViewModel>> GetLatestStockWaves();
    }
}