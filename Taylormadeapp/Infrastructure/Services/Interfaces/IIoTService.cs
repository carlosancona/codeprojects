using TMGA.Domain.Models.Engine;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;
using System.Collections.Generic;

namespace TMGA.Infrastructure.Services
{
    public interface IIoTService
    {
        Task<bool> RecordCuringTemperature(TemperatureCuringViewModel curingViewModel);
        Task<List<TemperatureCuringViewModel>> GetRecentTemperatureReadings(int n);
        Task<List<TemperatureCuringViewModel>> GetRecentTemperatureReadingsByStationId(int Rows, int StationId);
        Task<bool> CreateCuringRecord(CuringRecordViewModel curingRecordViewModel);
        //Task<bool> CreateCuringRecordByWO(string WorkOrder);
        Task<bool> CreateCuringRecordByWO(CuringRecordViewModel curingRecordViewModel);
        Task<List<CuringRecordViewModel>> GetCuringRecordsByWorkOrder(string WorkOrder);
        Task<List<CuringRecordViewModel>> GetRecentCuringRecords(int Rows, int StationId);
        Task<List<CuringRecordViewModel>> GetAllRecentCuringRecords(int StationId);
        Task<bool> FinishedProcessing(string WO);

    }
}