
using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Intercept;
using TMGA.Shared.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TMGA.Infrastructure.Services
{
    public interface ICapacityPlanningService
    {

        Task<WavePlanViewModel> GetWavePlan(string waveId);

        Cell GetLeastBusyCell(IEnumerable<Cell> cells, string cellType, WorkOrderViewModel workOrder);

        Task<WavePlanViewModel> ForceWaveReRelease(string waveId);
    }
}