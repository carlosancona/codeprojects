using TMGA.Domain.Models.Intercept;
using TMGA.Shared.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TMGA.Infrastructure.Services
{
    public interface IInterceptService
    {
        Task<WorkOrderViewModel> GetOrder(string id);

        Task<List<WorkOrderViewModel>> GetOrders(List<string> ids);

        Task<ToShopFloorViewModel> GetOrder(ScannedFromShopFloorViewModel vm);
        Task<List<ToShopFloorViewModel>> GetShipment(ScannedFromShopFloorViewModel vm);

        void UpdateShipmentIds(List<WorkOrderViewModel> vm);
    }
}