using TMGA.Shared.ViewModels;
using System.Threading.Tasks;

namespace TMGA.Infrastructure.Services
{
    public interface IQueueService
    {
        Task<ToShopFloorViewModel> Done(StationDoneViewModel stationDoneViewModel);
        Task<bool> LogDefect(DefectViewModel vm);
    }
}