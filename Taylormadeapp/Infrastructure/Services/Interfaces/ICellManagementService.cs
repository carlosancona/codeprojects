using TMGA.Domain.Models.Engine;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace TMGA.Infrastructure.Services
{
    public interface ICellManagementService
    {
        //Individiual Cells
        Task<CellViewModel> GetCell(int id);

        Task<CellViewModel> GetCellWithQueue(int id);

        Task<CellViewModel> GetCellWithStations(int id);

        Task<CellViewModel> GetCellWithQueueAndStations(int id);

        Task<List<CellViewModel>> GetCells();

        //Multiple Cells
        Task<List<CellViewModel>> GetAllCells();

        Task<List<CellViewModel>> GetAllCellsBySiteId(int siteId);

        Task<List<CellViewModel>> GetCells(List<int> ids);

        Task<List<CellViewModel>> GetCellWithQueue(List<int> ids);

        Task<List<CellViewModel>> GetCellWithStations(List<int> ids);

        Task<List<CellViewModel>> GetCellWithQueueAndStations(List<int> ids);

        //Inserting Cells
        Task<CellViewModel> CreateCell(CellViewModel newCell);

        //Editing Cells
        Task<CellViewModel> EditCell(CellViewModel changedCell);

        Task<CellViewModel> ToggleActiveOrInactive(CellViewModel existingCell);

        Task<CellViewModel> ToggleCustomOrStock(CellViewModel existingCell); 
    }
}