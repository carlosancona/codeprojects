using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Intercept;
using TMGA.Domain.Models.Wms;
using TMGA.Shared.ViewModels;
using Api.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace TMGA.Infrastructure.Services
{
    public class CapacityPlanningService : ICapacityPlanningService
    {
        private readonly ILogger<CapacityPlanningService> _logger;
        private readonly IDistributedCache _redis;
        private readonly InterceptContext _interceptContext;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        private readonly CoreContext _coreContext;
        private readonly WMSContext _wmsContext;

        private readonly IWmsService _wmsService;

        public CapacityPlanningService(CoreContext coreContext, IWmsService wmsService, ILogger<CapacityPlanningService> logger, IDistributedCache redis, InterceptContext interceptContext, WMSContext wmsContext, IConfiguration configuration, IMapper mapper)
        {
            _coreContext = coreContext;
            _wmsContext = wmsContext;
            _wmsService = wmsService;
            _logger = logger;
            _redis = redis;
            _interceptContext = interceptContext;
            _configuration = configuration;
            _mapper = mapper;
        }

        public Cell GetLeastBusyCell(IEnumerable<Cell> cells, string cellType, WorkOrderViewModel workOrder)
        {
            Cell ret;
            var siteId = Convert.ToInt32(_configuration["SITE_ID"]);

            //parameter 1, cell order
            var cellsOrderedByClubsQueued = cells.Where(_ => _.Site.Id == siteId && _.IsActive && _.CellType == cellType).ToList();

            //parameter 2 & 3, bending ratios, remove those at capacity, then sort by if last had bending (false first)
            if (workOrder.HasBending())
            {
                cellsOrderedByClubsQueued =
                    cellsOrderedByClubsQueued.Where(_ => _.CurrentBendingRatio <= _.FixedBendingRatio)
                        .OrderBy(_ => _.TotalOrdersThatHaveHadBending)
                            .ThenBy(_ => _.LastHadBending)
                            .ThenBy(_ => _.ClubsInCell)
                        .ToList();
            }

            //parameter 3, large order cutoff 
            cellsOrderedByClubsQueued = cellsOrderedByClubsQueued.Where(_ => _.ClubCutoffForLargeOrders >= workOrder.ClubCount()).ToList();

            ret = cellsOrderedByClubsQueued.FirstOrDefault();

            //if no cells, return cell with least clubs (or enter infinity loop as per docs)
            if (ret == null)
            {
                if (workOrder.HasBending())
                {
                    //get the best bending line, regardless of prior rules
                    ret = cells.Where(_ => _.Site.Id == siteId && _.IsActive && _.CellType == cellType).OrderBy(_ => _.ClubsInCell).ThenBy(_ => _.CurrentBendingRatio).Take(1).FirstOrDefault();
                }
                else
                {
                    //default, select cell with least clubs (no bending)
                    ret =  cells.Where(_ => _.Site.Id == siteId && _.IsActive && _.CellType == cellType).OrderBy(_ => _.ClubsInCell).Take(1).FirstOrDefault();
                }
            }

            return ret;
        }
         
        public async Task<WavePlanViewModel> GetWavePlan(string waveId)
        {
            var siteId = Convert.ToInt32(_configuration["SITE_ID"]);
            var output = new WavePlanViewModel();
            var isReleased = this._coreContext.ReleasedWaves.FirstOrDefault(_ => _.ReleasedWaveId == waveId);

            if (isReleased != null)
            {
                output.WavePlanStatus = WavePlanStatus.InProgress;
                output.ReleasedOn = isReleased.ReleasedOn;
                // wave released and currently working, send back real time cell return
                output.CellQueues = _mapper.Map<List<CellQueueViewModel>>(await this._coreContext.CellQueues.Where(_ => _.Wave == waveId).Include(_ => _.Cell).ToListAsync().ConfigureAwait(false));
                return output;
            }
            else
            {
                // wave may not exist in the queue, but may be released and completed. check snapshot table
                // else, recalculate new plan and release wave
                var isSnapshot = this._coreContext.ReleasedWaves.FirstOrDefault(_ => _.ReleasedWaveId == waveId);

                if (isSnapshot != null)
                {
                    output.WavePlanStatus = WavePlanStatus.Complete;
                    output.ReleasedOn = isSnapshot.ReleasedOn;

                    // automapper pls thanks
                    output.CellQueues = _mapper.Map<List<CellQueueViewModel>>(await this._coreContext.CellQueues.Where(_ => _.Wave == waveId).Include(_ => _.Cell).Select(_ => new CellQueue
                    {
                        AssemblySequence = _.AssemblySequence,
                        Cell = _.Cell,
                        ClubCount = _.ClubCount,
                        Id = _.Id,
                        IsPrinted = _.IsPrinted,
                        NextWorkOrder = _.NextWorkOrder,
                        ReasonCode = _.ReasonCode,
                        Station = _.Station,
                        Wave = _.Wave,
                        WO = _.WO
                    }).ToListAsync().ConfigureAwait(false));

                    return output;
                }
                else
                {
                    // if not previously released
                    if (!this._coreContext.ReleasedWaves.Any(_ => _.ReleasedWaveId == waveId))
                    {

                        // brand new wave plan at this point
                        output.WavePlanStatus = WavePlanStatus.New;

                        var cellQueues = new List<CellQueue>();
                        var cellQueueSnapshots = new List<CellQueueSnapshot>();
                        var wave = await this._wmsService.GetWaveById(waveId).ConfigureAwait(false);
                        var wmsWorkOrderNumbers = wave.WorkOrders.Select(_ => _.WorkOrder).Distinct().ToList();
                        var nonSleeveWorkOrderNumbers = _interceptContext.WoOrder.Where(_ => wmsWorkOrderNumbers.Contains(_.WorkOrder)).Select(_ => _.WorkOrder).Distinct().ToList();
                        var sleeveWorkOrderNumbers = _wmsContext.WorkOrderHeaders.Where(_ => !nonSleeveWorkOrderNumbers.Contains(_.WorkOrderHeaderId) && _.WaveId == waveId).Select(_ => _.WorkOrderHeaderId).Distinct().ToList();
                        var cells = _coreContext.Cells.Where(_ => _.Site.Id == siteId && _.IsActive).Include(_ => _.Site).Include(_ => _.Stations); // magic number 6 == tijuana
                        var groupedByShipment = wave.WorkOrders.GroupBy(_ => _.Shipment).OrderBy(shipment => shipment.Select(x => x.CreationDate).FirstOrDefault()).ToList();

                        // add to released waves table
                        var releasedWave = new ReleasedWave { ReleasedWaveId = wave.WaveId, ReleasedOn = DateTime.Now.ToUniversalTime(), Notes = string.Empty };
                        this._coreContext.ReleasedWaves.Add(releasedWave);
                        this._coreContext.SaveChanges();

                        foreach (var shipment in groupedByShipment)
                        {
                            var determineStockOrCustom = shipment.First();
                            var cell = new Cell();

                            if (determineStockOrCustom.WorkOrderType == "KUST")
                            {
                                cell = this.GetLeastBusyCell(cells, "Custom", shipment.First());
                            }
                            else
                            {
                                cell = this.GetLeastBusyCell(cells, "Stock", shipment.First());
                            }

                            foreach (var order in shipment)
                            {
                                // cache order
                                this._logger.LogInformation($"Setting cache for order {order.WorkOrder}");
                                await this._redis.SetAsync($"WO-{order.WorkOrder}", order, new DistributedCacheEntryOptions() { SlidingExpiration = TimeSpan.FromDays(7) }).ConfigureAwait(false);

                                var clubCount = order.ClubCount(); 
                                var thisStation = cell.Stations.Where(_ => _.StationOrder > 0).OrderBy(_ => _.StationOrder).Take(1).FirstOrDefault();
                                var thisStationId = cell.Stations.Where(_ => _.StationOrder > 0).OrderBy(_ => _.StationOrder).Take(1).FirstOrDefault().Id;

                                if (order.HasBending())
                                {
                                    cell.LastHadBending = true;
                                    cell.TotalOrdersThatHaveHadBending++;
                                }
                                else
                                {
                                    cell.LastHadBending = false;
                                }

                                order.AssignedCellId = cell.Id;
                                order.AssignedCell = cell;
                                cell.AssemblySequence++;
                                order.AssemblySequence = cell.AssemblySequence;
                                cell.ClubsInLastOrder = clubCount;
                                cell.TotalOrders++;
                                cell.ClubsInCell += clubCount;
                                cell.ClubsQueued += clubCount;
                                order.CapacityReason = "Normal";

                                var cellQueue = new CellQueue
                                {
                                    AssemblySequence = cell.AssemblySequence,
                                    Cell = cell,
                                    ClubCount = clubCount,
                                    IsPrinted = false,
                                    Station = thisStation,
                                    ReasonCode = order.CapacityReason,
                                    WO = order.WorkOrder,
                                    ShipmentId = shipment.Key,
                                    Wave = wave.WaveId,
                                };

                                //check if sleeve order
                                if (sleeveWorkOrderNumbers.Contains(order.WorkOrder)) cellQueue.IsSleeveOrder = true;

                                cellQueues.Add(cellQueue);

                                var cellQueueSnaphot = new CellQueueSnapshot();

                                cellQueueSnaphot.AssemblySequence = cellQueue.AssemblySequence;
                                cellQueueSnaphot.Cell = cellQueue.Cell;
                                cellQueueSnaphot.ClubCount = cellQueue.ClubCount;
                                cellQueueSnaphot.IsPrinted = cellQueue.IsPrinted;
                                cellQueueSnaphot.NextWorkOrder = cellQueue.NextWorkOrder;
                                cellQueueSnaphot.ReasonCode = cellQueue.ReasonCode;
                                cellQueueSnaphot.StationId = thisStationId;
                                cellQueueSnaphot.Wave = cellQueue.Wave;
                                cellQueueSnaphot.ShipmentId = cellQueue.ShipmentId;
                                cellQueueSnaphot.WO = cellQueue.WO;
                                cellQueueSnaphot.ReleasedWave = releasedWave;

                                cellQueueSnapshots.Add(cellQueueSnaphot);
                            }
                        }


                        this._coreContext.CellQueues.AddRange(cellQueues);
                        this._coreContext.CellQueueSnapshots.AddRange(cellQueueSnapshots);

                        this._coreContext.SaveChanges();

                        output.CellQueues = _mapper.Map<List<CellQueueViewModel>>(cellQueues);
                        output.ReleasedOn = releasedWave.ReleasedOn;
                    }

                    return output;
                }
            }
        }

        public async Task<WavePlanViewModel> ForceWaveReRelease(string waveId)
        {
            ///
            /// hard release, ignores what has been done already
            ///
            var cellQueueSnapshots = _coreContext.CellQueueSnapshots.Where(_ => _.Wave == waveId).ToList();
            var queuedWorkOrders = _coreContext.CellQueues.Where(_ => _.Wave == waveId).ToList();
            var releasedWave = _coreContext.ReleasedWaves.Where(_ => _.ReleasedWaveId == waveId).ToList();
            var siteId = Convert.ToInt32(_configuration["SITE_ID"]);

            //get existing work order numbers in queue
            var queuedWorkOrderNumbers = queuedWorkOrders.Select(_ => _.WO).ToList();
            var completedWorkOrderNumbers = _coreContext.StationHistory.Where(_ => queuedWorkOrderNumbers.Contains(_.WO)).Select(_ => _.WO).ToList();

            if (cellQueueSnapshots != null && cellQueueSnapshots.Count > 0)
            {
                _coreContext.Remove(cellQueueSnapshots);
                await _coreContext.SaveChangesAsync();
            }

            if (queuedWorkOrders != null && queuedWorkOrders.Count > 0)
            {
                _coreContext.Remove(queuedWorkOrders);
                await _coreContext.SaveChangesAsync();
            }

            if (releasedWave != null && releasedWave.Count > 0)
            {
                _coreContext.Remove(releasedWave);
                await _coreContext.SaveChangesAsync();
            }

            //clear cache
            foreach (var key in queuedWorkOrderNumbers) await _redis.RemoveAsync(key);

            return await GetWavePlan(waveId);

        }
    }
}