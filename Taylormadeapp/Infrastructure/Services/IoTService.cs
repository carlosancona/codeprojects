using TMGA.Domain.Models.Engine;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using TMGA.Domain.Models.Engine;
using TMGA.Shared.ViewModels;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using System.Linq;


namespace TMGA.Infrastructure.Services
{

    public class IoTService : IIoTService
    {
        private readonly CoreContext _coreContext;
        private readonly ILogger<IoTService> _logger;
        private readonly IMapper _mapper;
        public IConfiguration Configuration { get; }

        public IoTService(CoreContext coreContext, ILogger<IoTService> logger, IMapper mapper, IConfiguration configuration)
        {
            _coreContext = coreContext;
            _logger = logger;
            _mapper = mapper;
            Configuration = configuration;
        }

        public async Task<bool> RecordCuringTemperature(TemperatureCuringViewModel curingViewModel)
        {
            _logger.LogInformation($"Writing curing temperate for station {curingViewModel.StationId}");

            try
            {
                _coreContext.CuringTemperatures.Add(new CuringTemperature
                {
                    StationId = curingViewModel.StationId,
                    Temperature = curingViewModel.Temperature,
                    //Timestamp = DateTime.Now
                    Timestamp = curingViewModel.Timestamp
                });

                await _coreContext.SaveChangesAsync();
            }

            catch (Exception ex)
            {
                _logger.LogError(ex?.InnerException?.ToString());
                return false;
            }

            return true;

        }


        public async Task<List<TemperatureCuringViewModel>> GetRecentTemperatureReadings(int n)
        {
            var list = await _coreContext.CuringTemperatures.OrderByDescending(_ => _.Timestamp).Take(n).ToListAsync();

            var ret = new List<TemperatureCuringViewModel>();

            list.ForEach(_ => ret.Add(new TemperatureCuringViewModel
            {
                Temperature = _.Temperature,
                StationId = _.StationId,
                Timestamp = _.Timestamp
            }));

            return ret;

            //return _mapper.Map<List<TemperatureCuringViewModel>>(list);
        }


        public async Task<List<TemperatureCuringViewModel>> GetRecentTemperatureReadingsByStationId(int Rows, int StationId)
        {
            //var list = await _coreContext.CuringTemperatures.OrderByDescending(_ => _.Timestamp).Take(rows).ToListAsync();
            var list = await _coreContext.CuringTemperatures
                            .Where(r => r.StationId == StationId)
                            .OrderByDescending(_ => _.Timestamp).Take(Rows).ToListAsync();

            var ret = new List<TemperatureCuringViewModel>();

            list.ForEach(_ => ret.Add(new TemperatureCuringViewModel
            {
                Temperature = _.Temperature,
                StationId = _.StationId,
                Timestamp = _.Timestamp
            }));

            return ret;


        }


        public async Task<List<CuringRecordViewModel>> GetCuringRecordsByWorkOrder(string WorkOrder)
        {

            var list = await _coreContext.CuringRecords
                            .Where(r => r.WorkOrder == WorkOrder)
                            .OrderByDescending(_ => _.CurrentCuringTime).ToListAsync();

            var ret = new List<CuringRecordViewModel>();

            list.ForEach(_ => ret.Add(new CuringRecordViewModel
            {
                Id = _.Id,
                WorkOrder = _.WorkOrder,
                ClubDescription = _.ClubDescription,
                StationId = _.StationId,
                Temperature = _.Temperature,
                Timestamp = _.Timestamp,
                Status = _.Status,
                CurrentCuringTime = _.CurrentCuringTime,
                OriginalCuringTime = _.OriginalCuringTime,
                Quantity = _.Quantity,
                IsProcessed = _.IsProcessed
            }));

            return ret;


        }

        private double CalculateCarouselTime(double currentTemp, double carouselTime, int lowerBound, int upperBound)
        {
            double diff;
            double remainder;

            if (currentTemp < lowerBound)
            {
                diff = lowerBound - currentTemp;
                remainder = Math.Ceiling(diff / 5);
                carouselTime += remainder * 0.3;
            }
            else if (currentTemp > upperBound)
            {
                diff = currentTemp - upperBound;
                remainder = Math.Ceiling(diff / 5);
                carouselTime -= remainder * 0.3;
            }

            return carouselTime;

        }

        public async Task<bool> CreateCuringRecordByWO(CuringRecordViewModel curingRecordViewModel)
        {
            try
            {

                var LatestTempReading = await _coreContext.CuringTemperatures.Where(r => r.StationId == curingRecordViewModel.StationId).OrderByDescending(_ => _.Timestamp).FirstOrDefaultAsync();
                var CuringTimeCheck = Convert.ToInt32(Configuration["CURING_TIME_TESTING_SWITCH"]);
                double CuringTime;

                if (CuringTimeCheck < 0)
                {
                    CuringTime = Convert.ToDouble(Configuration["CURING_TIME_DEFAULT_VALUE"]);
                }
                else
                {
                    CuringTime = CalculateCarouselTime(Math.Round(LatestTempReading.Temperature, 2), 30.00, 38, 42);
                }




                _coreContext.CuringRecords.Add(new CuringRecord
                {
                    WorkOrder = curingRecordViewModel.WorkOrder,
                    ClubDescription = curingRecordViewModel.ClubDescription,
                    StationId = curingRecordViewModel.StationId,
                    Temperature = LatestTempReading.Temperature,
                    Timestamp = LatestTempReading.Timestamp,
                    Status = "Start",
                    CurrentCuringTime = CuringTime,
                    OriginalCuringTime = CuringTime,
                    Quantity = curingRecordViewModel.Quantity,
                    IsProcessed = "No"
                });

                await _coreContext.SaveChangesAsync();
            }

            catch (Exception ex)
            {
                _logger.LogError(ex?.InnerException?.ToString());
                return false;
            }

            return true;
        }


        public async Task<bool> CreateCuringRecord(CuringRecordViewModel curingRecordViewModel)
        {
            //_logger.LogInformation($"Writing curing temperate for station {curingRecordViewModel.StationId}");

            try
            {
                _coreContext.CuringRecords.Add(new CuringRecord
                {
                    WorkOrder = curingRecordViewModel.WorkOrder,
                    ClubDescription = curingRecordViewModel.ClubDescription,
                    StationId = curingRecordViewModel.StationId,
                    Temperature = curingRecordViewModel.Temperature,
                    Timestamp = curingRecordViewModel.Timestamp,
                    Status = curingRecordViewModel.Status,
                    CurrentCuringTime = curingRecordViewModel.CurrentCuringTime,
                    OriginalCuringTime = curingRecordViewModel.OriginalCuringTime,
                    Quantity = curingRecordViewModel.Quantity,
                    IsProcessed = curingRecordViewModel.IsProcessed
                });

                await _coreContext.SaveChangesAsync();
            }

            catch (Exception ex)
            {
                _logger.LogError(ex?.InnerException?.ToString());
                return false;
            }

            return true;

        }


        public async Task<bool> FinishedProcessing(string WO)
        {
            bool retStatus = false;
            var x = _coreContext.CuringRecords.Where(r => r.WorkOrder == WO).OrderByDescending(_ => _.CurrentCuringTime).LastOrDefault();
            if (x != null && x.Status == "End")
            {
                x.IsProcessed = "Yes";
                _coreContext.CuringRecords.Update(x);
                await _coreContext.SaveChangesAsync();
                retStatus = true;
            }
            return retStatus;

        }



        public async Task<List<CuringRecordViewModel>> GetAllRecentCuringRecords(int StationId)
        {

            var listtemp = await _coreContext.CuringRecords
                            .Where(r => r.StationId == StationId)
                            //.Select(wo => new { wo.WorkOrder, wo.CuringTime, wo.StationId, wo.Status, wo.Temperature, wo.Timestamp })
                            .Select(wo => wo.WorkOrder)
                            .Distinct()
                            .ToListAsync();

            var ret = new List<CuringRecordViewModel>();


            listtemp.ForEach(o =>
            {
                var x = _coreContext.CuringRecords.Where(r => r.WorkOrder == o).OrderByDescending(_ => _.CurrentCuringTime).LastOrDefault();
                if (x.IsProcessed != "Yes")
                {
                    ret.Add(new CuringRecordViewModel
                    {
                        Id = x.Id,
                        WorkOrder = x.WorkOrder,
                        ClubDescription = x.ClubDescription,
                        StationId = x.StationId,
                        Temperature = x.Temperature,
                        Timestamp = x.Timestamp,
                        Status = x.Status,
                        CurrentCuringTime = x.CurrentCuringTime,
                        OriginalCuringTime = x.OriginalCuringTime,
                        Quantity = x.Quantity,
                        IsProcessed = x.IsProcessed
                    });
                }

            });

            return ret;
        }

        public async Task<List<CuringRecordViewModel>> GetRecentCuringRecords(int Rows, int StationId)
        {




            var list = await _coreContext.CuringRecords
                            .Where(r => r.StationId == StationId && r.Status == "Start")
                            .OrderByDescending(_ => _.CurrentCuringTime).Take(Rows).ToListAsync();

            var ret = new List<CuringRecordViewModel>();

            list.ForEach(_ => ret.Add(new CuringRecordViewModel
            {
                Id = _.Id,
                WorkOrder = _.WorkOrder,
                ClubDescription = _.ClubDescription,
                StationId = _.StationId,
                Temperature = _.Temperature,
                Timestamp = _.Timestamp,
                Status = _.Status,
                CurrentCuringTime = _.CurrentCuringTime,
                OriginalCuringTime = _.OriginalCuringTime,
                Quantity = _.Quantity,
                IsProcessed = _.IsProcessed
            }));

            return ret;


        }
    }
}