using TMGA.Domain.Models.Engine;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;
using AutoMapper;

namespace TMGA.Infrastructure.Services
{
    public class CellManagementService : ICellManagementService
    {
        private readonly CoreContext _coreContext;
        private readonly ILogger<SiteManagementService> _logger;
        private readonly IMapper _mapper;

        public CellManagementService(CoreContext coreContext, ILogger<SiteManagementService> logger, IMapper mapper)
        {
            _coreContext = coreContext;
            _logger = logger;
            _mapper = mapper;
        }


        //Individual Cells
        public async Task<CellViewModel> GetCell(int id)
        {
            return _mapper.Map<CellViewModel>(await _coreContext.Cells.Include(_ => _.Site).FirstOrDefaultAsync(_ => _.Id == id));
        }

        public async Task<CellViewModel> GetCellWithQueue(int id)
        {
            return _mapper.Map<CellViewModel>(await _coreContext.Cells.Include(_ => _.Site).Include(_ => _.Queue).FirstOrDefaultAsync(_ => _.Id == id));
        }

        public async Task<CellViewModel> GetCellWithStations(int id)
        {
            return _mapper.Map<CellViewModel>(await _coreContext.Cells.Include(_ => _.Site).Include(_ => _.Stations).FirstOrDefaultAsync(_ => _.Id == id));
        }

        public async Task<CellViewModel> GetCellWithQueueAndStations(int id)
        {
            return _mapper.Map<CellViewModel>(await _coreContext.Cells.Include(_ => _.Site).Include(_ => _.Queue).Include(_ => _.Stations).FirstOrDefaultAsync(_ => _.Id == id));
        }

        public async Task<List<CellViewModel>> GetCells()
        {
            return _mapper.Map<List<CellViewModel>>(await _coreContext.Cells.Include(_ => _.Site).Include(_ => _.Stations).ToListAsync());
        }

        //Multiple Cells
        public async Task<List<CellViewModel>> GetAllCells()
        {
            return _mapper.Map<List<CellViewModel>>(await _coreContext.Cells.Include(_ => _.Site).Include(_ => _.Stations).ToListAsync());
        }

        public async Task<List<CellViewModel>> GetAllCellsBySiteId(int siteId)
        {
            return _mapper.Map<List<CellViewModel>>(await _coreContext.Cells.Where(_ => _.Site.Id == siteId).Include(_ => _.Site).ToListAsync());
        }

        public async Task<List<CellViewModel>> GetCells(List<int> ids) =>
              _mapper.Map<List<CellViewModel>>(await _coreContext.Cells.Include(_ => _.Site).Where(_ => ids.Contains(_.Id)).ToListAsync());


        public async Task<List<CellViewModel>> GetCellWithQueue(List<int> ids) =>
              _mapper.Map<List<CellViewModel>>(await _coreContext.Cells.Include(_ => _.Site).Include(_ => _.Queue).Where(_ => ids.Contains(_.Id)).ToListAsync());


        public async Task<List<CellViewModel>> GetCellWithStations(List<int> ids) =>
              _mapper.Map<List<CellViewModel>>(await _coreContext.Cells.Include(_ => _.Site).Include(_ => _.Stations).Where(_ => ids.Contains(_.Id)).ToListAsync());


        public async Task<List<CellViewModel>> GetCellWithQueueAndStations(List<int> ids) =>
              _mapper.Map<List<CellViewModel>>(await _coreContext.Cells.Include(_ => _.Site).Include(_ => _.Queue).Include(_ => _.Stations).Where(_ => ids.Contains(_.Id)).ToListAsync()); 

        //Inserting Cells
        public async Task<CellViewModel> CreateCell(CellViewModel newCell)
        {
            _logger.LogInformation($"Creating new cell: {newCell?.Desc}...");
            _coreContext.Cells.Add(_mapper.Map<Cell>(newCell));
            await _coreContext.SaveChangesAsync();
            return _mapper.Map<CellViewModel>(newCell);
        } 

        //Editing Cells
        public async Task<CellViewModel> EditCell(CellViewModel changedCell)
        {
            Cell theCell = await this._coreContext.Cells.FirstOrDefaultAsync(_ => _.Id == changedCell.Id);
            theCell.FixedBendingRatio = changedCell.FixedBendingRatio;
            theCell.CellType = changedCell.CellType;
            theCell.Grouping = changedCell.Grouping;
            this._coreContext.Entry(theCell).State = EntityState.Modified;
            await this._coreContext.SaveChangesAsync();
            return _mapper.Map<CellViewModel>(await this._coreContext.Cells.FirstOrDefaultAsync(_ => _.Id == changedCell.Id));
        }

        public async Task<CellViewModel> ToggleActiveOrInactive(CellViewModel existingCell)
        {
            Cell theCell = await this._coreContext.Cells.FirstOrDefaultAsync(_ => _.Id == existingCell.Id);
            theCell.IsActive = !theCell.IsActive;
            this._coreContext.Entry(theCell).State = EntityState.Modified;
            await this._coreContext.SaveChangesAsync();
            return _mapper.Map<CellViewModel>(await this._coreContext.Cells.FirstOrDefaultAsync(_ => _.Id == existingCell.Id));
        }

        public async Task<CellViewModel> ToggleCustomOrStock(CellViewModel existingCell)
        {
            Cell theCell = await this._coreContext.Cells.FirstOrDefaultAsync(_ => _.Id == existingCell.Id);
            theCell.CellType = existingCell.CellType;
            theCell.IsActive = existingCell.IsActive;
            this._coreContext.Entry(theCell).State = EntityState.Modified;
            await this._coreContext.SaveChangesAsync();
            return _mapper.Map<CellViewModel>(await this._coreContext.Cells.FirstOrDefaultAsync(_ => _.Id == existingCell.Id));
        }

        public async Task<List<Printer>> GetPrinters() => await this._coreContext.Printers.ToListAsync();

    }
}