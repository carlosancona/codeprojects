using TMGA.Domain.Models.Engine;
using TMGA.Shared.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using TMGA.Domain.Models.Intercept;

namespace TMGA.Infrastructure.Services
{
    public class SiteManagementService : ISiteManagementService
    {
        private readonly CoreContext _coreContext;
        private readonly InterceptContext _interceptContext;
        private readonly IInterceptService _interceptService;
        private readonly ILogger<SiteManagementService> _logger;
        private readonly IMapper _mapper;

        public SiteManagementService(CoreContext coreContext, InterceptContext interceptContext, ILogger<SiteManagementService> logger, IInterceptService interceptService, IMapper mapper)
        {
            _coreContext = coreContext;
            _interceptContext = interceptContext;
            _logger = logger;
            _interceptService = interceptService;
            _mapper = mapper;
        }

        //Sites
        public async Task<SiteViewModel> CreateSite(string siteName)
        {
            var site = new Site { Desc = siteName };
            _coreContext.Sites.Add(site);
            await _coreContext.SaveChangesAsync();
            return _mapper.Map<SiteViewModel>(site);
        }

        public async Task<SiteViewModel> GetSite(int id) =>
              _mapper.Map<SiteViewModel>(await _coreContext.Sites
                  .Include(_ => _.Cells)
                    .ThenInclude(_ => _.Queue)
                  .Include(_ => _.Cells)
                    .ThenInclude(_ => _.Stations)
                  .FirstOrDefaultAsync(_ => _.Id == id));

        public async Task<List<SiteViewModel>> GetSites() =>
              _mapper.Map<List<SiteViewModel>>(await _coreContext.Sites
                  .Include(_ => _.Cells)
                    .ThenInclude(_ => _.Stations)
                  .ToListAsync());

        public async Task<List<PrinterViewModel>> GetPrinters() => _mapper.Map<List<PrinterViewModel>>(await _coreContext.Printers.ToListAsync());

        public async Task<List<HistoryViewModel>> GetByWO(string workOrderId)
        {
            var output = new List<HistoryViewModel>();

            var thisWo = new HistoryViewModel();

            thisWo.WO = workOrderId;

            thisWo.Queue = _mapper.Map<List<CellQueueViewModel>>(await _coreContext.CellQueues.Where(_ => _.WO == workOrderId).ToListAsync());

            thisWo.StationHistory = _mapper.Map<List<StationHistoryViewModel>>(await _coreContext.StationHistory.Where(_ => _.WO == workOrderId).Include(_ => _.Station).ThenInclude(_ => _.Cell).ToListAsync());

            output.Add(thisWo);

            return output;
        }

        public async Task<List<HistoryViewModel>> GetByShipment(string workOrderId)
        {
            var output = new List<HistoryViewModel>();

            var allWOs = await _coreContext.StationHistory.Where(_ => _.ShipmentId == workOrderId).Select(_ => _.WO).Distinct().ToListAsync();

            //if user scans shipment, but no orders in the shipment have any stationhistory yet, create a dummy order
            if (allWOs.Count() == 0)
            {
                var thisWo = new HistoryViewModel();

                thisWo.WO = workOrderId;

                thisWo.Queue = _mapper.Map<List<CellQueueViewModel>>(await _coreContext.CellQueues.Where(_ => _.ShipmentId == workOrderId).ToListAsync());

                thisWo.StationHistory = _mapper.Map<List<StationHistoryViewModel>>(await _coreContext.StationHistory.Where(_ => _.ShipmentId == workOrderId).Include(_ => _.Station).ThenInclude(_ => _.Cell).ToListAsync());

                output.Add(thisWo);
            }
            else
            {
                foreach (var wo in allWOs)
                {
                    var thisWo = new HistoryViewModel();

                    thisWo.WO = wo;

                    thisWo.Queue = _mapper.Map<List<CellQueueViewModel>>(await _coreContext.CellQueues.Where(_ => _.ShipmentId == workOrderId && _.WO == wo).ToListAsync());

                    thisWo.StationHistory = _mapper.Map<List<StationHistoryViewModel>>(await _coreContext.StationHistory.Where(_ => _.ShipmentId == workOrderId && _.WO == wo).Include(_ => _.Station).ThenInclude(_ => _.Cell).ToListAsync());

                    output.Add(thisWo);
                }
            }
            return output;
        }

        public async Task<int> GetTotalOrdersByCell(int cellId) =>
            await _coreContext.StationHistory
                    .Where(_ => _.Station.Cell.Id == cellId)
                    .Select(_ => _.WO)
                    .Distinct()
                    .CountAsync();

        public async Task<CellAndStationTotalsByUserViewModel> GetCellAndStationTotalsByUser(int siteId, DateTime? from, DateTime? to)
        {
            //returns total orders and clubs for a site, by user, and optionally by date range

            //default to all time
            from = from ?? DateTime.MinValue;
            to = to ?? DateTime.MaxValue;

            return await _coreContext.StationHistory
                   .Where(_ => _.SiteId == siteId && _.On >= from && _.On <= to)
                   .GroupBy(_ => new { _.Station, _.UserId }, (station, user) => new CellAndStationTotalsByUserViewModel
                   {
                       StationId = station.Station.Id,
                       CellId = station.Station.Cell.Id,
                       StationName = station.Station.StationTypeTemp,
                       CellName = station.Station.Cell.Desc,
                       UserTotals = user.Select(stationHistory => new UserTotalsViewModel
                       {
                           UserId = stationHistory.UserId,
                           TotalOrders = stationHistory.WO.Distinct().Count(),
                           FirstOrder = user.Min(x => x.On),
                           LastOrder = user.Max(x => x.On)
                       }).ToList()
                   }).FirstOrDefaultAsync();
        }

        //Total Orders
        //all users
        public async Task<List<StationViewModel>> GetTotalOrdersByCell(int cellId, DateTime? startDate, DateTime? endDate)
        {
            throw new NotImplementedException();
        }

        public async Task<List<StationViewModel>> GetTotalOrdersByStation(int stationId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<StationViewModel>> GetTotalOrdersByStation(int stationId, DateTime? startDate, DateTime? endDate)
        {
            throw new NotImplementedException();
        }

        //Total Orders
        //by user
        public async Task<List<StationViewModel>> GetTotalOrdersByCellByUser(int cellId, int userId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<StationViewModel>> GetTotalOrdersByCellByUser(int cellId, DateTime? startDate, DateTime? endDate)
        {
            throw new NotImplementedException();
        }

        public async Task<List<StationViewModel>> GetTotalOrdersByStationByUser(int stationId)
        {
            throw new NotImplementedException();
        }

        public async Task<CellAndStationTotalsByUserViewModel> GetTotalOrdersByStationByUser(ScannedFromShopFloorViewModel vm)
        {
            var output = new CellAndStationTotalsByUserViewModel();

            var totalOrders = await _coreContext.StationHistory.Where(_ => _.StationId == vm.ThisStation.Id && _.UserId == vm.UserId).Select(_ => _.WO).Distinct().CountAsync();
            var totalOrdersToday = await _coreContext.StationHistory.Where(_ => _.StationId == vm.ThisStation.Id && _.UserId == vm.UserId && _.On.Date == DateTime.Now.Date).Select(_ => _.WO).Distinct().CountAsync();

            output.UserTotals = new List<UserTotalsViewModel>();
            output.UserTotals.Add(new UserTotalsViewModel { UserId = vm.UserId, TotalOrders = totalOrders, TotalOrdersToday = totalOrdersToday });

            return output;
        }

        public async Task<List<CuringViewModel>> GetCuringStats(ScannedFromShopFloorViewModel vm)
        {

            var output = new List<CuringViewModel>();

            var stationHistories = await _coreContext.StationHistory
                .Where(_ => _.StationId == vm.ThisStation.Id && _.On >= DateTime.Now.AddMinutes(-30) && _.Result == "Entered")
                .GroupBy(_ => _.WO)
                .Select(grp => new StationHistory
                {
                    WO = grp.Key,
                    On = grp.Min(x => x.On)
                })
                .ToListAsync();

            var workOrders = await _interceptService.GetOrders(stationHistories.Select(_ => _.WO).ToList());

            foreach (var stationHistory in stationHistories)
            {
                var thisVm = new CuringViewModel();
                thisVm.WorkOrder = workOrders.Where(_ => _.WorkOrder == stationHistory.WO.ToUpper()).FirstOrDefault();
                thisVm.On = stationHistory.On;
                output.Add(thisVm);
            }

            return output;
        }


        public async Task<bool> ToggleStationHold(StationHoldViewModel stationHoldViewModel)
        {
            var stationHistory = new StationHistory
            {
                //On = DateTime.Now,
                Result = stationHoldViewModel.IsOnHold ? "On Hold" : "Off Hold",
                ShipmentId = stationHoldViewModel.ShipmentId,
                StationId = stationHoldViewModel.StationId,
                UserId = stationHoldViewModel.UserId,
                WO = stationHoldViewModel.WO
            };

            _coreContext.StationHistory.Add(stationHistory);
            await _coreContext.SaveChangesAsync();

            return stationHoldViewModel.IsOnHold;
        }

        public async Task<List<DefectReasonViewModel>> GetEligibleStationDefects(int id)
        {
            var x = await _coreContext.StationDefectReasons.Where(_ => _.StationId == id).Include(_ => _.DefectReason).Include(_ => _.Station).ToListAsync();
            var ret = new List<DefectReasonViewModel>();
            x.ForEach(_ => ret.Add(new DefectReasonViewModel
            {
                DefectReason = _.DefectReason,
                DefectReasonId = _.DefectReasonId
            }));

            return ret;
        }

        public async Task<bool> LogSerial(SerialCodeCaptureViewModel serialCodeCaptureViewModel)
        {
            try
            {
                _coreContext.WorkOrderSerialCodes.Add(new WorkOrderSerialCode
                {
                    ScannedOn = DateTime.Now,
                    ScannedSerialCode = serialCodeCaptureViewModel.SerialNumber,
                    WorkOrder = serialCodeCaptureViewModel.WorkOrderNumber,
                    StationId = serialCodeCaptureViewModel.StationId,
                    UserId = serialCodeCaptureViewModel.User.Id,
                });

                await _coreContext.SaveChangesAsync();
                return true;
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException?.ToString());
                return false;
            }
        }

        public async Task<bool> CheckSerial(SerialCodeCaptureViewModel serialCodeCaptureViewModel) =>
            await _coreContext.WorkOrderSerialCodes.Where(_ => _.ScannedSerialCode == serialCodeCaptureViewModel.SerialNumber && _.WorkOrder == serialCodeCaptureViewModel.WorkOrderNumber).AnyAsync();
        public async Task<bool> CheckSerialWithoutWorkOrder(SerialCodeCaptureViewModel serialCodeCaptureViewModel) =>
      await _coreContext.WorkOrderSerialCodes.Where(_ => _.ScannedSerialCode == serialCodeCaptureViewModel.SerialNumber).AnyAsync();

        //TODO: refactor

        public async Task<List<string>> GetNeededEANs(List<string> skus)
        {
            //this should be one query
           // var metalwoodSkus = _interceptContext.WoSku.Where(_ => _.SkuType == "Metalwoods" && skus.Contains(_.Sku)).Select(_ => _.Sku).ToList();
            return await _interceptContext.WoSkuAlias.Where(_ => skus.Contains(_.Sku) && _.AliasType == "EAN").Select(_ => _.Alias).ToListAsync();
        }

        public async Task<List<string>> GetHeadCoverUPCs(List<string> skus)
        { 
            return await _interceptContext.WoSkuAlias.Where(_ => skus.Contains(_.Sku) && _.AliasType == "UPC").Select(_ => _.Alias).ToListAsync(); 
        }

        public async Task<string> GetComponentType(string workorderid)
        {
            string type = "";
            try
            {
                var worksku = await _interceptContext.WoSubsku.FirstOrDefaultAsync(_ => workorderid == _.WorkOrder);
                type = worksku.SubskuType;
            }
            catch
            {
                type = "";
            }

            return type;
        }

        public async Task<List<WoComponent>> GetHeadCoversAndMisc(string workorderid)
        {
               
            return await _interceptContext.WoComponent.Where(_ => workorderid == _.WorkOrder && (_.ComponentType == "HEADCOVER" || EF.Functions.Like( _.ComponentDescription, "%Manual%") || EF.Functions.Like(_.ComponentDescription, "%Kit%"))).ToListAsync();
        }

        public async Task<List<WoComponent>> GetHeadComponents(string workorderid)
        {

            return await _interceptContext.WoComponent.Where(_ => workorderid == _.WorkOrder && (_.ComponentType == "HEAD")).ToListAsync();
        }

        public async Task<WoSku> GetWoSku(string workorderid)
        {

            return await _interceptContext.WoSku.FirstOrDefaultAsync(_ => workorderid == _.WorkOrder);
        }
    }
}