using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Wms;
using TMGA.Shared.ViewModels;

namespace TMGA.Infrastructure.Services
{
    public class WmsService : IWmsService
    {
        private readonly WMSContext _wmsContext;
        private readonly ILogger<WmsService> _logger;
        private readonly IInterceptService _interceptService;
        private readonly Domain.Models.Engine.CoreContext _coreContext;
        private readonly IConfiguration _configuration;

        public WmsService(WMSContext wmsContext, ILogger<WmsService> logger, IInterceptService interceptService, CoreContext coreContext, IConfiguration configuration)
        {
            _wmsContext = wmsContext;
            _logger = logger;
            _interceptService = interceptService;
            _coreContext = coreContext;
            _configuration = configuration;
        }

        public async Task<WaveViewModel> GetWaveById(string waveId)
        {
            _logger.LogInformation($"Getting wave {waveId}...");

            var output = new WaveViewModel
            {
                WaveId = waveId
            };

            var wave = _wmsContext.Waves
                .Where(x => x.WaveId == waveId)
                .Include(x => x.WorkOrderHeaders)
                .FirstOrDefault();

            var allWONumbers = wave.WorkOrderHeaders.Select(_ => _.WorkOrderHeaderId).Distinct().ToList();
            var allShipmentIds = wave.WorkOrderHeaders.Select(_ => new { WO = _.WorkOrderHeaderId, _.ShipmentId }).ToList();

            output.WorkOrders = await _interceptService.GetOrders(allWONumbers);

            //fix shipping ids
            output.WorkOrders.ForEach(_ =>
            {
                _.Shipment = allShipmentIds.Where(x => x.WO == _.WorkOrder).FirstOrDefault().ShipmentId;
            });

            //fix them in local db
            _interceptService.UpdateShipmentIds(output.WorkOrders);
            _logger.LogInformation($"Returning wave {waveId}...");
            return output;
        }

        public async Task<IEnumerable<LatestWavesViewModel>> GetLatestCustomWaves()
        {
            var output = new List<LatestWavesViewModel>();
            var siteId = Convert.ToInt32(_configuration["SITE_ID"]);
            var wmsSiteName = _configuration["WMS_ID"];
            var site = _coreContext.Sites.Find(siteId);
            var previouslyReleasedWaves = await _coreContext.ReleasedWaves.Where(_ => _.ReleasedOn >= DateTime.Now.AddDays(-4)).ToListAsync();

            var waves = await _wmsContext.Waves
               .Where(x => x.ReleaseDate.Date >= DateTime.Now.Date.AddDays(-20)
                && x.WarehouseId == wmsSiteName
               && x.WorkOrderHeaders.Any(y => y.Type == "KUST") && x.WorkOrderHeaders.Count() > 0 && x.WaveStatus == "RDY ") //TODO: Magic numbers
               .Include(x => x.WorkOrderHeaders)
               //    .ThenInclude(y => y.WorkOrderDetails)
               //.Include(x => x.WorkOrderHeaders)
               //    .ThenInclude(y => y.Warehouse)
               .AsNoTracking()
               .OrderByDescending(x => x.ReleaseDate) 
               .ToListAsync();

            //cache everything so we don't query in the the upcoming per-wave loop
            //var allWoNumbers = waves.SelectMany(_ => _.WorkOrderHeaders).Select(_ => _.WorkOrderHeaderId).Distinct().ToList();
            //var cellQueueCache = await _coreContext.CellQueues.Select(_ => _.WO).ToListAsync();
            //var completedWoNumbers = cellQueueCache.Where(_ => !allWoNumbers.Contains(_)).Select(_ => _).Distinct().ToList();
            //var incompleteWoNumbers = cellQueueCache.Where(_ => allWoNumbers.Contains(_)).Select(_ => _).Distinct().ToList();

            //var allWorkOrders = await _interceptService.GetOrders(allWoNumbers);
            //var completedOrders = await _interceptService.GetOrders(completedWoNumbers);
            //var incompleteOrders = await _interceptService.GetOrders(incompleteWoNumbers);

            //mark already processed waves
            foreach (var wave in waves)
            {
                var thisWave = new LatestWavesViewModel();
                thisWave.WaveId = wave.WaveId;
                thisWave.Warehouse = wave.WarehouseId;
                thisWave.AllWorkOrdersCount = wave.WorkOrderHeaders.Distinct().Count();
                //var thisWavesWorkOrderNumbers = wave.WorkOrderHeaders.Select(_ => _.WorkOrderHeaderId).Distinct().ToList();

                //thisWave.AllWorkOrders = allWorkOrders.Where(_ => thisWavesWorkOrderNumbers.Contains(_.WorkOrder)).ToList();
                //thisWave.CompletedOrders = completedOrders.Where(_ => thisWavesWorkOrderNumbers.Contains(_.WorkOrder)).ToList();
                //thisWave.IncompleteOrders = incompleteOrders.Where(_ => thisWavesWorkOrderNumbers.Contains(_.WorkOrder)).ToList();

                if (previouslyReleasedWaves.Any(x => x.ReleasedWaveId == wave.WaveId))
                {
                    thisWave.IsReleased = true;
                    thisWave.ReleasedOn = previouslyReleasedWaves.Where(x => x.ReleasedWaveId == wave.WaveId).FirstOrDefault().ReleasedOn;
                }

                output.Add(thisWave);
            }

            return output;
        }

        public async Task<IEnumerable<LatestWavesViewModel>> GetLatestStockWaves() 
        {
            var output = new List<LatestWavesViewModel>();
            var siteId = Convert.ToInt32(_configuration["SITE_ID"]);
            var wmsSiteName = _configuration["WMS_ID"];
            var previouslyReleasedWaves = await _coreContext.ReleasedWaves.Where(_ => _.ReleasedOn >= DateTime.Now.AddDays(-4)).ToListAsync();

            var waves = await _wmsContext.Waves
               .Where(x => x.ReleaseDate.Date >= DateTime.Now.Date.AddDays(-4) && x.WarehouseId == wmsSiteName && x.WorkOrderHeaders.Any(y => y.Type == "MFGP") && x.WorkOrderHeaders.Count() > 0)
               //.Include(x => x.WorkOrderHeaders)
               //    .ThenInclude(y => y.WorkOrderDetails)
               //.Include(x => x.WorkOrderHeaders)
               //    .ThenInclude(y => y.Warehouse)
               .AsNoTracking()
               .OrderByDescending(x => x.ReleaseDate)
               .ToListAsync();

            //cache everything so we don't query in the the upcoming per-wave loop
            //var allWoNumbers = waves.SelectMany(_ => _.WorkOrderHeaders).Select(_ => _.WorkOrderHeaderId).Distinct().ToList();
            //var cellQueueCache = await _coreContext.CellQueues.Select(_ => _.WO).ToListAsync();
            //var completedWoNumbers = cellQueueCache.Where(_ => !allWoNumbers.Contains(_)).Select(_ => _).Distinct().ToList();
            //var incompleteWoNumbers = cellQueueCache.Where(_ => allWoNumbers.Contains(_)).Select(_ => _).Distinct().ToList();

            //var allWorkOrders = await _interceptService.GetOrders(allWoNumbers);
            //var completedOrders = await _interceptService.GetOrders(completedWoNumbers);
            //var incompleteOrders = await _interceptService.GetOrders(incompleteWoNumbers);

            //mark already processed waves
            foreach (var wave in waves)
            {
                var thisWave = new LatestWavesViewModel();
                thisWave.WaveId = wave.WaveId;
                thisWave.Warehouse = wave.WarehouseId;

                //var thisWavesWorkOrderNumbers = wave.WorkOrderHeaders.Select(_ => _.WorkOrderHeaderId).Distinct().ToList();

                //thisWave.AllWorkOrders = allWorkOrders.Where(_ => thisWavesWorkOrderNumbers.Contains(_.WorkOrder)).ToList();
                //thisWave.CompletedOrders = completedOrders.Where(_ => thisWavesWorkOrderNumbers.Contains(_.WorkOrder)).ToList();
                //thisWave.IncompleteOrders = incompleteOrders.Where(_ => thisWavesWorkOrderNumbers.Contains(_.WorkOrder)).ToList();

                if (previouslyReleasedWaves.Any(x => x.ReleasedWaveId == wave.WaveId))
                {
                    thisWave.IsReleased = true;
                    thisWave.ReleasedOn = previouslyReleasedWaves.Where(x => x.ReleasedWaveId == wave.WaveId).FirstOrDefault().ReleasedOn;
                }

                output.Add(thisWave);
            }

            return output;
        }

    }
}