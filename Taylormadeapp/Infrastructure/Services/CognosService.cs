using TMGA.Domain.Models.Engine;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using TMGA.Shared.ViewModels;

//https://stackoverflow.com/questions/48625444/calling-a-soap-service-in-net-core

namespace TMGA.Infrastructure.Services
{
    public class CognosService : ICognosService
    {
        private readonly ILogger<CognosService> _logger;
        private readonly IConfiguration _configuration;
        private readonly CoreContext _coreContext;
        private readonly BasicHttpBinding binding;
        private readonly EndpointAddress endpoint;
        private readonly Cognos.PrintReportSoapClient client;
        private readonly string todaysFolderName;

        public string endpointHostName;


        //uk/ja qa
        // http://uscaramap261/wmstest74/ReportDispatcher/PrintReport.asmx


        //private string pdfSavePath = @"\\uscaramap211\Infor\wmprovia74\dat\rpt\queue\cognos_save\"; //dev
        //private string pdfSavePath = @"\\uscaramap262\Infor\wmprovia74\dat\rpt\queue\cognos_save\"; //test
        private string pdfSavePath { get; set; }
        private string printQueuePath { get; set; }
        // private string pdfUrlPath = @"file://uscaramap212/Infor/wmprovia74/dat/rpt/cognos_save"; //prod url path

        public string GetPdfSavePath() => pdfSavePath;

        public string GetTodaysFolderName() => todaysFolderName;

        public CognosService(ILogger<CognosService> logger, CoreContext coreContext, IConfiguration configuration)
        {
            _logger = logger;
            _logger = logger;
            _coreContext = coreContext;
            _configuration = configuration;

            //tijuana dev uscaramap211, test uscaramap262, prod 10.10.10.177 
            //uk test uscaramap261
            //10.10.131.56 japan
            //wmstest74
            if (_configuration["COGNOS_ENDPOINT"] == "10.10.131.56")
            {
                //japan hack
                endpoint = new EndpointAddress(new Uri($"http://{_configuration["COGNOS_ENDPOINT"] ?? "10.10.10.177"}/wmstest74/ReportDispatcher/PrintReport.asmx")); //prod

            }
            else
            {
                endpoint = new EndpointAddress(new Uri($"http://{_configuration["COGNOS_ENDPOINT"] ?? "10.10.10.177"}/wmprovia74/ReportDispatcher/PrintReport.asmx")); //prod

            }

            binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
            client = new Cognos.PrintReportSoapClient(binding, endpoint);

            pdfSavePath = @$"\\{_configuration["COGNOS_ENDPOINT"]}\Infor\wmprovia74\dat\rpt\cognos_save"; //prod

            printQueuePath = @$"\\{_configuration["COGNOS_ENDPOINT"]}\Infor\wmprovia74\dat\rpt\queue"; //prod

            todaysFolderName = DateTime.Now.ToString("yyMMdd"); //like '190930' for September 30, 2019
        }

        //TODO: Refactor custom vs. stock
        public async Task<string> GenerateCustomPickList(CellQueueViewModel workOrder)
        {
            var wmsSiteName = _configuration["WMS_ID"];

            _logger.LogInformation($"Requesting picklist generation for {workOrder.ShipmentId}....");

            //string fullPath = $@"{pdfSavePath}\{todaysFolderName}\{wmsSiteName}Pick_{id}.pdf";
            //string fullUrlPath = $@"{pdfUrlPath}/{todaysFolderName}/{wmsSiteName}Pick_{id}.pdf";
            string fullPath = $@"{printQueuePath}\{wmsSiteName}Pick_{workOrder.ShipmentId}.pdf";
            string fullUrlPath = $@"{printQueuePath}/{wmsSiteName}Pick_{workOrder.ShipmentId}.pdf+{workOrder.SelectedPrinter}+portrait.pdf";

            try
            {
                _logger.LogInformation($"Requesting custom picklist generation for {workOrder.ShipmentId}...sending Cognos request to endpoint at {endpoint}...");

                await client.PrintPackingListAsync(
                    PackId: $"{workOrder.ShipmentId}-{workOrder.Cell.Desc.Replace("Cell ", "").Replace("Line ", "")}",
                    ReportNetPkg: "viaware",
                    ReportNetReport: $"{wmsSiteName}Pick",
                    ViaWarePrinter: "",
                    saveDir: "",
                    printFromCognos: "N");

                await client.savePaperworkByDateAsync($"{wmsSiteName}Pick_{workOrder.ShipmentId}-{workOrder.Cell.Desc.Replace("Cell ", "").Replace("Line ", "")}", todaysFolderName);

                await client.ReprintPaperworkByFileWithOrientationAsync(
                    FileID: $"{wmsSiteName}Pick_{workOrder.ShipmentId}-{workOrder.Cell.Desc.Replace("Cell ", "").Replace("Line ", "")}",
                    ViaWarePrinter: workOrder.SelectedPrinter,
                    datedDir: todaysFolderName,
                    printOrientation: "portrait",
                    printerHostname: workOrder.SelectedPrinter);

                await client.RemoveSavedReportsByFileAsync($"{wmsSiteName}Pick_{workOrder.ShipmentId}-{workOrder.Cell.Desc.Replace("Cell ", "").Replace("Line ", "")}");


                var queue = _coreContext.CellQueues.Where(_ => _.WO == workOrder.WO).FirstOrDefault();
                //queue.IsPrinted = true;
                _coreContext.SaveChanges();

                _logger.LogInformation($@"Site {wmsSiteName} - custom pick list successfully written to {fullPath}. Web URL is {fullUrlPath}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException.ToString());
            }
            finally
            {
                await client.CloseAsync().ConfigureAwait(false);
            }

            return $"{workOrder.ShipmentId}";
        }

        public async Task<string> GenerateStockPickList(CellQueueViewModel workOrder)
        {

            var wmsSiteName = _configuration["WMS_ID"];
            _logger.LogInformation($"Requesting stock picklist generation for {workOrder.WO}....");

            //string fullPath = $@"{pdfSavePath}\{todaysFolderName}\{wmsSiteName}Pick_{id}.pdf";
            //string fullUrlPath = $@"{pdfUrlPath}/{todaysFolderName}/{wmsSiteName}Pick_{id}.pdf";
            string fullPath = $@"{printQueuePath}\{wmsSiteName}StockPickV2_{workOrder.WO}.pdf";
            string fullUrlPath = $@"{printQueuePath}/{wmsSiteName}StockPickV2_{workOrder.WO}.pdf+{workOrder.SelectedPrinter}+portrait.pdf";

            try
            {
                _logger.LogInformation($"Requesting stock picklist generation for {workOrder.WO}...sending Cognos request to endpoint at {endpoint}...");

                await client.PrintPackingListAsync(
                    PackId: $"{workOrder.WO}-{workOrder.Cell.Desc.Replace("Cell ", "").Replace("Line ", "")}",
                    ReportNetPkg: "viaware",
                    ReportNetReport: $"{wmsSiteName}StockPickV2",
                    ViaWarePrinter: "",
                    saveDir: "",
                    printFromCognos: "N");

                await client.savePaperworkByDateAsync($"{wmsSiteName}StockPickV2_{workOrder.WO}-{workOrder.Cell.Desc.Replace("Cell ", "").Replace("Line ", "")}", todaysFolderName);

                await client.ReprintPaperworkByFileWithOrientationAsync(
                    FileID: $"{wmsSiteName}StockPickV2_{workOrder.WO}-{workOrder.Cell.Desc.Replace("Cell ", "").Replace("Line ", "")}",
                    ViaWarePrinter: workOrder.SelectedPrinter,
                    datedDir: todaysFolderName,
                    printOrientation: "portrait",
                    printerHostname: workOrder.SelectedPrinter);

                await client.RemoveSavedReportsByFileAsync($"{wmsSiteName}StockPickV2_{workOrder.WO}-{workOrder.Cell.Desc.Replace("Cell ", "").Replace("Line ", "")}");

                var queue = _coreContext.CellQueues.Where(_ => _.WO == workOrder.WO).FirstOrDefault();
                queue.IsPrinted = true;
                _coreContext.SaveChanges();

                _logger.LogInformation($@"Stock pick list successfully written to {fullPath}. Web URL is {fullUrlPath}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException.ToString());
            }
            finally
            {
                await client.CloseAsync().ConfigureAwait(false);
            }

            return $"{workOrder.ShipmentId}";
        }

    }
}