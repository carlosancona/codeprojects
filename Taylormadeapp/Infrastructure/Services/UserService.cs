using TMGA.Domain.Models.Engine;
using TMGA.Shared.ViewModels;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace TMGA.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly CoreContext _coreContext;
        private readonly ILogger<IUserService> _logger;

        public UserService(CoreContext coreContext, ILogger<IUserService> logger)
        {
            _coreContext = coreContext;
            _logger = logger;
        }

        public async Task<UserViewModel> Login(string id)
        {
            _logger.LogInformation($"Login {id}");

            var user = await _coreContext.Users.FindAsync(id);

            if (user != null)
            {
                return new UserViewModel
                {
                    Id = user.Id,
                    Name = user.Name,
                    Role = user.Role
                };
            }
            else
                //non-table user
                return new UserViewModel
                {
                    Id = id,
                    Name = id,
                    Role = "User"
                };
        }

        public void Logout(string id)
        {
            _logger.LogInformation($"Logout {id}");
        }
    }
}