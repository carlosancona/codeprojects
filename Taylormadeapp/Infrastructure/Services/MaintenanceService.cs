using TMGA.Domain.Models.Engine;
using TMGA.Shared.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace TMGA.Infrastructure.Services
{
    public class MaintenanceService : IMaintenanceService
    {
        private readonly CoreContext _coreContext;
        private readonly IInterceptService _interceptService;
        private readonly ILogger<SiteManagementService> _logger;
        private readonly IMapper _mapper;

        public MaintenanceService(CoreContext coreContext, ILogger<SiteManagementService> logger, IInterceptService interceptService, IMapper mapper)
        {
            _coreContext = coreContext;
            _logger = logger;
            _interceptService = interceptService;
            _mapper = mapper;
        }


        public async Task<List<MaintenanceQuestionViewModel>> GetEligibleMaintenanceQuestions(int siteId) =>
          await _coreContext.MaintenanceQuestions.Where(_ => _.SiteId == siteId).Select(_ => new MaintenanceQuestionViewModel
          {
              Description = _.Description,
              DisplayOrder = _.DisplayOrder,
              Id = _.Id
          }).ToListAsync();

        public async Task<bool> AnswerQuestion(AnsweredQuestion answeredQuestion)
        {
            _coreContext.Add(new MaintenanceHistory
            {
                MaintenanceQuestionId = answeredQuestion.QuestionId,
                Notes = answeredQuestion.Answer.ToString(),
                StationId = answeredQuestion.StationId,
                UserId = answeredQuestion.UserId,
                PerformedOn = DateTime.Now,
                MaintenanceTypeId = 1
            });

            await _coreContext.SaveChangesAsync();

            return true;
        }
        //public async Task CheckForScheduledMaintenance(int stationId)
        //{

        //}

        //public async Task LogMaintenance(object maintenanceViewModel)
        //{

        //}
        //public async Task SetStationMaintenanceAlert(object maintenanceViewModel)
        //{

        //}

    }
}