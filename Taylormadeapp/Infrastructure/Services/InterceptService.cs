using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Intercept;
using TMGA.Shared.ViewModels;
using Api.Infrastructure.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace TMGA.Infrastructure.Services
{
    public class InterceptService : IInterceptService
    {
        private readonly InterceptContext _interceptContext;
        private readonly CoreContext _coreContext;
        private readonly ILogger<InterceptService> _logger;
        private readonly IDistributedCache _redis;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public InterceptService(
            InterceptContext interceptContext,
            CoreContext coreContext,
            ILogger<InterceptService> logger,
            IDistributedCache redis,
            IConfiguration configuration,
            IMapper mapper)
        {
            _interceptContext = interceptContext;
            _coreContext = coreContext;
            _logger = logger;
            _redis = redis;
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<WorkOrderViewModel> GetOrder(string id)
        {
            //check cache first
            var output = await _redis.GetAsync<WorkOrderViewModel>($"Order-{id}");

            if (output != null)
            {
                _logger.LogInformation($"Found in redis cache - {id}");
                return output;
            }
            else
            {
                _logger.LogInformation($"Not found in redis cache - {id}");

                var order = _mapper.Map<WorkOrderViewModel>(await _interceptContext.WoOrder.Where(_ => _.WorkOrder == id)
                    .Include(_ => _.SKUs)
                        .ThenInclude(_ => _.SubSKUs)
                            .ThenInclude(_ => _.Components)
                    .FirstOrDefaultAsync());

                await _redis.SetAsync<WorkOrderViewModel>($"Order-{id}", order, new DistributedCacheEntryOptions { SlidingExpiration = TimeSpan.FromDays(7) });
                _logger.LogInformation($"Added order {id} to redis cache");

                return order;
            }
        }

        public async Task<List<WorkOrderViewModel>> GetOrders(List<string> ids)
        {
            //var d = await _interceptContext.WoOrder.Where(_ => ids.Contains(_.WorkOrder))
            //       .Include(_ => _.SKUs)
            //           .ThenInclude(_ => _.SubSKUs)
            //               .ThenInclude(_ => _.Components)
            //       .ToListAsync();

            //var output = _mapper.Map<List<WorkOrderViewModel>>(d);

            var output = _mapper.Map<List<WorkOrderViewModel>>(await _interceptContext.WoOrder.Where(_ => ids.Contains(_.WorkOrder))
                   .Include(_ => _.SKUs)
                       .ThenInclude(_ => _.SubSKUs)
                           .ThenInclude(_ => _.Components)
                   .ToListAsync());

            //sleeve order fix
            var sleeveOrders = ids.Where(_ => !output.Select(wo => wo.WorkOrder).ToList().Contains(_)).ToList();

            if (output.Count != ids.Count)
            {
                foreach (var order in sleeveOrders)
                {
                    output.Add(new WorkOrderViewModel
                    {
                        WorkOrder = order
                    });
                }
            }

            return output;
        }

        public async Task<ToShopFloorViewModel> GetOrder(ScannedFromShopFloorViewModel vm)
        {
            var output = new ToShopFloorViewModel();

            //check if this WO has passed this step in the process yet
            //if (_coreContext.StationHistory.Any(_ => _.WO == vm.ScannedWO && _.Station.StationOrder == vm.ThisStation.StationOrder && _.Result == "OK"))
            //{
            //    output.IsAlreadyCompleted = true;
            //    output.WorkOrder = null;
            //    return output;
            //}

            //check if this Is open on another station
            //if (_coreContext.Stations.Any(_ => _.CurrentOrder == vm.ScannedWO && _.Id != vm.ThisStation.Id))
            //{
            //    var currentStation = _coreContext.Stations.Include(_ => _.Cell).First(_ => _.CurrentOrder == vm.ScannedWO);
            //    output.IsOpenOnAnotherStation = true;
            //    output.CurrentStation = currentStation;
            //    output.WorkOrder = null;
            //    return output;
            //}

            //check cache first
            var cache = await _redis.GetAsync<WorkOrderViewModel>($"Order-{vm.ScannedWO}");

            if (cache != null)
            {
                _logger.LogInformation($"Found in redis cache - order {vm.ScannedWO}...");

                var thisStation = _coreContext.Stations.Find(vm.ThisStation.Id);
                thisStation.CurrentOrder = vm.ScannedWO;
                await _coreContext.SaveChangesAsync();

                //log to station history
                _coreContext.StationHistory.Add(new StationHistory { UserId = vm.UserId, WO = vm.ScannedWO, ShipmentId = cache.Shipment, Result = "Entered", StationId = vm.ThisStation.Id, SiteId = vm.ThisStation.Cell.Site.Id });
                _coreContext.SaveChanges();

                output.WorkOrder = cache;
                return output;
            }
            else
            {
                _logger.LogInformation($"Not found in redis cache - order {vm.ScannedWO}...");

                var order = _mapper.Map<WorkOrderViewModel>(await _interceptContext.WoOrder.Where(_ => _.WorkOrder == vm.ScannedWO)
                    .Include(_ => _.SKUs)
                        .ThenInclude(_ => _.SubSKUs)
                            .ThenInclude(_ => _.Components)
                    .FirstOrDefaultAsync());

                await _redis.SetAsync<WorkOrderViewModel>($"Order-{vm.ScannedWO}", order, new DistributedCacheEntryOptions { SlidingExpiration = TimeSpan.FromDays(7) });


                //log to station history
                _coreContext.StationHistory.Add(new StationHistory { UserId = vm.UserId, WO = vm.ScannedWO, ShipmentId = order.Shipment, Result = "Entered", StationId = vm.ThisStation.Id, SiteId = vm.ThisStation.Cell.Site.Id });
                _coreContext.SaveChanges();

                var thisStation = _coreContext.Stations.Find(vm.ThisStation.Id);
                thisStation.CurrentOrder = vm.ScannedWO;
                await _coreContext.SaveChangesAsync();

                output.WorkOrder = order;

                return output;
            }
        }

        public async Task<List<ToShopFloorViewModel>> GetShipment(ScannedFromShopFloorViewModel vm)
        {
            //get all WOS in shipment
            var workOrderNumbers = await _interceptContext.WoOrder.Where(_ => _.Shipment == vm.ScannedWO).Select(_ => _.WorkOrder).ToListAsync();

            var finalOutput = new List<ToShopFloorViewModel>();

            foreach (var thisOrder in workOrderNumbers)
            {
                var output = new ToShopFloorViewModel();

                //check if this WO has passed this step in the process yet
                //if (_coreContext.StationHistory.Any(_ => _.WO == thisOrder && _.Station.StationOrder == vm.ThisStation.StationOrder && _.Result == "OK"))
                //{
                //    output.IsAlreadyCompleted = true;
                //    output.WorkOrder = null;
                //    finalOutput.Add(output);
                //}

                //check if this Is open on another station
                //if (_coreContext.Stations.Any(_ => _.CurrentOrder == vm.ScannedWO && _.Id != vm.ThisStation.Id))
                //{
                //    var currentStation = _coreContext.Stations.Include(_ => _.Cell).First(_ => _.CurrentOrder == vm.ScannedWO);
                //    output.IsOpenOnAnotherStation = true;
                //    output.CurrentStation = currentStation;
                //    output.WorkOrder = null;
                //    return output;
                //}

                //check cache first
                var cache = await _redis.GetAsync<WorkOrderViewModel>($"Order-{thisOrder}");

                if (cache != null)
                {
                    _logger.LogInformation($"Found in redis cache - order {thisOrder}...");

                    var thisStation = _coreContext.Stations.Find(vm.ThisStation.Id);
                    thisStation.CurrentOrder = thisOrder;
                    await _coreContext.SaveChangesAsync();

                    //log to station history
                    _coreContext.StationHistory.Add(new StationHistory { UserId = vm.UserId, WO = thisOrder, ShipmentId = cache.Shipment, Result = "Entered", StationId = vm.ThisStation.Id, SiteId = vm.ThisStation.Cell.Site.Id });
                    _coreContext.SaveChanges();

                    output.WorkOrder = cache;
                    finalOutput.Add(output);
                }
                else
                {
                    _logger.LogInformation($"Not found in redis cache - order {thisOrder}...");

                    var order = _mapper.Map<WorkOrderViewModel>(await _interceptContext.WoOrder.Where(_ => _.WorkOrder == thisOrder)
                        .Include(_ => _.SKUs)
                            .ThenInclude(_ => _.SubSKUs)
                                .ThenInclude(_ => _.Components)
                        .FirstOrDefaultAsync());

                    await _redis.SetAsync<WorkOrderViewModel>($"Order-{thisOrder}", order, new DistributedCacheEntryOptions { SlidingExpiration = TimeSpan.FromDays(7) });


                    //log to station history
                    _coreContext.StationHistory.Add(new StationHistory { UserId = vm.UserId, WO = thisOrder, ShipmentId = order.Shipment, Result = "Entered", StationId = vm.ThisStation.Id, SiteId = vm.ThisStation.Cell.Site.Id });
                    _coreContext.SaveChanges();

                    var thisStation = _coreContext.Stations.Find(vm.ThisStation.Id);
                    thisStation.CurrentOrder = thisOrder;
                    await _coreContext.SaveChangesAsync();

                    output.WorkOrder = order;
                    finalOutput.Add(output);

                }
            }
            return finalOutput;
        }

        public void UpdateShipmentIds(List<WorkOrderViewModel> vm)
        {
            var workOrderNumbers = vm.Select(_ => _.WorkOrder).ToList();

            //these work orders contain the correct shipment ids. update them in rick's (intercept) db.
            //get the work orders we are dealing with
            var workOrdersToUpdate = _interceptContext.WoOrder.Where(_ => workOrderNumbers.Contains(_.WorkOrder)).ToList();

            //update shipping info

            workOrdersToUpdate.ForEach(_ =>
            {
                _.Shipment = vm.Where(x => x.WorkOrder == _.WorkOrder).FirstOrDefault().Shipment;
            });

            _interceptContext.SaveChanges();
        }
    }
}