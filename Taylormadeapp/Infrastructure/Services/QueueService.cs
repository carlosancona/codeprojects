using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Intercept;
using TMGA.Shared.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

//https://stackoverflow.com/questions/48625444/calling-a-soap-service-in-net-core

namespace TMGA.Infrastructure.Services
{
    public class QueueService : IQueueService
    {
        private readonly ILogger<CognosService> _logger;
        private readonly CoreContext _coreContext;
        private readonly InterceptContext _interceptContext;

        public QueueService(ILogger<CognosService> logger, CoreContext coreContext, InterceptContext interceptContext)
        {
            _logger = logger;
            _coreContext = coreContext;
            _interceptContext = interceptContext;
        }

        public async Task<ToShopFloorViewModel> Done(StationDoneViewModel stationDoneViewModel)
        {
            //with cell check logic
            //var queue = _coreContext.CellQueues.Where(_ => _.WO == stationDoneViewModel.ShipmentId && _.Station.Id == stationDoneViewModel.Station.Id).Include(_ => _.Station).ThenInclude(_ => _.Cell).FirstOrDefault();
            var output = new ToShopFloorViewModel
            {
                UserId = stationDoneViewModel.UserId,
                CurrentStation = stationDoneViewModel.Station,
                IsAlreadyCompleted = false,
                IsFromRedisCache = false,
                IsNotInQueue = false,
                IsOpenOnAnotherStation = false,
                IsWrongCell = false
            };

            var thisCell = _coreContext.Cells.Where(_ => _.Id == stationDoneViewModel.Station.Cell.Id).First();

            //without cell check logic(any cell can do any order)
            var queue = _coreContext.CellQueues.Where(_ => _.WO == stationDoneViewModel.WO).Include(_ => _.Station).ThenInclude(_ => _.Cell).FirstOrDefault();
            var thisWorkOrder = await _interceptContext.WoOrder.Where(_ => _.WorkOrder == stationDoneViewModel.WO).FirstOrDefaultAsync();

            //if (queue == null)
            //{
            //    //order isn't in queue, return result
            //    output.IsNotInQueue = true;
            //    return output;
            //}

            //in case it's in the wrong cell, accomodate that and switch to queue cell. 
            var nextStation = _coreContext.Stations.Where(_ => _.Cell == stationDoneViewModel.Station.Cell && _.StationOrder == stationDoneViewModel.Station.StationOrder + 1).Include(_ => _.Cell).FirstOrDefault();

            //if (stationDoneViewModel.Station.Id != queue.Station.Id)
            //{
            //    _logger.LogInformation($"{stationDoneViewModel.ShipmentId} - switched cells from  {queue.Station.Cell.Desc} to {stationDoneViewModel.Station.Cell.Desc}. Next station is {nextStation.StationTypeTemp} in {nextStation.Cell.Desc}");
            //}

            //if this is last station, remove from queue
            if (nextStation == null)
            {
                try
                {
                    _coreContext.Remove(queue);
                    await _coreContext.SaveChangesAsync();
                    thisCell.ClubsInCell -= queue.ClubCount;
                    thisCell.ClubsQueued -= queue.ClubCount;
                    thisCell.ClubsInLastOrder = queue.ClubCount;
                    if (thisWorkOrder.HasBending()) thisCell.TotalOrdersThatHaveHadBending--;
                }
                catch
                {
                    _logger.LogError("Tried to remove from queue but failed");
                }

            }
            //else
            //{
            //    queue.Station = nextStation;
            //}

            _coreContext.StationHistory.Add(
                new StationHistory
                {
                    //On = DateTime.Now,
                    WO = stationDoneViewModel.WO,
                    ShipmentId = stationDoneViewModel.ShipmentId,
                    Result = "OK",
                    StationId = stationDoneViewModel.Station.Id,
                    UserId = stationDoneViewModel.UserId,
                    SiteId = stationDoneViewModel.Station.Cell.Site.Id,
                });

            //remove order from cell state
            var thisStation = _coreContext.Stations.Find(stationDoneViewModel.Station.Id);
            thisStation.CurrentOrder = string.Empty;

            await this._coreContext.SaveChangesAsync();

            return output;
        }

        public async Task<bool> LogDefect(DefectViewModel vm)
        {
            var newDefect = new Defect
            {
                ReportedOn = DateTime.Now,
                DefectReasonId = vm.DefectReasonId,
                WorkOrderId = vm.WorkOrderId,
                UserId = vm.UserId,
                StationId = vm.StationId,
                ShipmentId = vm.ShipmentId,
                DefectiveSKU = vm.DefectiveSKU,
                Quantity = vm.Quantity,
                WaveId = vm.WaveId,
                DefectAction = vm.WriteOffType,
                BullseyeX = vm.BullseyeX,
                BullseyeY = vm.BullseyeY
            };

            _coreContext.Defects.Add(newDefect);

            await _coreContext.SaveChangesAsync();
            return true;
        }
    }
}