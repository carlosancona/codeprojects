﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:Api.Infrastructure.Services.CapacityPlanningService.ForceWaveReRelease(System.String)~System.Threading.Tasks.Task{Api.Domain.ViewModels.WavePlanViewModel}")]
