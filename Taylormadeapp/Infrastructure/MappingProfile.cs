﻿using AutoMapper;
using System.Collections.Generic;
using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Intercept;
using TMGA.Shared.ViewModels;

//for automapper

namespace Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        { 
            CreateMap<WoOrder, WorkOrderViewModel>(); 
            CreateMap<CellQueue, CellQueueViewModel>();
            CreateMap<Station, StationViewModel>();
            CreateMap<StationHistory, StationHistoryViewModel>();
            CreateMap<WoComponent, WorkOrderComponentViewModel>();
            CreateMap<StationHistory, StationHistoryViewModel>();
            CreateMap<Site, SiteViewModel>();
            CreateMap<Cell, CellViewModel>();
            CreateMap<Printer, PrinterViewModel>();
            CreateMap<DefectReason, DefectReasonViewModel>();
            CreateMap<StationDefectReason, DefectReasonViewModel>();
            CreateMap<MaintenanceQuestion, MaintenanceQuestionViewModel>();
            CreateMap<List<MaintenanceQuestion>, List<MaintenanceQuestionViewModel>>();
            CreateMap<CuringTemperature, TemperatureCuringViewModel>();
            CreateMap<CuringRecord, CuringRecordViewModel>();
        }
    }
}
