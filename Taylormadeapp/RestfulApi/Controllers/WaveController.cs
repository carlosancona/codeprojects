﻿using TMGA.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

//Do these look familiar? Container Formats

//CUSTORDR
//DEFAULT
//PRETICKE

//[‎9/‎12/‎2019 1:01 PM]  GURUNG, BUDDHA[External]:
//yes
//that tells viaware what format of the report to use

//[‎9 /‎12 /‎2019 1:02 PM]
//Awesome. I guess I'm also missing how to link the OM_F/OD_F to the container tables. I'm not seeing a link

//[‎9 /‎12 /‎2019 1:03 PM] GURUNG, BUDDHA[External]:
//but I think those formats for the zpl 4x6 labels printed outside of cognos
//the format for cognos comes from rpt_fmt table

//[‎9 /‎12 /‎2019 1:03 PM]
//That would make sense, that was all Tijuana orders

//[‎9 /‎12 /‎2019 1:03 PM] GURUNG, BUDDHA[External]:
//ok

//[‎9 /‎12 /‎2019 1:04 PM]
//ah, i see the reports table.That looks pretty clear

//[‎9/‎12/‎2019 1:04 PM]  GURUNG, BUDDHA[External]:
//there are a few ways to link OM_F/OD_F to container tables
//for TJ, you can use CM_F table if the commands are not executed to start picking the inventory
//if it's picked you can use IV_F to link the container

//[‎9/‎12/‎2019 1:06 PM]
//So join CM_F.UC1 = OM_F.OB_OID(except for the X - batchID)?

//[‎9 /‎12 /‎2019 1:06 PM]  GURUNG, BUDDHA[External]:
//you have a same order# so I can quickly check?

//[‎9 /‎12 /‎2019 1:07 PM]
//WO3055671X1
//OH wait, i see OB_OID in CM_F.Nevermind

//[‎9 /‎12 /‎2019 1:07 PM] GURUNG, BUDDHA[External]:
//ok cool

//[‎9/‎12/‎2019 1:12 PM]
//it looks like CM_F is a union table of Order Master, Order Detail, Waves and containers?

//[‎9 /‎12 /‎2019 1:14 PM] GURUNG, BUDDHA[External]:
//it has bits of information from all those tables.Basically it's stores all outstanding command for any order, once those commands are executed by the operator, it gets removed from the table

//[‎9/‎12/‎2019 1:14 PM]
//OK, makes sense. Looks like only used for picking and replenishment right now?

//[‎9 /‎12 /‎2019 1:15 PM] GURUNG, BUDDHA[External]:
//right

//[‎9 /‎12 /‎2019 1:15 PM]
//Do newly released waves automatically become populated in this table? Or do they have to be put into "picking mode" first?

//[‎9 /‎12 /‎2019 1:17 PM] GURUNG, BUDDHA[External]:
//They will get populated on this table as long as we have the inventory on hand. i.e. if the OD_F.OB_LNO_sTT = 'RDY' and OD_F.SCHED_QTY > 0
//sometimes, if we don't have inventory, it won't get populated here. i.e.when OD_F.OB_LNO_STT = 'BACK' and OD_F.SCHED_QTY = 0

//JOIN FROM CM_F by SKU, PKG (single space) (or STK CUST for TJA) and owner
//IF CONT != null on CM_F, order has been, TO_CONT
//Once done in CM_F (all commands executed), orders go to IV_F table

//Define:
//TAG	TAG NUMBER
//CONT  CONTAINER NUMBER
//IBO   INBOUND ORDER ID
//OBO   OUTBOUND ORDER ID
//WAVE  WAVE NUMBER
//CYCC  CYCLE COUNT ORDER NUMBER
//BACH  BATCH ORDER NUMBER
//CLUS  CLUSTER PICKING
//PLAB  Pick Label number
//ASNG  ASN Groups
//PROD  Kit Order ID
//KWAV  Kit Wave ID
//DMD   Demand Orders

//Custom Club Specs table?
// IS ZZ% tables? Cannot access in VIAREAD schema, only VIAWARE

//AL_F = Alias SKU Table -- what use?
//CA_F = Carrier lookup?
//CA_WHSE = Carrier Wherehouse rules?
//CC_F =  Cycle Count Order Number?
//CM_F = Big union table of Order Master, Order Detail, Waves and containers
//CMD_SNAPSHOT = ?
//CN_F = Containers
//CNTY_F = Container Types and specs/size/weight
//CP_F = picking?
//CS_F = Container Select?
//FC_F = ?
//FT_F = ?
//GN_F = Generic descriptors?
//HLD_F = Hold Reason codes?
//IBOD_F - Inbound order detail (eVision import? No "WAVE" Specified)
//IBOM_F - Inbound order header (eVision import? No "WAVE" Specified)
//IV_F - Inventory Master
//MS_F = SKU Substitutions?
//PC_F = Picklist stations by warehouse (P363, P141 etc), only FERMI currently in table
//PK_F = Packlist by warehouse
//PKC_F = Packlist by container?
//PM_F = Appears to be massive product master
//PR_F = Check Pack printing?
//RECV_UNIT_T = inventory?
//RF-F = more inventory?
//RL_F = Warehouse zones and rules?
//ROLE = picking roles?
//ROLE_TASK = picking roles along with pick, replenish, cycc, store etc
//RPT_F = Custom query master
//RPT_FMT = Cognos report formats
//RS_F = Reason codes master
//RT_F = Route mapping (current, destination, next)
//SA_F = Picking and replenishing data and schedule (by area, T09, T09, DP4 etc), by warehouse
//SER_F = ?
//SH_F = Shipping master
//SHIPUNIT_F = extended shipping data?
//TEMPLATE_MASTER = WMS advanced queries (custom)
//WV_F = wave master
//WVA_F = predefined waves
//XD_F = current status by order ID and Line number (in picking, packing, labeling, BOL, manifest
//
namespace Api.Wms.Controllers
{
    [ApiController]
    [Produces("application/json")]
    public class WaveController : ControllerBase
    {
        private readonly IWmsService _wmsService;
        private readonly ICapacityPlanningService _capacityPlanningService;
        private readonly ILogger<WaveController> _logger;

        public WaveController(IWmsService wmsService, ICapacityPlanningService capacityPlanningService, ILogger<WaveController> logger)
        {
            _wmsService = wmsService;
            _capacityPlanningService = capacityPlanningService;
            _logger = logger;
        }

        [HttpGet]
        [Route("api/wave/custom")]
        public async Task<IActionResult> GetLatestCustomWaves()
        {
            _logger.LogInformation("Requesting custom waves...");

            try
            {
                var result = await _wmsService.GetLatestCustomWaves();
                _logger.LogInformation("Requesting custom waves completed sucessfully.");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/wave/stock")]
        public async Task<IActionResult> GetLatestStockWaves() 
        {
            _logger.LogInformation("Requesting stock waves...");

            try
            {
                var result = await _wmsService.GetLatestStockWaves();
                _logger.LogInformation("Requesting stock waves completed sucessfully.");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw ex;
            }
        }



        [HttpGet]
        [Route("api/wave/plan/{waveId}")]
        public async Task<IActionResult> GetPlan(string waveId)
        {
            _logger.LogInformation($"Requesting wave capacity plan for wave {waveId}...");

            try
            {
                var result = await _capacityPlanningService.GetWavePlan(waveId); 
                _logger.LogInformation($"Request for wave capacity plan for wave {waveId} completed successfully.");

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw ex;
            }
        }

        [HttpGet]
        [Route("api/wave/force/{waveId}")]
        public async Task<IActionResult> ForceReRelease(string waveId)
        {
            _logger.LogInformation($"Force wave capacity plan for wave {waveId}...");

            try
            {
                var result = await _capacityPlanningService.ForceWaveReRelease(waveId); 
                _logger.LogInformation($"Force wave capacity plan for wave {waveId} completed successfully.");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw ex;
            }
        }


        [HttpGet]
        [Route("api/wave/{waveId}")]
        public async Task<IActionResult> GetById(string waveId)
        {
            _logger.LogInformation($"Requesting wave {waveId}...");

            try
            {
                var result = await _wmsService.GetWaveById(waveId);
                _logger.LogInformation($"Request for wave {waveId} completed successfully.");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw ex;
            }
        }
    }
}