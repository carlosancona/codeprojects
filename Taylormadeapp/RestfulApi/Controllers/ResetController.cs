﻿using TMGA.Domain.Models.Engine;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Engine.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ResetController : ControllerBase
    {
        private readonly CoreContext _coreContext;

        public ResetController(CoreContext coreContext)
        {
            _coreContext = coreContext;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
         {
        //    _coreContext.CellQueues.RemoveRange(_coreContext.CellQueues);
        //    await _coreContext.SaveChangesAsync();
        //    _coreContext.CellQueueSnapshots.RemoveRange(_coreContext.CellQueueSnapshots);
        //    await _coreContext.SaveChangesAsync();
        //    _coreContext.ReleasedWaves.RemoveRange(_coreContext.ReleasedWaves);
        //    await _coreContext.SaveChangesAsync();
        //    _coreContext.StationHistory.RemoveRange(_coreContext.StationHistory);
        //    await _coreContext.SaveChangesAsync();

        //    var cells = _coreContext.Cells.ToList();
        //    cells.ForEach(_ =>
        //    {
        //        _.ClubsInCell = 0;
        //        _.ClubsQueued = 0;
        //        _.ClubsInLastOrder = 0;
        //        _.ClubsInLastOrder = 0;
        //        _.AssemblySequence = 0;
        //        _.TotalOrders = 0;
        //        _.TotalOrdersThatHaveHadBending = 0;
        //    });

        //    await _coreContext.SaveChangesAsync();

            return Content("This functionality has been disabled.");
        }
    }
}