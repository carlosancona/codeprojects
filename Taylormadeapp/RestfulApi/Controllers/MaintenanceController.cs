﻿ 
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Intercept;
using TMGA.Domain.Models.Wms;
using TMGA.Infrastructure.Services;
using TMGA.Shared.ViewModels;

namespace Api.Engine.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class MaintenanceController : ControllerBase
    {
        private readonly ISiteManagementService _siteManagementService;
        private readonly CoreContext _coreContext;
        private readonly InterceptContext _interceptContext;
        private readonly WMSContext _wmsContext;
        private readonly IConfiguration _configuration;
        private readonly IMaintenanceService _maintenanceService;

        public MaintenanceController(ISiteManagementService siteManagementService, CoreContext coreContext, InterceptContext interceptContext, WMSContext wmsContext, IConfiguration configuration, IMaintenanceService maintenanceService)
        {
            _siteManagementService = siteManagementService;
            _coreContext = coreContext;
            _interceptContext = interceptContext;
            _wmsContext = wmsContext;
            _configuration = configuration;
            _maintenanceService = maintenanceService;
        }

        [HttpGet]
        [Route("questions")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get() => Ok(await _maintenanceService.GetEligibleMaintenanceQuestions(siteId: Convert.ToInt32(_configuration["SITE_ID"])));

        [HttpPost]
        [Route("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> PostAnswer(AnsweredQuestion answeredQuestion) => Ok(await _maintenanceService.AnswerQuestion(answeredQuestion));


    }
}