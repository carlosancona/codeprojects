﻿using TMGA.Domain.Models.Engine;
using TMGA.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace Api.Wms.Controllers
{
    [ApiController, Produces("application/json")]
    public class PacklistController : ControllerBase
    {
        private readonly ICognosService _cognosService;
        private readonly IConfiguration _configuration;

        public PacklistController(ICognosService cognosService, IConfiguration configuration)
        {
            _cognosService = cognosService;
            _configuration = configuration;
        }

        //[HttpGet]
        //[Route("api/packlist/{id}")]
        //public async Task<IActionResult> GetPacklist(string id) => Ok(await _cognosService.GeneratePackList(id));

        [HttpPost]
        [Route("api/picklist/custom")]
        public async Task<IActionResult> GetCustomPicklist([FromBody] CellQueueViewModel workOrder) => Ok(await _cognosService.GenerateCustomPickList(workOrder));

        [HttpPost]
        [Route("api/picklist/stock")]
        public async Task<IActionResult> GetStockPicklist([FromBody] CellQueueViewModel workOrder) => Ok(await _cognosService.GenerateStockPickList(workOrder));

        [HttpGet]
        [Route("api/picklist/pdf/{id}")]
        public IActionResult ServePdf(string id)
        {
            //id is full filename
            //endpoint for serving the picklist PDFs so we don't have to use hacky CORS stuff and chrome hacks to access file:// 
            var wmsSiteName = _configuration["WMS_ID"];
            return new PhysicalFileResult($@"{_cognosService.GetPdfSavePath()}\{_cognosService.GetTodaysFolderName()}\{wmsSiteName}Pick_{id}.pdf", "application/pdf");
        }
    }
}