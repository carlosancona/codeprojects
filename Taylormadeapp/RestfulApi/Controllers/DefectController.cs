﻿namespace Api.Engine.Controllers
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;
    using TMGA.Infrastructure.Services;
    using TMGA.Shared.ViewModels;

    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class DefectController : ControllerBase
    {
        private readonly IQueueService _queueService;
        private readonly ICellManagementService _cellManagementService;
        private readonly ISiteManagementService _siteManagementService;
        private readonly ILogger<CellController> _logger;

        public DefectController(IQueueService queueService, ICellManagementService cellManagementService, ILogger<CellController> logger, ISiteManagementService siteManagementService)
        {
            _queueService = queueService;
            _cellManagementService = cellManagementService;
            _siteManagementService = siteManagementService;
            _logger = logger;
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateDefect([FromBody]DefectViewModel vm) => Ok(await _queueService.LogDefect(vm));

        
    }
}