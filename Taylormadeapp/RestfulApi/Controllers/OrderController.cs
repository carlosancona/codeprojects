﻿ 
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using TMGA.Infrastructure.Services;
using TMGA.Shared.ViewModels;

namespace Api.Wms.Controllers
{
    [ApiController, Produces("application/json")]
    public class OrderController : ControllerBase
    {
        private readonly IInterceptService _interceptService;
        private readonly ILogger<OrderController> _logger;
        private readonly IErrorProofingService _errorProofingService;

        public OrderController(IInterceptService interceptService, ILogger<OrderController> logger, IErrorProofingService errorProofingService)
        {
            _interceptService = interceptService;
            _logger = logger;
            _errorProofingService = errorProofingService;
        }

        [HttpGet]
        [Route("api/order/{id}")]
        public async Task<IActionResult> GetById(string id)
        {
            _logger.LogInformation($"Requesting order {id}...");

            try
            {
                var result = await _interceptService.GetOrder(id);
                _logger.LogInformation($"Request for order {id} completed sucessfully.");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw ex;
            }
        }

        [HttpPost]
        [Route("api/order")]
        public async Task<IActionResult> GetById([FromBody]ScannedFromShopFloorViewModel vm)
        {
            _logger.LogInformation($"Requesting order {vm.ScannedWO}...");

            try
            {
                var result = await _interceptService.GetOrder(vm);
                _logger.LogInformation($"Request for order {vm.ScannedWO} completed sucessfully.");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }

        [HttpPost]
        [Route("api/shipment")]
        public async Task<IActionResult> GetShipmentById([FromBody]ScannedFromShopFloorViewModel vm)
        {
            _logger.LogInformation($"Requesting shipment {vm.ScannedWO}...");

            try
            {
                var result = await _interceptService.GetShipment(vm);
                _logger.LogInformation($"Request for shipment {vm.ScannedWO} completed sucessfully.");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }

        [HttpGet]
        [Route("api/order/serials/{workOrder}")]
        public async Task<IActionResult> GetScannedSerials(string workOrder)
        {
            _logger.LogInformation($"Requesting scanned serials for {workOrder}...");

            try
            {
                var result = await _errorProofingService.GetScannedSerialNumbers(workOrder);
                _logger.LogInformation($"Request for scanned serials for {workOrder} completed sucessfully.");
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }

        //[HttpPost]
        //[Route("api/order")]
        //public async Task<IActionResult> GetById([FromBody]ScannedFromShopFloorViewModel vm)
        //{
        //    _logger.LogInformation($"Requesting order {vm.ScannedWO}...");

        //    try
        //    {
        //        var result = await _wmsService.GetOrderByWO(vm);
        //        _logger.LogInformation($"Request for order {vm.ScannedWO} completed sucessfully.");
        //        return Ok(result);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError(ex.ToString());
        //        throw ex;
        //    }
        //}
    }
}