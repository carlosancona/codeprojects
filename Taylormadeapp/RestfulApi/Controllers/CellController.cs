﻿namespace Api.Engine.Controllers
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;
    using TMGA.Infrastructure.Services;
    using TMGA.Shared.ViewModels;

    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class CellController : ControllerBase
    {
        private readonly ISiteManagementService _siteManagementService;
        private readonly ICellManagementService _cellManagementService;

        private readonly ILogger<CellController> _logger;

        public CellController(ISiteManagementService siteManagementServicee, ICellManagementService cellManagementService, ILogger<CellController> logger)
        {
            _siteManagementService = siteManagementServicee;
            _cellManagementService = cellManagementService;
            _logger = logger;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            return Ok(await _cellManagementService.GetAllCells());
        }

        [HttpPost("toggle")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Toggle([FromBody]CellViewModel cell) => Ok(await _cellManagementService.ToggleCustomOrStock(cell));

        [HttpPost("togglecustom")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> ToggleCustom([FromBody]CellViewModel cell) => Ok(await _cellManagementService.ToggleCustomOrStock(cell));


        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int id) => Ok(await _cellManagementService.GetCell(id));

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody]CellViewModel newCell) => Ok(await _cellManagementService.CreateCell(newCell));

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]CellViewModel editedCell) => Ok(await _cellManagementService.EditCell(editedCell));
    }
}