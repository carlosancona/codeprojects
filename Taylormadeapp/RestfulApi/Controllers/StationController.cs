﻿namespace Api.Engine.Controllers
{
    using TMGA.Domain.Models.Engine;
    using TMGA.Shared.ViewModels;
    using TMGA.Infrastructure.Services;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Collections.Generic;

    [ApiController]
    [Produces("application/json")]
    public class StationController : ControllerBase
    {
        private readonly CoreContext _coreContext;
        private readonly IQueueService _queueService;
        private readonly ISiteManagementService _siteManagementService;

        public StationController(CoreContext coreContext, IQueueService queueService, ISiteManagementService siteManagementService)
        {
            _coreContext = coreContext;
            _queueService = queueService;
            _siteManagementService = siteManagementService;
        }

        [HttpGet]
        [Route("api/station/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        //TODO: refactor to service
        public async Task<IActionResult> Get(int id) => Ok(await _coreContext.Stations.Where(_ => _.Id == id).Include(_ => _.Cell).ThenInclude(_ => _.Site).Include(_ => _.Cell).ThenInclude(_ => _.Queue).FirstOrDefaultAsync());

        [HttpPost]
        [Route("api/station/done")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Done([FromBody] StationDoneViewModel stationDoneViewModel) => Ok(await _queueService.Done(stationDoneViewModel));

        [HttpPost]
        [Route("api/station/curing")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCuringStats(ScannedFromShopFloorViewModel vm) => Ok(await _siteManagementService.GetCuringStats(vm));
      
        [HttpPost]
        [Route("api/station/hold")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Hold(StationHoldViewModel vm) => Ok(await _siteManagementService.ToggleStationHold(vm));


        [HttpPost]
        [Route("api/station/EligibleDefects")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> EligibleDefects([FromBody]int id) => Ok(await _siteManagementService.GetEligibleStationDefects(id));

        [HttpPost]
        [Route("api/station/serial")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> LogSerial([FromBody]SerialCodeCaptureViewModel serialCodeCaptureViewModel) => Ok(await _siteManagementService.LogSerial(serialCodeCaptureViewModel));

        [HttpPost]
        [Route("api/station/serial/check")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckSerial([FromBody]SerialCodeCaptureViewModel serialCodeCaptureViewModel) => Ok(await _siteManagementService.CheckSerial(serialCodeCaptureViewModel));

        [HttpPost]
        [Route("api/station/serial/check/any")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> CheckSerialWithoutWo([FromBody]SerialCodeCaptureViewModel serialCodeCaptureViewModel) => Ok(await _siteManagementService.CheckSerialWithoutWorkOrder(serialCodeCaptureViewModel));

        [HttpPost]
        [Route("api/station/headcovers")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetHeadCoverSkus([FromBody]List<string> skus) => Ok(await _siteManagementService.GetHeadCoverUPCs(skus));

        [HttpPost]
        [Route("api/station/eans")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetNeededEans([FromBody]List<string> skus) => Ok(await _siteManagementService.GetNeededEANs(skus));

        [HttpGet]
        [Route("api/station/componenttype/{workorderid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetComponentType(string workorderid) => Ok(await _siteManagementService.GetComponentType(workorderid));


        [HttpGet]
        [Route("api/station/componentscoversmisc/{workorderid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetHeadCoversAndMisc(string workorderid) => Ok(await _siteManagementService.GetHeadCoversAndMisc(workorderid));

        [HttpGet]
        [Route("api/station/headcomponents/{workorderid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetHeadComponents(string workorderid) => Ok(await _siteManagementService.GetHeadComponents(workorderid));


        [HttpGet]
        [Route("api/station/wosku/{workorderid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetWoSku(string workorderid) => Ok(await _siteManagementService.GetWoSku(workorderid));
    }
}