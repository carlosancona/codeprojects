﻿ 
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TMGA.Infrastructure.Services;

namespace Api.Wms.Controllers
{
    [ApiController, Produces("application/json")]
    public class HistoryController : ControllerBase
    {
        private readonly ICapacityPlanningService _capacityPlanningService;
        private readonly ISiteManagementService _siteManagementService;

        public HistoryController(ICapacityPlanningService capacityPlanningService, ISiteManagementService siteManagementService)
        {
            _capacityPlanningService = capacityPlanningService;
            _siteManagementService = siteManagementService;
        }

        //[HttpPost]
        //[Route("api/history/station")]
        //public async Task<IActionResult> RecordStationHistory([FromBody]int stationId)
        //{
        //    await _capacityPlanningService.CreateStationHistory(stationId);
        //    return Ok(stationId);
        //}

        [HttpPost]
        [Route("api/history/wo")]
        public async Task<IActionResult> GetByWO([FromBody]string workOrderId)
        {
            var output = await _siteManagementService.GetByWO(workOrderId);
            return Ok(output);
        }

        [HttpPost]
        [Route("api/history/shipment")]
        public async Task<IActionResult> GetByShipment([FromBody]string workOrderId)
        {
            var output = await _siteManagementService.GetByShipment(workOrderId);
            return Ok(output);
        }
         

    }
}