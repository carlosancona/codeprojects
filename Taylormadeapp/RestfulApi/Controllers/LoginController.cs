﻿using TMGA.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TMGA.Infrastructure.Services;

namespace Api.Wms.Controllers
{
    [ApiController, Produces("application/json")]
    public class LoginController : ControllerBase
    {
        private readonly IUserService _userService;

        public LoginController(IUserService userService) => _userService = userService;

        [HttpPost]
        [Route("api/login")]
        public async Task<IActionResult> Post([FromBody]string id)
        {
            var t = await _userService.Login(id);
            return Ok(t);
        }
    }
}