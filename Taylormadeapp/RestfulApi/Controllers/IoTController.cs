﻿namespace Api.Engine.Controllers
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Threading.Tasks;
    using TMGA.Infrastructure.Services;
    using TMGA.Shared.ViewModels;

    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class IoTController : ControllerBase
    {
        
        private readonly IIoTService _iotService;
        private readonly ILogger<CellController> _logger;

        public IoTController(ILogger<CellController> logger, IIoTService iotService)
        {
            
            _logger = logger;
            _iotService = iotService;
        }
        
        [HttpPost("temperature")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RecordCuringTemperature([FromBody]TemperatureCuringViewModel curingViewModel) => Ok(await _iotService.RecordCuringTemperature(curingViewModel));


        
        [HttpGet("temperature/getrecenttemperaturefeed/{n}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRecentTemperatureReadings(int n) => Ok(await _iotService.GetRecentTemperatureReadings(n));


        [HttpGet("temperature/getrecenttemperaturefeedbystation/{Rows}/{StationId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRecentTemperatureReadingsByStationId(int Rows, int StationId) => Ok(await _iotService.GetRecentTemperatureReadingsByStationId(Rows, StationId));


        [HttpPost("temperature/createcuringrecord")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateCuringRecord([FromBody]CuringRecordViewModel curingRecordViewModel) => Ok(await _iotService.CreateCuringRecord(curingRecordViewModel));

        [HttpGet("temperature/getcuringrecordsbywo/{workorder}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetCuringRecordsByWorkOrder(string WorkOrder) => Ok(await _iotService.GetCuringRecordsByWorkOrder(WorkOrder));

        [HttpPost("temperature/createcuringrecordbywo")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        //public async Task<IActionResult> CreateCuringRecordByWO([FromBody]string WorkOrder) => Ok(await _iotService.CreateCuringRecordByWO(WorkOrder));
        public async Task<IActionResult> CreateCuringRecordByWO([FromBody]CuringRecordViewModel curingRecordViewModel) => Ok(await _iotService.CreateCuringRecordByWO(curingRecordViewModel));


        [HttpGet("temperature/getrecentcuringrecords/{Rows}/{StationId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetRecentCuringRecords(int Rows, int StationId) => Ok(await _iotService.GetRecentCuringRecords(Rows, StationId));


        [HttpGet("temperature/getallrecentcuringrecords/{StationId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAllRecentCuringRecords(int StationId) => Ok(await _iotService.GetAllRecentCuringRecords(StationId));

        [HttpGet("temperature/isfinishedprocessing/{WorkOrder}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> FinishedProcessing(string WorkOrder) => Ok(await _iotService.FinishedProcessing(WorkOrder));

    }
}