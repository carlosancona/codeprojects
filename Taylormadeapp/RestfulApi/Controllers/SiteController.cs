﻿ 
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Intercept;
using TMGA.Domain.Models.Wms;
using TMGA.Infrastructure.Services;
using TMGA.Shared.ViewModels;

namespace Api.Engine.Controllers
{
    [ApiController]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class SiteController : ControllerBase
    {
        private readonly ISiteManagementService _siteManagementService;
        private readonly CoreContext _coreContext;
        private readonly InterceptContext _interceptContext;
        private readonly WMSContext _wmsContext;
        private readonly IConfiguration _configuration;

        public SiteController(ISiteManagementService siteManagementService, CoreContext coreContext, InterceptContext interceptContext, WMSContext wmsContext, IConfiguration configuration)
        {
            _siteManagementService = siteManagementService;
            _coreContext = coreContext;
            _interceptContext = interceptContext;
            _wmsContext = wmsContext;
            _configuration = configuration;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get() => Ok(await _siteManagementService.GetSites());

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int id) => Ok(await _siteManagementService.GetSite(id));

        [HttpGet("printers")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetPrinters(int id) => Ok(await _siteManagementService.GetPrinters());


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Post([FromBody]string newSiteName) => Ok(await _siteManagementService.CreateSite(newSiteName));

        //TODO: refactor to service
        [HttpGet("status")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetServerStatus()
        { 
            //TODO: make this not awful, refactor to service
            var output = new ServerStatusViewModel();

            output.Environment = _configuration["ENV"];
            output.SiteId = _configuration["SITE_ID"];
            output.SiteDescription = _configuration["SITE_DESCRIPTION"];

            output.CoreDatabase = _coreContext.Database.GetDbConnection().Database;
            output.CoreServer = _coreContext.Database.GetDbConnection().DataSource;

            output.InterceptDatabase = _interceptContext.Database.GetDbConnection().Database;
            output.InterceptServer = _interceptContext.Database.GetDbConnection().DataSource;

            output.WmsDatabase = _wmsContext.Database.GetDbConnection().Database;
            output.WmsServer = _wmsContext.Database.GetDbConnection().DataSource;

            output.HoldOrDone = _configuration["HOLD_OR_DONE"];
            output.WmsId = _configuration["WMS_ID"];
            output.InterceptId = _configuration["INTERCEPT_ID"];
            

            return Ok(output);
        }

        [HttpPost("totals/by/user")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSiteTotalsByUser(ScannedFromShopFloorViewModel vm) => Ok(await _siteManagementService.GetTotalOrdersByStationByUser(vm));

    }
}