using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Intercept;
using TMGA.Domain.Models.Wms;
using TMGA.Infrastructure.Services;

namespace Api.Domain.Models.Wms
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        private readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration) => Configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            TimeZoneInfo.ClearCachedData();

            services.AddControllers().AddNewtonsoftJson(_ => _.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            var env = Configuration["ENV"].ToUpper() ?? "DEVELOPMENT";
            var siteName = Configuration["SITE_DESCRIPTION"].ToUpper() ?? "TIJUANA";

            switch (siteName)
            {
                case "TIJUANA":
                    switch (env)
                    {
                        case "DEVELOPMENT":
                            services.AddDbContext<CoreContext>(_ => _.UseSqlServer("Server=tcp:tmga-engine.database.windows.net,1433;Initial Catalog=dev;Persist Security Info=False;User ID=tmga_admin;Password=Gf22Bg4xXp]U;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
                            services.AddDbContext<InterceptContext>(_ => _.UseSqlServer("Server=tcp:tmga-engine.database.windows.net,1433;Initial Catalog=tmga_db;Persist Security Info=False;User ID=tmga_admin;Password=Gf22Bg4xXp]U;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
                            services.AddDbContext<WMSContext>(_ => _.UseOracle("User Id=TMGA_RO;Password=Tmg@_r0PD;Direct=true;Validate Connection=true;Data Source=scan-viaprod.tmgolf.biz;Port=1541;Service Name=viausprd.tmgolf.biz;License Key=O7z3PhN62a2N6RSWSzvkC5R9dulRCkV9925gKCTNmgJewlzx6oRQyW+7LCVbep8wqkNCTh9IUJkqLlRT9y6EtKAPtBfB3POsbv2PZSBmqBIaoJJC/aMOQWtJY0azI0azRUslE0KW/GDtPs0oNYpbUmb9BYLBJ6XnwbgL83l0rKWbS7fAoe/gL5LHil7UvIHA/p/T9jQi9I8FLbWFuzBZcs40m0ujPRfzcnADoQnpULiBUHpYicpBqRI1TU4cdss95UJe6aXGtC9WfOGjkVlZTBhwa42VSVu8Hk+9Tyrvzc8="));
                            break;

                        case "STAGING":
                            services.AddDbContext<CoreContext>(_ => _.UseSqlServer("Server=tcp:tmga-engine.database.windows.net,1433;Initial Catalog=qa-mx;Persist Security Info=False;User ID=tmga_admin;Password=Gf22Bg4xXp]U;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
                            services.AddDbContext<InterceptContext>(_ => _.UseSqlServer("Server=tcp:tmga-prod.database.windows.net,1433;Initial Catalog=tmga_db;Persist Security Info=False;User ID=tmga_admin;Password=Gl0bal@$$embly;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
                            services.AddDbContext<WMSContext>(_ => _.UseOracle("User Id=TMGA_RO;Password=Tmg@_r0PD;Direct=true;Validate Connection=true;Data Source=scan-viaprod.tmgolf.biz;Port=1541;Service Name=viausprd.tmgolf.biz;License Key=O7z3PhN62a2N6RSWSzvkC5R9dulRCkV9925gKCTNmgJewlzx6oRQyW+7LCVbep8wqkNCTh9IUJkqLlRT9y6EtKAPtBfB3POsbv2PZSBmqBIaoJJC/aMOQWtJY0azI0azRUslE0KW/GDtPs0oNYpbUmb9BYLBJ6XnwbgL83l0rKWbS7fAoe/gL5LHil7UvIHA/p/T9jQi9I8FLbWFuzBZcs40m0ujPRfzcnADoQnpULiBUHpYicpBqRI1TU4cdss95UJe6aXGtC9WfOGjkVlZTBhwa42VSVu8Hk+9Tyrvzc8="));
                            break;

                        case "PRODUCTION":
                            services.AddDbContext<WMSContext>(_ => _.UseOracle("User Id=TMGA_RO;Password=Tmg@_r0PD;Direct=true;Validate Connection=true;Data Source=scan-viaprod.tmgolf.biz;Port=1541;Service Name=viausprd.tmgolf.biz;License Key=O7z3PhN62a2N6RSWSzvkC5R9dulRCkV9925gKCTNmgJewlzx6oRQyW+7LCVbep8wqkNCTh9IUJkqLlRT9y6EtKAPtBfB3POsbv2PZSBmqBIaoJJC/aMOQWtJY0azI0azRUslE0KW/GDtPs0oNYpbUmb9BYLBJ6XnwbgL83l0rKWbS7fAoe/gL5LHil7UvIHA/p/T9jQi9I8FLbWFuzBZcs40m0ujPRfzcnADoQnpULiBUHpYicpBqRI1TU4cdss95UJe6aXGtC9WfOGjkVlZTBhwa42VSVu8Hk+9Tyrvzc8="));
                            services.AddDbContext<CoreContext>(_ => _.UseSqlServer("Server=tcp:10.40.12.21,1433;Initial Catalog=prod;Persist Security Info=False;User ID=ga_user;Password=L0calU$erGA;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;"));
                            services.AddDbContext<InterceptContext>(_ => _.UseSqlServer("Server=tcp:10.40.12.21,1433;Initial Catalog=tmga_db;Persist Security Info=False;User ID=ga_user;Password=L0calU$erGA;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;"));
                            break;
                    }
                    break;

                case "BASINGSTOKE":
                    switch (env)
                    { 
                        case "STAGING":
                            services.AddDbContext<CoreContext>(_ => _.UseSqlServer("Server=tcp:tmga-engine.database.windows.net,1433;Initial Catalog=qa-uk;Persist Security Info=False;User ID=tmga_admin;Password=Gf22Bg4xXp]U;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
                            services.AddDbContext<InterceptContext>(_ => _.UseSqlServer("Server=tcp:tmga-engine.database.windows.net,1433;Initial Catalog=tmga_db;Persist Security Info=False;User ID=tmga_admin;Password=Gf22Bg4xXp]U;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
                            services.AddDbContext<WMSContext>(_ => _.UseOracle("User Id=TMGA_RO;Password=Tmg@_r0;Direct=true;Validate Connection=true;Data Source=uscarlxdbs20t.tmgolf.biz;Port=1521;Service Name=viaukqa;License Key=O7z3PhN62a2N6RSWSzvkC5R9dulRCkV9925gKCTNmgJewlzx6oRQyW+7LCVbep8wqkNCTh9IUJkqLlRT9y6EtKAPtBfB3POsbv2PZSBmqBIaoJJC/aMOQWtJY0azI0azRUslE0KW/GDtPs0oNYpbUmb9BYLBJ6XnwbgL83l0rKWbS7fAoe/gL5LHil7UvIHA/p/T9jQi9I8FLbWFuzBZcs40m0ujPRfzcnADoQnpULiBUHpYicpBqRI1TU4cdss95UJe6aXGtC9WfOGjkVlZTBhwa42VSVu8Hk+9Tyrvzc8="));

                            break;

                        case "PRODUCTION":
                            services.AddDbContext<WMSContext>(_ => _.UseOracle("User Id=TMGA_RO;Password=Tmg@_r0;Direct=true;Validate Connection=true;Data Source=scan-viaprod.tmgolf.biz;Port=1541;Service Name=viaukprd.tmgolf.biz;License Key=O7z3PhN62a2N6RSWSzvkC5R9dulRCkV9925gKCTNmgJewlzx6oRQyW+7LCVbep8wqkNCTh9IUJkqLlRT9y6EtKAPtBfB3POsbv2PZSBmqBIaoJJC/aMOQWtJY0azI0azRUslE0KW/GDtPs0oNYpbUmb9BYLBJ6XnwbgL83l0rKWbS7fAoe/gL5LHil7UvIHA/p/T9jQi9I8FLbWFuzBZcs40m0ujPRfzcnADoQnpULiBUHpYicpBqRI1TU4cdss95UJe6aXGtC9WfOGjkVlZTBhwa42VSVu8Hk+9Tyrvzc8="));
                            break;
                    }
                    break;

                case "AOMI":
                    switch (env)
                    {
                        case "STAGING":
                            //services.AddDbContext<CoreContext>(_ => _.UseSqlServer("Server=tcp:tmga-engine.database.windows.net,1433;Initial Catalog=qa-ja;Persist Security Info=False;User ID=tmga_admin;Password=Gf22Bg4xXp]U;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"));
                            services.AddDbContext<CoreContext>(_ => _.UseSqlServer("Server=tcp:jpaomwnsql01p,1433;Initial Catalog=qa-ja;Persist Security Info=False;User ID=tmadmin;Password=L0calU$erGA!!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;"));
                            services.AddDbContext<InterceptContext>(_ => _.UseSqlServer("Server=tcp:jpaomwnsql01p,1433;Initial Catalog=tmga_db;Persist Security Info=False;User ID=tmadmin;Password=L0calU$erGA!!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;"));
                            services.AddDbContext<WMSContext>(_ => _.UseOracle("User Id=TMGA_RO;Password=Tmg@_r0;Direct=true;Validate Connection=true;Data Source=uscarlxdbs20t.tmgolf.biz;Port=1521;Service Name=viaukqa;License Key=O7z3PhN62a2N6RSWSzvkC5R9dulRCkV9925gKCTNmgJewlzx6oRQyW+7LCVbep8wqkNCTh9IUJkqLlRT9y6EtKAPtBfB3POsbv2PZSBmqBIaoJJC/aMOQWtJY0azI0azRUslE0KW/GDtPs0oNYpbUmb9BYLBJ6XnwbgL83l0rKWbS7fAoe/gL5LHil7UvIHA/p/T9jQi9I8FLbWFuzBZcs40m0ujPRfzcnADoQnpULiBUHpYicpBqRI1TU4cdss95UJe6aXGtC9WfOGjkVlZTBhwa42VSVu8Hk+9Tyrvzc8="));  
                            break; 

                        case "PRODUCTION":
                            services.AddDbContext<CoreContext>(_ => _.UseSqlServer("Server=tcp:jpaomwnsql01p,1433;Initial Catalog=prod;Persist Security Info=False;User ID=tmadmin;Password=L0calU$erGA!!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;"));
                            services.AddDbContext<InterceptContext>(_ => _.UseSqlServer("Server=tcp:jpaomwnsql01p,1433;Initial Catalog=tmga_db_prod;Persist Security Info=False;User ID=tmadmin;Password=L0calU$erGA!!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True;Connection Timeout=30;"));
                            services.AddDbContext<WMSContext>(_ => _.UseOracle("User Id=TMGA_RO;Password=Tmg@_r0;Direct=true;Validate Connection=true;Data Source=scan-viaprod.tmgolf.biz;Port=1541;Service Name=viaukprd.tmgolf.biz;License Key=O7z3PhN62a2N6RSWSzvkC5R9dulRCkV9925gKCTNmgJewlzx6oRQyW+7LCVbep8wqkNCTh9IUJkqLlRT9y6EtKAPtBfB3POsbv2PZSBmqBIaoJJC/aMOQWtJY0azI0azRUslE0KW/GDtPs0oNYpbUmb9BYLBJ6XnwbgL83l0rKWbS7fAoe/gL5LHil7UvIHA/p/T9jQi9I8FLbWFuzBZcs40m0ujPRfzcnADoQnpULiBUHpYicpBqRI1TU4cdss95UJe6aXGtC9WfOGjkVlZTBhwa42VSVu8Hk+9Tyrvzc8="));
                            break;
                    }
                    break;
            }

            //logging
            //see https://portal.azure.com/#@tmgolf.onmicrosoft.com/resource/subscriptions/d5716614-2b29-49ba-92ae-e62762b89996/resourceGroups/Global_Assembly_POC/providers/microsoft.insights/components/tmga-logging/overview
            //services.AddApplicationInsightsTelemetry();

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.AllowAnyHeader()
                           .AllowAnyMethod()
                           .AllowAnyOrigin()
                           .WithExposedHeaders("Accept-Ranges", "Content-Range", "Content-Encoding", "Content-Length", "Content-Type")
                           .WithHeaders("Accept-Ranges", "Content-Range", "Content-Encoding", "Content-Length", "Content-Type");
                });
            });

            //gzip
            services.AddResponseCompression();

            //api documentation - swagger/shawshbuckle/openapi
            services.AddSwaggerGen(_ => _.SwaggerDoc("v1", new OpenApiInfo { Title = "TMGA API", Version = "v1" }));

            //location of redis server - internal docker service address
            services.AddDistributedRedisCache(options =>
                {
                    options.ConfigurationOptions = new StackExchange.Redis.ConfigurationOptions
                    {
                        AbortOnConnectFail = false,
                        EndPoints = { "redis:6379" }
                    };
                });

            //injecting our custom services, see Infrastructure project
            services.AddTransient<IWmsService, WmsService>();
            services.AddScoped<ICapacityPlanningService, CapacityPlanningService>();
            services.AddScoped<ICognosService, CognosService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IQueueService, QueueService>();
            services.AddScoped<ISiteManagementService, SiteManagementService>();
            services.AddScoped<ICellManagementService, CellManagementService>();
            services.AddScoped<IInterceptService, InterceptService>();
            services.AddScoped<IMaintenanceService, MaintenanceService>();
            services.AddScoped<IIoTService, IoTService>();
            services.AddScoped<IErrorProofingService, ErrorProofingService>();

            services.AddAutoMapper(typeof(Startup));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            else app.UseHsts();

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(MyAllowSpecificOrigins);

            app.UseAuthorization();

            app.UseEndpoints(_ => _.MapControllers());

            app.UseSwagger();

            app.UseSwaggerUI(_ => _.SwaggerEndpoint("/swagger/v1/swagger.json", "WMS API v1"));

            app.UseResponseCaching();

        }
    }
}