﻿using AutoMapper;
using System.Collections.Generic;
using TMGA.Domain.Models.Engine;
using TMGA.Domain.Models.Intercept;
using TMGA.Shared.ViewModels;

namespace Infrastructure
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        { 
            CreateMap<WoOrder, WorkOrderViewModel>(); 
            CreateMap<CellQueue, CellQueueViewModel>();
            CreateMap<Station, StationViewModel>();
            CreateMap<StationHistory, StationHistoryViewModel>();
            CreateMap<WoComponent, WorkOrderComponentViewModel>();
            CreateMap<StationHistory, StationHistoryViewModel>();
            CreateMap<Site, SiteViewModel>();
            CreateMap<Cell, CellViewModel>();
            CreateMap<Printer, PrinterViewModel>();
            CreateMap<DefectReasonViewModel, StationDefectReason>();
            CreateMap<List<CuringTemperature>, List<TemperatureCuringViewModel>>().ReverseMap();
            CreateMap<CuringTemperature, TemperatureCuringViewModel>().ReverseMap();
            CreateMap<List<DefectReasonViewModel>, List<StationDefectReason>>();
        }
    }
}
