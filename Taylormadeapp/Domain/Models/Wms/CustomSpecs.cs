﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("TM_CUSTOM_CLUB_SPECS")]
    [Serializable]
    public partial class CustomSpecs
    {
        [Key]
        [Column("WO")]
        public string Id { get; set; }

        [Column("SO")]
        public string SO { get; set; }

        [Column("SO_LINE")]
        public string SoLine { get; set; }

        [Column("SO_LINE_CONTEXT")]
        public string SoLineContext { get; set; }

        [Column("SPEC1")]
        public string SPN { get; set; } //SPN

        [NotMapped]
        public string[] SPNList => SPN?.Split("_");

        [Column("SPEC2")]
        public string FitFor { get; set; }

        [Column("SPEC3")]
        public string Model { get; set; }

        [Column("SPEC4")]
        public string Sleeve { get; set; } //(FCT, Loft)

        [Column("SPEC6")]
        public string Weight { get; set; } //(MWT, Flight)

        [Column("SPEC7")]
        public string Sole { get; set; }

        [Column("SPEC8")]
        public string Lie { get; set; }

        [Column("SPEC9")]
        public string Loft { get; set; }

        [Column("SPEC10")]
        public string ShaftLogoOrientation { get; set; }

        [Column("SPEC11")]
        public string ShaftTipAdjustment { get; set; } //hard, firm, soft

        [Column("SPEC12")]
        public string ShaftLengthAdjustment { get; set; }

        [Column("SPEC13")]
        public string ShaftTipCutCalculated { get; set; }

        [Column("SPEC14")]
        public string ShaftLengthCutCalculated { get; set; }

        [Column("SPEC15")]
        public double? ClubCount { get; set; } = 0;

        [Column("SPEC16")]
        public string GripLogoOrientation { get; set; } //up or down

        [Column("SPEC17")]
        public string GripWraps { get; set; }

        [Column("SPEC18")]
        public string MacroCode { get; set; }

        [Column("SPEC20")]
        public string ClubLogo { get; set; }

        [Column("INSERTED")]
        public DateTime? Inserted { get; set; }

        public WorkOrderDetail WorkOrderDetail { get; set; }
    }
}