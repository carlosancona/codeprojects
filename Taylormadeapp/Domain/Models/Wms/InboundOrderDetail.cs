using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("IBOD_F")]
    public partial class InboundOrderDetail
    {
        [Key]
        [Column("IBOD_RID")]
        public string Id { get; set; }

        [Column("IB_TYPE")]
        public string Type { get; set; }

        [Column("IB_LNO")]
        public string IbLNO { get; set; }

        [Column("SKU")]
        public string SKU { get; set; }

        [Column("PKG")]
        public string Package { get; set; }

        [Column("LOT")]
        public string Lot { get; set; }

        [Column("UC1")]
        public string UC1 { get; set; }

        [Column("UC2")]
        public string UC2 { get; set; }

        [Column("UC3")]
        public string UC3 { get; set; }

        [Column("IB_LNO_STT")]
        public string Status { get; set; }

        [Column("ORD_QTY")]
        public int OrderQuantity { get; set; }

        [Column("ORD_UOM")]
        public int UnitOfMeasure { get; set; }

        [Column("CMP_QTY")]
        public int CMPQuantity { get; set; }

        [Column("DT_MFG")]
        public DateTime? ManufacturingDate { get; set; }

        [Column("DT_EXPIRE")]
        public DateTime? Expires { get; set; }

        [Column("MODCNT")]
        public DateTime? ModCount { get; set; }

        [Column("DTIMECRE")]
        public DateTime? Created { get; set; }

        [Column("DTIMEMOD")]
        public DateTime? LastModified { get; set; }

        [Column("DT_DELIVER_BY")]
        public DateTime? DeliverBy { get; set; }

        [Column("DT_BEST_BY")]
        public DateTime? BestBy { get; set; }

        [Column("DT_EXPECT")]
        public DateTime? Expected { get; set; }

        [Column("OWNER")]
        public string Owner { get; set; }

        //Foreign Keys
        [Column("IB_OID")]
        public string InboundOrderHeaderId { get; set; }

        [ForeignKey("IB_OID")]
        public InboundOrderHeader Header { get; set; }

        [Column("WHSE_ID")]
        public string WarehouseId { get; set; }

        [ForeignKey("WHSE_ID")]
        public Warehouse Warehouse { get; set; }
    }
}