using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    ///

    [Table("CA_F")]
    [Serializable]
    public partial class Carrier
    {
        [Key]
        [Column("CARRIER")]
        public string Id { get; set; }
    }
}