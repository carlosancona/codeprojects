using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("ZZSHAFTCUT")]
    [Serializable]
    public partial class Shaft
    {
        [Key]
        [Column("SHAFT_PART_NUMBER")]
        public string Id { get; set; }

        [Column("SHAFT_TYPE")]
        public string ShaftType { get; set; }

        [Column("HEAD_TYPE_DESC")]
        public string HeadTypeDescription { get; set; }

        [Column("TEMPLATE1")]
        public string TemplateId { get; set; }

        [Column("TEMPLATE_TIP")]
        public string TemplateTip { get; set; }

        [Column("TEMPLATE_BUTT")]
        public string TemplateButt { get; set; }

        [Column("RAW_LENGTH")]
        public string RawLength { get; set; }

        [Column("TIP")]
        public string Tip { get; set; }

        [Column("STD_LEN_BUTT")]
        public string StandardLengthButt { get; set; }

        [Column("FINAL_BUTT")]
        public string FinalButt { get; set; }

        [Column("TIP_PREP")]
        public string TipPrep { get; set; }

        [Column("FERRULE")]
        public string Ferrule { get; set; }

        [Column("FERRULE_SET")]
        public string FerruleSet { get; set; }

        [Column("FINAL_CLUB_LENGTH")]
        public string FinalClubLength { get; set; }

        [Column("LIE")]
        public string Lie { get; set; }

        [Column("SW")]
        public string SwingWeight { get; set; }

        [Column("TIP_TEMPLATE_NUMBER")]
        public string TemplateTipNumber { get; set; }

        [Column("COO")]
        public string CountryOfOrigin { get; set; }

        [Column("SHIM")]
        public string Shim { get; set; }

        [Column("TIP_CAP_NO")]
        public string TipCapNumber { get; set; }

        [Column("SBL_PART_NO")]
        public string SblPartNumber { get; set; }

        [Column("PARALLEL_SHAFT")]
        public string ParallelShaft { get; set; }

        [Column("STOCK_GRIP")]
        public string StockGrip { get; set; }

        [Column("PLUG_FAMILY")]
        public string PlugFamily { get; set; }

        [Column("ID_NO")]
        public string IdNumber { get; set; }
    }
}