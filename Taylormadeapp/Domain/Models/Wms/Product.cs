using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("PM_F")]
    [Serializable]
    public partial class Product
    {
        [Key]
        [Column("SKU")]
        public string Id { get; set; }

        [Column("SKU_TYPE")]
        public string SkuType { get; set; }

        [Column("SKU_DESC")]
        public string Desc { get; set; }

        [Column("UNIT_COST")]
        public double UnitCost { get; set; }

        [ForeignKey("Id")]
        public Head Head { get; set; }

        [ForeignKey("Id")]
        public Shaft Shaft { get; set; }
    }
}