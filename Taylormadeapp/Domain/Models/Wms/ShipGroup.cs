﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Wms
{
    public partial class ShipGroup
    {
        [Key]
        public string ShipGroupId { get; set; }

        public string ShipGroupName { get; set; }
        public DateTime Dtimecre { get; set; }
        public DateTime Dtimemod { get; set; }
        public string Usrmod { get; set; }
        public string Pgmmod { get; set; }
        public int Modcnt { get; set; }
    }
}