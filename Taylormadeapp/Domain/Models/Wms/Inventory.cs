﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("IV_F")]
    [Serializable]
    public partial class Inventory
    {
        [Key]
        public long IvRid { get; set; }

        public string Tag { get; set; }
        public string Loc { get; set; }
        public string Cont { get; set; }
        public string ObOid { get; set; }
        public string ObType { get; set; }
        public int ObLno { get; set; }
        public string Sku { get; set; }
        public string Pkg { get; set; }
        public string Hold { get; set; }
        public DateTime DtExpire { get; set; }
        public DateTime ConsDate { get; set; }
        public string Lot { get; set; }
        public string Uc1 { get; set; }
        public string Uc2 { get; set; }
        public string Uc3 { get; set; }
        public string RequestLoc { get; set; }
        public string InvStt { get; set; }
        public DateTime DtRecv { get; set; }
        public decimal Qty { get; set; }
        public decimal AllocQty { get; set; }
        public decimal LoadMinQty { get; set; }
        public decimal LoadMaxQty { get; set; }
        public string Uom1 { get; set; }
        public string Uom2 { get; set; }
        public string Uom3 { get; set; }
        public string Uom4 { get; set; }
        public decimal Qty1in2 { get; set; }
        public decimal Qty2in3 { get; set; }
        public decimal Qty3in4 { get; set; }
        public decimal Wgt1 { get; set; }
        public decimal Wgt2 { get; set; }
        public decimal Wgt3 { get; set; }
        public decimal Wgt4 { get; set; }
        public decimal MaxStack { get; set; }
        public decimal Hgt1 { get; set; }
        public decimal Hgt2 { get; set; }
        public decimal Hgt3 { get; set; }
        public decimal Hgt4 { get; set; }
        public decimal Wid1 { get; set; }
        public decimal Wid2 { get; set; }
        public decimal Wid3 { get; set; }
        public decimal Wid4 { get; set; }
        public decimal Dpth1 { get; set; }
        public decimal Dpth2 { get; set; }
        public decimal Dpth3 { get; set; }
        public decimal Dpth4 { get; set; }
        public DateTime LastCyccDate { get; set; }
        public DateTime DtMove { get; set; }
        public decimal IntranQty { get; set; }
        public decimal LoadFactor { get; set; }
        public string LoadUom { get; set; }
        public string LabUom { get; set; }
        public string BackUom { get; set; }
        public string BrkUom { get; set; }
        public string CmdUom { get; set; }
        public string CrtnUom { get; set; }
        public decimal CrtnUomQty { get; set; }
        public int ShipunitRid { get; set; }
        public string Pgmmod { get; set; }
        public string Usrmod { get; set; }
        public int Modcnt { get; set; }
        public DateTime Dtimecre { get; set; }
        public DateTime Dtimemod { get; set; }
        public int InvId { get; set; }
        public DateTime Dtimecure { get; set; }
        public string KitCode { get; set; }
        public string KitCpnt { get; set; }
        public DateTime? DtMfg { get; set; }
        public DateTime? DtDeliverBy { get; set; }
        public DateTime? DtBestBy { get; set; }
        public string OmClass { get; set; }
        public decimal RcptNum { get; set; }
        public string Vendor { get; set; }
        public string IbOid { get; set; }
        public string IbType { get; set; }
        public decimal IbLno { get; set; }
        public decimal SampQty { get; set; }
        public string Owner { get; set; }
        public string WhseId { get; set; }
        public int EpcId { get; set; }
        public string FromMatselZone { get; set; }
        public string MatselZone { get; set; }
        public string SkuDesc { get; set; }
    }
}