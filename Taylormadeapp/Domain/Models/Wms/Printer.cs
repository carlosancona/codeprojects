using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("PRTP_F")]
    public partial class Printer
    {
        [Key]
        [Column("QUE")]
        public int Id { get; set; }
    }
}