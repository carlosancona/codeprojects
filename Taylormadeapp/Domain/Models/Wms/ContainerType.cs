using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("CNTY_F")]
    [Serializable]
    public partial class ContainerType
    {
        [Key]
        [Column("CNTYPE")]
        public string Id { get; set; }

        [Column("CNTYPE_DESC")]
        public string Description { get; set; }
    }
}