﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("CM_F")]
    [Serializable]
    public partial class CommandMaster
    {
        [Key]
        [Column("CM_RID")]
        public long Id { get; set; }

        [Column("TASK")]
        public string Task { get; set; }

        //Foreign Keys
        [Column("LOC")]
        public string LocationId { get; set; }

        [ForeignKey("LocationId")]
        public Location Location { get; set; }

        [Column("WAVE")]
        public string WaveId { get; set; }

        [ForeignKey("WaveId")]
        public Wave Wave { get; set; }

        [Column("OD_RID")]
        public string WorkOrderDetailId { get; set; }

        [ForeignKey("WorkOrderDetailId")]
        public WorkOrderDetail WorkOrderDetail { get; set; }

        [Column("CONT")]
        public string ContainerId { get; set; }

        public List<Container> Containers { get; set; }

        //[Column("WAVE")]
        //public string WaveId { get; set; }
        //[ForeignKey("WaveId")]
        //public List<Wave> Waves { get; set; }

        //public string NextLoc { get; set; }
        //public string Dest { get; set; }
        //public string CmdStt { get; set; }
        //public int CmdSeq { get; set; }
        //public decimal DestSeq { get; set; }
        //public string Cont { get; set; }
        //public string TransCont { get; set; }
        //public string Sku { get; set; }
        //public string Pkg { get; set; }

        //public string ObOid { get; set; }
        //public string ObType { get; set; }
        //public string IbOid { get; set; }
        //public string IbType { get; set; }
        //public decimal OdRid { get; set; }
        //public int ObLno { get; set; }
        //public int IbLno { get; set; }
        //public int Pri { get; set; }
        //public string Shipment { get; set; }
        //public string Pams { get; set; }
        //public string Grp1 { get; set; }
        //public string Grp2 { get; set; }
        //public string Grp3 { get; set; }
        //public int? CurrInv { get; set; }
        //public int? DestInv { get; set; }
        //public string Station { get; set; }
        //public string Opr { get; set; }
        //public string Inst { get; set; }
        //public string LockType { get; set; }
        //public string Hold { get; set; }
        //public string Lot { get; set; }
        //public decimal? Qty { get; set; }
        //public decimal? ActQty { get; set; }
        //public string Tag { get; set; }
        //public string Uc1 { get; set; }
        //public string Uc2 { get; set; }
        //public string Uc3 { get; set; }
        //public string CmdUom { get; set; }
        //public decimal? LoadFactor { get; set; }
        //public decimal? Wgt { get; set; }
        //public decimal? Volume { get; set; }
        //public string EqSvc { get; set; }
        //public string Area { get; set; }
        //public string SrcEqClass { get; set; }
        //public string DestEqClass { get; set; }
        //public string List { get; set; }
        //public int? NumCrtn { get; set; }
        //public string Pgmmod { get; set; }
        //public string Usrmod { get; set; }
        //public int Modcnt { get; set; }
        //public DateTime Dtimecre { get; set; }
        //public DateTime Dtimemod { get; set; }
        //public string Skipped { get; set; }
        //public string ToCont { get; set; }
        //public int? CartAct { get; set; }
        //public int? CartBeg { get; set; }
        //public int? CartEnd { get; set; }
        //public string ClustContType { get; set; }
        //public string Clust { get; set; }
        //public string Grp4 { get; set; }
        //public string Grp5 { get; set; }
        //public string Grp6 { get; set; }
        //public string Grp7 { get; set; }
        //public string ProdId { get; set; }
        //public decimal OpNum { get; set; }
        //public string KitCode { get; set; }
        //public string OmClass { get; set; }
        //public string Method { get; set; }
        //public string Owner { get; set; }
        //public string WhseId { get; set; }
        //public string NeedStageMsg { get; set; }
        //public string StoreZone { get; set; }
        //public string DefToCont { get; set; }
        //public int EpcId { get; set; }
        //public string VoiceCapable { get; set; }
        //public string AltPickEligible { get; set; }
    }
}