using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("IBOM_F")]
    public partial class InboundOrderHeader
    {
        [Key]
        [Column("IB_OID")]
        public string Id { get; set; }
    }
}