﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("LC_F")]
    [Serializable]
    public partial class Location
    {
        [Key]
        [Column("LOC")]
        public string Id { get; set; }

        [Column("LOC_TYPE")]
        public string Type { get; set; }
    }
}