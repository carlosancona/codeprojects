﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Serializable]
    [Table("WV_F")]
    public partial class Wave
    {
        [Key]
        [Column("WAVE")]
        public string WaveId { get; set; }

        [Column("RELEASE_DATE")]
        public DateTime ReleaseDate { get; set; }

        public List<WorkOrderHeader> WorkOrderHeaders { get; set; }
        //public long CmFId { get; set; }
        //[ForeignKey("CmfId")]
        //public CmF CmF { get; set; }

        [Column("WHSE_ID")]
        public string WarehouseId { get; set; }

        [Column("WAVE_STT")]
        public string WaveStatus { get; set; }

        //[ForeignKey("WarehouseId")]
        public Warehouse Warehouse { get; set; }

    

    }
}