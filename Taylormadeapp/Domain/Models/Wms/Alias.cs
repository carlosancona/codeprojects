using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("AL_F")]
    [Serializable]
    public partial class Alias
    {
        [Key]
        [Column("ALIAS_SKU")]
        public string Id { get; set; }
    }
}