using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("OCAN_F")]
    [Serializable]
    public partial class CancelledOrders
    {
        [Key]
        [Column("CN_RID")]
        public string Id { get; set; }

        [Column("SHIPMENT")]
        public string Shipment { get; set; }

        [Column("OB_OID")]
        public string WorkOrderHeaderId { get; set; }
    }
}