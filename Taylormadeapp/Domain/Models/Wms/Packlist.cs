﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("PK_F")]
    [Serializable]
    public partial class Packlist
    {
        [Key]
        [Column("PACKLIST")]
        public string Id { get; set; }
    }
}