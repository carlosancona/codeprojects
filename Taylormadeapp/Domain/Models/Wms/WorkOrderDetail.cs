﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace TMGA.Domain.Models.Wms
{
    [Table("OD_F")]
    [Serializable]
    public partial class WorkOrderDetail
    {
        [Key]
        [Column("OD_RID")]
        public string Id { get; set; }

        [Column("OB_LNO")]
        public int LineNumber { get; set; }

        [Column("CUST_PART_DESC")]
        public string Description { get; set; }

        [Column("ORG_QTY")]
        public decimal OriginalQuantity { get; set; }

        [Column("ORD_QTY")]
        public decimal OrderQuantity { get; set; }

        [Column("PLAN_QTY")]
        public decimal PlanQuantity { get; set; }

        [Column("BAL_PLAN_QTY")]
        public decimal BalPlanQuantity { get; set; }

        [Column("SCHED_QTY")]
        public decimal ScheduledQuantity { get; set; }

        [Column("SHIP_QTY")]
        public decimal ShipQuantity { get; set; }

        [Column("CMP_QTY")]
        public decimal CompleteQuantity { get; set; }

        [Column("ORD_UOM")]
        public string UnitOfMeasure { get; set; }

        //Derived readonly properties
        [NotMapped]
        public bool HasBending
        {
            get
            {
                if (CustomSpecs == null) return false;
                return CustomSpecs.Any(_ => !Globals.StandardLieLoftValues.Contains(_.Lie?.ToLower()) || !Globals.StandardLieLoftValues.Contains(_.Loft?.ToLower()));
            }
        }

        //Navigation properties
        public List<CommandMaster> CommandMasters { get; set; }

        [Column("SKU")]
        public string ProductId { get; set; } //also SHAFT_PART_NUMBER in ZZSHAFTCUT

        public Product Product { get; set; }

        [Column("OB_OID")]
        public string WorkOrderHeaderId { get; set; }

        public WorkOrderHeader WorkOrderHeader { get; set; }

        [Column("UC1")]
        public string CustomSpecsId { get; set; }

        //[ForeignKey("CustomSpecsId")]
        public List<CustomSpecs> CustomSpecs { get; set; }

        //[Column("BATCH_OID")]
        //public string BatchOid { get; set; }

        //[Column("BATCH_TYPE")]
        //public string BatchType { get; set; }

        //[Column("BATCH_LNO")]
        //public int BatchLno { get; set; }
        //public string ObLnoStt { get; set; }
        //public string Sku { get; set; }
        //public string Pkg { get; set; }
        //public string Lot { get; set; }
        //public string Uc1 { get; set; }
        //public string Uc2 { get; set; }
        //public string Uc3 { get; set; }
        //public string CustOid { get; set; }
        //public int CustLno { get; set; }
        //public string CustPart { get; set; }
        //public string CustUom { get; set; }
        //public decimal CustUomfact { get; set; }
        //public decimal UnitPrice { get; set; }
        //public string Hold { get; set; }
        //public string FillShort { get; set; }
        //public string Tag { get; set; }
        //public string Cont { get; set; }
        //public string Pgmmod { get; set; }
        //public string Usrmod { get; set; }
        //public int Modcnt { get; set; }
        //public DateTime Dtimecre { get; set; }
        //public DateTime Dtimemod { get; set; }
        //public string ForceRpln { get; set; }
        //public string HostBackordered { get; set; }
        //public string SerReq { get; set; }
        //public string SerUom { get; set; }
        //public string CwgtReq { get; set; }
        //public string CwgtUom { get; set; }
        //public string CustPartDesc { get; set; }
        //public int ContLno { get; set; }
        //public string SchedCan { get; set; }
        //public string PrevObLnoStt { get; set; }
        //public decimal ExtendPrice { get; set; }
        //public decimal AltsrcCons { get; set; }
        //public decimal AltsrcDirect { get; set; }
        //public decimal DiscountPercent { get; set; }
        //public decimal TaxPercent { get; set; }
        //public string PlanTrailer { get; set; }
        //public string Mbol { get; set; }
        //public string PoolCarrier { get; set; }
        //public int Version { get; set; }
        //public string Uom1WaveRelFmt { get; set; }
        //public string Uom2WaveRelFmt { get; set; }
        //public string Uom3WaveRelFmt { get; set; }
        //public string Uom4WaveRelFmt { get; set; }
        //public string TolLine { get; set; }
        //public decimal TolMaxQty { get; set; }
        //public decimal TolMinQty { get; set; }
        //public string PlanTrailerType { get; set; }
        //public string Search { get; set; }
        //public string ProdId { get; set; }
        //public string KitCode { get; set; }
        //public string ActTrailer { get; set; }
        //public string SearchDateType { get; set; }
        //public DateTime? DtStart { get; set; }
        //public DateTime? DtEnd { get; set; }
        //public string DateRange { get; set; }
        //public string DateSortOrder { get; set; }
        //public string Appt { get; set; }
        //public string Owner { get; set; }
    }
}