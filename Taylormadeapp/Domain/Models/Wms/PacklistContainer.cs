using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("PKC_F")]
    public partial class PacklistContainer
    {
        [Key]
        [Column("CONT_PACKLIST")]
        public string Id { get; set; }
    }
}