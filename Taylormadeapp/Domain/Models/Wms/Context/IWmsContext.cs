﻿using Microsoft.EntityFrameworkCore;

namespace TMGA.Domain.Models.Wms
{
    public interface IWmsContext
    {
        DbSet<WorkOrderHeader> WorkOrderHeaders { get; set; }
        DbSet<Wave> Waves { get; set; }
        DbSet<CommandMaster> CmF { get; set; }
        DbSet<HistoryMaster> HistoryMaster { get; set; }
        DbSet<Inventory> Inventory { get; set; }
        DbSet<WorkOrderDetail> WorkOrderDetails { get; set; }
        DbSet<ShipGroup> ShipGroup { get; set; }
        DbSet<TaskMgmt> TaskMgmt { get; set; }
        DbSet<CustomSpecs> CustomSpecs { get; set; }
        DbSet<Packlist> Packlists { get; set; }
        DbSet<PacklistContainer> PacklistContainers { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<PrintDocument> PrintDocuments { get; set; }
        DbSet<Printer> Printers { get; set; }
        DbSet<ReasonCode> ReasonCodes { get; set; }
        DbSet<Alias> Aliases { get; set; }
        DbSet<Carrier> Carriers { get; set; }
        DbSet<Carton> Cartons { get; set; }
        DbSet<Container> Containers { get; set; }
        DbSet<Warehouse> Warehouses { get; set; }
        DbSet<InboundOrderHeader> InboundOrderHeaders { get; set; }
        DbSet<InboundOrderDetail> InboundOrderDetails { get; set; }
    }
}