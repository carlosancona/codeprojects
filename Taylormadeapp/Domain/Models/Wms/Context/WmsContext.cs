﻿using Microsoft.EntityFrameworkCore;
using System;

//test

//[‎8/‎27/‎2019 3:39 PM]
//Is the SO linked back to any tables?

//[‎8 /‎27 /‎2019 3:43 PM] GURUNG, BUDDHA[External]:
//yes it is linked to the outbound shipping order for the assembled club.but this outbound shipping order will have have SO concatenated with 'X' followed by a random batch id
//WO order is for picking the individual components and assembling the club
//SO order is for picking and shipping the assembled club to the customer
//that's where all the shippable unit records are saved at a container level. it also has status like if the container is full picked, packed, paperwork flag, all these need to be completed before a container can be shipped to the customer

//[‎9 /‎9 /‎2019 9:29 AM] GURUNG, BUDDHA[External]:
//urgent order flag is OM_F.EMERGENCY

//componets for stock will have OD_F.PKG = 'STK' & for stock witll have 'STK' on the outbound detail order, they will be available in the IV_F table with corresponding IV_F.PKG code values

namespace TMGA.Domain.Models.Wms
{
    public partial class WMSContext : DbContext
    {
        public WMSContext()
        {
        }

        public WMSContext(DbContextOptions<WMSContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:DefaultSchema", "VIAWARE");

            //modelBuilder.Entity<WorkOrderDetail>()
            //    .HasOne(p => p.WorkOrderHeader)
            //    .WithMany(b => b.Details)
            //    .HasForeignKey(p => p.WorkOrderHeaderId)
            //    .HasPrincipalKey(b => b.Id);

            //modelBuilder.Entity<WorkOrderHeader>()
            //    .HasMany(p => p.Details)
            //    .WithOne(b => b.WorkOrderHeader)
            //    .HasPrincipalKey(b => b.Id);

            //modelBuilder.Entity<WorkOrderDetail>()
            //    .Property(_ => _.SO)
            //    .ValueGeneratedNever();

            //modelBuilder.Entity<WorkOrderDetail>()
            //    .HasAlternateKey(p => p.SO);

            //modelBuilder
            //   .Entity<WorkOrderDetail>()
            //   .Property(p => p.SO)
            //   .UsePropertyAccessMode(PropertyAccessMode.Field);

            //modelBuilder.Entity<WorkOrderDetail>()
            //   .HasMany(_ => _.CustomSpecs)
            //   .WithOne()
            //   .HasForeignKey(_ => _.Id)
            //   .HasPrincipalKey(_ => _.CustomSpecsId);

            modelBuilder.Entity<CustomSpecs>()
               .HasOne(_ => _.WorkOrderDetail)
               .WithMany(_ => _.CustomSpecs)
               .HasForeignKey(_ => _.Id)
               .HasPrincipalKey(_ => _.CustomSpecsId);

            modelBuilder.Entity<WorkOrderDetail>()
               .HasMany(_ => _.CommandMasters)
               .WithOne(_ => _.WorkOrderDetail)
               .HasForeignKey(_ => _.WorkOrderDetailId)
               .HasPrincipalKey(_ => _.Id);

            modelBuilder.Entity<CommandMaster>()
               .HasMany(_ => _.Containers)
               .WithOne(_ => _.CommandMaster)
               .HasForeignKey(_ => _.ContainerId)
               .HasPrincipalKey(_ => _.ContainerId);

            //modelBuilder.Entity<Product>()
            //   .HasOne(_ => _.Head)
            //   .WithOne();

            //modelBuilder.Entity<WorkOrderHeader>()
            //   .HasOne(_ => _.Warehouse)
            //   .WithOn
            //   .HasForeignKey(_ => _.WarehouseId)
            //   .HasPrincipalKey(_ => _.Id);
            //modelBuilder.Entity<CustomSpecs>()
            //  .Property(_ => _.Id)
            //  .HasConversion(_ => _.Trim(), _ => _.Trim());

            //auto trimmer

            //foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            //{
            //    foreach (var property in entityType.GetProperties())
            //    {
            //        var attributes = property.PropertyInfo.GetCustomAttributes(typeof(TrimAttribute), false);
            //        if (attributes.Any())
            //        {
            //            property.SetValueConverter(new TrimConverter());
            //        }
            //    }
            //}
            //_logger.LogInformation(modelBuilder.Model.ToDebugString());
        }

        public override int SaveChanges()
        {
            //Safety override
            //Insurance against saving anything in WMS
            //Remove to re-enable writes (also change connection strings)
            throw new InvalidOperationException("WMS is read-only.");
        }

        public DbSet<WorkOrderHeader> WorkOrderHeaders { get; set; }
        public DbSet<Wave> Waves { get; set; }
        public DbSet<CommandMaster> CmF { get; set; }
        public DbSet<HistoryMaster> HistoryMaster { get; set; }
        public DbSet<Inventory> IvF { get; set; }
        public DbSet<WorkOrderDetail> WorkOrderDetails { get; set; }
        public DbSet<ShipGroup> ShipGroup { get; set; }
        public DbSet<TaskMgmt> TaskMgmt { get; set; }
        public DbSet<CustomSpecs> CustomSpecs { get; set; }
        public DbSet<Packlist> Packlists { get; set; }
        public DbSet<PacklistContainer> PacklistContainers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<PrintDocument> PrintDocuments { get; set; }
        public DbSet<Printer> Printers { get; set; }
        public DbSet<ReasonCode> ReasonCodes { get; set; }
        public DbSet<Alias> Aliases { get; set; }
        public DbSet<Carrier> Carriers { get; set; }
        public DbSet<Carton> Cartons { get; set; }
        public DbSet<Container> Containers { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        public DbSet<InboundOrderHeader> InboundOrderHeaders { get; set; }
        public DbSet<InboundOrderDetail> InboundOrderDetails { get; set; }
        public DbSet<Head> Heads { get; set; }
    }

    //internal static class ModelBuilderExtensions
    //{
    //    static readonly ValueConverter<string, string> _trim = new ValueConverter<string, string>(v => v.TrimEnd(), v => v.TrimEnd());

    //    internal static QueryTypeBuilder<TQuery> TrimStrings<TQuery>(this QueryTypeBuilder<TQuery> query) where TQuery : class
    //    {
    //        var properties = query.Metadata.ClrType.GetProperties().Where(p => p.PropertyType == typeof(string));
    //        foreach (var property in properties)
    //        {
    //            query.Property(property.Name).HasConversion(_trim);
    //        }
    //        return query;
    //    }
    //}
}