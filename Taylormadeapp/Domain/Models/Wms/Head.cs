using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("ZZHEADPARTNUMBER")]
    [Serializable]
    public partial class Head
    {
        [Key]
        [Column("SKU")]
        public string Id { get; set; }

        [Column("SKU_DESC")]
        public string SkuDescription { get; set; }

        [Column("HEAD_TYPE_DESC")]
        public string HeadTypeDescription { get; set; }

        [Column("HEAD_TYPE")]
        public string HeadTypeId { get; set; }
    }
}