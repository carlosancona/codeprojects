using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("PR_F")]
    public partial class PrintDocument
    {
        [Key]
        [Column("PR_RID")]
        public int Id { get; set; }
    }
}