using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("CN_F")]
    [Serializable]
    public partial class Container
    {
        [Key]
        [Column("CN_RID")]
        public string Id { get; set; }

        [Column("CONT")]
        public string ContainerId { get; set; }

        [Column("CNTYPE")]
        public string ContainerTypeId { get; set; }

        public ContainerType ContainerType { get; set; }
        public CommandMaster CommandMaster { get; set; }
    }
}