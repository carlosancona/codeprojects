using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Serializable]
    [Table("WHSE")]
    public partial class Warehouse
    {
        [Key]
        [Column("WHSE_ID")]
        public string Id { get; set; }

        [Column("WHSE_NAME")]
        public string Name { get; set; }
    }
}