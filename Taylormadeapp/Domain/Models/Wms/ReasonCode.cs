using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("RS_F")]
    public partial class ReasonCode
    {
        [Key]
        [Column("RSN_CODE")]
        public string Id { get; set; }
    }
}