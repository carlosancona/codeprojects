using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("CARTON_T")]
    [Serializable]
    public partial class Carton
    {
        [Key]
        [Column("CARTON_ID")]
        public string Id { get; set; }
    }
}