﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Wms
{
    [Table("SHIPUNIT_F")]
    public partial class Shipment
    {
        [Key]
        [Column("SHIPUNIT_RID")]
        public string Id { get; set; }

        [Column("SHIPMENT")]
        public string ShipmentId { get; set; }
    }
}