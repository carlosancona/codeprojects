﻿using TMGA.Domain.Models.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace TMGA.Domain.Models.Wms
{
    [Serializable]
    [Table("OM_F")]
    public partial class WorkOrderHeader
    {
        [Key]
        [Column("OB_OID")]
        public string WorkOrderHeaderId { get; set; }

        [Column("OB_TYPE")]
        public string Type { get; set; } //CUST, KUST, EDI, BUDG, PGAP

        [Column("GRP2")]
        public string ProductType { get; set; }

        [Column("ORD_DATE")]
        public DateTime OrderDate { get; set; }

        [Column("SHIPMENT")]
        public string ShipmentId { get; set; }

        [Column("SHIP_NAME")]
        public string ShipName { get; set; }

        [Column("SHIP_ADDR1")]
        public string ShipAddress1 { get; set; }

        [Column("SHIP_ADDR2")]
        public string ShipAddress2 { get; set; }

        [Column("SHIP_ADDR3")]
        public string ShipAddress3 { get; set; }

        [Column("SHIP_CITY")]
        public string ShipCity { get; set; }

        [Column("SHIP_STATE")]
        public string ShipState { get; set; }

        [Column("SHIP_ZIP")]
        public string ShipZip { get; set; }

        [Column("SHIP_CNTRY")]
        public string ShipCountry { get; set; }

        [Column("TOTAL_VALUE")]
        public double TotalValue { get; set; }

        [Column("EMERGENCY")]
        public string Emergency { get; set; }

        //Derived read-only properties
        [NotMapped]
        public bool IsUrgent => Emergency.Trim().ToLower() == "y" ? true : false;

        [NotMapped]
        public bool IsCustom => Globals.CustomOrderTypeValues.Contains(Type?.ToLower());

        [NotMapped]
        public int CustomClubQuantity => WorkOrderDetails?.Sum(_ => _.CustomSpecs?.Sum(c => Convert.ToInt32(c?.ClubCount))) ?? 0;

        [NotMapped]
        public bool HasBending
        {
            get
            {
                if (!WorkOrderDetails.Where(_ => _.CustomSpecs != null).Any()) return false;

                return WorkOrderDetails.Any(_ => _.CustomSpecs.Any(_ =>
                                    (Globals.StandardLieLoftValues.Contains(_.Lie?.ToLower())) ||
                                    (Globals.StandardLieLoftValues.Contains(_.Loft?.ToLower()))));
            }
        }

        [NotMapped]
        public int HasBendingCount
        {
            get
            {
                if (!WorkOrderDetails.Where(_ => _.CustomSpecs != null).Any()) return 0;

                return WorkOrderDetails?.Sum(_ => _.CustomSpecs.Count(_ =>
                                (Globals.StandardLieLoftValues.Contains(_.Lie?.ToLower())) ||
                                (Globals.StandardLieLoftValues.Contains(_.Loft?.ToLower())))) ?? 0;
            }
        }

        [NotMapped]
        public bool? ReadyForProduction { get; set; } = true;

        //Navigation properties
        [Column("WAVE")]
        public string WaveId { get; set; }

        public Wave Wave { get; set; }

        [Column("WHSE_ID")]
        public string WarehouseId { get; set; }

        public Warehouse Warehouse { get; set; }

        //component join, super weird. needs to be flattened into a WMS View. still need to Include() it from the service call, too! see WmsService
        [NotMapped]
        public List<Component> Components { get; set; }

        public List<WorkOrderDetail> WorkOrderDetails { get; set; }

        [NotMapped]
        public int? AssignedCellId { get; set; }

        [NotMapped]
        public Cell AssignedCell { get; set; }

        [NotMapped]
        public int? AssemblySequence { get; set; }

        [NotMapped]
        public string CapacityReason { get; set; }
    }
}