﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Wms
{
    public partial class TaskMgmt
    {
        [Key]
        public string TaskMgmtType { get; set; }

        public string WhseId { get; set; }
        public string Inst { get; set; }
        public string Locked { get; set; }
        public string Task { get; set; }
        public string Area { get; set; }
        public string ObOid { get; set; }
        public string ObType { get; set; }
        public string Shipment { get; set; }
        public string Trailer { get; set; }
        public string Carrier { get; set; }
        public string Clust { get; set; }
        public string Pgmmod { get; set; }
        public string Usrmod { get; set; }
        public int Modcnt { get; set; }
        public DateTime Dtimecre { get; set; }
        public DateTime Dtimemod { get; set; }
        public string ProdId { get; set; }
    }
}