﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Wms
{
    public partial class HistoryMaster
    {
        [Key]
        public long TransSeqNum { get; set; }

        public string TransClass { get; set; }
        public string TransObj { get; set; }
        public string TransAct { get; set; }
        public string TransActMod { get; set; }
        public string UserId { get; set; }
        public string WhseId { get; set; }
        public DateTime? DtStart { get; set; }
        public decimal? DtDuration { get; set; }
        public string Sku { get; set; }
        public string Pkg { get; set; }
        public string Owner { get; set; }
        public string KitCode { get; set; }
        public string FromLoc { get; set; }
        public string ToLoc { get; set; }
        public string Tag { get; set; }
        public string OrigTag { get; set; }
        public string Cont { get; set; }
        public string ToCont { get; set; }
        public string OrigToLoc { get; set; }
        public string TypeOfOrder { get; set; }
        public string Oid { get; set; }
        public string OidType { get; set; }
        public int? Lno { get; set; }
        public int? OidVersion { get; set; }
        public int? LnoVersion { get; set; }
        public string TradePartner { get; set; }
        public string Shipment { get; set; }
        public decimal? OrigQty { get; set; }
        public decimal? ActQty { get; set; }
        public string QtyUom { get; set; }
        public decimal? Wgt { get; set; }
        public string Wave { get; set; }
        public string RsnCode { get; set; }
        public string CndCode { get; set; }
        public string List { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public decimal? WhseTmOffset { get; set; }
        public DateTime? Dtimecre { get; set; }
        public DateTime? Dtimemod { get; set; }
        public string Usrmod { get; set; }
        public string Pgmmod { get; set; }
        public int? Modcnt { get; set; }
    }
}