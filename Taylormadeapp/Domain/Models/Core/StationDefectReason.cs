using TMGA.Domain.Models.Wms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public class StationDefectReason
    {
        [Key]
        public int Id { get; set; }
        
        public int StationId { get; set; }
        [ForeignKey("StationId")]
        public Station Station { get; set; }
         
        public int DefectReasonId { get; set; }
        [ForeignKey("DefectReasonId")]
        public DefectReason DefectReason { get; set; } 
    }
}