using System;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public class CellQueueSnapshot
    {
        public int Id { get; set; }

        public Cell Cell { get; set; }

        public int StationId { get; set; }

        public ReleasedWave ReleasedWave { get; set; }

        public Station Station { get; set; }

        public string Wave { get; set; }

        public string WO { get; set; }

        public int ClubCount { get; set; }

        public int AssemblySequence { get; set; }

        public string NextWorkOrder { get; set; }

        public bool IsPrinted { get; set; }

        public string ReasonCode { get; set; }

        public string ShipmentId { get; set; }
    }
}