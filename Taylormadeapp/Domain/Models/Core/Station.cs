using TMGA.Domain.Models.Wms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public class Station
    {
        [Key]
        public int Id { get; set; }

        public Cell Cell { get; set; }

        public List<StationHistory> StationHistory;

        public string StationTypeTemp { get; set; }

        public int StationOrder { get; set; }

        public string CurrentOrder { get; set; }
    }
}