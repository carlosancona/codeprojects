using System;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public partial class DefectReason
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
    }
}