﻿//using Domain.Models.Core;
using Microsoft.EntityFrameworkCore;

namespace TMGA.Domain.Models.Engine
{
    public class CoreContext : DbContext
    {
        public CoreContext(DbContextOptions<CoreContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
             
        } 

        public DbSet<Site> Sites { get; set; }

        public DbSet<Cell> Cells { get; set; }

        public DbSet<Station> Stations { get; set; }

        public DbSet<StationHistory> StationHistory { get; set; }

        public DbSet<CellQueue> CellQueues { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<ReleasedWave> ReleasedWaves { get; set; }

        public DbSet<CellQueueSnapshot> CellQueueSnapshots { get; set; }

        public DbSet<Printer> Printers { get; set; }

        public DbSet<Defect> Defects { get; set; }

        public DbSet<StationDefectReason> StationDefectReasons { get; set; }

        public DbSet<DefectReason> DefectReasons { get; set; }

        public DbSet<StationType> StationTypes { get; set; }

        public DbSet<WorkOrderSerialCode> WorkOrderSerialCodes { get; set; }

        public DbSet<MaintenanceHistory> MaintenanceHistory { get; set; }

        public DbSet<MaintenanceType> MaintenanceTypes { get; set; }

        public DbSet<MaintenanceQuestion> MaintenanceQuestions { get; set; }

        public DbSet<CuringTemperature> CuringTemperatures { get; set; }

        public DbSet<CuringRecord> CuringRecords { get; set; }

    }
}