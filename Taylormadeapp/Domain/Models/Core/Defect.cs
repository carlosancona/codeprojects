using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public partial class Defect
    {
        [Key]
        public int Id { get; set; }

        public DateTime ReportedOn { get; set; }

        public string WaveId { get; set; }

        public string WorkOrderId { get; set; }

        public string ShipmentId { get; set; }

        public string DefectiveSKU { get; set; }

        public string DefectReason { get; set; }

        public int StationId { get; set; }

        public string UserId { get; set; }


        public int Quantity { get; set; }

        public string DefectAction { get; set; }
         
        public double? BullseyeX { get; set; }

        public double? BullseyeY { get; set; }
         
        public int DefectReasonId { get; set; }
        [ForeignKey("DefectReasonId")]
        public DefectReason Reason { get; set; }  
    }
}