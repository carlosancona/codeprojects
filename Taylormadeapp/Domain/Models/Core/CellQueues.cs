using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public class CellQueue
    {
        public int Id { get; set; }
        public Cell Cell { get; set; }
        public Station Station { get; set; }
        public string Wave { get; set; }
        public string WO { get; set; }
        public string ShipmentId { get; set; }
        public int ClubCount { get; set; }
        public int AssemblySequence { get; set; }
        public string NextWorkOrder { get; set; }
        public bool IsPrinted { get; set; }
        public string ReasonCode { get; set; }
        public string PickListPdfUrl { get; set; }

        public bool PickListGenerated { get; set; }

        [NotMapped]
        public bool IsGeneratingPicklist { get; set; }

        [NotMapped]
        public string SelectedPrinter { get; set; }

        [NotMapped]
        public string CurrentState { get; set; }

        [NotMapped]
        public bool? IsSleeveOrder { get; set; }
    }
}