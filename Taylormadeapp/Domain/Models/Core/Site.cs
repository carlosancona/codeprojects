using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public class Site
    {
        [Key]
        public int Id { get; set; }

        public string Desc { get; set; }

        public List<Cell> Cells { get; set; }
    }
}