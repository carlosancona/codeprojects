using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    [Table("MaintenanceTypes")]
    public partial class MaintenanceType
    {
        [Key]
        public int Id { get; set; } 

        public string Description { get; set; }
        
        public string Notes { get; set; }  
    }
}