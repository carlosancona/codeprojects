namespace TMGA.Domain.Models.Engine
{
    public enum ProductType
    {
        Iron,
        Mw,
        Driver,
        Putter,
        Wedge,
        Bag,
        Ball,
        Apparel,
    }
}