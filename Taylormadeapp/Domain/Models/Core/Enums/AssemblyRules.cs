namespace TMGA.Domain.Models.Engine
{
    public enum AssemblyRules
    {
        AutoProcessing,
        AutoRelease,
        Restriction1,
        Restriction2,
        EnableBending,
        RequiresBending,
        CurrentBendingRatio,
        BendingRatioThreshold,
        LastReleaseHadBending,
        LastReleaseQuantity,
        LowQuantityThreshold,
        GroupByShipTo,
        GroupbyOrder,
        ReadyForRelease,
        RequireOverridePermission,
        PrioritizeUrgent,
        RequireCancellationPermission
    }
}