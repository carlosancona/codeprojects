namespace TMGA.Domain.Models.Engine
{
    public enum ComponentType
    {
        Shaft,
        Head,
        Grip,
        Weight,
        Glue,
        Box,
    }
}