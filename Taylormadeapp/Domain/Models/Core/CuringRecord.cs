﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TMGA.Domain.Models.Engine;

//namespace Domain.Models.Core
namespace TMGA.Domain.Models.Engine
{
    public class CuringRecord
    {
        [Key]
        public int Id { get; set; }
        public string WorkOrder { get; set; }
        public string ClubDescription { get; set; }
        public int StationId { get; set; }
        public double Temperature { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Timestamp { get; set; }
        public string Status { get; set; }
        public double CurrentCuringTime { get; set; }
        public double OriginalCuringTime { get; set; }
        public int Quantity { get; set; }
        public string IsProcessed { get; set; }
    }
}
