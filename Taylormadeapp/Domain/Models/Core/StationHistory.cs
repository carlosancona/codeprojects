using TMGA.Domain.Models.Intercept;
using TMGA.Domain.Models.Wms;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public class StationHistory
    {
        [Key]
        public int Id { get; set; }

        public string Result { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime On { get; set; }

        //public string UserName { get; set; }
        public string WO { get; set; }

        public int StationId { get; set; }

        public string UserId { get; set; }

        public string ShipmentId { get; set; }

        [ForeignKey("StationId")]
        public Station Station { get; set; }

        [NotMapped]
        public WoOrder WorkOrder { get; set; }

         

        public int SiteId { get; set; }
        public Site Site { get; set; }
    }
}