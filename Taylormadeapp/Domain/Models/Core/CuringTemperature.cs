﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TMGA.Domain.Models.Engine;

//namespace Domain.Models.Core
namespace TMGA.Domain.Models.Engine
{
    public class CuringTemperature
    {
        [Key]
        public int Id { get; set; }
        public double Temperature { get; set; }
        public int StationId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime Timestamp { get; set; }

        [ForeignKey("StationId")]
        public Station Station { get; set; }

    }
}
