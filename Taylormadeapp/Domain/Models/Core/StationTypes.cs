using TMGA.Domain.Models.Wms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public class StationType
    {
        [Key]
        public int Id { get; set; }
         
    }
}