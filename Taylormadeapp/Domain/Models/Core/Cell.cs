using TMGA.Domain.Models.Wms;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public partial class Cell
    {
        [Key]
        public int Id { get; set; }

        public string Desc { get; set; }

        public string CellType { get; set; }

        public string Grouping { get; set; }

        public Site Site { get; set; }

        public List<Station> Stations { get; set; }

        public double FixedBendingRatio { get; set; }

        public int ClubsInCell { get; set; }
        public int ClubCutoffForLargeOrders { get; set; }
        public int ClubsInLastOrder { get; set; }
        public int ClubsQueued { get; set; }

        public bool IsActive { get; set; }
        public bool LastHadBending { get; set; }

        public WorkOrderHeader WorkOrder { get; set; }

        public string CurrentWorkOrderNumber { get; set; }

        public int TotalOrders { get; set; }
        public int TotalOrdersThatHaveHadBending { get; set; }

        public int AssemblySequence { get; set; } = 0;

        public List<CellQueue> Queue { get; set; }

        [NotMapped]
        public bool IsLoading { get; set; } = false;
        [NotMapped]
        public double CurrentBendingRatio
        {
            get
            {
                if (this.Queue == null) return 0;
                return this.Queue.Count == 0 ? 0 : (double)TotalOrdersThatHaveHadBending / (double)this.Queue.Count;
            }
        }
    }
}