using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public class WorkOrderSerialCode
    {
        [Key]
        public int Id { get; set; }

        public string WorkOrder { get; set; }

        public string Sku { get; set; }

        public string ComponentDescription { get; set; }

        public string ScannedSerialCode { get; set; }

        public DateTime ScannedOn { get; set; }

        public int StationId { get; set; }

        public string UserId { get; set; }
    }
}