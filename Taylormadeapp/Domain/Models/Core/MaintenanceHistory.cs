using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public partial class MaintenanceHistory
    {
        [Key]
        public int Id { get; set; } 

        public DateTime PerformedOn { get; set; }
        
        public string Notes { get; set; } 
        
        public int StationId { get; set; }
        [ForeignKey("StationId")]
        public Station Station { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }

        public int MaintenanceQuestionId { get; set; }
        [ForeignKey("MaintenanceQuestionId")]
        public MaintenanceQuestion MaintenanceQuestion { get; set; }
         
        public int MaintenanceTypeId { get; set; }
        [ForeignKey("MaintenanceTypeId")]
        public MaintenanceType MaintenanceType { get; set; }
    }
}