using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public partial class MaintenanceQuestion
    {
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }

        public string Notes { get; set; }
          
        public int DisplayOrder { get; set; }

        public int SiteId { get; set; }
        [ForeignKey("SiteId")]
        public Site Site { get; set; }



 
    }
}