namespace TMGA.Domain.Models.Engine
{
    using System;
    using System.ComponentModel.DataAnnotations;

    [Serializable]
    public class User
    {
        [Key]
        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public bool IsEnabled { get; set; } = true;

        public bool IsAdmin { get; set; } = false;

        public int? StationId { get; set; }

        public Station Station { get; set; }

        public int RoleId { get; set; }

        public string Role { get; set; }
    }
}