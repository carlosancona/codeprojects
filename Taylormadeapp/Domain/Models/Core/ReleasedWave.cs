﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public class ReleasedWave
    {
        [Key]
        public int Id { get; set; }

        public string ReleasedWaveId { get; set; }
        public DateTime ReleasedOn { get; set; }
        public string Notes { get; set; }
    }
}