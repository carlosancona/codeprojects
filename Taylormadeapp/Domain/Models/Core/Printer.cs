using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Engine
{
    [Serializable]
    public class Printer
    {
        [Key]
        public string Id { get; set; }

        public string Desc { get; set; } 
    }
}