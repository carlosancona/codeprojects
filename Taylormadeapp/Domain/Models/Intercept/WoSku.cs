﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Intercept
{
    [Serializable]
    public partial class WoSku
    {
      
        public string MfgFaclty { get; set; }

        public string WorkOrder { get; set; }

       
        public string WorkOrderType { get; set; }

       
        public string WorkOrderLine { get; set; }

        public List<WoSubsku> SubSKUs { get; set; }

        public List<WoSkuAlias> Aliases { get; set; }

        public string Sku { get; set; }
        public string SkuType { get; set; }
        public string TipTemplate { get; set; }
        public decimal? TipTemplateLength { get; set; }
        public string ButtTemplate { get; set; }
        public decimal? ButtTemplateLength { get; set; }
        public decimal? TipCutLength { get; set; }
        public decimal? ButtCutLength { get; set; }
        public string SwingweightCode { get; set; }
        public double? SwingweightValue { get; set; }
        public DateTime? CreationDate { get; set; }
        public string SkuDescription { get; set; }
        public string GripWraps { get; set; }
        public string COO { get; set; }
        public string HeadCoverSku { get; set; }
        public int HeadCoverQuantity { get; set; }
        public string HeadCoverDescription { get; set; }

    }
}