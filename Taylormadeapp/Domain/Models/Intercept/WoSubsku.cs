﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TMGA.Domain.Models.Intercept
{
    [Serializable]
    public partial class WoSubsku
    {
        public string MfgFaclty { get; set; }
        public string WorkOrder { get; set; }
        public string WorkOrderType { get; set; }
        public string WorkOrderLine { get; set; }
        public string SubskuLine { get; set; }
        public List<WoComponent> Components { get; set; }
        public string SubskuSku { get; set; }
        public string SubskuType { get; set; }
        public double? SubskuQty { get; set; }
        public string TipTemplate { get; set; }
        public decimal? TipTemplateLength { get; set; }
        public string ButtTemplate { get; set; }
        public decimal? ButtTemplateLength { get; set; }
        public decimal? TipCutLength { get; set; }
        public decimal? SwingweightValue { get; set; }
        public DateTime? CreationDate { get; set; }
        public decimal? ButtCutLength { get; set; }
        public string SubskuDescription { get; set; }
        public string TipPrep { get; set; }
        public string SwingweightCode { get; set; }
        public decimal? SwTolerance { get; set; }
        public string PlugFamily { get; set; }
        public decimal? SwShaftLength { get; set; }
        public string COO { get; set; }
        public double? Adhesive { get; set; }
        public decimal? Shots { get; set; }
        public string SwRange { get; set; }
        public string TipCap { get; set; }
        public string Ferrule { get; set; }
        public string FerruleType { get; set; }
        public decimal? FinalClubLength { get; set; }

        public string HeadType { get; set; }
        public string FctSet { get; set; }
        public string LieAngle { get; set; }

        //[NotMapped]
        //public decimal FinalClubLengthMm() => Math.Round(((this.FinalClubLength ?? (decimal)0) * (decimal)25.4), 0);
    }
}