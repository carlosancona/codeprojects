﻿using Microsoft.EntityFrameworkCore;

namespace TMGA.Domain.Models.Intercept
{
    public class InterceptContext : DbContext
    {
        public InterceptContext(DbContextOptions<InterceptContext> options)
            : base(options)
        {
        }

        public InterceptContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<WoComponent>(entity =>
            {
                entity.HasKey(e => new { e.MfgFaclty, e.WorkOrder, e.WorkOrderType, e.WorkOrderLine, e.SubskuLine, e.ComponentLine });

                entity.ToTable("WO_COMPONENT");

                entity.Property(e => e.MfgFaclty)
                    .HasColumnName("MFG_FACLTY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrder)
                    .HasColumnName("WORK_ORDER")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrderType)
                    .HasColumnName("WORK_ORDER_TYPE")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrderLine)
                    .HasColumnName("WORK_ORDER_LINE")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.SubskuLine)
                    .HasColumnName("SUBSKU_LINE")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.ComponentLine)
                    .HasColumnName("COMPONENT_LINE")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.ComponentDescription)
                    .HasColumnName("COMPONENT_DESCRIPTION")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ComponentQty).HasColumnName("COMPONENT_QTY");

                entity.Property(e => e.ComponentSku)
                    .HasColumnName("COMPONENT_SKU")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ComponentType)
                    .HasColumnName("COMPONENT_TYPE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("smalldatetime");
            });

            modelBuilder.Entity<WoOrder>(entity =>
            {

                entity.HasKey(e => new { e.MfgFaclty, e.WorkOrder, e.WorkOrderType });

                entity.ToTable("WO_ORDER");

                entity.Property(e => e.FCT)
                .HasColumnName("SPEC4");

                entity.Property(e => e.MfgFaclty)
                    .HasColumnName("MFG_FACLTY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrder)
                    .HasColumnName("WORK_ORDER")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrderType)
                    .HasColumnName("WORK_ORDER_TYPE")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.SalesOrder)
                    .HasColumnName("SALES_ORDER")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SalesOrderLine)
                    .HasColumnName("SALES_ORDER_LINE")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Shipment)
                    .HasColumnName("SHIPMENT")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Emergency)
                .HasColumnName("EMERGENCY")
                .HasMaxLength(15)
                .IsUnicode(false);

                entity.Property(e => e.SoLineContext)
                    .HasColumnName("SO_LINE_CONTEXT")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Spec1)
                    .HasColumnName("SPEC1")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Spec10)
                    .HasColumnName("SPEC10")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec11)
                    .HasColumnName("SPEC11")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec12)
                    .HasColumnName("SPEC12")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec13)
                    .HasColumnName("SPEC13")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec14)
                    .HasColumnName("SPEC14")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec15)
                    .HasColumnName("SPEC15")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GripLogoDirection)
                    .HasColumnName("SPEC16")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GripWraps)
                    .HasColumnName("SPEC17")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec18)
                    .HasColumnName("SPEC18")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec19)
                    .HasColumnName("SPEC19")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec2)
                    .HasColumnName("SPEC2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec20)
                    .HasColumnName("SPEC20")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Spec21)
                    .HasColumnName("SPEC21")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec22)
                    .HasColumnName("SPEC22")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec23)
                    .HasColumnName("SPEC23")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec24)
                    .HasColumnName("SPEC24")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec25)
                    .HasColumnName("SPEC25")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec26)
                    .HasColumnName("SPEC26")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec27)
                    .HasColumnName("SPEC27")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec28)
                    .HasColumnName("SPEC28")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec29)
                    .HasColumnName("SPEC29")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec3)
                    .HasColumnName("SPEC3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec30)
                    .HasColumnName("SPEC30")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec5)
                    .HasColumnName("SPEC5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec6)
                    .HasColumnName("SPEC6")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Spec7)
                    .HasColumnName("SPEC7")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lie)
                    .HasColumnName("SPEC8")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Loft)
                    .HasColumnName("SPEC9")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrderBaseId)
                    .HasColumnName("WORK_ORDER_BASE_ID")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WoShipment>(entity =>
            {

                entity.HasKey(e => new { e.MfgFaclty, e.SalesOrder, e.DowndoadBatchId });

                entity.ToTable("WO_SHIPMENT");

                entity.Property(e => e.MfgFaclty)
                    .HasColumnName("MFG_FACLTY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SalesOrder)
                    .HasColumnName("SALES_ORDER")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DowndoadBatchId)
                    .HasColumnName("DOWNDOAD_BATCH_ID")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Shipment)
                    .HasColumnName("SHIPMENT")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrder)
                    .HasColumnName("WORK_ORDER")
                    .HasMaxLength(15)
                    .IsFixedLength();

                entity.Property(e => e.WorkOrderType)
                    .HasColumnName("WORK_ORDER_TYPE")
                    .HasMaxLength(6)
                    .IsFixedLength();
            });

            modelBuilder.Entity<WoSku>(entity =>
            {

                entity.HasKey(e => new { e.MfgFaclty, e.WorkOrder, e.WorkOrderType, e.WorkOrderLine });

                entity.ToTable("WO_SKU");

                entity.Property(e => e.MfgFaclty)
                    .HasColumnName("MFG_FACLTY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrder)
                    .HasColumnName("WORK_ORDER")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrderType)
                    .HasColumnName("WORK_ORDER_TYPE")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrderLine)
                    .HasColumnName("WORK_ORDER_LINE")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.ButtCutLength)
                    .HasColumnName("BUTT_CUT_LENGTH")
                    .HasColumnType("decimal(13, 6)");

                entity.Property(e => e.ButtTemplate)
                    .HasColumnName("BUTT_TEMPLATE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ButtTemplateLength)
                    .HasColumnName("BUTT_TEMPLATE_LENGTH")
                    .HasColumnType("decimal(13, 6)");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Sku)
                    .HasColumnName("SKU")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SkuDescription)
                    .HasColumnName("SKU_DESCRIPTION")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SkuType)
                    .HasColumnName("SKU_TYPE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SwingweightCode)
                    .HasColumnName("SWINGWEIGHT_CODE")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SwingweightValue)
                    .HasColumnName("SWINGWEIGHT_VALUE");

                entity.Property(e => e.TipCutLength)
                    .HasColumnName("TIP_CUT_LENGTH")
                    .HasColumnType("decimal(13, 6)");

                entity.Property(e => e.TipTemplate)
                    .HasColumnName("TIP_TEMPLATE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.TipTemplateLength)
                     .HasColumnName("TIP_TEMPLATE_LENGTH")
                     .HasColumnType("decimal(13, 6)");

                entity.Property(e => e.GripWraps)
                 .HasColumnName("GRIP_WRAPS")
                 .HasColumnType("varchar(40)");

                entity.Property(e => e.HeadCoverSku)
                 .HasColumnName("HEADCOVER_SKU")
                 .HasColumnType("varchar(20)");

                entity.Property(e => e.HeadCoverQuantity)
                    .HasColumnName("HEADCOVER_QTY")
                    .HasColumnType("int");

                entity.Property(e => e.HeadCoverDescription)
                 .HasColumnName("HEADCOVER_DESC")
                 .HasColumnType("varchar(20)");
            });

            modelBuilder.Entity<WoSubsku>(entity =>
            {

                entity.HasKey(e => new { e.MfgFaclty, e.WorkOrder, e.WorkOrderType, e.WorkOrderLine, e.SubskuLine });


                entity.ToTable("WO_SUBSKU");
                entity.Property(e => e.FerruleType)
           .HasColumnName("FERRULE_TYPE")
           .IsUnicode(false);

                entity.Property(e => e.FinalClubLength)
           .HasColumnName("FINAL_CLUB_LENGTH")
           .IsUnicode(false);

                entity.Property(e => e.LieAngle)
.HasColumnName("LIE")
.IsUnicode(false);

                entity.Property(e => e.FctSet)
.HasColumnName("FCT_SET")
.IsUnicode(false);
                entity.Property(e => e.HeadType)
.HasColumnName("HEAD_TYPE")
.IsUnicode(false);


                entity.Property(e => e.Ferrule)
                 .HasColumnName("FERRULE")
                 .IsUnicode(false);

                entity.Property(e => e.SwRange)
                    .HasColumnName("SW_RANGE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.MfgFaclty)
                    .HasColumnName("MFG_FACLTY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrder)
                    .HasColumnName("WORK_ORDER")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrderType)
                    .HasColumnName("WORK_ORDER_TYPE")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.WorkOrderLine)
                    .HasColumnName("WORK_ORDER_LINE")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.SubskuLine)
                    .HasColumnName("SUBSKU_LINE")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.ButtCutLength)
                    .HasColumnName("BUTT_CUT_LENGTH")
                    .HasColumnType("decimal(13, 6)");

                entity.Property(e => e.ButtTemplate)
                    .HasColumnName("BUTT_TEMPLATE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ButtTemplateLength)
                    .HasColumnName("BUTT_TEMPLATE_LENGTH")
                    .HasColumnType("decimal(12, 6)");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.PlugFamily)
                    .HasColumnName("PLUG_FAMILY")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SubskuDescription)
                    .HasColumnName("SUBSKU_DESCRIPTION")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SubskuQty).HasColumnName("SUBSKU_QTY");

                entity.Property(e => e.SubskuSku)
                    .HasColumnName("SUBSKU_SKU")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SubskuType)
                    .HasColumnName("SUBSKU_TYPE")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SwShaftLength)
                    .HasColumnName("SW_SHAFT_LENGTH")
                    .HasColumnType("decimal(13, 6)");

                entity.Property(e => e.SwTolerance)
                    .HasColumnName("SW_TOLERANCE")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.SwingweightCode)
                    .HasColumnName("SWINGWEIGHT_CODE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SwingweightValue)
                    .HasColumnName("SWINGWEIGHT_VALUE")
                    .HasColumnType("decimal(12, 6)");

                entity.Property(e => e.TipCutLength)
                    .HasColumnName("TIP_CUT_LENGTH")
                    .HasColumnType("decimal(12, 6)");

                entity.Property(e => e.TipPrep)
                    .HasColumnName("TIP_PREP")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.TipTemplate)
                    .HasColumnName("TIP_TEMPLATE")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.TipTemplateLength)
                    .HasColumnName("TIP_TEMPLATE_LENGTH")
                    .HasColumnType("decimal(12, 6)");

                entity.Property(e => e.COO)
                    .HasColumnName("COO");

                entity.Property(e => e.Adhesive)
                .HasColumnName("ADHESIVE")
                .HasColumnType("decimal(7, 2)");

                entity.Property(e => e.TipCap)
                .HasColumnName("TIP_CAP")
                .HasColumnType("varchar(20)");
            });


            modelBuilder.Entity<WoSkuAlias>(entity =>
            {
                entity.HasKey(e => new { e.Alias });

                entity.ToTable("WO_SKU_ALIAS");

                entity.Property(e => e.Alias)
                    .HasColumnName("ALIAS");

                entity.Property(e => e.AliasType)
                    .HasColumnName("TYPE");

                entity.Property(e => e.Sku)
                    .HasColumnName("SKU");
            });

        }

        public DbSet<WoComponent> WoComponent { get; set; }
        public DbSet<WoOrder> WoOrder { get; set; }
        public DbSet<WoShipment> WoShipment { get; set; }
        public DbSet<WoSku> WoSku { get; set; }
        public DbSet<WoSubsku> WoSubsku { get; set; }
        public DbSet<WoSkuAlias> WoSkuAlias { get; set; }





    }
}