﻿using TMGA.Domain.Models.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace TMGA.Domain.Models.Intercept
{
    [Serializable]
    public partial class WoSkuAlias
    { 
        public string Alias { get; set; }
        public string AliasType { get; set; }
        public string Sku { get; set; }
    }
}