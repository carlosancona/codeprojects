﻿using TMGA.Domain.Models.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace TMGA.Domain.Models.Intercept
{
    [Serializable]
    public class WoOrder
    {
        //keys
        public string WorkOrder { get; set; }
        public string MfgFaclty { get; set; }
        public string WorkOrderType { get; set; }

        public List<WoSku> SKUs { get; set; }
        public string WorkOrderBaseId { get; set; }
        public string SalesOrder { get; set; }
        public string SalesOrderLine { get; set; }
        public string Shipment { get; set; }
        public string Emergency { get; set; }
        public DateTime? CreationDate { get; set; }
        public string SoLineContext { get; set; }
        public string Spec1 { get; set; }
        public string Spec2 { get; set; }
        public string Spec3 { get; set; }
        public string FCT { get; set; }
        public string Spec5 { get; set; }
        public string Spec6 { get; set; }
        public string Spec7 { get; set; }
        public string Lie { get; set; }
        public string Loft { get; set; }
        public string Spec10 { get; set; }
        public string Spec11 { get; set; }
        public string Spec12 { get; set; }
        public string Spec13 { get; set; }
        public string Spec14 { get; set; }
        public string Spec15 { get; set; }
        public string GripLogoDirection { get; set; }
        public string GripWraps { get; set; }
        public string Spec18 { get; set; }
        public string Spec19 { get; set; }
        public string Spec20 { get; set; }
        public string Spec21 { get; set; }
        public string Spec22 { get; set; }
        public string Spec23 { get; set; }
        public string Spec24 { get; set; }
        public string Spec25 { get; set; }
        public string Spec26 { get; set; }
        public string Spec27 { get; set; }
        public string Spec28 { get; set; }
        public string Spec29 { get; set; }
        public string Spec30 { get; set; }

        [NotMapped]
        public int? AssignedCellId { get; set; }

        [NotMapped]
        public Cell AssignedCell { get; set; }

        [NotMapped]
        public int? AssemblySequence { get; set; }

        [NotMapped]
        public string CapacityReason { get; set; }

        [NotMapped]
        public string WaveId { get; set; }
    }

    public static class WoExtensions
    {
        public static WoComponent Head(this WoOrder workOrder) => workOrder?.SKUs.FirstOrDefault().SubSKUs.FirstOrDefault().Components.Where(_ => _.ComponentType == "HEAD").FirstOrDefault();

        public static WoComponent Shaft(this WoOrder workOrder) => workOrder?.SKUs.FirstOrDefault().SubSKUs.FirstOrDefault()?.Components.Where(_ => _.ComponentType == "SHAFT").FirstOrDefault();

        public static WoComponent Grip(this WoOrder workOrder) => workOrder?.SKUs.FirstOrDefault().SubSKUs.FirstOrDefault().Components.Where(_ => _.ComponentType == "GRIP").FirstOrDefault();
         
        public static int ClubCount(this WoOrder workOrder) => Convert.ToInt32(workOrder?.SKUs?.Sum(_ => _?.SubSKUs?.Sum(__ => __?.SubskuQty)));

        public static int CustomClubCount(this WoOrder workOrder) => Convert.ToInt32(workOrder?.SKUs.Where(_ => _.WorkOrderType == "KUST")?.Sum(_ => _?.SubSKUs?.Sum(__ => __?.SubskuQty)));

        public static bool HasBending(this WoOrder workOrder) => (workOrder.Lie != "Standard" || workOrder.Lie != null) || (workOrder.Loft != "Standard" || workOrder.Loft != null);

        public static double LongestClubLength(this WoOrder workOrder) => Convert.ToDouble(workOrder.SKUs.Max(_ => _.ButtTemplateLength));

        public static decimal? SevenIronLength(this WoOrder workOrder) =>
            workOrder?.SKUs.Select(sku =>
                sku.SubSKUs.Where(subsku =>
                    subsku.Components.Any(component =>
                        component.ComponentDescription.Contains("#7")))
                        .FirstOrDefault())
                    .FirstOrDefault()?.ButtCutLength;

        public static string ClubName(this WoOrder workOrder) => workOrder?.SKUs.FirstOrDefault().SkuDescription;

        public static string ClubType(this WoOrder workOrder) => workOrder?.SKUs.FirstOrDefault().SkuType;

        public static string GetLie(this WoOrder workOrder)
        {
            if (workOrder.Lie != "Standard" && !String.IsNullOrEmpty(workOrder.Lie))
                return workOrder.Lie;
            else
                return "Standard";
        }

        public static string GetLoft(this WoOrder workOrder)
        {
            if (workOrder.Loft != "Standard" && !String.IsNullOrEmpty(workOrder.Loft))
                return workOrder.Loft;
            else
                return "Standard";
        }

        public static string GetGripLogoOrientation(this WoOrder workOrder) =>
            workOrder.GripLogoDirection != "Standard" && !String.IsNullOrEmpty(workOrder.GripLogoDirection) ?
                 workOrder.GripLogoDirection : "Standard";

        public static string SwingWeightRange(this WoOrder workOrder)
        {

            return workOrder?.SKUs?.First()?.SubSKUs?.First()?.SwRange;
            //TODO: this should be a service at this point
            //if (workOrder?.SKUs?.FirstOrDefault()?.SubSKUs?.FirstOrDefault().SwingweightCode != null)
            //{
            //    var firstSwingWeightCharacter = workOrder?.SKUs?.FirstOrDefault()?.SubSKUs?.FirstOrDefault()?.SwingweightCode;
            //    var swingWeightBase = Convert.ToDouble(workOrder.SKUs.FirstOrDefault().SubSKUs.FirstOrDefault().SwingweightCode.Substring(1, 3).Trim());
            //    var swingWeightRange = workOrder.SKUs.FirstOrDefault().SubSKUs.FirstOrDefault().SwTolerance;
            //    var lowerSwingWeightRange = swingWeightBase - Convert.ToDouble(swingWeightRange);
            //    var higherSwingWeightRange = swingWeightBase + Convert.ToDouble(swingWeightRange);

            //    return $"{firstSwingWeightCharacter}{lowerSwingWeightRange.ToString("n1")}-{higherSwingWeightRange.ToString("n1")}";
            //}
            //else
            //{
            //    return "N/A";
            //}
        }

        public static string GetHeadRange(this WoOrder workOrder)
        {

            return workOrder?.SKUs.First()?.SubSKUs?.First().HeadType;
            //if (workOrder?.SKUs?.FirstOrDefault()?.SkuType == "Irons")
            //{
            //    return workOrder.Spec1 != "" && workOrder.Spec1 != null ? workOrder.Spec1.Split("_")[4] : "None";
            //}
            //else 
            //{
            //    return workOrder.Spec1 != "" && workOrder.Spec1 != null ? workOrder.Spec1.Split("_")[1] : "None";
            //}
        }
            

    }
}