﻿using System;

namespace TMGA.Domain.Models.Intercept
{
    public partial class WoShipment
    {
        public string MfgFaclty { get; set; }
        public string SalesOrder { get; set; }
        public string DowndoadBatchId { get; set; }
        public string Shipment { get; set; }
        public string WorkOrder { get; set; }
        public string WorkOrderType { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}