﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Domain.Models.Intercept
{
    [Serializable]
    public class WoComponent
    {
        //keys
        public string MfgFaclty { get; set; }
        public string WorkOrder { get; set; }
        public string WorkOrderType { get; set; }
        public string WorkOrderLine { get; set; }
        public string SubskuLine { get; set; }

        public string ComponentLine { get; set; }
        public string ComponentSku { get; set; }
        public string ComponentType { get; set; }
        public double? ComponentQty { get; set; }
        public DateTime? CreationDate { get; set; }
        public string ComponentDescription { get; set; }

        public static implicit operator List<object>(WoComponent v)
        {
            throw new NotImplementedException();
        }
    }
}