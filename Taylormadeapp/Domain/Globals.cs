﻿using System.Collections.Generic;

namespace TMGA.Domain
{
    public static class Globals
    {
        public static List<string> StandardLieLoftValues => new List<string> { "standard", "std", "", string.Empty, null };
        public static List<string> CustomOrderTypeValues => new List<string> { "cust", "CUST", "KUST", "kust" };
    }
}
