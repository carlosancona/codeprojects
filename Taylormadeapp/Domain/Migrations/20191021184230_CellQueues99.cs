﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Domain.Migrations
{
    public partial class CellQueues99 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AssemblySequence",
                table: "CellQueues",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ClubCount",
                table: "CellQueues",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsPrinted",
                table: "CellQueues",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "NextWorkOrder",
                table: "CellQueues",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReasonCode",
                table: "CellQueues",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WO",
                table: "CellQueues",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssemblySequence",
                table: "CellQueues");

            migrationBuilder.DropColumn(
                name: "ClubCount",
                table: "CellQueues");

            migrationBuilder.DropColumn(
                name: "IsPrinted",
                table: "CellQueues");

            migrationBuilder.DropColumn(
                name: "NextWorkOrder",
                table: "CellQueues");

            migrationBuilder.DropColumn(
                name: "ReasonCode",
                table: "CellQueues");

            migrationBuilder.DropColumn(
                name: "WO",
                table: "CellQueues");
        }
    }
}