﻿namespace Domain.Migrations
{
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class AddSiteCells : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CellQueueId",
                table: "Sites",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SiteId1",
                table: "Cells",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sites_CellQueueId",
                table: "Sites",
                column: "CellQueueId");

            migrationBuilder.CreateIndex(
                name: "IX_Cells_SiteId1",
                table: "Cells",
                column: "SiteId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Cells_Sites_SiteId1",
                table: "Cells",
                column: "SiteId1",
                principalTable: "Sites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sites_CellQueues_CellQueueId",
                table: "Sites",
                column: "CellQueueId",
                principalTable: "CellQueues",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cells_Sites_SiteId1",
                table: "Cells");

            migrationBuilder.DropForeignKey(
                name: "FK_Sites_CellQueues_CellQueueId",
                table: "Sites");

            migrationBuilder.DropIndex(
                name: "IX_Sites_CellQueueId",
                table: "Sites");

            migrationBuilder.DropIndex(
                name: "IX_Cells_SiteId1",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "CellQueueId",
                table: "Sites");

            migrationBuilder.DropColumn(
                name: "SiteId1",
                table: "Cells");
        }
    }
}