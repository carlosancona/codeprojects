﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Domain.Migrations
{
    public partial class StationsAgain343r93 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_CellQueues_Stations_StationId",
            //    table: "CellQueues");

            //migrationBuilder.AlterColumn<int>(
            //    name: "StationId",
            //    table: "CellQueues",
            //    nullable: false,
            //    oldClrType: typeof(int),
            //    oldNullable: true);

            migrationBuilder.CreateTable(
                name: "CellQueueSnapshots",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CellId = table.Column<int>(nullable: true),
                    StationId = table.Column<int>(nullable: false),
                    ReleasedWaveId = table.Column<int>(nullable: true),
                    Wave = table.Column<string>(nullable: true),
                    WO = table.Column<string>(nullable: true),
                    ClubCount = table.Column<int>(nullable: false),
                    AssemblySequence = table.Column<int>(nullable: false),
                    NextWorkOrder = table.Column<string>(nullable: true),
                    IsPrinted = table.Column<bool>(nullable: false),
                    ReasonCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CellQueueSnapshots", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CellQueueSnapshots_Cells_CellId",
                        column: x => x.CellId,
                        principalTable: "Cells",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CellQueueSnapshots_ReleasedWaves_ReleasedWaveId",
                        column: x => x.ReleasedWaveId,
                        principalTable: "ReleasedWaves",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CellQueueSnapshots_Stations_StationId",
                        column: x => x.StationId,
                        principalTable: "Stations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CellQueueSnapshots_CellId",
                table: "CellQueueSnapshots",
                column: "CellId");

            migrationBuilder.CreateIndex(
                name: "IX_CellQueueSnapshots_ReleasedWaveId",
                table: "CellQueueSnapshots",
                column: "ReleasedWaveId");

            //migrationBuilder.CreateIndex(
            //    name: "IX_CellQueueSnapshots_StationId",
            //    table: "CellQueueSnapshots",
            //    column: "StationId");

            migrationBuilder.AddForeignKey(
                name: "FK_CellQueues_Stations_StationId",
                table: "CellQueues",
                column: "StationId",
                principalTable: "Stations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CellQueues_Stations_StationId",
                table: "CellQueues");

            migrationBuilder.DropTable(
                name: "CellQueueSnapshots");

            migrationBuilder.AlterColumn<int>(
                name: "StationId",
                table: "CellQueues",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_CellQueues_Stations_StationId",
                table: "CellQueues",
                column: "StationId",
                principalTable: "Stations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}