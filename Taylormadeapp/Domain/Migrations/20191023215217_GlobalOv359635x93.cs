﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Domain.Migrations
{
    public partial class GlobalOv359635x93 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "PickListGenerated",
                table: "CellQueues",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PickListPdfUrl",
                table: "CellQueues",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PickListGenerated",
                table: "CellQueues");

            migrationBuilder.DropColumn(
                name: "PickListPdfUrl",
                table: "CellQueues");
        }
    }
}