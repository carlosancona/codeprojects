﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Domain.Migrations
{
    public partial class StationHistoryChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Created",
                table: "StationHistory");

            migrationBuilder.RenameColumn(
                name: "CurrentOrder",
                table: "Stations",
                newName: "CurrentOrderWorkOrderHeaderId");

            migrationBuilder.RenameColumn(
                name: "EnteredCell",
                table: "StationHistory",
                newName: "Entered");

            migrationBuilder.RenameColumn(
                name: "DepartedCell",
                table: "StationHistory",
                newName: "Departed");

            migrationBuilder.AlterColumn<string>(
                name: "CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Stations_CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                column: "CurrentOrderWorkOrderHeaderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Stations_OM_F_CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                column: "CurrentOrderWorkOrderHeaderId",
                principalTable: "OM_F",
                principalColumn: "OB_OID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stations_OM_F_CurrentOrderWorkOrderHeaderId",
                table: "Stations");

            migrationBuilder.DropIndex(
                name: "IX_Stations_CurrentOrderWorkOrderHeaderId",
                table: "Stations");

            migrationBuilder.RenameColumn(
                name: "CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                newName: "CurrentOrder");

            migrationBuilder.RenameColumn(
                name: "Entered",
                table: "StationHistory",
                newName: "EnteredCell");

            migrationBuilder.RenameColumn(
                name: "Departed",
                table: "StationHistory",
                newName: "DepartedCell");

            migrationBuilder.AlterColumn<string>(
                name: "CurrentOrder",
                table: "Stations",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "StationHistory",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}