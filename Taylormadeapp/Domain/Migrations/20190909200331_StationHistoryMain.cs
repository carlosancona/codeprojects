﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Domain.Migrations
{
    public partial class StationHistoryMain : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "StationHistory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Result = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    EnteredCell = table.Column<DateTime>(nullable: false),
                    DepartedCell = table.Column<DateTime>(nullable: false),
                    WO = table.Column<string>(nullable: true),
                    LineNumber = table.Column<int>(nullable: false),
                    StationId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StationHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_StationHistory_Stations_StationId",
                        column: x => x.StationId,
                        principalTable: "Stations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StationHistory_StationId",
                table: "StationHistory",
                column: "StationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StationHistory");
        }
    }
}