﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace Domain.Migrations
{
    public partial class CapPlan2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalOrders",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "TotalOrdersThatHaveHadBending",
                table: "Cells");

            migrationBuilder.AddColumn<string>(
                name: "CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CurrentWorkOrderNumber",
                table: "Cells",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WorkOrderHeaderId",
                table: "Cells",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PM_F",
                columns: table => new
                {
                    SKU = table.Column<string>(nullable: false),
                    SKU_TYPE = table.Column<string>(nullable: true),
                    SKU_DESC = table.Column<string>(nullable: true),
                    UNIT_COST = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PM_F", x => x.SKU);
                });

            migrationBuilder.CreateTable(
                name: "WHSE",
                columns: table => new
                {
                    WHSE_ID = table.Column<string>(nullable: false),
                    WHSE_NAME = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WHSE", x => x.WHSE_ID);
                });

            migrationBuilder.CreateTable(
                name: "WV_F",
                columns: table => new
                {
                    WAVE = table.Column<string>(nullable: false),
                    RELEASE_DATE = table.Column<DateTime>(nullable: false),
                    WHSE_ID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WV_F", x => x.WAVE);
                    table.ForeignKey(
                        name: "FK_WV_F_WHSE_WHSE_ID",
                        column: x => x.WHSE_ID,
                        principalTable: "WHSE",
                        principalColumn: "WHSE_ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OM_F",
                columns: table => new
                {
                    OB_OID = table.Column<string>(nullable: false),
                    OB_TYPE = table.Column<string>(nullable: true),
                    EMERGENCY = table.Column<string>(nullable: true),
                    ORD_DATE = table.Column<DateTime>(nullable: false),
                    SHIP_NAME = table.Column<string>(nullable: true),
                    SHIP_ADDR1 = table.Column<string>(nullable: true),
                    SHIP_ADDR2 = table.Column<string>(nullable: true),
                    SHIP_ADDR3 = table.Column<string>(nullable: true),
                    SHIP_CITY = table.Column<string>(nullable: true),
                    SHIP_STATE = table.Column<string>(nullable: true),
                    SHIP_ZIP = table.Column<string>(nullable: true),
                    SHIP_CNTRY = table.Column<string>(nullable: true),
                    TOTAL_VALUE = table.Column<double>(nullable: false),
                    GRP2 = table.Column<string>(nullable: true),
                    WAVE = table.Column<string>(nullable: true),
                    WHSE_ID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OM_F", x => x.OB_OID);
                    table.ForeignKey(
                        name: "FK_OM_F_WHSE_WHSE_ID",
                        column: x => x.WHSE_ID,
                        principalTable: "WHSE",
                        principalColumn: "WHSE_ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OM_F_WV_F_WAVE",
                        column: x => x.WAVE,
                        principalTable: "WV_F",
                        principalColumn: "WAVE",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OD_F",
                columns: table => new
                {
                    OD_RID = table.Column<string>(nullable: false),
                    OB_LNO = table.Column<int>(nullable: false),
                    CUST_PART_DESC = table.Column<string>(nullable: true),
                    SKU = table.Column<string>(nullable: true),
                    OB_OID = table.Column<string>(nullable: true),
                    UC1 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OD_F", x => x.OD_RID);
                    table.ForeignKey(
                        name: "FK_OD_F_PM_F_SKU",
                        column: x => x.SKU,
                        principalTable: "PM_F",
                        principalColumn: "SKU",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OD_F_OM_F_OB_OID",
                        column: x => x.OB_OID,
                        principalTable: "OM_F",
                        principalColumn: "OB_OID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TM_CUSTOM_CLUB_SPECS",
                columns: table => new
                {
                    WO = table.Column<string>(nullable: false),
                    SO = table.Column<string>(nullable: true),
                    SO_LINE = table.Column<string>(nullable: true),
                    SO_LINE_CONTEXT = table.Column<string>(nullable: true),
                    SPEC1 = table.Column<string>(nullable: true),
                    SPEC2 = table.Column<string>(nullable: true),
                    SPEC3 = table.Column<string>(nullable: true),
                    SPEC4 = table.Column<string>(nullable: true),
                    SPEC5 = table.Column<string>(nullable: true),
                    SPEC6 = table.Column<string>(nullable: true),
                    SPEC7 = table.Column<string>(nullable: true),
                    SPEC8 = table.Column<string>(nullable: true),
                    SPEC9 = table.Column<string>(nullable: true),
                    SPEC10 = table.Column<string>(nullable: true),
                    SPEC11 = table.Column<string>(nullable: true),
                    SPEC12 = table.Column<string>(nullable: true),
                    SPEC13 = table.Column<string>(nullable: true),
                    SPEC14 = table.Column<string>(nullable: true),
                    SPEC15 = table.Column<string>(nullable: true),
                    SPEC16 = table.Column<string>(nullable: true),
                    SPEC17 = table.Column<string>(nullable: true),
                    SPEC18 = table.Column<string>(nullable: true),
                    SPEC19 = table.Column<string>(nullable: true),
                    SPEC20 = table.Column<string>(nullable: true),
                    SPEC21 = table.Column<string>(nullable: true),
                    SPEC22 = table.Column<string>(nullable: true),
                    SPEC23 = table.Column<string>(nullable: true),
                    SPEC24 = table.Column<string>(nullable: true),
                    SPEC25 = table.Column<string>(nullable: true),
                    SPEC26 = table.Column<string>(nullable: true),
                    SPEC27 = table.Column<string>(nullable: true),
                    SPEC28 = table.Column<string>(nullable: true),
                    SPEC29 = table.Column<string>(nullable: true),
                    SPEC30 = table.Column<string>(nullable: true),
                    INSERTED = table.Column<DateTime>(nullable: true),
                    WorkOrderDetailId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TM_CUSTOM_CLUB_SPECS", x => x.WO);
                    table.ForeignKey(
                        name: "FK_TM_CUSTOM_CLUB_SPECS_OD_F_WorkOrderDetailId",
                        column: x => x.WorkOrderDetailId,
                        principalTable: "OD_F",
                        principalColumn: "OD_RID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Stations_CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                column: "CurrentOrderWorkOrderHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_Cells_WorkOrderHeaderId",
                table: "Cells",
                column: "WorkOrderHeaderId");

            migrationBuilder.CreateIndex(
                name: "IX_OD_F_SKU",
                table: "OD_F",
                column: "SKU");

            migrationBuilder.CreateIndex(
                name: "IX_OD_F_OB_OID",
                table: "OD_F",
                column: "OB_OID");

            migrationBuilder.CreateIndex(
                name: "IX_OM_F_WHSE_ID",
                table: "OM_F",
                column: "WHSE_ID");

            migrationBuilder.CreateIndex(
                name: "IX_OM_F_WAVE",
                table: "OM_F",
                column: "WAVE");

            migrationBuilder.CreateIndex(
                name: "IX_TM_CUSTOM_CLUB_SPECS_WorkOrderDetailId",
                table: "TM_CUSTOM_CLUB_SPECS",
                column: "WorkOrderDetailId");

            migrationBuilder.CreateIndex(
                name: "IX_WV_F_WHSE_ID",
                table: "WV_F",
                column: "WHSE_ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Cells_OM_F_WorkOrderHeaderId",
                table: "Cells",
                column: "WorkOrderHeaderId",
                principalTable: "OM_F",
                principalColumn: "OB_OID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Stations_OM_F_CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                column: "CurrentOrderWorkOrderHeaderId",
                principalTable: "OM_F",
                principalColumn: "OB_OID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cells_OM_F_WorkOrderHeaderId",
                table: "Cells");

            migrationBuilder.DropForeignKey(
                name: "FK_Stations_OM_F_CurrentOrderWorkOrderHeaderId",
                table: "Stations");

            migrationBuilder.DropTable(
                name: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropTable(
                name: "OD_F");

            migrationBuilder.DropTable(
                name: "PM_F");

            migrationBuilder.DropTable(
                name: "OM_F");

            migrationBuilder.DropTable(
                name: "WV_F");

            migrationBuilder.DropTable(
                name: "WHSE");

            migrationBuilder.DropIndex(
                name: "IX_Stations_CurrentOrderWorkOrderHeaderId",
                table: "Stations");

            migrationBuilder.DropIndex(
                name: "IX_Cells_WorkOrderHeaderId",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "CurrentOrderWorkOrderHeaderId",
                table: "Stations");

            migrationBuilder.DropColumn(
                name: "CurrentWorkOrderNumber",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "WorkOrderHeaderId",
                table: "Cells");

            migrationBuilder.AddColumn<int>(
                name: "TotalOrders",
                table: "Cells",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalOrdersThatHaveHadBending",
                table: "Cells",
                nullable: false,
                defaultValue: 0);
        }
    }
}