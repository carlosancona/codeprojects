﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Domain.Migrations
{
    public partial class ModelingCells : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "BendingRatio",
                table: "Cells",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "ClubsInCell",
                table: "Cells",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ClubsQueued",
                table: "Cells",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Grouping",
                table: "Cells",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "LastHadBending",
                table: "Cells",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BendingRatio",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "ClubsInCell",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "ClubsQueued",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "Grouping",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "LastHadBending",
                table: "Cells");
        }
    }
}