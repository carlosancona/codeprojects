﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Domain.Migrations
{
    public partial class AddSiteCells2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cells_Sites_SiteId1",
                table: "Cells");

            migrationBuilder.DropIndex(
                name: "IX_Cells_SiteId1",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "SiteId1",
                table: "Cells");

            migrationBuilder.AlterColumn<int>(
                name: "SiteId",
                table: "Cells",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cells_SiteId",
                table: "Cells",
                column: "SiteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cells_Sites_SiteId",
                table: "Cells",
                column: "SiteId",
                principalTable: "Sites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cells_Sites_SiteId",
                table: "Cells");

            migrationBuilder.DropIndex(
                name: "IX_Cells_SiteId",
                table: "Cells");

            migrationBuilder.AlterColumn<string>(
                name: "SiteId",
                table: "Cells",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SiteId1",
                table: "Cells",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cells_SiteId1",
                table: "Cells",
                column: "SiteId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Cells_Sites_SiteId1",
                table: "Cells",
                column: "SiteId1",
                principalTable: "Sites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}