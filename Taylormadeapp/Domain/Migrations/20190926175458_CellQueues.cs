﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Domain.Migrations
{
    public partial class CellQueues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SPEC5",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC19",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC21",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC22",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC23",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC24",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC25",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC26",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC27",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC28",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC29",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.DropColumn(
                name: "SPEC30",
                table: "TM_CUSTOM_CLUB_SPECS");

            migrationBuilder.AlterColumn<double>(
                name: "SPEC15",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SHIPMENT",
                table: "OM_F",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "BAL_PLAN_QTY",
                table: "OD_F",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CMP_QTY",
                table: "OD_F",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ORD_QTY",
                table: "OD_F",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ORG_QTY",
                table: "OD_F",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "PLAN_QTY",
                table: "OD_F",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SCHED_QTY",
                table: "OD_F",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SHIP_QTY",
                table: "OD_F",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "ORD_UOM",
                table: "OD_F",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AssemblySequence",
                table: "Cells",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalOrders",
                table: "Cells",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalOrdersThatHaveHadBending",
                table: "Cells",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CNTY_F",
                columns: table => new
                {
                    CNTYPE = table.Column<string>(nullable: false),
                    CNTYPE_DESC = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CNTY_F", x => x.CNTYPE);
                });

            migrationBuilder.CreateTable(
                name: "LC_F",
                columns: table => new
                {
                    LOC = table.Column<string>(nullable: false),
                    LOC_TYPE = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LC_F", x => x.LOC);
                });

            migrationBuilder.CreateTable(
                name: "ZZHEADPARTNUMBER",
                columns: table => new
                {
                    SKU = table.Column<string>(nullable: false),
                    SKU_DESC = table.Column<string>(nullable: true),
                    HEAD_TYPE_DESC = table.Column<string>(nullable: true),
                    HEAD_TYPE = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZZHEADPARTNUMBER", x => x.SKU);
                });

            migrationBuilder.CreateTable(
                name: "ZZSHAFTCUT",
                columns: table => new
                {
                    SHAFT_PART_NUMBER = table.Column<string>(nullable: false),
                    SHAFT_TYPE = table.Column<string>(nullable: true),
                    HEAD_TYPE_DESC = table.Column<string>(nullable: true),
                    TEMPLATE1 = table.Column<string>(nullable: true),
                    TEMPLATE_TIP = table.Column<string>(nullable: true),
                    TEMPLATE_BUTT = table.Column<string>(nullable: true),
                    RAW_LENGTH = table.Column<string>(nullable: true),
                    TIP = table.Column<string>(nullable: true),
                    STD_LEN_BUTT = table.Column<string>(nullable: true),
                    FINAL_BUTT = table.Column<string>(nullable: true),
                    TIP_PREP = table.Column<string>(nullable: true),
                    FERRULE = table.Column<string>(nullable: true),
                    FERRULE_SET = table.Column<string>(nullable: true),
                    FINAL_CLUB_LENGTH = table.Column<string>(nullable: true),
                    LIE = table.Column<string>(nullable: true),
                    SW = table.Column<string>(nullable: true),
                    TIP_TEMPLATE_NUMBER = table.Column<string>(nullable: true),
                    COO = table.Column<string>(nullable: true),
                    SHIM = table.Column<string>(nullable: true),
                    TIP_CAP_NO = table.Column<string>(nullable: true),
                    SBL_PART_NO = table.Column<string>(nullable: true),
                    PARALLEL_SHAFT = table.Column<string>(nullable: true),
                    STOCK_GRIP = table.Column<string>(nullable: true),
                    PLUG_FAMILY = table.Column<string>(nullable: true),
                    ID_NO = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZZSHAFTCUT", x => x.SHAFT_PART_NUMBER);
                });

            migrationBuilder.CreateTable(
                name: "CM_F",
                columns: table => new
                {
                    CM_RID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TASK = table.Column<string>(nullable: true),
                    LOC = table.Column<string>(nullable: true),
                    WAVE = table.Column<string>(nullable: true),
                    OD_RID = table.Column<string>(nullable: true),
                    CONT = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CM_F", x => x.CM_RID);
                    table.ForeignKey(
                        name: "FK_CM_F_LC_F_LOC",
                        column: x => x.LOC,
                        principalTable: "LC_F",
                        principalColumn: "LOC",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CM_F_WV_F_WAVE",
                        column: x => x.WAVE,
                        principalTable: "WV_F",
                        principalColumn: "WAVE",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CM_F_OD_F_OD_RID",
                        column: x => x.OD_RID,
                        principalTable: "OD_F",
                        principalColumn: "OD_RID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CN_F",
                columns: table => new
                {
                    CN_RID = table.Column<string>(nullable: false),
                    CONT = table.Column<string>(nullable: true),
                    CNTYPE = table.Column<string>(nullable: true),
                    CommandMasterId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CN_F", x => x.CN_RID);
                    table.ForeignKey(
                        name: "FK_CN_F_CM_F_CommandMasterId",
                        column: x => x.CommandMasterId,
                        principalTable: "CM_F",
                        principalColumn: "CM_RID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CN_F_CNTY_F_CNTYPE",
                        column: x => x.CNTYPE,
                        principalTable: "CNTY_F",
                        principalColumn: "CNTYPE",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CM_F_LOC",
                table: "CM_F",
                column: "LOC");

            migrationBuilder.CreateIndex(
                name: "IX_CM_F_WAVE",
                table: "CM_F",
                column: "WAVE");

            migrationBuilder.CreateIndex(
                name: "IX_CM_F_OD_RID",
                table: "CM_F",
                column: "OD_RID");

            migrationBuilder.CreateIndex(
                name: "IX_CN_F_CommandMasterId",
                table: "CN_F",
                column: "CommandMasterId");

            migrationBuilder.CreateIndex(
                name: "IX_CN_F_CNTYPE",
                table: "CN_F",
                column: "CNTYPE");

            migrationBuilder.AddForeignKey(
                name: "FK_PM_F_ZZHEADPARTNUMBER_SKU",
                table: "PM_F",
                column: "SKU",
                principalTable: "ZZHEADPARTNUMBER",
                principalColumn: "SKU",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PM_F_ZZSHAFTCUT_SKU",
                table: "PM_F",
                column: "SKU",
                principalTable: "ZZSHAFTCUT",
                principalColumn: "SHAFT_PART_NUMBER",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PM_F_ZZHEADPARTNUMBER_SKU",
                table: "PM_F");

            migrationBuilder.DropForeignKey(
                name: "FK_PM_F_ZZSHAFTCUT_SKU",
                table: "PM_F");

            migrationBuilder.DropTable(
                name: "CN_F");

            migrationBuilder.DropTable(
                name: "ZZHEADPARTNUMBER");

            migrationBuilder.DropTable(
                name: "ZZSHAFTCUT");

            migrationBuilder.DropTable(
                name: "CM_F");

            migrationBuilder.DropTable(
                name: "CNTY_F");

            migrationBuilder.DropTable(
                name: "LC_F");

            migrationBuilder.DropColumn(
                name: "SHIPMENT",
                table: "OM_F");

            migrationBuilder.DropColumn(
                name: "BAL_PLAN_QTY",
                table: "OD_F");

            migrationBuilder.DropColumn(
                name: "CMP_QTY",
                table: "OD_F");

            migrationBuilder.DropColumn(
                name: "ORD_QTY",
                table: "OD_F");

            migrationBuilder.DropColumn(
                name: "ORG_QTY",
                table: "OD_F");

            migrationBuilder.DropColumn(
                name: "PLAN_QTY",
                table: "OD_F");

            migrationBuilder.DropColumn(
                name: "SCHED_QTY",
                table: "OD_F");

            migrationBuilder.DropColumn(
                name: "SHIP_QTY",
                table: "OD_F");

            migrationBuilder.DropColumn(
                name: "ORD_UOM",
                table: "OD_F");

            migrationBuilder.DropColumn(
                name: "AssemblySequence",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "TotalOrders",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "TotalOrdersThatHaveHadBending",
                table: "Cells");

            migrationBuilder.AlterColumn<string>(
                name: "SPEC15",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true,
                oldClrType: typeof(double),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC5",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC19",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC21",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC22",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC23",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC24",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC25",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC26",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC27",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC28",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC29",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SPEC30",
                table: "TM_CUSTOM_CLUB_SPECS",
                nullable: true);
        }
    }
}