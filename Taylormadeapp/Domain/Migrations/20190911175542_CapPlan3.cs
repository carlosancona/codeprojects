﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Domain.Migrations
{
    public partial class CapPlan3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Stations_OM_F_CurrentOrderWorkOrderHeaderId",
                table: "Stations");

            migrationBuilder.DropIndex(
                name: "IX_Stations_CurrentOrderWorkOrderHeaderId",
                table: "Stations");

            migrationBuilder.RenameColumn(
                name: "CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                newName: "CurrentOrder");

            migrationBuilder.AlterColumn<string>(
                name: "CurrentOrder",
                table: "Stations",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CurrentOrder",
                table: "Stations",
                newName: "CurrentOrderWorkOrderHeaderId");

            migrationBuilder.AlterColumn<string>(
                name: "CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Stations_CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                column: "CurrentOrderWorkOrderHeaderId");

            migrationBuilder.AddForeignKey(
                name: "FK_Stations_OM_F_CurrentOrderWorkOrderHeaderId",
                table: "Stations",
                column: "CurrentOrderWorkOrderHeaderId",
                principalTable: "OM_F",
                principalColumn: "OB_OID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}