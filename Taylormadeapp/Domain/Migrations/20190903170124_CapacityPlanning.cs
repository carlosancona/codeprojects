﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Domain.Migrations
{
    public partial class CapacityPlanning : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BendingRatio",
                table: "Cells",
                newName: "FixedBendingRatio");

            migrationBuilder.AddColumn<int>(
                name: "StationOrder",
                table: "Stations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ClubCutoffForLargeOrders",
                table: "Cells",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ClubsInLastOrder",
                table: "Cells",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Cells",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "TotalOrders",
                table: "Cells",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalOrdersThatHaveHadBending",
                table: "Cells",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StationOrder",
                table: "Stations");

            migrationBuilder.DropColumn(
                name: "ClubCutoffForLargeOrders",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "ClubsInLastOrder",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "TotalOrders",
                table: "Cells");

            migrationBuilder.DropColumn(
                name: "TotalOrdersThatHaveHadBending",
                table: "Cells");

            migrationBuilder.RenameColumn(
                name: "FixedBendingRatio",
                table: "Cells",
                newName: "BendingRatio");
        }
    }
}