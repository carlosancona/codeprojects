using System.Collections.Generic;

namespace TMGA.Shared.ViewModels
{
    public class HeadCoverViewModel
    {
        public string Description { get; set; }
        public int Quantity { get; set; }
        public string Sku { get; set; }
        public List<string> EANs { get; set; }
    }
}