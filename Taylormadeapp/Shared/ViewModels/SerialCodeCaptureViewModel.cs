namespace TMGA.Shared.ViewModels
{
    public partial class SerialCodeCaptureViewModel
    {
        public string Id { get; set; } 
        public string SerialNumber { get; set; }
        public string WorkOrderNumber { get; set; }
        public UserViewModel User { get; set; }
        public int StationId { get; set; } 
    }
}