using TMGA.Domain.Models.Engine;

namespace TMGA.Shared.ViewModels
{
    public partial class MaintenanceQuestionViewModel : MaintenanceQuestion
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public string Notes { get; set; }

        public int DisplayOrder { get; set; }
    }

    public class AnsweredQuestion
    {
        public int QuestionId { get; set; }
        public bool Answer { get; set; }
        public string UserId { get; set; }
        public int StationId { get; set; } 
    }
}