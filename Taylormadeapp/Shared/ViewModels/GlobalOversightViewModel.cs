
using System.Collections.Generic;

namespace TMGA.Shared.ViewModels
{
    public partial class GlobalOversightViewModel
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
         
        public int ClubsCurrentlyInProduction { get; set; } 
        public int ClubsCurrentlyInQueue { get; set; }
        public int ClubsCompletedToday { get; set; }
        public int ClubsCompletedAllTime { get; set; }

        public int ClubsCurrentlyInProductionThatHaveHadBending { get; set; }
        public int ClubsCurrentlyInQueueThatHaveHadBending { get; set; }
        public int ClubsCompletedTodayThatHaveHadBending { get; set; }
        public int ClubsCompletedAllTimeThatHaveHadBending { get; set; } 

        public int OrdersCurrentlyInProduction { get; set; }
        public int OrdersCurrentlyInQueue { get; set; }
        public int OrdersCompletedToday { get; set; }
        public int OrdersCompletedAllTime { get; set; }

        public int OrdersCurrentlyInProductionThatHaveHadBending { get; set; }
        public int OrdersCurrentlyInQueueThatHaveHadBending { get; set; }
        public int OrdersCompletedTodayThatHaveHadBending { get; set; }
        public int OrdersCompletedAllTimeThatHaveHadBending { get; set; } 

        public int TotalOrdersThatHaveHadBending { get; set; }

        public List<CellViewModel> Cells { get; set; }
    }
     
}