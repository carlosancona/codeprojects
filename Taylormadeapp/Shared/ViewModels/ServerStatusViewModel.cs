
using System;

namespace TMGA.Shared.ViewModels
{
    public partial class ServerStatusViewModel
    {
        public string Environment { get; set; }

        public string CoreServer { get; set; }
        public string CoreDatabase { get; set; }

        public string InterceptServer { get; set; }
        public string InterceptDatabase { get; set; }

        public string WmsServer { get; set; }
        public string WmsDatabase { get; set; }

        public string SiteId { get; set; }
        public string SiteDescription { get; set; }

        public string HoldOrDone { get; set; } 
        public string WmsId { get; set; }
        public string InterceptId { get; set; }
    }
}