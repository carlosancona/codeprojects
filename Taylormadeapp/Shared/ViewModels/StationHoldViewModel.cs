

namespace TMGA.Shared.ViewModels
{
    public partial class StationHoldViewModel
    {
        public string WO { get; set; }
        public string ShipmentId { get; set; }
        public int StationId { get; set; }
        public string UserId { get; set; }
        public bool IsOnHold { get; set; }
    }
}