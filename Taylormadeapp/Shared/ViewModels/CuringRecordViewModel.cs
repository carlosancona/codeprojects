﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using TMGA.Domain.Models.Engine;

namespace TMGA.Shared.ViewModels
{
    
    public class CuringRecordViewModel
    {
        public int Id { get; set; }
        public string WorkOrder { get; set; }
        public string ClubDescription { get; set; }
        public int StationId { get; set; }
        public double Temperature { get; set; }
        public DateTime Timestamp { get; set; }
        public string Status { get; set; }
        public double CurrentCuringTime { get; set; }
        public double OriginalCuringTime { get; set; }
        public int Quantity { get; set; }
        public string IsProcessed { get; set; }

    }
}
