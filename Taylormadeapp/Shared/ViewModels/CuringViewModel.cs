using System; 

namespace TMGA.Shared.ViewModels
{
    public class CuringViewModel
    {
        public int StationId { get; set; }

        public string StationName { get; set; }

        public int CellId { get; set; }

        public string CellName { get; set; }

        public DateTime On { get; set; }

        public WorkOrderViewModel WorkOrder { get; set; }
    }
}