using TMGA.Domain.Models.Engine;

namespace TMGA.Shared.ViewModels
{
    public partial class ScannedFromShopFloorViewModel
    {
        public string ScannedWO { get; set; }
        public Station ThisStation { get; set; }
        public string Message { get; set; }
        public string UserId { get; set; }
        public string ShipmentId { get; set; }
    }
}