using System;

namespace TMGA.Shared.ViewModels
{
    public class UserTotalsViewModel
    {
        public string UserId { get; set; }

        public int TotalOrders { get; set; }
        public int TotalOrdersToday { get; set; }

        public DateTime FirstOrder { get; set; }

        public DateTime LastOrder { get; set; }
    }
}