 
namespace TMGA.Shared.ViewModels
{
    public partial class ToShopFloorViewModel
    {
        public bool IsAlreadyCompleted { get; set; }
        public bool IsWrongCell { get; set; }
        public bool IsFromRedisCache { get; set; }
        public bool IsNotInQueue { get; set; }
        public bool IsOpenOnAnotherStation { get; set; }
        public StationViewModel CurrentStation { get; set; }
        public string UserId { get; set; }
        public WorkOrderViewModel WorkOrder { get; set; }
    }
}