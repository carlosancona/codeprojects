 
using System;
using System.Collections.Generic;

namespace TMGA.Shared.ViewModels
{
    public partial class LatestWavesViewModel
    {
        public string WaveId { get; set; } 
        public List<WorkOrderViewModel> AllWorkOrders { get; set; }
        public List<WorkOrderViewModel> CompletedOrders { get; set; } 
        public List<WorkOrderViewModel> IncompleteOrders { get; set; }
        public string Warehouse { get; set; }

        public int AllWorkOrdersCount { get; set; }
        public int CompletedOrdersCount { get; set; }
        public int IncompleteOrdersCount { get; set; }

        public int AllClubsCount { get; set; }
        public int CompleteClubsCount { get; set; }
        public int IncompleteClubsCount { get; set; }

        public bool IsReleased { get; set; } 

        public bool IsReleasing { get; set; } 
        public DateTime ReleasedOn { get; set; } 
        public bool IsInFocus { get; set; } 
        public bool IsPrinted { get; set; }
    }
}