 
namespace TMGA.Shared.ViewModels
{
    public partial class DefectViewModel
    {
        public string WaveId { get; set; }
        public string WorkOrderId { get; set; }
        public string ShipmentId { get; set; }
        public string DefectiveSKU { get; set; }
        public int DefectReasonId { get; set; }
        public int StationId { get; set; }
        public string UserId { get; set; }
        public int SiteId { get; set; } 
        public int Quantity { get; set; }
        public string WriteOffType { get; set; }
        public double BullseyeX { get; set; }
        public double BullseyeY { get; set; }
    }
}