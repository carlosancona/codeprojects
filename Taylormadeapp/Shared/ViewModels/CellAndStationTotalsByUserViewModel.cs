using System;
using System.Collections.Generic;
using System.Linq;

namespace TMGA.Shared.ViewModels
{
    public class CellAndStationTotalsByUserViewModel
    {
        public int StationId { get; set; }

        public string StationName { get; set; }

        public int CellId { get; set; }

        public string CellName { get; set; }

        public List<UserTotalsViewModel> UserTotals { get; set; }

        public int TotalOrders => UserTotals.Sum(_ => _.TotalOrders);
        public int TotalOrdersToday => UserTotals.Sum(_ => _.TotalOrdersToday);

        public DateTime FirstOrder => UserTotals.Min(_ => _.FirstOrder);

        public DateTime LastOrder => UserTotals.Max(_ => _.LastOrder);

    }
}