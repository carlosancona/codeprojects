 
namespace TMGA.Shared.ViewModels
{
    public partial class PickingViewModel
    {
        public string Desc { get; set; }
        public double? Quantity { get; set; }
        public string Sku { get; set; } 
    }
}