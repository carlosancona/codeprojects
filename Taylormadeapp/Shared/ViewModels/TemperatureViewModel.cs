﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TMGA.Domain.Models.Engine;

namespace Shared.ViewModels
{
    public class TemperatureViewModel
    {   
        [Key]
        public int Id { get; set; }
        public float Temperature { get; set; }
        public int StationId { get; set; }

        public DateTime Timestamp { get; set; }

        [ForeignKey("StationId")]
        public Station Station { get; set; }

    }
}
