
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TMGA.Shared.ViewModels
{
    public partial class WavePlanViewModel
    {
        public WavePlanStatus WavePlanStatus { get; set; }
        public DateTime ReleasedOn { get; set; }
        public List<CellQueueViewModel> CellQueues { get; set; }
    }

    public enum WavePlanStatus
    {
        [Display(Name = "Released just now")]
        New,

        [Display(Name = "In progress")]
        InProgress,

        [Display(Name = "Completed, showing original snapshot")]
        Complete
    }
}

