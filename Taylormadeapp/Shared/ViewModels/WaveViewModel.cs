using System.Collections.Generic;

namespace TMGA.Shared.ViewModels
{
    public partial class WaveViewModel
    {
        public string WaveId { get; set; }
        public List<WorkOrderViewModel> WorkOrders { get; set; }
    }
}