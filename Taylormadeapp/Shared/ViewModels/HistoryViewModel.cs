 
using System.Collections.Generic;
 
namespace TMGA.Shared.ViewModels
{
    public partial class HistoryViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public bool IsAlreadyCompleted { get; set; }
        public bool IsWrongCell { get; set; }
        public bool IsFromRedisCache { get; set; }
        public bool IsNotInQueue { get; set; }
        public bool IsOpenOnAnotherStation { get; set; }
        public StationHistoryViewModel CurrentStation { get; set; }
        public int UserId { get; set; }
        public WorkOrderViewModel WorkOrder { get; set; }
        public string WO { get; set; }
        public List<CellQueueViewModel> Queue { get; set; }
        public List<StationHistoryViewModel> StationHistory { get; set; }
    }
}