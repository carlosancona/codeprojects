
using TMGA.Domain.Models.Engine;

namespace TMGA.Shared.ViewModels
{
    public partial class DefectReasonViewModel : StationDefectReason
     {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Notes { get; set; }
    }
}