
using System.Collections.Generic;

namespace TMGA.Shared.ViewModels
{
    public partial class CellInformationViewModel 
    { 
        public int CellId { get; set; }
        public string CellName { get; set; }
        public string CellType { get; set; }
        public string GroupingMethod { get; set; }
        public double FixedBendingRatio { get; set; } 

        public bool IsActive { get; set; }
        public bool LastHadBending { get; set; }

        public int TotalOrders { get; set; }
        public int TotalOrdersThatHaveHadBending { get; set; }
         
        public int ClubsInCell { get; set; }
        public int ClubCutoffForLargeOrders { get; set; }
        public int ClubsInLastOrder { get; set; }
        public int ClubsQueued { get; set; } 

        public string CurrentWorkOrderNumber { get; set; } 
        public double CurrentBendingRatio => TotalOrders <= 0 ? 0 : TotalOrdersThatHaveHadBending / TotalOrders; 

        public SiteViewModel Site { get; set; }
        public List<StationViewModel> Stations { get; set; } 
    }
}