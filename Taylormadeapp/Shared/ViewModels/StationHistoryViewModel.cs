
using System;

namespace TMGA.Shared.ViewModels
{
    public partial class c
    {
        public int StationId { get; set; }
        public string StationName { get; set; }

        public int ClubsInQueue { get; set; }
        public int OrdersInQueue { get; set; }

        public string CurrentUser { get; set; }
        public DateTime LastActive { get; set; }

        public int OrdersCompletedToday { get; set; }
        public int OrdersCompletedAllTime { get; set; }

        public int UserOrdersCompletedToday { get; set; }
        public int UserOrdersCompletedAllTime { get; set; }

        public TimeSpan AverageTimePerClub { get; set; }
    }
}