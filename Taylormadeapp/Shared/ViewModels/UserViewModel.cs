namespace TMGA.Shared.ViewModels
{
    public partial class UserViewModel
    {
        public string Id { get; set; } 
        public string Name { get; set; }
        public string Role { get; set; } 
    }
}