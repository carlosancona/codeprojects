

namespace TMGA.Shared.ViewModels
{
    public partial class StationDoneViewModel
    {
        public string WO { get; set; }
        public string ShipmentId { get; set; }
        public StationViewModel Station { get; set; }
        public string UserId { get; set; }
    }
}