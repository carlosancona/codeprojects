using System;
using System.ComponentModel.DataAnnotations.Schema;
using TMGA.Domain.Models.Engine;

namespace TMGA.Shared.ViewModels
{
    public class TemperatureCuringViewModel
    {

        public int Id { get; set; }
        public double Temperature { get; set; }
        public int StationId { get; set; }

        public DateTime Timestamp { get; set; }

        [ForeignKey("StationId")]
        public Station Station { get; set; }

    }
}