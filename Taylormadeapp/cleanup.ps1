Remove-Item bin -Force -Recurse
Remove-Item obj -Force -Recurse
Remove-Item .vs -Force -Recurse

Remove-Item Restfu\lAPI\bin -Force -Recurse
Remove-Item RestfulAPI\obj -Force -Recurse
Remove-Item RestfulAPI\.vs -Force -Recurse

Remove-Item WebUI\bin -Force -Recurse
Remove-Item WebUI\obj -Force -Recurse
Remove-Item WebUI\.vs -Force -Recurse


Remove-Item Domain\bin -Force -Recurse
Remove-Item Domain\obj -Force -Recurse
Remove-Item Domain\.vs -Force -Recurse

Remove-Item Infrastructure\bin -Force -Recurse
Remove-Item Infrastructure\obj -Force -Recurse
Remove-Item Infrastructure\.vs -Force -Recurse

Remove-Item Shared\bin -Force -Recurse
Remove-Item Shared\obj -Force -Recurse
Remove-Item Shared\.vs -Force -Recurse

Remove-Item Assets\bin -Force -Recurse
Remove-Item Assets\obj -Force -Recurse
Remove-Item Assets\.vs -Force -Recurse