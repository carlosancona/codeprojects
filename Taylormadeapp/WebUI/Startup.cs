using Blazored.LocalStorage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using WebUI.Models;
using Toolbelt.Blazor.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;
using Toolbelt.Blazor.I18nText;
using System.Threading.Tasks;
using Microsoft.Win32;
using MediatR;

namespace WebUI
{
    public class Startup
    {
        public IWebHostEnvironment _env;
        public IConfiguration Configuration { get; }
        private readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration, Microsoft.AspNetCore.Hosting.IWebHostEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        { 
            TimeZoneInfo.ClearCachedData();
            services.AddRazorPages();
            SystemEvents.TimeChanged += (s, e) => TimeZoneInfo.ClearCachedData();

            services.AddServerSideBlazor()
                .AddCircuitOptions(_ =>
                {
                    _.DisconnectedCircuitMaxRetained = 512;
                    _.DetailedErrors = true;
                    _.DisconnectedCircuitRetentionPeriod = TimeSpan.FromHours(1);
                });

            //localization
            var defaultCulture = Configuration["CULTURE_OVERRIDE"] ?? "en";

            Console.WriteLine($"Culture {defaultCulture}");  

            var supportedCultures = new List<CultureInfo> { new CultureInfo(defaultCulture) };

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture(defaultCulture);
                options.SetDefaultCulture(defaultCulture);
                options.SupportedUICultures = supportedCultures;
                options.SupportedCultures = supportedCultures;
                options.RequestCultureProviders = new List<IRequestCultureProvider>();
            });
             

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder
                           .AllowAnyHeader()
                           .AllowAnyMethod()
                           .AllowAnyOrigin();
                });
            });

            //services.AddBlazorise(options =>
            //{
            //    options.ChangeTextOnKeyPress = true; // optional
            //})
            //.AddBootstrapProviders()
            //.AddFontAwesomeIcons();

            services.AddHttpClient("wmsapi", c =>
            {
                c.BaseAddress = new Uri("http://restfulapi");
                // Github API versioning
                //c.DefaultRequestHeaders.Add("Accept", "application/vnd.github.v3+json");
                // Github requires a user-agent
                //c.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory-Sample");
            });

            services.AddBlazoredLocalStorage();

            services.AddScoped<ShopFloorState>();
            services.AddScoped<ShaftCutTemplateVerificationComponentState>();

            services.AddMediatR(typeof(Startup).Assembly);

            services.AddI18nText<Startup>(options =>
            {
                options.GetInitialLanguageAsync = (svc, opt) => new ValueTask<string>(Configuration["CULTURE_OVERRIDE"] ?? "en");
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //disabled in order to ignore browser settings 
            // var localizationOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            //app.UseRequestLocalization();

            var provider = new FileExtensionContentTypeProvider();
            // Add new MIME type mappings
            provider.Mappings[".res"] = "application/octet-stream";
            provider.Mappings[".pexe"] = "application/x-pnacl";
            provider.Mappings[".nmf"] = "application/octet-stream";
            provider.Mappings[".mem"] = "application/octet-stream";
            provider.Mappings[".wasm"] = "application/wasm";

            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")),
                ContentTypeProvider = provider
            });

            app.UseRouting();

            app.UseCors(MyAllowSpecificOrigins);

            //app.ApplicationServices
            //   .UseBootstrapProviders()
            //   .UseFontAwesomeIcons();

            //Custom Middleware injection
            //app.UseMiddleware<Timing>();

            //app.UseHttpsRedirection();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });

            
        }
    }
}