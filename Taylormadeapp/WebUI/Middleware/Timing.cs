﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace WebUI.Middleware
{
    public class Timing
    {
        private RequestDelegate _next { get; set; }

        public Timing(RequestDelegate next) => _next = next;

        public async Task InvokeAsync(HttpContext context)
        {
            DateTime startTime = DateTime.Now;
            await _next(context);
            TimeSpan elapsed = DateTime.Now - startTime;
           // _logger.LogInformation($"Elapsed: {elapsed.TotalMilliseconds.ToString("n1")}ms ");
        }
    }
}