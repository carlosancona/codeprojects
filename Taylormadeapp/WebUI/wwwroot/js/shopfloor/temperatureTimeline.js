﻿window.plotlyFunctions = {
    generateTemperaturePlot: function (chartid, tempfeed) {

        var trace1 = {
            x: Array.from(tempfeed, time => time.timestamp),
            y: Array.from(tempfeed, time => time.temperature),
            mode: 'lines',
            line: {
                color: 'rgb(255, 140, 0)',
                shape: 'spline',
                width: 3
            }
        }

        var data = [trace1];

        var layout = {
            autosize: false,
            width: 610,
            height: 300,
            plot_bgcolor: "transparent",
            paper_bgcolor: "transparent",
            xaxis: {
                title: {
                    text: 'Time',
                    font: {
                        family: 'Courier New, monospace',
                        color: 'rgb(255, 140, 0)'
                    }

                },
                showgrid: false,
                showline: true,
                showticklabels: false
            },
            yaxis: {
                title: {
                    text: 'Temperature',
                    font: {
                        family: 'Courier New, monospace',
                        color: 'rgb(255, 140, 0)'
                    }
                },
                showgrid: false,
                showline: true,
                showticklabels: false
            },
            //shapes: [
            //    {
            //        type: 'rect',
            //        x0: 3,
            //        y0: 1,
            //        x1: 6,
            //        y1: 2,
            //        line: { 
            //            color: 'rgba(255, 253, 105, .2)',
            //            width: 0
            //        },
            //        fillcolor: 'rgba(255, 253, 105, .2)'
            //    }],
        };

        Plotly.newPlot("tempchart", data, layout, { displayModeBar: false });
    },

    show: function (WorkOrder) {
        alert('Received this WO: ' + WorkOrder);
    },

    showNotReady: function () {
        //alert('Check');
        $("#notReady").modal("show");
        setTimeout(function () { $('#notReady').modal('hide'); }, 5000);
    },

    showReady: function () {
        $("#ready").modal("show");
        setTimeout(function () { $('#ready').modal('hide'); }, 5000);
    },

    extendtraces: function (chart, tempfeed, timer) {
        var Temparray = Array.from(tempfeed, temp => temp.temperature);
        alert(JSON.stringify(Temparray));
        var Timearray = Array.from(tempfeed, time => time.timestamp);
        alert(JSON.stringify(Timearray));
        Plotly.extendTraces(
            "tempchart",
            {
                x: [[22.70, 50.40, 22.75]],
                y: [Temparray]
            },
            [0],
            10
        );

    }
};
