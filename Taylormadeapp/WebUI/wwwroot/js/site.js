﻿//call from blazor using JSInterop

window.RefocusWOInput = () => {
    //if mouse is down, don't refocus
    var down = false;
    $(document).mousedown(function () {
        down = true;
    }).mouseup(function () {
        down = false;
    });

    if (!down) {
        //console.log("Forcing focus to input");
        $("#woInput").focus();
    } else {
        console.log("Mousedown, no force focus");
    }
};

window.RefocusScannedSerialInput = () => {
    //if mouse is down, don't refocus
    var down = false;
    $(document).mousedown(function () {
        down = true;
    }).mouseup(function () {
        down = false;
    });

    if (!down) {
        //console.log("Forcing focus to input");
        $("#serialInput").focus();
    } else {
        console.log("Mousedown, no force focus");
    }
};


window.RefocusTipTemplateInput = () => {
    //if mouse is down, don't refocus
    var down = false;
    $(document).mousedown(function () {
        down = true;
    }).mouseup(function () {
        down = false;
    });

    if (!down) {
        //console.log("Forcing focus to input");
        $("#shaftCutTemplateTipInput").focus();
    } else {
        console.log("Mousedown, no force focus");
    }
};

window.RefocusButtTemplateInput = () => {
    //if mouse is down, don't refocus
    var down = false;
    $(document).mousedown(function () {
        down = true;
    }).mouseup(function () {
        down = false;
    });

    if (!down) {
        //console.log("Forcing focus to input");
        $("#shaftCutTemplateButtInput").focus();
    } else {
        console.log("Mousedown, no force focus");
    }
};

window.RefocusScannedStationInput = () => {
    //if mouse is down, don't refocus
    var down = false;
    $(document).mousedown(function () {
        down = true;
    }).mouseup(function () {
        down = false;
    });

    if (!down) {
        //console.log("Forcing focus to input");
        $("#scannedStationInput").focus();
    } else {
        console.log("Mousedown, no force focus");
    }
};


//autorefresh on broken blazor circuit
//press retry button if screen once per second (if down)
///**
// * This function allow you to modify a JS Promise by adding some status properties.
// * Based on: http://stackoverflow.com/questions/21485545/is-there-a-way-to-tell-if-an-es6-promise-is-fulfilled-rejected-resolved
// * But modified according to the specs of promises : https://promisesaplus.com/
// */

function MakeQueryablePromise(promise) {
    // Don't modify any promise that has been already modified.
    if (promise.isResolved) return promise;

    // Set initial state
    var isPending = true;
    var isRejected = false;
    var isFulfilled = false;

    // Observe the promise, saving the fulfillment in a closure scope.
    var result = promise.then(
        function (v) {
            isFulfilled = true;
            isPending = false;
            return v;
        },
        function (e) {
            isRejected = true;
            isPending = false;
            throw e;
        }
    );

    result.isFulfilled = function () { return isFulfilled; };
    result.isPending = function () { return isPending; };
    result.isRejected = function () { return isRejected; };
    return result;
}

async function wrapperFunc() {
    try {
        let r1 = await window.Blazor.reconnect();
        return someValue;     // this will be the resolved value of the returned promise
    } catch (e) {
        console.log(e);
        throw e;      // let caller know the promise was rejected with this reason
    }
}

function successCallback(result) {
    console.log("Reloading... ");
    window.location.reload();
}

function failureCallback(error) {
    console.error("Not available for reload...");
}

setInterval(function () {
    var modal = document.getElementById("components-reconnect-modal");
    if (modal !== null) {
        console.log('Detected circuit break. Attempting to reconnect.');
        window.Blazor.reconnect().then(successCallback, failureCallback);
    }
}, 3000);



window.getCulture = function () {
    return (navigator.languages && navigator.languages.length) ? navigator.languages[0] :
        navigator.userLanguage || navigator.language || navigator.browserLanguage || 'en';
};

window.getBullseyeData = function () {
    var x = document.getElementById('xPosition').textContent;
    var y = document.getElementById('yPosition').textContent;
    return { 'x': x, 'y': y };
};



//jquery stuff
 