#pragma checksum "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Components\HelpModal.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c798a219c14ca722fb38914800e95c92386ca24a"
// <auto-generated/>
#pragma warning disable 1591
namespace WebUI.Components
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.Extensions.Localization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI.Components;

#line default
#line hidden
#nullable disable
    public partial class HelpModal : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<style>
    #helpModalContainer {
        height: 95%;
        width: 95%;
        top: 2%;
        left: 2%;
        position: fixed;
        /*background-image: linear-gradient(to right, #f83600 0%, #f9d423 100%);*/
        background: radial-gradient(circle, rgba(0,0,50,.4) 0%, rgba(0,0,10,.4) 100%), url('../img/aluminum-darkblue.jpg');
        /*background-image: linear-gradient(to right, #f83600 0%, #f9d423 100%);*/
        /*background-color: rgba(255,215,0, .7);*/
        z-index: 5000; 
        align-content: center;
        align-items: center;
        border: 4px solid #757390f5;
    }
</style>

");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "id", "helpModalContainer");
            __builder.AddAttribute(3, "class", "roundCorners" + " " + (
#nullable restore
#line 19 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Components\HelpModal.razor"
                                                   isClosing ? "scale-out-center" : "scale-in-center"

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(4, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 19 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Components\HelpModal.razor"
                                                                                                                   CloseWindow

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(5, "\r\n    <embed src=\"/pdf/helpexample.pdf\" width=\"95%\" height=\"90%\">\r\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
