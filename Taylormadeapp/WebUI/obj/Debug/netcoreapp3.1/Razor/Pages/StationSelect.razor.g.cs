#pragma checksum "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "af172e746799de41ae3374e283b5ac860153f1b9"
// <auto-generated/>
#pragma warning disable 1591
namespace WebUI.Pages
{
    #line hidden
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 2 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.Extensions.Localization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
using System.Threading.Tasks;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
using System.Threading;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
using System;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
using TMGA.Shared.ViewModels;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(Shared.Clean))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/StationSelect")]
    public partial class StationSelect : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, "<style>\r\n    .roundCorners {\r\n        /*overrides*/\r\n        padding: 10px; \r\n        width: 125px;\r\n        height: 125px;\r\n        max-width: 125px;\r\n        max-height: 125px;\r\n        min-width: 125px;\r\n        min-height: 125px;\r\n    }\r\n</style>\r\n");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "class", "row");
            __builder.AddMarkupContent(3, "\r\n    ");
            __builder.OpenElement(4, "div");
            __builder.AddAttribute(5, "class", "col text-center");
            __builder.AddMarkupContent(6, "\r\n        ");
            __builder.OpenElement(7, "h1");
            __builder.AddAttribute(8, "style", "font-size:60px;");
            __builder.AddContent(9, 
#nullable restore
#line 29 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                     L.StationSelect

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(10, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(11, "\r\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(12, "\r\n\r\n");
            __builder.OpenElement(13, "div");
            __builder.AddAttribute(14, "class", "row");
            __builder.AddMarkupContent(15, "\r\n    ");
            __builder.OpenElement(16, "div");
            __builder.AddAttribute(17, "class", "col text-center centeredOnScreen");
            __builder.AddAttribute(18, "style", "margin-top:-50px;");
            __builder.AddMarkupContent(19, "\r\n        ");
            __builder.OpenElement(20, "div");
            __builder.AddAttribute(21, "class", "row");
            __builder.AddAttribute(22, "style", "margin-bottom:20px;");
            __builder.AddMarkupContent(23, "\r\n            ");
            __builder.OpenElement(24, "div");
            __builder.AddAttribute(25, "class", "col text-center");
            __builder.AddAttribute(26, "style", "margin-top:-150px;");
            __builder.AddMarkupContent(27, "\r\n                ");
            __builder.OpenElement(28, "input");
            __builder.AddAttribute(29, "type", "password");
            __builder.AddAttribute(30, "class", "text-center");
            __builder.AddAttribute(31, "id", "scannedStationInput");
            __builder.AddAttribute(32, "style", "font-size:55px;height:100px;width:500px;margin-top:220px;color:white;border:0px solid white;background-color:transparent;padding:5px;");
            __builder.AddAttribute(33, "placeholder", 
#nullable restore
#line 39 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                    L.ScanStationCode

#line default
#line hidden
#nullable disable
            );
            __builder.AddAttribute(34, "autofocus", true);
            __builder.AddAttribute(35, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 37 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                                                                                  scannedStationId

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(36, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => scannedStationId = __value, scannedStationId));
            __builder.SetUpdatesAttributeName("value");
            __builder.CloseElement();
            __builder.AddMarkupContent(37, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(38, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(39, "\r\n\r\n        ");
            __builder.OpenElement(40, "div");
            __builder.AddAttribute(41, "class", "row");
            __builder.AddAttribute(42, "style", "margin-bottom:20px;");
            __builder.AddMarkupContent(43, "\r\n            ");
            __builder.OpenElement(44, "div");
            __builder.AddAttribute(45, "class", "col text-center");
            __builder.AddMarkupContent(46, "\r\n");
#nullable restore
#line 45 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                 if (msg == "Line is inactive")
                {

#line default
#line hidden
#nullable disable
            __builder.AddContent(47, "                    ");
            __builder.OpenElement(48, "h3");
            __builder.AddAttribute(49, "class", "text-danger");
            __builder.AddContent(50, 
#nullable restore
#line 47 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                             msg

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(51, "\r\n");
#nullable restore
#line 48 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                }
                else
                {
                    

#line default
#line hidden
#nullable disable
            __builder.AddContent(52, 
#nullable restore
#line 51 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                     msg

#line default
#line hidden
#nullable disable
            );
#nullable restore
#line 51 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                        
                }

#line default
#line hidden
#nullable disable
            __builder.AddContent(53, "            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(54, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(55, "\r\n\r\n");
#nullable restore
#line 56 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
         if (Cells != null)
        {

#line default
#line hidden
#nullable disable
            __builder.AddContent(56, "            ");
            __builder.OpenElement(57, "select");
            __builder.AddAttribute(58, "class", "form-control");
            __builder.AddAttribute(59, "style", "width:300px;font-size:30px;margin:0px auto");
            __builder.AddAttribute(60, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 58 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                                                                                        CellChanged

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(61, "\r\n");
#nullable restore
#line 59 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                 foreach (var cell in Cells) // _companies is List<Company> Company object contain Id and name
                {

#line default
#line hidden
#nullable disable
            __builder.AddContent(62, "                    ");
            __builder.OpenElement(63, "option");
            __builder.AddAttribute(64, "value", 
#nullable restore
#line 61 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                    cell.Id

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(65, 
#nullable restore
#line 61 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                              L[cell.Desc.Replace(" ", "").Trim()]

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(66, "\r\n");
#nullable restore
#line 62 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                }

#line default
#line hidden
#nullable disable
            __builder.AddContent(67, "            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(68, "\r\n");
#nullable restore
#line 65 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
             if (CurrentCell != null && CurrentCell.Stations.Any())
            {

#line default
#line hidden
#nullable disable
            __builder.AddContent(69, "                ");
            __builder.OpenElement(70, "select");
            __builder.AddAttribute(71, "class", "form-control");
            __builder.AddAttribute(72, "style", "width:300px;font-size:30px;margin:5px auto");
            __builder.AddAttribute(73, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.ChangeEventArgs>(this, 
#nullable restore
#line 67 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                                                                                            StationChanged

#line default
#line hidden
#nullable disable
            ));
            __builder.AddMarkupContent(74, "\r\n\r\n");
#nullable restore
#line 69 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                     foreach (var station in CurrentCell?.Stations.OrderBy(_ => _.StationOrder)) // _companies is List<Company> Company object contain Id and name
                    {

#line default
#line hidden
#nullable disable
            __builder.AddContent(75, "                        ");
            __builder.OpenElement(76, "option");
            __builder.AddAttribute(77, "value", 
#nullable restore
#line 71 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                        station?.Id

#line default
#line hidden
#nullable disable
            );
            __builder.AddContent(78, 
#nullable restore
#line 71 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                                      L[station?.StationTypeTemp.Replace("/", "").Replace(" ", "")]

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(79, "\r\n");
#nullable restore
#line 72 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                    }

#line default
#line hidden
#nullable disable
            __builder.AddContent(80, "                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(81, "\r\n");
            __builder.AddContent(82, "                ");
            __builder.OpenElement(83, "a");
            __builder.AddAttribute(84, "class", "btn btn-primary btn-lg m-2 p-3");
            __builder.AddAttribute(85, "style", "font-size:30px;");
            __builder.AddAttribute(86, "href", "/ShopFloor/" + (
#nullable restore
#line 75 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                                                                                    CurrentStation?.StationTypeTemp.Replace(" ", "").Replace("/", "")

#line default
#line hidden
#nullable disable
            ) + "/" + (
#nullable restore
#line 75 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                                                                                                                                                       CurrentStationId

#line default
#line hidden
#nullable disable
            ));
            __builder.AddContent(87, 
#nullable restore
#line 75 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                                                                                                                                                                          L.Launch

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(88, "\r\n                <br>\r\n                ");
            __builder.OpenElement(89, "a");
            __builder.AddAttribute(90, "class", "btn btn-warning btn-lg m-2 p-3");
            __builder.AddAttribute(91, "style", "font-size:30px;margin-top:100px;");
            __builder.AddAttribute(92, "href", "/");
            __builder.AddContent(93, 
#nullable restore
#line 77 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
                                                                                                             L.BackToLogin

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(94, "\r\n");
#nullable restore
#line 78 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"

            }

#line default
#line hidden
#nullable disable
#nullable restore
#line 79 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
             
        }

#line default
#line hidden
#nullable disable
            __builder.AddContent(95, "    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(96, "\r\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 84 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\StationSelect.razor"
        

    public HttpClient httpClient;
    public UserViewModel CurrentUser;
    public List<CellViewModel> Cells;
    public string msg;
    public CellViewModel CurrentCell;
    public StationViewModel CurrentStation;

    private string _scannedStationId;
    public string scannedStationId
    {
        get => _scannedStationId;
        set
        {
            _scannedStationId = value;
            //intercept change event (for barcode scanning)
            if (_scannedStationId?.Length > 1) GoToStationAfterScan();
        }
    }
    public int CurrentCellId;
    public int CurrentStationId;
    public I18nText.Resources L { get; set; } = new I18nText.Resources();

    protected override async Task OnInitializedAsync()
    {
        httpClient = new HttpClient
        {
            BaseAddress = new System.Uri("http://restfulapi")
        };

        //
        L = await _localizer.GetTextTableAsync<I18nText.Resources>(this);

        msg = L.SelectBelow;
        Cells = await httpClient.GetJsonAsync<List<CellViewModel>>($"api/cell");
        Cells = Cells.Where(_ => _.IsActive && _.Site.Id == Convert.ToInt32(_configuration["SITE_ID"])).ToList();
        CurrentCellId = Cells.First().Id;
        CurrentStationId = Cells.First().Stations.OrderBy(_ => _.StationOrder).First().Id;
        CurrentCell = Cells.Where(_ => _.Id == CurrentCellId).FirstOrDefault();

        CurrentStation = CurrentCell.Stations.Where(_ => _.Id == CurrentStationId)
                   .Select(_ => new StationViewModel
                   {
                       Id = _.Id,
                       Cell = _.Cell,
                       StationTypeTemp = _.StationTypeTemp,
                       StationOrder = _.StationOrder,
                       CurrentOrder = _.CurrentOrder
                   }).FirstOrDefault();
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (firstRender)
        {
            JSRuntime.InvokeAsync<object>("RefocusScannedStationInput");
        }
    }


    public void CellChanged(ChangeEventArgs e)
    {
        CurrentCellId = Convert.ToInt32(e.Value);
        CurrentCell = Cells.Where(_ => _.Id == CurrentCellId).FirstOrDefault();
        CurrentStationId = CurrentCell.Stations?.OrderBy(_ => _.StationOrder).First().Id ?? 0;
    }

    public void StationChanged(ChangeEventArgs e)
    {
        CurrentStationId = Convert.ToInt32(e.Value);
        CurrentStation = CurrentCell.Stations.Where(_ => _.Id == CurrentStationId)
                  .Select(_ => new StationViewModel
                  {
                      Id = _.Id,
                      Cell = _.Cell,
                      StationTypeTemp = _.StationTypeTemp,
                      StationOrder = _.StationOrder,
                      CurrentOrder = _.CurrentOrder
                  }).FirstOrDefault();
    }

    public void GoToStationAfterScan()
    {
        msg = "";
        try
        {
            CurrentStationId = Convert.ToInt32(scannedStationId);
            CurrentCellId = Cells.FirstOrDefault(e => e.Stations.Any(a => a.Id == CurrentStationId)).Id;
            CurrentCell = Cells.Where(_ => _.Id == CurrentCellId).FirstOrDefault();

            CurrentStation = CurrentCell.Stations.Where(_ => _.Id == CurrentStationId)
                .Select(_ => new StationViewModel
                {
                    Id = _.Id,
                    Cell = _.Cell,
                    StationTypeTemp = _.StationTypeTemp,
                    StationOrder = _.StationOrder,
                    CurrentOrder = _.CurrentOrder
                }).FirstOrDefault();

            NavigationManager.NavigateTo($"/ShopFloor/{CurrentStation?.StationTypeTemp.Replace(" ", "").Replace("/", "")}/{CurrentStationId}");
        }
        catch
        {
            msg = "Line is inactive";
        }
        finally
        {
            scannedStationId = "";
            JSRuntime.InvokeAsync<object>("RefocusScannedStationInput");
        }
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Microsoft.Extensions.Configuration.IConfiguration _configuration { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Toolbelt.Blazor.I18nText.I18nText _localizer { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
#pragma warning restore 1591
