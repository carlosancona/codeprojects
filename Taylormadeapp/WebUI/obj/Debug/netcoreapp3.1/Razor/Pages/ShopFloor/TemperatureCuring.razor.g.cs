#pragma checksum "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\ShopFloor\TemperatureCuring.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ef536c90fd9a7e1236f8a9770299c4f322788b6e"
// <auto-generated/>
#pragma warning disable 1591
namespace WebUI.Pages.ShopFloor
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
#nullable restore
#line 1 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.Extensions.Localization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\ShopFloor\TemperatureCuring.razor"
using WebUI.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\ShopFloor\TemperatureCuring.razor"
using Microsoft.AspNetCore.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\ShopFloor\TemperatureCuring.razor"
using TMGA.Shared.ViewModels;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(TemperatureCuringWorkOrder))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/shopfloor/AlignmentCuring/{stationId}")]
    public partial class TemperatureCuring : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __Blazor.WebUI.Pages.ShopFloor.TemperatureCuring.TypeInference.CreateCascadingValue_0(__builder, 0, 1, 
#nullable restore
#line 7 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\ShopFloor\TemperatureCuring.razor"
                       Station

#line default
#line hidden
#nullable disable
            , 2, "Station", 3, (__builder2) => {
                __builder2.AddMarkupContent(4, "\r\n    ");
                __Blazor.WebUI.Pages.ShopFloor.TemperatureCuring.TypeInference.CreateCascadingValue_1(__builder2, 5, 6, 
#nullable restore
#line 8 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\ShopFloor\TemperatureCuring.razor"
                           WorkOrder

#line default
#line hidden
#nullable disable
                , 7, "WorkOrder", 8, (__builder3) => {
                    __builder3.AddMarkupContent(9, "\r\n        ");
                    __Blazor.WebUI.Pages.ShopFloor.TemperatureCuring.TypeInference.CreateCascadingValue_2(__builder3, 10, 11, 
#nullable restore
#line 9 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\ShopFloor\TemperatureCuring.razor"
                               User

#line default
#line hidden
#nullable disable
                    , 12, "User", 13, (__builder4) => {
                        __builder4.AddMarkupContent(14, "\r\n            ");
                        __builder4.OpenComponent<WebUI.Components.TemperaturePlottingComponent>(15);
                        __builder4.CloseComponent();
                        __builder4.AddMarkupContent(16, "\r\n        ");
                    }
                    );
                    __builder3.AddMarkupContent(17, "\r\n    ");
                }
                );
                __builder2.AddMarkupContent(18, "\r\n");
            }
            );
        }
        #pragma warning restore 1998
#nullable restore
#line 15 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\ShopFloor\TemperatureCuring.razor"
       

    [Parameter] public string stationId { get; set; }

    [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
    [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
    [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }


#line default
#line hidden
#nullable disable
    }
}
namespace __Blazor.WebUI.Pages.ShopFloor.TemperatureCuring
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateCascadingValue_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, TValue __arg0, int __seq1, global::System.String __arg1, int __seq2, global::Microsoft.AspNetCore.Components.RenderFragment __arg2)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.CascadingValue<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Value", __arg0);
        __builder.AddAttribute(__seq1, "Name", __arg1);
        __builder.AddAttribute(__seq2, "ChildContent", __arg2);
        __builder.CloseComponent();
        }
        public static void CreateCascadingValue_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, TValue __arg0, int __seq1, global::System.String __arg1, int __seq2, global::Microsoft.AspNetCore.Components.RenderFragment __arg2)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.CascadingValue<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Value", __arg0);
        __builder.AddAttribute(__seq1, "Name", __arg1);
        __builder.AddAttribute(__seq2, "ChildContent", __arg2);
        __builder.CloseComponent();
        }
        public static void CreateCascadingValue_2<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, TValue __arg0, int __seq1, global::System.String __arg1, int __seq2, global::Microsoft.AspNetCore.Components.RenderFragment __arg2)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.CascadingValue<TValue>>(seq);
        __builder.AddAttribute(__seq0, "Value", __arg0);
        __builder.AddAttribute(__seq1, "Name", __arg1);
        __builder.AddAttribute(__seq2, "ChildContent", __arg2);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
