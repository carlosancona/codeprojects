#pragma checksum "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\Welcome.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3b2f5300a937dc6669327d2e44e113f39f53d57c"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace WebUI.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\Welcome.razor"
using Microsoft.Extensions.Localization;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.LayoutAttribute(typeof(MainLayout))]
    [Microsoft.AspNetCore.Components.RouteAttribute("/Welcome")]
    public partial class Welcome : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 11 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Pages\Welcome.razor"
       

    public I18nText.Resources L { get; set; } = new I18nText.Resources(); 

    protected override async Task OnInitializedAsync()
    {
        L = await _localizer.GetTextTableAsync<I18nText.Resources>(this);
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Toolbelt.Blazor.I18nText.I18nText _localizer { get; set; }
    }
}
#pragma warning restore 1591
