#pragma checksum "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Shared\MainLayout.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4d30c71140380999e11e32a7ed19b206bcd5e4e8"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace WebUI.Shared
{
    #line hidden
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using Microsoft.Extensions.Localization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\_Imports.razor"
using WebUI.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Shared\MainLayout.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Shared\MainLayout.razor"
using System.Threading.Tasks;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Shared\MainLayout.razor"
using System.Threading;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Shared\MainLayout.razor"
using System;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Shared\MainLayout.razor"
using TMGA.Shared.ViewModels;

#line default
#line hidden
#nullable disable
    public partial class MainLayout : LayoutComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 55 "C:\Users\Carlos Ancona\Documents\Sonata\app\WebUI\Shared\MainLayout.razor"
        

    public string CurrentTime { get; set; }
    public Timer CurrentTimeTimer { get; set; }
    public UserViewModel CurrentUser { get; set; }
    public string currentCulture { get; set; }

    public I18nText.Resources L { get; set; } = new I18nText.Resources();


    protected override async Task OnInitializedAsync()
    {

        CurrentTimeTimer = new Timer((_) =>
        {
            CurrentTime = DateTime.Now.ToLocalTime().ToString();
            InvokeAsync(StateHasChanged);
        }, null, 0, 1000);

        L = await _localizer.GetTextTableAsync<I18nText.Resources>(this);

    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (firstRender)
        {
            CurrentUser = await localStorage.GetItemAsync<UserViewModel>("currentUser");
            currentCulture = await JSRuntime.InvokeAsync<string>("getCulture");
        }
    }

    public async Task<string> Logout()
    {
        await localStorage.SetItemAsync("currentUser", "");
        //TODO; make api call to logout
        NavigationManager.NavigateTo("/");
        return "";
    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration _configuration { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Toolbelt.Blazor.I18nText.I18nText _localizer { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private Blazored.LocalStorage.ILocalStorageService localStorage { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
    }
}
#pragma warning restore 1591
