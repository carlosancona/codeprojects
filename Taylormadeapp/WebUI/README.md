﻿# Global Assembly DevOps
The Global Assembly application uses `docker-compose` to pull images from the private TaylorMade Azure Container Registry located at `tmgacr.azurecr.io`.



## Server list
 1. **Development** - uscarlxgas01d
 2. **QA** - uscarlxgas01q
 3. **Production (Tijuana)** - mxtijlxgas01p
 


## SSH to servers
In an administrator PowerShell, or from a Linux command line, enter `ssh username@server`. Gain root access by entering `sudo su` and then re-entering your password.



## Pull new images to server

Images have three **tags**, `:dev:`, `:qa`, and `:prod`. Depending on the server you are on, the `docker-compose.yml` file on that server will pull the correct images from the container registry. To update server to the latest application images, run the following commands:

 ```
 docker-compose pull
 docker-compose up -d
 ```



## Viewing streaming logs

Use `docker ps` to get a list of running containers. The leftmost column, Container ID, contains a unique id for that container instance. For instance, the container id may be **9640e6c1a78d** on the RestfulAPI container. To see the logs, issue this command:

`docker logs 964 -f`

*Notice there is no need to type the entire container id.*

## Misc

To break out of log streaming, or cancel any operation, press <kbd>CTRL-C</kbd> 
