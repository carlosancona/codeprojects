using Microsoft.EntityFrameworkCore;

namespace WebUI.Models
{
    public class TempContext : DbContext
    {
        public TempContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<ShaftCutReading> ShaftCutReadings { get; set; }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<ShaftCutReading>().ToTable("ShaftCutReading");
        //}
    }
}