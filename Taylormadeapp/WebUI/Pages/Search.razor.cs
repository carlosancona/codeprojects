﻿

using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Pages
{
    public class SearchBase : ComponentBase
    {

        [Inject] protected IConfiguration _config { get; set; }
        [Inject] protected ILogger<IndexBase> _logger { get; set; }
        [Inject] protected Toolbelt.Blazor.I18nText.I18nText _localizer { get; set; }
        [Inject] protected IJSRuntime JSRuntime { get; set; }

        public I18nText.Resources L { get; set; } = new I18nText.Resources();


        protected WorkOrderViewModel currentWorkOrder;
        public bool isLoading = false;
        public HttpClient HttpClient;
        public List<HistoryViewModel> historyViewModel;

        public Timer ForceFocusTimer;
        private string _workOrderId;

        [Parameter] public int stationId { get; set; }

        public string workOrderId
        {
            get => _workOrderId;
            set
            {
                _workOrderId = value;
                //intercept change event (for barcode scanning)
                if (_workOrderId?.Length > 1) GetWorkOrder();
            }
        }

        protected override async Task OnInitializedAsync()
        {

            HttpClient = new HttpClient
            {
                BaseAddress = new System.Uri("http://restfulapi")
            };

            ForceFocusTimer = new Timer((_) =>
            {
                JSRuntime.InvokeAsync<object>("RefocusWOInput");
            }, null, 0, 1000);

            L = await _localizer.GetTextTableAsync<I18nText.Resources>(this);


        }


        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {

            }
        }

        protected async void GetWorkOrder()
        {
            currentWorkOrder = null;
            isLoading = true;

            if (workOrderId.StartsWith("WO"))
            {
                //query by WO number
                _logger.LogInformation($"Searching for Work Order - {workOrderId}");
                historyViewModel = await HttpClient.PostJsonAsync<List<HistoryViewModel>>($"api/history/wo", workOrderId);
            }

            else
            {
                //query by shipment
                _logger.LogInformation($"Searching for Shipment - {workOrderId}");
                historyViewModel = await HttpClient.PostJsonAsync<List<HistoryViewModel>>($"api/history/shipment", workOrderId);

            }

            workOrderId = null;
            isLoading = false;
            StateHasChanged();
        }

        public async Task Refresh(string theWorkOrderId)
        {
            workOrderId = theWorkOrderId;
            GetWorkOrder();
        }


    }
}