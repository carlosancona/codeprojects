﻿using Microsoft.AspNetCore.Components;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebUI.Pages
{
    public class SiteConfigurationBase : ComponentBase
    {
        protected HttpClient httpClient;

        [Parameter] public int siteId { get; set; }

        protected override async Task OnInitializedAsync() => httpClient = new HttpClient() { BaseAddress = new Uri("http://restfulapi") };

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {

        }

    }
}