﻿
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Pages
{
    public class StatusBase : ComponentBase
    {

        [Inject] protected ILogger<IndexBase> _logger { get; set; }
        [Inject] protected IConfiguration _configuration { get; set; }

        public HttpClient httpClient;
        public ServerStatusViewModel serverStatusViewModel;

        protected override async Task OnInitializedAsync()
        {
            httpClient = new HttpClient
            {
                BaseAddress = new System.Uri("http://restfulapi")
            };

            serverStatusViewModel = await httpClient.GetJsonAsync<ServerStatusViewModel>($"api/site/status");
        }



    }
}