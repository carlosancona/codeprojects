﻿using Microsoft.AspNetCore.Components;
using TMGA.Shared.ViewModels;

namespace WebUI.Pages.ShopFloor
{
    public partial class AcrylicGluing
    {
        [Parameter] public string stationId { get; set; }

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }
      
       
    }
}
