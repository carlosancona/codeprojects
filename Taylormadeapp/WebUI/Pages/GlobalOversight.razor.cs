using Microsoft.Extensions.Logging; 
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks; 
using Microsoft.Extensions.Configuration;
using TMGA.Shared.ViewModels;
using TMGA.Domain.Models.Engine;

namespace WebUI.Pages
{

    public partial class GlobalOversightBase : ComponentBase
    {
        [Inject] protected ILogger<GlobalOversightBase> _logger { get; set; }
        [Inject] protected Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] protected IConfiguration _configuration { get; set; } 

        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();
         
        public HttpClient httpClient;
        public string NewSiteName;
        public List<SiteViewModel> Sites;
        public List<GroupingMethodDdl> CellTypes = new List<GroupingMethodDdl>();

        public int SelectedSiteId { get; set; }
        public SiteViewModel SelectedSite { get; set; } = new SiteViewModel();

        public int SelectedCellId { get; set; }
        public CellViewModel SelectedCell { get; set; } = new CellViewModel();

        public List<GroupingMethodDdl> GroupingMethods = new List<GroupingMethodDdl>();
        public string CellType { get; set; }
        public string GroupingMethod { get; set; }
        public decimal BendingRatio { get; set; } = .50M;

        public string CurrentTime { get; set; }

        public bool IsStarted { get; set; } = false;
        public bool IsRefreshing { get; set; } = false;
        public bool AutoRefresh { get; set; } = false;
        public int AutoRefreshInterval { get; set; } = 5000;

        //public WorkOrderHeader CurrentOrderCell1 { get; set; }
        public int CurrentStationIdCell1 { get; set; }

        public Timer AutoRefreshTimer { get; set; }
        public Timer CurrentTimeTimer { get; set; }

        public bool TijuanaActive { get; set; } = false;
        //public  TelerikNumericTextBox<int> bendingRatioRef;
        //Telerik.Blazor.Components.AnimationContainer.TelerikAnimationContainer myPopupRef;

        public bool GettingNextOrder { get; set; } = false;

        public void ToggleIsStarted() => IsStarted = !IsStarted;

        public void ToggleAutoRefresh() => AutoRefresh = !AutoRefresh;

        public void ToggleIsRefreshing() => IsRefreshing = !IsRefreshing;

        public async void ToggleTijuanaActive()
        {
            TijuanaActive = !TijuanaActive;
            if (TijuanaActive) await GetNextWorkOrder();
        }

        protected override async Task OnInitializedAsync()
        {
            httpClient = new HttpClient
            {
                BaseAddress = new System.Uri("http://restfulapi")
            };
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);

            //dropdowns
            GroupingMethods.Add(new GroupingMethodDdl { MyTextField = _localizer.ByOrder, MyValueField = _localizer.ByOrder });
            GroupingMethods.Add(new GroupingMethodDdl { MyTextField = _localizer.ByShipTo, MyValueField = _localizer.ByShipTo });
            CellTypes.Add(new GroupingMethodDdl { MyTextField = _localizer.Stock, MyValueField = _localizer.Stock });
            CellTypes.Add(new GroupingMethodDdl { MyTextField = _localizer.Custom, MyValueField = _localizer.Custom });
           // CellTypes.Add(new GroupingMethodDdl { MyTextField = L.Mixed, MyValueField = _localizer["Mixed"] });
            
            CellType = _localizer.Stock;
            GroupingMethod = _localizer.ByOrder;

            //populate/bind data
            await GetSites();
            //LoadSiteConfigAndStatus(1);
            //LoadCellConfigAndStatus(1);

            CurrentTimeTimer = new Timer((_) =>
            {
                CurrentTime = DateTime.UtcNow.ToString();
                InvokeAsync(StateHasChanged);
            }, null, 0, 1000);

            //AutoRefreshTimer = new Timer((_) =>
            //{
            //    if (AutoRefresh)
            //    {
            //        IsRefreshing = true;
            //        Task.Run(async () => await GetSites());
            //        InvokeAsync(StateHasChanged);
            //        IsRefreshing = false;
            //    }
            //}, null, 0, AutoRefreshInterval);

            SelectedSiteId = Convert.ToInt32(_configuration["SITE_ID"]);
            LoadSiteConfigAndStatus();
        }

        public class GroupingMethodDdl
        {
            public string MyValueField { get; set; }
            public string MyTextField { get; set; }
        }

        public async Task ToggleCell(Cell cell)
        {
            cell.IsActive = !cell.IsActive; 
            await httpClient.PostJsonAsync<CellViewModel>($"api/cell/toggle", cell); 
        }

        public async Task ToggleCustom(Cell cell)
        {
            cell.CellType = cell.CellType == "Stock" ? "Custom" : "Stock";
            await httpClient.PostJsonAsync<CellViewModel>($"api/cell/togglecustom", cell);
        }

        public async Task AddNewSite()
        {
            try
            {
                await httpClient.PostJsonAsync($"api/site", NewSiteName);
                Sites = await httpClient.GetJsonAsync<List<SiteViewModel>>($"api/site");
                StateHasChanged();
            }
            catch (Exception e)
            {
                _logger.LogInformation(e.InnerException.ToString());
            }
        }

        public void LoadSiteConfigAndStatus()
        {
            var siteId = Convert.ToInt32(_configuration["SITE_ID"]);

            SelectedSiteId = siteId;
            SelectedSite = Sites.Where(_ => _.Id == siteId).FirstOrDefault();
        }

        public async Task GetNextWorkOrder()
        {
            GettingNextOrder = true;
            var siteId = Convert.ToInt32(_configuration["SITE_ID"]);

            try
            {
                var site = Sites.Where(_ => _.Id == siteId).First();
                //CurrentOrderCell1 = await httpClient.GetJsonAsync<WorkOrderHeader>($"api/order/next");
                CurrentStationIdCell1 = 21; //first cell TJA
                GettingNextOrder = false;
                StateHasChanged();
            }
            catch (Exception e)
            {
                GettingNextOrder = false;
                _logger.LogInformation(e.InnerException.ToString());
            }
        }

        public async Task AdvanceCell(int cellId)
        {
            //write departing station history record
            await httpClient.PostJsonAsync($"api/history/station", CurrentStationIdCell1);

            var site = Sites.Where(_ => _.Id == SelectedSiteId).First();
            var cell = site.Cells.Where(_ => _.Id == cellId).FirstOrDefault();

            var activeStation = cell.Stations.Where(x => x.Id == CurrentStationIdCell1).FirstOrDefault();
            var nextStationOrder = 0;

            if (activeStation.StationOrder == 10)
            {
                nextStationOrder = 1;
                await GetNextWorkOrder();
            }
            else
            {
                nextStationOrder = activeStation.StationOrder + 1;
            }

            var nextStation = cell.Stations.Where(x => x.StationOrder == nextStationOrder).FirstOrDefault();
            CurrentStationIdCell1 = nextStation.Id;
            StateHasChanged();

            //send api call to update station history
        }

        public void LoadCellConfigAndStatus(int cellId)
        {
            SelectedCell = (CellViewModel)Sites.SelectMany(_ => _.Cells.Where(__ => __.Id == cellId)).FirstOrDefault();
            SelectedCellId = SelectedCell.Id;
            GroupingMethod = SelectedCell.Grouping;
            BendingRatio = Convert.ToDecimal(SelectedCell.FixedBendingRatio);
            CellType = SelectedCell.CellType;
            StateHasChanged();
            //GroupingMethod = Sites.SelectMany(_ => _.Cells.Where(__ => __.Id == SelectedCellId).Select(___ => ___.Grouping)).FirstOrDefault();
            //BendingRatio = Convert.ToDecimal(Sites.SelectMany(_ => _.Cells.Where(__ => __.Id == SelectedCellId).Select(___ => ___.BendingRatio)).FirstOrDefault());
            //CellType = Sites.SelectMany(_ => _.Cells.Where(__ => __.Id == SelectedCellId).Select(___ => ___.CellType)).FirstOrDefault();
        }

        public async Task AddCell()
        {
            try
            {
                await httpClient.PostJsonAsync($"api/cell", "newCellName");
                Sites = await httpClient.GetJsonAsync<List<SiteViewModel>>($"api/site/{SelectedSiteId}");
                StateHasChanged();
            }
            catch (Exception e)
            {
                _logger.LogInformation(e.InnerException.ToString());
            }
        }

        public async Task GetSites()
        {
            Sites = await httpClient.GetJsonAsync<List<SiteViewModel>>($"api/site");
            StateHasChanged();
        }

        public async Task UpdateCellConfiguration()
        {
            _logger.LogInformation("Saving...");
            await httpClient.PutJsonAsync($"api/cell", SelectedCell);
            LoadCellConfigAndStatus(SelectedCell.Id);
        }

        public async void HandleValidSubmit() => await UpdateCellConfiguration();
    }
}