﻿
using TMGA.Domain.Models.Wms;

using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Text.RegularExpressions;
using TMGA.Shared.ViewModels;

namespace WebUI.Pages
{
    public class IndexBase : ComponentBase
    {
        [Inject] protected ILogger<IndexBase> _logger { get; set; }
        [Inject] protected IConfiguration _configuration { get; set; }

        [Inject] protected Toolbelt.Blazor.I18nText.I18nText localizer { get; set; } 
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        protected List<LatestWavesViewModel> CustomWaves { get; set; } //= new List<LatestWavesViewModel>();
        protected List<LatestWavesViewModel> StockWaves { get; set; } //= new List<LatestWavesViewModel>();
        protected List<string> ReleasedWaves { get; set; } = new List<string>();
        protected HttpClient _httpClient =  new HttpClient() { BaseAddress = new Uri("http://restfulapi") };
        protected WavePlanViewModel WavePlan { get; set; } = new WavePlanViewModel();
        public List<PrinterViewModel> Printers { get; set; }

        public string selectedPrinter = ""; //carlsbad default 
        protected string WaveId = "";
        public string PrintAllMessage;
        public string SelectedWorkOrderId { get; set; }

        protected bool IsWindowShown;
        protected bool ShowWavePlan = false;
        protected bool ShowCustomWaves = true;
        protected bool ShowStockWaves = false;

        public bool isLoading = true;

        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
             
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                //var cultureOverride = _configuration["CULTURE_OVERRIDE"];
                //await _localizer.SetCurrentLanguageAsync(cultureOverride ?? "en");

               
                PrintAllMessage = _localizer.PrintAll;
                GetCustomWaves();
                GetStockWaves();
                GetPrinters();

                selectedPrinter = _configuration["DEFAULT_PRINTER"] ?? "HPLJ_IT"; //carlsbad little printer

               // await InvokeAsync(StateHasChanged);
            }
        }

        public async Task SwitchToCustomView()
        {
            ShowCustomWaves = true;
            ShowStockWaves = false;
        }

        public async Task SwitchToStockView()
        {
            ShowCustomWaves = false;
            ShowStockWaves = true;
        }

        public async Task GetCustomWaves()
        {
            isLoading = true;
            CustomWaves = await _httpClient.GetJsonAsync<List<LatestWavesViewModel>>("api/wave/custom");
            isLoading = false;
            StateHasChanged();
        }

        public async Task GetStockWaves()
        {
            isLoading = true;
            StockWaves = await _httpClient.GetJsonAsync<List<LatestWavesViewModel>>("api/wave/stock"); 
            isLoading = false;
            StateHasChanged();
        }

        public async Task RefreshCustomWaves()
        { 
            CustomWaves = null;
            await GetCustomWaves(); 
        }

        public async Task RefreshStockWaves()
        { 
            StockWaves = null;
            await GetStockWaves(); 
        }

        public async Task GetPrinters()
        {
            Printers = await _httpClient.GetJsonAsync<List<PrinterViewModel>>("api/site/printers");
        }


        public async Task GetCustomWavePlan(string selectedWaveId)
        {
            WaveId = selectedWaveId;
            WavePlan = null;

            var thisWave = CustomWaves.Where(x => x.WaveId == selectedWaveId).FirstOrDefault();

            CustomWaves.ForEach(_ => _.IsInFocus = false);
            thisWave.IsInFocus = true;
            thisWave.IsReleasing = true;
            ShowWavePlan = true;
            _logger.LogInformation($"Getting plan for Wave {selectedWaveId}");

            //var request = new HttpRequestMessage(HttpMethod.Get, $"api/wave/plan/{WaveId}");
            //var response = await httpClient.SendAsync(request);

            //if (response.IsSuccessStatusCode)
            //{
            //CellQueue = await response.Content.ReadAsAsync<List<CellQueue>>();
            WavePlan = await _httpClient.GetJsonAsync<WavePlanViewModel>($"api/wave/plan/{WaveId}");
            ReleasedWaves.Add(selectedWaveId);
            thisWave.IsReleasing = false;
            thisWave.IsReleased = true;
            thisWave.ReleasedOn = WavePlan.ReleasedOn;
            ShowWavePlan = true;
            //}
            //else
            //{
            //    //error
            //}
        }

        public async Task GetStockWavePlan(string selectedWaveId)
        {
            WaveId = selectedWaveId;
            WavePlan = null;

            var thisWave = StockWaves.Where(x => x.WaveId == selectedWaveId).FirstOrDefault();

            StockWaves.ForEach(_ => _.IsInFocus = false);
            thisWave.IsInFocus = true;
            thisWave.IsReleasing = true;
            ShowWavePlan = true;
            _logger.LogInformation($"Getting plan for Wave {selectedWaveId}");

            //var request = new HttpRequestMessage(HttpMethod.Get, $"api/wave/plan/{WaveId}");
            //var response = await httpClient.SendAsync(request);

            //if (response.IsSuccessStatusCode)
            //{
            //CellQueue = await response.Content.ReadAsAsync<List<CellQueue>>();
            WavePlan = await _httpClient.GetJsonAsync<WavePlanViewModel>($"api/wave/plan/{WaveId}");
            ReleasedWaves.Add(selectedWaveId);
            thisWave.IsReleasing = false;
            thisWave.IsReleased = true;
            thisWave.ReleasedOn = WavePlan.ReleasedOn;
            ShowWavePlan = true;
            //}
            //else
            //{
            //    //error
            //}
        }

        public async Task ForceWavePlan(string selectedWaveId)
        {
            WaveId = selectedWaveId;
            WavePlan = null;

            var thisWave = CustomWaves.Where(x => x.WaveId == selectedWaveId).FirstOrDefault();

            CustomWaves.ForEach(_ => _.IsInFocus = false);
            thisWave.IsInFocus = true;
            thisWave.IsReleasing = true;
            ShowWavePlan = true;
            _logger.LogInformation($"Getting plan for Wave {selectedWaveId}");

            //var request = new HttpRequestMessage(HttpMethod.Get, $"api/wave/plan/{WaveId}");
            //var response = await httpClient.SendAsync(request);

            //if (response.IsSuccessStatusCode)
            //{
            //CellQueue = await response.Content.ReadAsAsync<List<CellQueue>>();
            WavePlan = await _httpClient.GetJsonAsync<WavePlanViewModel>($"api/wave/force/{WaveId}");
            ReleasedWaves.Add(selectedWaveId);
            thisWave.IsReleasing = false;
            thisWave.IsReleased = true;
            thisWave.ReleasedOn = WavePlan.ReleasedOn;
            ShowWavePlan = true;
            //}
            //else
            //{
            //    //error
            //}
        }

        public async void PrintPickList(CellQueueViewModel cellQueue)
        {
            try
            {
                // selectedWorkOrderId = thisOrder.WO;
                cellQueue.IsGeneratingPicklist = true;
                await InvokeAsync(() =>
                {
                    StateHasChanged();
                });
                //var request = new HttpRequestMessage(HttpMethod.Get, $"api/picklist/{thisOrder.WO}");
                //var response = await httpClient.SendAsync(request);
                cellQueue.SelectedPrinter = selectedPrinter;

                //if (response.IsSuccessStatusCode)
                //{

                if (ShowCustomWaves)
                {
                    cellQueue.PickListPdfUrl = await _httpClient.PostJsonAsync<string>($"api/picklist/custom", cellQueue);
                }
                else
                {
                    cellQueue.PickListPdfUrl = await _httpClient.PostJsonAsync<string>($"api/picklist/stock", cellQueue);
                }
                cellQueue.IsGeneratingPicklist = false;
                cellQueue.IsPrinted = true;

                await InvokeAsync(() =>
                {
                    StateHasChanged();
                });
            }
            catch (Exception ex)
            {
                cellQueue.IsGeneratingPicklist = false;
                _logger.LogError(ex?.InnerException?.ToString()); 
            } 
        }

        public async void PrintAll()
        {
            PrintAllMessage = _localizer.Printing;

            await InvokeAsync(() =>
            {
                StateHasChanged();
            });

            var groupedByShipmentThenCell = this.WavePlan.CellQueues
                .GroupBy(_ => _.ShipmentId)
                .Distinct()
                .OrderBy(shipment => shipment.Select(x => x.Cell.Desc).FirstOrDefault())
                .ToList();

            foreach (var cellGroup in groupedByShipmentThenCell)
            {
                await Task.Run(() => PrintPickList(cellGroup.First())); // shipmentid
                System.Threading.Thread.Sleep(7000); //to ensure cognos doesn't get overwhelmed and prints in the correct order. can't go lower than this
            }

            PrintAllMessage = _localizer.PrintAll;
            await InvokeAsync(() =>
            {
                StateHasChanged();
            });
        }

        public void ToggleWindow() => IsWindowShown = !IsWindowShown;

    }
}