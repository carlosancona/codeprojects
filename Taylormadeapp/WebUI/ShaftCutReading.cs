using System.ComponentModel.DataAnnotations;

namespace WebUI.Models
{
    public class ShaftCutReading
    {
        [Key]
        public int Id { get; set; }

        public string Desc { get; set; }
    }
}