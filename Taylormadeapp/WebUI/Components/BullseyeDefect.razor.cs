﻿


using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System; 
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class BullseyeDefect : ComponentBase
    {
        [Inject] IWebHostEnvironment _env { get; set; }
        [Inject] IJSRuntime JSRuntime { get; set; }
        [Inject] ILogger<DefectViewModel> _logger { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText _localizer { get; set; }

        [CascadingParameter(Name = "WorkOrder")]
        public WorkOrderViewModel workOrder { get; set; }

        [CascadingParameter(Name = "ThisStation")]
        public StationViewModel thisStation { get; set; }

        [CascadingParameter(Name = "IsProcessingDefect")]
        public bool isProcessingDefect { get; set; }

        [CascadingParameter(Name = "DefectiveComponent")]
        public WorkOrderComponentViewModel DefectiveComponent { get; set; }

        [CascadingParameter(Name = "DefectReason")]
        public DefectReasonViewModel defectReason { get; set; } 

        [Parameter] public EventCallback OnClickCancel { get; set; }
        [Parameter] public EventCallback OnResetScreen { get; set; }
         
        public I18nText.Resources L { get; set; } = new I18nText.Resources();

        public HttpClient HttpClient;
        public Timer ForceFocusTimer;
        public UserViewModel CurrentUser;

        string Message;
        public string defectiveComponentSku;
        public int quantityAffected; 
        public float BullseyeX { get; set; }
        public float BullseyeY { get; set; }
         
        protected override async Task OnInitializedAsync()
        {
            HttpClient = new HttpClient
            {
                BaseAddress = new System.Uri("http://restfulapi")
            };
            L = await _localizer.GetTextTableAsync<I18nText.Resources>(this);
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            CurrentUser = await localStorage.GetItemAsync<UserViewModel>("currentUser");
            //await JSRuntime.InvokeAsync<object>("InitializeBullseye");
        }

        public class BullseyeData
        {
            public string x { get; set; }
            public string y { get; set; }
        }

        public async Task CreateDefectWithBending()
        {

            var bullseyeData = await JSRuntime.InvokeAsync<BullseyeData>("getBullseyeData");

            //var dict = System.Text.Json.JsonSerializer.Deserialize<Dictionary<float, float>>(bullseyeData);

            //var x = System.Text.Json.JsonDocument.Parse(bullseyeData).RootElement.GetProperty("x").GetDecimal();
            //var y = System.Text.Json.JsonDocument.Parse(bullseyeData).RootElement.GetProperty("y").GetDecimal();

            var request = new DefectViewModel();
            var components = workOrder.SKUs.SelectMany(sku => sku.SubSKUs.SelectMany(subSku => subSku.Components.ToList()));

            request.DefectiveSKU = components.Where(_ => _.ComponentType == "HEAD").First().ComponentSku;
            request.DefectReasonId = defectReason.DefectReasonId;
            request.StationId = thisStation.Id;
            request.WorkOrderId = workOrder.WorkOrder;
            request.WaveId = workOrder.WaveId;
            request.UserId = CurrentUser.Id;
            request.SiteId = thisStation.Cell.Site.Id;
            request.ShipmentId = workOrder.Shipment;
            request.Quantity = 1;
            request.WriteOffType = "Write Off";
            request.BullseyeX = Math.Round(Convert.ToDouble(bullseyeData.x), 4);
            request.BullseyeY = Math.Round(Convert.ToDouble(bullseyeData.y), 4);


            var result = await HttpClient.PostJsonAsync<bool>($"api/defect", request);

            OnResetScreen.InvokeAsync(null);
        }




    }
}
