﻿
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class HelpModal : ComponentBase
    {
        [Inject] ILogger<HelpModal> _logger { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };

        [Parameter] public RenderFragment ChildContent { get; set; }
        [Parameter] public EventCallback<bool> OnClickCancel { get; set; }
        [Parameter] public EventCallback OnOpen { get; set; }
        [Parameter] public EventCallback OnClose { get; set; }

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        private TimeSpan elapsed = new TimeSpan();
        private Timer elapsedTimer;
        private bool isClosing = false;

        protected async override Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
            await OnOpen.InvokeAsync(null); 
        }

        private async Task CloseWindow()
        {
            isClosing = true;
            
            await InvokeAsync(StateHasChanged);

            //give it a bit for the window to scale out
            //Thread.Sleep(500);

            //bubble up events to HoldButton component
            await OnClose.InvokeAsync(null);
            await OnClickCancel.InvokeAsync(false);
            isClosing = false;
        }


    }
}
