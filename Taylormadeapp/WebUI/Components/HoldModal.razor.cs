﻿
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class HoldModal : ComponentBase
    {
        [Inject] ILogger<HoldModal> _logger { get; set; } 
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; } 
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };

        [Parameter] public RenderFragment ChildContent { get; set; }
        [Parameter] public EventCallback<bool> OnClickCancel { get; set; }
        [Parameter] public EventCallback OnOpen { get; set; }
        [Parameter] public EventCallback OnClose { get; set; }

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        private TimeSpan elapsed = new TimeSpan();
        private Timer elapsedTimer;
        private bool isClosing = false;

        protected async override Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
            await OnOpen.InvokeAsync(null);

            elapsed = TimeSpan.FromSeconds(0);

            elapsedTimer = new Timer((_) =>
            {
                elapsed = elapsed.Add(new TimeSpan(0, 0, 0, 1));
                InvokeAsync(StateHasChanged);
            }, null, 0, 1000);

            var stationHoldViewModel = new StationHoldViewModel
            {
                ShipmentId = WorkOrder?.Shipment,
                StationId = Station.Id,
                UserId = User.Id,
                WO = WorkOrder?.WorkOrder,
                IsOnHold = true
            };

            await _httpClient.PostJsonAsync<object>("api/station/hold", stationHoldViewModel); 
        }

        private async Task CloseWindow()
        {
            isClosing = true;
            var stationHoldViewModel = new StationHoldViewModel
            {
                ShipmentId = WorkOrder.Shipment,
                StationId = Station.Id,
                UserId = User.Id,
                WO = WorkOrder.WorkOrder,
                IsOnHold = false
            };

            await _httpClient.PostJsonAsync<object>("api/station/hold", stationHoldViewModel);

            await InvokeAsync(StateHasChanged);

            //give it a bit for the window to scale out
            //Thread.Sleep(500);

            //bubble up events to HoldButton component
            await OnClose.InvokeAsync(null);
            await OnClickCancel.InvokeAsync(false);
            isClosing = false;
        }


    }
}
