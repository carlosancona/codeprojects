﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using TMGA.Shared.ViewModels;
//using System.Threading;
using System.Timers;

namespace WebUI.Components
{
    public partial class TemperatureListComponent : ComponentBase, IDisposable
    {
        [Inject] IJSRuntime _javascript { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        private I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }
        private HttpClient HttpClient;
        private Timer timer;
        //private string CurrentWorkOrder { get; set; } = "";
        private List<TemperatureCuringViewModel> temperaturevm { get; set; }
        private double TotalCarouselTime { get; set; }
        private double PreviousCuringTime { get; set; } = 0.00;
        //private bool IsFinished { get; set; } = false;
        private List<CuringRecordViewModel> CuringRowsList { get; set; }

        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
            HttpClient = new HttpClient
            {
                BaseAddress = new System.Uri("http://restfulapi")
            };

            timer = new Timer();

        }

        public void Dispose()
        {
            if (timer != null)
            {
                timer.Dispose();
            }
        }


        private void StartTimer()
        {
            //timer.Interval = 10000;
            timer.Interval = 2000;
            timer.Elapsed += TimerOnElapsed;
            timer.Start();
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            ShowWOList().ConfigureAwait(false);
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                StartTimer();

            }

        }

        public async Task ShowWOList()
        {
            Console.WriteLine($"The Elapsed time is: {DateTime.Now.ToUniversalTime():HH:mm:ss.fff}: done.");
            Console.WriteLine($"The current station id is: {Station.Id}");


            temperaturevm = await HttpClient.GetJsonAsync<List<TemperatureCuringViewModel>>($"api/iot/temperature/getrecenttemperaturefeedbystation/1/{Station.Id}");

            CuringRowsList = await HttpClient.GetJsonAsync<List<CuringRecordViewModel>>($"api/iot/temperature/getallrecentcuringrecords/{Station.Id}");
            CuringRowsList.Reverse();
            CuringRowsList.ForEach(async _ =>
            {
                if (_.IsProcessed != "Yes")
                {
                    await RunCuring(_);
                }
            });


            if (WorkOrder != null)
            {
                if (!String.IsNullOrEmpty(WorkOrder.WorkOrder))
                {
                    bool CheckoutStatus = await HttpClient.GetJsonAsync<bool>($"api/iot/temperature/isfinishedprocessing/{WorkOrder.WorkOrder}");
                    if (CheckoutStatus)
                    {
                        await _javascript.InvokeVoidAsync("plotlyFunctions.showReady");
                    }
                    else
                    {
                        await _javascript.InvokeVoidAsync("plotlyFunctions.showNotReady");
                    }

                    WorkOrder.WorkOrder = "";
                }

            }


            //StateHasChanged();
        }
        private async Task AddCuringRecordRows(CuringRecordViewModel cvm, string Status, double CuringTime)
        {
            if (cvm.Status != "End")
            {
                await HttpClient.SendJsonAsync(HttpMethod.Post, "api/iot/temperature/createcuringrecord", new CuringRecordViewModel
                {
                    WorkOrder = cvm.WorkOrder,
                    ClubDescription = cvm.ClubDescription,
                    StationId = temperaturevm.Last().StationId,
                    Temperature = temperaturevm.Last().Temperature,
                    Timestamp = DateTime.UtcNow,
                    Status = Status,
                    CurrentCuringTime = CuringTime,
                    OriginalCuringTime = cvm.OriginalCuringTime,
                    Quantity = cvm.Quantity,
                    IsProcessed = cvm.IsProcessed

                });

            }



        }

        private async Task RunCuring(CuringRecordViewModel cvm)
        {
            double CalculatedReminingTime = CalculateCarouselTime(Math.Round(temperaturevm.Last().Temperature, 2), cvm, 30.00, 38, 42);


            if (CalculatedReminingTime <= 0.0)
            {
                await AddCuringRecordRows(cvm, "End", 0.0);
            }
            else
            {
                await AddCuringRecordRows(cvm, "In Progress", CalculatedReminingTime);
            }
        }

        private double CalculateCarouselTime(double currentTemp, CuringRecordViewModel cvm, double DefCarouselTime, int lowerBound, int upperBound)
        {
            double diff;
            double remainder;

            if (currentTemp < lowerBound)
            {
                diff = lowerBound - currentTemp;
                remainder = Math.Ceiling(diff / 5);
                DefCarouselTime += remainder * 0.3;
            }
            else if (currentTemp > upperBound)
            {
                diff = currentTemp - upperBound;
                remainder = Math.Ceiling(diff / 5);
                DefCarouselTime -= remainder * 0.3;
            }

            double CarouselTimeDifference = 30.0 - cvm.OriginalCuringTime;
            double checkval = cvm.CurrentCuringTime;
            checkval = checkval + CarouselTimeDifference;

            /*
            DateTime currTime = DateTime.UtcNow;
            double seconds = currTime.Subtract(cvm.Timestamp).TotalSeconds;

            
             
                Need to call api to get the not utc
              
             */

            //return Math.Round(cvm.CurrentCuringTime - 0.10, 2);
            return Math.Round(cvm.CurrentCuringTime - 0.02, 2);


        }
    }

}
