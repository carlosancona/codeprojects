﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class HoldButton : ComponentBase
    {
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; } 
        [Inject] IConfiguration _configuration { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }
        [CascadingParameter(Name = "WorkOrders")] public List<WorkOrderViewModel> WorkOrders { get; set; }

        [Parameter] public EventCallback OnOpen { get; set; }
        [Parameter] public EventCallback OnClose { get; set; }

        public bool IsOpen { get; set; } = false;
      
        protected override async Task OnInitializedAsync() => _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
  
        public void OpenModal() => IsOpen = true;

        public void CloseModal() => IsOpen = false;

        public async Task ModalOpened() => await OnOpen.InvokeAsync(null);

        public async Task ModalClosed()
        {
            CloseModal(); 
            await OnClose.InvokeAsync(null);
        }        
    }
}
