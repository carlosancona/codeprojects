﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class PickingComponent : ComponentBase
    {
        [Inject] ShopFloorState AppState { get; set; }

        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();
        [Inject] IConfiguration _configuration { get; set; }

        [CascadingParameter(Name = "WorkOrders")] public List<WorkOrderViewModel> WorkOrders { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }
        [Parameter] public bool HasRefreshButton { get; set; } = true;
        [Parameter] public bool HasDefectButton { get; set; } = true;
        [Parameter] public bool HasHoldButton { get; set; } = true;
        [Parameter] public bool HasDoneButton { get; set; } = false;
        [Parameter] public bool HasCloseButton { get; set; } = true;
        [Parameter] public bool HasStationQueueInformation { get; set; } = true;
        [Parameter] public bool HasMaintenanceButton { get; set; } = false;
        [Parameter] public bool HasHelpButton { get; set; } = true;

        protected WorkOrderViewModel currentWorkOrder;
        protected List<WorkOrderViewModel> currentShipment;

        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
        }

        protected List<PickingViewModel> GetGrouped(string criteria)
        { 
            var a = WorkOrders?.SelectMany(x => x.SKUs.SelectMany(sku =>
            sku.SubSKUs.SelectMany(subSku =>
                subSku.Components.Where(component =>
                    component.ComponentType == criteria).ToList())));

            return a.GroupBy(component => component.ComponentSku).Select(grp => new PickingViewModel { Sku = grp.Key, Desc = grp.First().ComponentDescription, Quantity = grp.Sum(cc => cc.ComponentQty) }).ToList();
        }


        protected List<PickingViewModel> GetGroupedEntireShipment(string criteria)
        {
            var a = currentShipment?.SelectMany(wo => wo.SKUs.SelectMany(sku =>
            sku.SubSKUs.SelectMany(subSku =>
                subSku.Components.Where(component =>
                    component.ComponentType == criteria).ToList())));
            return a.GroupBy(component => component.ComponentSku).Select(grp => new PickingViewModel { Desc = grp.First().ComponentDescription, Quantity = grp.Sum(cc => cc.ComponentQty) }).ToList();
        }
    }
}
