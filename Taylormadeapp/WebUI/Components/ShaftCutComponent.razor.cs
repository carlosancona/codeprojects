﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using System;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class ShaftCutComponent : ComponentBase
    {
        [Inject] ShaftCutTemplateVerificationComponentState ShaftCutTemplateVerificationComponentState { get; set; }
        [Inject] ShopFloorState AppState { get; set; }
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] IConfiguration _configuration { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }

        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        public int? MaintenanceInterval;

        protected override async Task OnInitializedAsync()
        {

            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);

            var configuredMaintenanceInterval = _configuration["SHAFT_CUT_MAINTENANCE_FREQUENCY"];
            if (configuredMaintenanceInterval != null)
            {
                MaintenanceInterval = Convert.ToInt32(configuredMaintenanceInterval);
            }
            else
            {
                MaintenanceInterval = 100;
            }

            AppState.OnWorkOrderNumberChanged += ResetScreen;

            ShaftCutTemplateVerificationComponentState.OnShaftCutTemplateTipVerifyBegin += DummyFunction;
            ShaftCutTemplateVerificationComponentState.OnShaftCutTemplateTipVerifyEnd += DummyFunction;
            ShaftCutTemplateVerificationComponentState.OnShaftCutTemplateTipVerifySuccess += DummyFunction;

            ShaftCutTemplateVerificationComponentState.OnShaftCutTemplateButtVerifyBegin += DummyFunction;
            ShaftCutTemplateVerificationComponentState.OnShaftCutTemplateButtVerifyEnd += DummyFunction;
            ShaftCutTemplateVerificationComponentState.OnShaftCutTemplateButtVerifySuccess += StateHasChanged;
        }

        public string ConvertToMM(string value)
        {
            if (!String.IsNullOrEmpty(value) && value.Contains("X"))
            {
                var split = value.Split("X");
                var left = Math.Round(Decimal.Parse(split[0].Trim()) * (decimal)25.4, 2);
                var right = Math.Round(Decimal.Parse(split[1].Trim()) * (decimal)25.4, 0);
                return $"{left} mm X {right} mm";
            }
            else if (!String.IsNullOrEmpty(value) && value.Contains("Grind"))
            {
                var split = value.Split(@"""");
                var left = Math.Round(Decimal.Parse(split[0].Trim()) * (decimal)25.4, 2);
                return $"{left} mm Grind";
            }
            else
            {
                return value;
            }
        }


        public void DummyFunction()
        {

        }

        public void ResetScreen()
        {
            ShaftCutTemplateVerificationComponentState.TipTemplateVerified = false;
            ShaftCutTemplateVerificationComponentState.ButtTemplateVerified = false;
            //StateHasChanged();
        }
    }
}
