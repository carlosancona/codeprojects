﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class ScanInput : ComponentBase
    {
        [Inject] ShopFloorState AppState { get; set; }
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        [Inject] ILogger<ScanInput> _logger { get; set; }

        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };
        private I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [Parameter] public bool EnableForceFocus { get; set; } = true;
        [Parameter] public int ForceFocusRefreshRate { get; set; } = 1000;

        /// <summary>
        /// Returns string of the scanned work order id as it appeared in the input box.
        /// </summary>
        [Parameter] public EventCallback<string> OnScanBegin { get; set; }

        /// <summary> 
        /// Returns a WoOrder mn
        [Parameter] public EventCallback<WorkOrderViewModel> OnSuccess { get; set; }

        /// <summary> 
        /// Returns a 
        [Parameter] public EventCallback<List<WorkOrderViewModel>> OnSuccessMultipleOrders { get; set; }

        /// <summary>
        /// Returns an Exception.
        /// </summary>
        [Parameter] public EventCallback<Exception> OnError { get; set; }
          
        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }
        [CascadingParameter(Name = "WorkOrders")] public List<WorkOrderViewModel> WorkOrders { get; set; }

        public List<WorkOrderViewModel> currentShipment { get; set; }

        private Timer ForceFocusTimer { get; set; }
         
        private string _workOrderId;
        private string workOrderId
        {
            get => _workOrderId;
            set
            {
                _workOrderId = value;
                //intercept change event (for barcode scanning)
                if (_workOrderId?.Length > 1) GetShipmentOrWorkOrder();
            }
        }

        public string ScannedWorkOrderId => workOrderId;

        private WorkOrderViewModel workOrder;
        public WorkOrderViewModel CurrentWorkOrder => workOrder;

        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);

          
                ForceFocusTimer = new Timer((_) =>
                {
                    ExecuteForceFocus();
                }, null, 0, ForceFocusRefreshRate);
            
        }

        private async Task ExecuteForceFocus()
        {
            if (EnableForceFocus && AppState.EnableScanInputForceFocus) _javascript.InvokeAsync<object>("RefocusWOInput"); 

        }

        protected async Task GetShipmentOrWorkOrder()
        {
            //WO or shipment?
            if (workOrderId.ToUpper().StartsWith("WO"))
            {
                await GetWorkOrder(); 
            }
            else
            {
                await GetShipment();
            }

            AppState.WorkOrderNumberChanged();
            OnScanBegin.InvokeAsync(null);

        }


        protected async Task GetWorkOrder()
        {
            workOrder = null;

            try
            {
                var request = new ScannedFromShopFloorViewModel
                {
                    ThisStation = Station,
                    ScannedWO = workOrderId,
                    Message = "",
                    UserId = User.Name,
                };

                var result = await _httpClient.PostJsonAsync<ToShopFloorViewModel>($"api/order", request);

                if (result.IsAlreadyCompleted)
                {
                    //this should happen in the parent component
                }

                workOrderId = null;
                workOrder = result.WorkOrder;
                await OnSuccess.InvokeAsync(workOrder);
            }
            catch (Exception ex)
            {
                  OnError.InvokeAsync(ex);
                _logger.LogError($"Scan Error: {ex.ToString()}");
                
                workOrder = null;
            }
            finally
            {
                workOrderId = null;
              // InvokeAsync(StateHasChanged);
            } 
        }


        protected async Task GetShipment()
        {
            currentShipment = null;

            var request = new ScannedFromShopFloorViewModel
            {
                ThisStation = Station,
                ScannedWO = workOrderId,
                Message = "",
                UserId = User.Name,
            }; 

            try
            {
                var result = await _httpClient.PostJsonAsync<List<ToShopFloorViewModel>>($"api/shipment", request);
                currentShipment = result.Select(_ => _.WorkOrder).ToList();
                if (currentShipment.Count() == 0)
                {
                    currentShipment = null;
                    workOrderId = null;

                    await OnError.InvokeAsync(new Exception());
                }
                else
                {
                    // Message = "";
                } 

                workOrderId = null;
                await OnSuccessMultipleOrders.InvokeAsync(result.Select(_ => _.WorkOrder).ToList()); 
            }
            catch (Exception ex)
            {
                _logger.LogError($"Scan Error: {ex.ToString()}");
                //currentWorkOrder = null;
                workOrderId = null; 
                await OnError.InvokeAsync(ex);
                // Message = L.ErrorScanningOrder + $" ({ex?.InnerException?.ToString() ?? "Null error"})";
            }
            finally
            {
                workOrderId = null;
                //isLoading = false;
                  // InvokeAsync(StateHasChanged);
            }

        }

    }
}
