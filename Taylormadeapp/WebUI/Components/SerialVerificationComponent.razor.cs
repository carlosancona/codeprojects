﻿using MediatR;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Domain.Models.Intercept;
using TMGA.Shared.ViewModels;
using Microsoft.Extensions.Configuration;

namespace WebUI.Components
{
    public partial class SerialVerificationComponent
    {
        [Inject] ShopFloorState AppState { get; set; }
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        [Inject] ILogger<SerialVerificationComponent> _logger { get; set; }
        [Inject] IMediator _mediator { get; set; }
        [Inject] IConfiguration _configuration { get; set; }

        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };
        private I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        public ScanInput scanInputField { get; set; }

        public int numberOfSerialsRemainingToScan { get; set; }
        public int numberOfHeadCoversRemaining { get; set; }

        private string _scannedInput;
        private string scannedInput
        {
            get => _scannedInput;
            set
            {
                //intercept change event (for barcode scanning)
                _scannedInput = value;
                if (_scannedInput?.Length > 1) VerifySerialOrHeadCover();
            }
        }

        public List<string> CoverNeededEans { get; set; } = new List<string>();
        public List<string> CoverNeededUpc { get; set; } = new List<string>();
        public List<string> ManualNeededEans { get; set; } = new List<string>();
        public List<string> ManualNeededUpc { get; set; } = new List<string>();
        public List<string> WrenchNeededEans { get; set; } = new List<string>();
        public List<string> WrenchNeededUpc { get; set; } = new List<string>();
        public List<string> scannedInputs { get; set; } = new List<string>();
        public List<string> NeededEans { get; set; } = new List<string>();
        public List<string> NeededUpc { get; set; } = new List<string>();
        public List<string> NeededSerials { get; set; } = new List<string>();
        public List<WoComponent> ComponentsList { get; set; } = new List<WoComponent>();
        public List<WoComponent> HeadComponentsList { get; set; } = new List<WoComponent>();
        public List<string> WoHeadSku { get; set; } = new List<string>();

        public WoSku WoSku { get; set; } = new WoSku();

        private Timer ForceFocusTimer { get; set; }
        public bool IsLoading { get; set; } = true;
        private string redOrGreen { get; set; } = "Red";

        int coverquantity { get; set; }
        int wrenchquantity { get; set; }
        int manualquantity { get; set; }

        public string ScanSerialMessage = "";

        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
            RefreshComponent();
           
            AppState.OnWorkOrderNumberChanged += RefreshComponent;

            NeededSerials = await _httpClient.GetJsonAsync<List<string>>($"api/order/serials/{WorkOrder.WorkOrder}");

            List<string> skus = new List<string>();
            
            try
            {
                WoSku = await _httpClient.GetJsonAsync<WoSku>($"api/station/wosku/{WorkOrder.WorkOrder}");
            }
            catch { }
            HeadComponentsList = await _httpClient.GetJsonAsync<List<WoComponent>>($"api/station/headcomponents/{WorkOrder.WorkOrder}");

            try
            {
                ComponentsList = await _httpClient.GetJsonAsync<List<WoComponent>>($"api/station/componentscoversmisc/{WorkOrder.WorkOrder}");

                if (ComponentsList.Count > 0)
                {
                    List<string> coverskus = new List<string>();
                    List<string> manualskus = new List<string>();
                    List<string> wrenchskus = new List<string>();

                    foreach (var item in ComponentsList)
                    {
                        skus.Add(item.ComponentSku);

                        if (item.ComponentType == "HEADCOVER")
                        {
                            coverskus.Add(item.ComponentSku);
                        }
                        string componentdesc = item.ComponentDescription.ToLower();
                        if (componentdesc.Contains("kit"))
                        {
                            wrenchskus.Add(item.ComponentSku);
                            wrenchquantity = Convert.ToInt32(item.ComponentQty);

                        }
                        if (item.ComponentDescription.Contains("MANUAL") || item.ComponentDescription.Contains("Manual"))
                        {
                            manualskus.Add(item.ComponentSku);
                            manualquantity = Convert.ToInt32(item.ComponentQty);
                        }
                    }

                    if (coverskus.Count > 0)
                    {
                        CoverNeededEans = await _httpClient.PostJsonAsync<List<string>>("api/station/headcovers", coverskus);
                        CoverNeededUpc = await _httpClient.PostJsonAsync<List<string>>("api/station/eans", coverskus);
                        NeededEans.AddRange(CoverNeededEans);
                        NeededUpc.AddRange(CoverNeededUpc);

                        try
                        {
                            if (WoSku.HeadCoverQuantity > 0)
                            {
                                coverquantity = WoSku.HeadCoverQuantity;
                            }
                        }
                        catch
                        {
                            coverquantity = coverskus.Count;
                        }
                    }
                    else
                    {
                        try
                        {
                            if (WoSku.HeadCoverQuantity > 0)
                            {
                                WoHeadSku.Add(WoSku.HeadCoverSku);

                                CoverNeededEans.AddRange(await _httpClient.PostJsonAsync<List<string>>("api/station/headcovers", WoHeadSku));
                                CoverNeededUpc.AddRange(await _httpClient.PostJsonAsync<List<string>>("api/station/eans", WoHeadSku));
                                NeededUpc.AddRange(CoverNeededUpc);
                                NeededEans.AddRange(CoverNeededEans);

                                try
                                {
                                    coverquantity = WoSku.HeadCoverQuantity;

                                    WoComponent wocomp = new WoComponent();
                                    wocomp.ComponentSku = WoSku.HeadCoverSku;
                                    wocomp.ComponentType = "HEADCOVER";
                                    ComponentsList.Add(wocomp);

                                }
                                catch
                                {
                                    coverquantity = 0;
                                }
                            }

                        }
                        catch
                        {
                            coverquantity = 0;
                        }
                    }
                    if (wrenchskus.Count > 0)
                    {
                        WrenchNeededEans = await _httpClient.PostJsonAsync<List<string>>("api/station/headcovers", wrenchskus);
                        WrenchNeededUpc = await _httpClient.PostJsonAsync<List<string>>("api/station/eans", wrenchskus);
                        NeededEans.AddRange(WrenchNeededEans);
                        NeededUpc.AddRange(WrenchNeededUpc);
                    }
                    else
                    {
                        wrenchquantity = 0;
                    }
                    if (manualskus.Count > 0)
                    {
                        ManualNeededEans = await _httpClient.PostJsonAsync<List<string>>("api/station/headcovers", manualskus);
                        ManualNeededUpc = await _httpClient.PostJsonAsync<List<string>>("api/station/eans", manualskus);
                        NeededEans.AddRange(ManualNeededEans);
                        NeededUpc.AddRange(ManualNeededUpc);
                    }
                    else
                    {
                        manualquantity = 0;
                    }
                }
                else
                {
                    manualquantity = 0;
                    wrenchquantity = 0;
                    coverquantity = 0;
                    try
                    {
                        if (WoSku.HeadCoverQuantity > 0)
                        {
                            WoHeadSku.Add(WoSku.HeadCoverSku);

                            CoverNeededEans.AddRange(await _httpClient.PostJsonAsync<List<string>>("api/station/headcovers", WoHeadSku));
                            CoverNeededUpc.AddRange(await _httpClient.PostJsonAsync<List<string>>("api/station/eans", WoHeadSku));
                            NeededUpc.AddRange(CoverNeededUpc);
                            NeededEans.AddRange(CoverNeededEans);

                            try
                            {
                                coverquantity = WoSku.HeadCoverQuantity;

                                WoComponent wocomp = new WoComponent();
                                wocomp.ComponentSku = WoSku.HeadCoverSku;
                                wocomp.ComponentType = "HEADCOVER";
                                ComponentsList.Add(wocomp);

                            }
                            catch
                            {
                                coverquantity = 0;
                            }
                        }
                    
                    }
                    catch
                    {
                        coverquantity = 0;
                    }
                }
            }
            catch
            {
                coverquantity = 0;
            }
          
            IsLoading = false;
        }

        public int CoverRemaining()
        {
            int result = 0;

            //if (CoverNeededEans.Count >= CoverNeededUpc.Count)
            //{
            //    result = CoverNeededUpc.Count;
            //}
            //if (CoverNeededUpc.Count >= CoverNeededEans.Count)
            //{
            //    result = CoverNeededEans.Count;
            //}

            result = coverquantity;

            return result;
        }

        public int WrenchRemaining()
        {
            int result = 0;

            result = wrenchquantity;

            return result;
        }

        public int ManualRemaining()
        {
            int result = 0;

            result = manualquantity;

            return result;
        }

        public int HeadAccesoriesRemaining()
        {
            int result = CoverRemaining() + WrenchRemaining() + ManualRemaining();
            return result;
        }

        public async Task VerifySerialOrHeadCover()
        {
            if (scannedInputs.Contains(scannedInput))
            {
                redOrGreen = "Red";
                ScanSerialMessage = _localizer.AlreadyScanned;
            }
            else
                if (NeededSerials.Contains(scannedInput))
            {
                redOrGreen = "Green";
                NeededSerials.Remove(scannedInput);
                ScanSerialMessage = _localizer.SerialNumberScannedSuccesfully;
            }

            else
            {
                //determine if exists 
                //var serialCodeCaptureViewModel = new SerialCodeCaptureViewModel
                //{
                //    SerialNumber = scannedInput,
                //    StationId = Station.Id,
                //    WorkOrderNumber = WorkOrder.WorkOrder,
                //    User = User,
                //};

                //var previouslyExists = await _httpClient.PostJsonAsync<bool>("api/station/serial/check", serialCodeCaptureViewModel);

                if (NeededEans.Contains(scannedInput) || NeededUpc.Contains(scannedInput))
                {
                    if (NeededEans.Contains(scannedInput))
                    {
                        redOrGreen = "Green";
                        ScanSerialMessage = _localizer.HeadCoverEANVerified;

                        //if (CoverNeededEans.Contains(scannedInput)) CoverNeededEans.Remove(scannedInput);
                        if (CoverNeededEans.Contains(scannedInput)) coverquantity--;
                        if (ManualNeededEans.Contains(scannedInput)) manualquantity--;
                        if (WrenchNeededEans.Contains(scannedInput)) wrenchquantity--;

                    }

                    if (NeededUpc.Contains(scannedInput))
                    {
                        redOrGreen = "Green";
                        ScanSerialMessage = _localizer.HeadCoverSKUVerified;

                        //if (CoverNeededUpc.Contains(scannedInput)) CoverNeededUpc.Remove(scannedInput);
                        if (CoverNeededUpc.Contains(scannedInput)) coverquantity--;
                        if (ManualNeededUpc.Contains(scannedInput)) manualquantity--;
                        if (WrenchNeededUpc.Contains(scannedInput)) wrenchquantity--;
                    }
                }
                else
                {
                    //unable to verify
                    redOrGreen = "Red";
                    ScanSerialMessage = _localizer.CannotVerify;
                }
            }

            scannedInput = null;

            //    if (numberOfSerialsRemainingToScan == 0 && (NeededEans.Count == 0 || NeededUpc.Count == 0)) Cleanup();
            if (numberOfSerialsRemainingToScan == 0 && HeadAccesoriesRemaining() == 0) Cleanup();

        }



        public void RefreshComponent()
        {
            AppState.EnableScanInputForceFocus = false;

            ForceFocusTimer = new Timer((_) =>
            {
                ExecuteForceFocus();
            }, null, 0, 1000);


            numberOfSerialsRemainingToScan = WorkOrder.ClubCount();
        }

        public async Task ExecuteForceFocus() => _javascript.InvokeAsync<object>("RefocusScannedSerialInput");

        public void Cleanup()
        {
            scannedInput = null;
            _scannedInput = null;
            scannedInputs = new List<string>();

            redOrGreen = "Green";
            ScanSerialMessage = "";

            //this should be a callback, not setting a boolean
            AppState.EnableScanInputForceFocus = true;
        }
    }
}
