﻿using TMGA.Shared.ViewModels;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebUI.Components
{
    /// <summary>
    /// The done button component is found on applicable shop floor screens
    /// </summary>
    public partial class DoneButton : ComponentBase
    {
        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        [Parameter] public EventCallback OnClick { get; set; }
        [Parameter] public EventCallback OnClickBegin { get; set; }
        [Parameter] public EventCallback OnClickEnd { get; set; }
        [Parameter] public EventCallback<Exception> OnException { get; set; }

        [Inject] ILogger<DoneButton> _logger { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IConfiguration _configuration { get; set; }

        private HttpClient httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };
        private I18nText.Resources _localizer { get; set; }

        public string Message { get; set; }

        protected async override Task OnInitializedAsync()
        { 
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
        }

        public async Task Done()
        {
            await OnClickBegin.InvokeAsync(null);

            try
            {
                var stationDoneViewModel = new StationDoneViewModel
                {
                    WO = WorkOrder.WorkOrder,
                    ShipmentId = WorkOrder.Shipment,
                    Station = Station,
                    UserId = User.Name
                };

                var result = await httpClient.PostJsonAsync<ToShopFloorViewModel>($"api/station/done", stationDoneViewModel);
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.ToString()); 
                await OnException.InvokeAsync(ex);
            }

            finally
            {
                Station = await httpClient.GetJsonAsync<StationViewModel>($"api/station/{Station.Id}");
                await OnClickEnd.InvokeAsync(null);
            }

            await OnClick.InvokeAsync(null);
        }
    }
}
