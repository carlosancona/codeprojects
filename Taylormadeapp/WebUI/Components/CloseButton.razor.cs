﻿using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;

namespace WebUI.Components
{
    public partial class CloseButton : ComponentBase
    {
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IConfiguration _configuration { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }
        [CascadingParameter(Name = "WorkOrders")] public List<WorkOrderViewModel> WorkOrders { get; set; }

        [Parameter] public EventCallback OnOpen { get; set; }
        [Parameter] public EventCallback OnClose { get; set; }

        public bool IsOpen { get; set; } = false;

        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
        }
 
        public void RedirectToLogin()
        {
            _navigationManager.NavigateTo("/");
            //StateHasChanged();
        }
       
    }
}
