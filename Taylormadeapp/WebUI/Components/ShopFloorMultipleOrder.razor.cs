﻿
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class ShopFloorMultipleOrder 
    {
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        [Inject] IConfiguration _configuration { get; set; }
        [Inject] ILogger<ShopFloorSingleOrder> _logger { get; set; }

        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();
         
        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };

        [Parameter] public bool HasRefreshButton { get; set; } = true;
        [Parameter] public bool HasDefectButton { get; set; } = true;
        [Parameter] public bool HasHoldButton { get; set; } = true;
        [Parameter] public bool HasDoneButton { get; set; } = false;
        [Parameter] public bool HasCloseButton { get; set; } = true;
        [Parameter] public bool HasStationQueueInformation { get; set; } = true;
        [Parameter] public bool HasMaintenanceButton { get; set; } = true;
        [Parameter] public bool HasHelpButton { get; set; } = true;

        [CascadingParameter(Name = "WorkOrders")] public List<WorkOrderViewModel> WorkOrders { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }
        [CascadingParameter] public RouteData routeData { get; set; }

        public HoldButton holdButtonRef { get; set; }
        public Stopwatch CurrentWorkOrderStopwatch { get; set; } = new Stopwatch();

        public string currentCulture { get; set; }
        public string Message { get; set; }
        public ScanInput ScanInputField { get; set; }

        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
            Message = _localizer.ScanWorkOrder;
            int stationId;
            int.TryParse(routeData.RouteValues["stationId"].ToString(), out stationId); 
            Station = await _httpClient.GetJsonAsync<StationViewModel>($"api/station/{stationId}");
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (!firstRender)
            {
                //logged in
                try
                {
                    if (await _localStorage.ContainKeyAsync("currentUser"))
                    {
                        User = await _localStorage.GetItemAsync<UserViewModel>("currentUser");
                        StateHasChanged();
                    }
                    else
                    {
                        _navigationManager.NavigateTo("/");
                    }
                }
                catch (Exception ex)
                { 
                    _navigationManager.NavigateTo("/");
                   _logger.LogError("Couldn't get logged in user, redirecting to login screen");
                }
            }
            //currentCulture = await _javascript.InvokeAsync<string>("getCulture");
        }

        public async Task<string> Logout()
        {
            await _localStorage.RemoveItemAsync("currentUser");
            //TODO; make api call to logout
            _navigationManager.NavigateTo("/");
            return "";
        }

        private async Task WorkOrderScanSuccess(WorkOrderViewModel wo)
        {
            HideHoldScreen();

            try
            {
                //var stationDoneViewModel = new StationDoneViewModel
                //{
                //    WO = WorkOrders.WorkOrder,
                //    ShipmentId = WorkOrders.Shipment,
                //    Station = Station,
                //    UserId = User.Name
                ////};

                //var result = await _httpClient.PostJsonAsync<ToShopFloorViewModel>($"api/station/done", stationDoneViewModel);
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                //await OnError.InvokeAsync(ex);
            }

            finally
            {
                Station = await _httpClient.GetJsonAsync<StationViewModel>($"api/station/{Station.Id}");
            }

            CurrentWorkOrderStopwatch.Reset();
            CurrentWorkOrderStopwatch.Start();
            WorkOrders = null;
            WorkOrders = new List<WorkOrderViewModel>();
            WorkOrders.Add(wo);
            Message = _localizer.ScanWorkOrder;
            StateHasChanged();
        }

        private async Task WorkOrderScanSuccessMultipleOrders(List<WorkOrderViewModel> wos)
        {
            HideHoldScreen();

            try
            {
                //var stationDoneViewModel = new StationDoneViewModel
                //{
                //    WO = WorkOrders.WorkOrder,
                //    ShipmentId = WorkOrders.Shipment,
                //    Station = Station,
                //    UserId = User.Name
                ////};

                //var result = await _httpClient.PostJsonAsync<ToShopFloorViewModel>($"api/station/done", stationDoneViewModel);
            }

            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                //await OnError.InvokeAsync(ex);
            }

            finally
            {
                Station = await _httpClient.GetJsonAsync<StationViewModel>($"api/station/{Station.Id}");
            }

            CurrentWorkOrderStopwatch.Reset();
            CurrentWorkOrderStopwatch.Start();
            WorkOrders = null;
            WorkOrders = new List<WorkOrderViewModel>();
            WorkOrders = wos;
            //Message = _localizer.ScanWorkOrder;
            StateHasChanged();
        }


        private void HoldScreenOpened() => CurrentWorkOrderStopwatch.Stop();

        private void HoldScreenClosed() => CurrentWorkOrderStopwatch.Start();

        private void HideHoldScreen() => holdButtonRef.CloseModal();

        private void Loading() => Message = _localizer.Loading;
         
        private void ErrorScanning()
        {
            WorkOrders = null; 
            Message = _localizer.ErrorScanningOrder; 
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
