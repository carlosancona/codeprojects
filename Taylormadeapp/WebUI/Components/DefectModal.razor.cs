﻿using TMGA.Shared.ViewModels;
using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using TMGA.Domain.Models.Intercept;
using Microsoft.Extensions.Configuration;
using System;
using Microsoft.Extensions.Logging;

namespace WebUI.Components
{
    public partial class DefectModal : ComponentBase
    {
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; } 
        [Inject] IConfiguration _configuration { get; set; }
        [Inject] ILogger<DefectModal> _logger { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [Parameter] public bool IsVisible { get; set; }
        [Parameter] public EventCallback OnClickCancel { get; set; }
        [Parameter] public EventCallback OnOpen { get; set; }
        [Parameter] public EventCallback OnClose { get; set; }
        [Parameter] public RenderFragment ChildContent { get; set; }

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        public HttpClient _httpClient = new HttpClient
        {
            BaseAddress = new System.Uri("http://restfulapi")
        };

        public Timer ForceFocusTimer;
        public UserViewModel CurrentUser;
        public WoComponent DefectiveComponent;

        public List<CellViewModel> Cells = new List<CellViewModel>(); 
        public List<DefectReasonViewModel> EligibleDefects = new List<DefectReasonViewModel>();

        public bool screenOneVisible = true;
        public bool screenTwoVisible = false;
        public bool screenThreeVisible = false;
        public bool screenFourVisible = false;
        public int currentlySelectedStationId;
        public int quantityAffected = 1;
        public float BullseyeXFromSubComponent { get; set; }
        public float BullseyeYFromSubComponent { get; set; }
        public string Message;
        public DefectReasonViewModel defectReason;
        public string WriteOffOrRepair;
        public string stationName;
        public List<string> DefectTypes;

        protected override async Task OnInitializedAsync()
        { 
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
            Cells = await _httpClient.GetJsonAsync<List<CellViewModel>>("api/cell");

            Cells = Cells.Where(_ => _.Site.Id == Convert.ToInt32(_configuration["SITE_ID"])).ToList();

            currentlySelectedStationId = Station?.Id ?? 0;
            await RefreshEligibleStationDefects();

            await OnOpen.InvokeAsync(null);
        }

      public async Task RefreshEligibleStationDefects()
        {
            EligibleDefects = await _httpClient.PostJsonAsync<List<DefectReasonViewModel>>($"api/station/eligibledefects/", currentlySelectedStationId); 
        }

    protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            CurrentUser = await _localStorage.GetItemAsync<UserViewModel>("currentUser");
           
        }

        public async Task SwitchToScreenOne()
        {
            screenOneVisible = true;
            screenTwoVisible = false;
            screenThreeVisible = true;
            screenFourVisible = false;
        }

        public async Task SwitchToScreenTwo()
        {
            screenOneVisible = false;
            screenThreeVisible = false;
            screenFourVisible = false;

            if (!defectReason.DefectReason.Description.ToLower().Contains("hosel"))
            {
                screenTwoVisible = true;

            }
        }

        public async Task SwitchToScreenThree()
        {
            screenOneVisible = false;
            screenTwoVisible = false;
            screenThreeVisible = true;
            screenFourVisible = false;
        }

        public async Task SwitchToScreenFour()
        {
            screenOneVisible = false;
            screenTwoVisible = false;
            screenThreeVisible = false;
            screenFourVisible = true;
        }

        public async Task SetDefectiveComponent(WoComponent component)
        {
            DefectiveComponent = component;
            SwitchToScreenThree();
        }


        public async Task SetWriteOffOrRepair(string writeOffOrRepair)
        {
            WriteOffOrRepair = writeOffOrRepair;
        }


        public async Task SetDefectReason(DefectReasonViewModel newDefectReason)
        {
            defectReason = newDefectReason;
            if (currentlySelectedStationId != null && newDefectReason != null) SwitchToScreenTwo();
        }

        public async Task SetStation(int stationId, string newStationName)
        {
            currentlySelectedStationId = stationId;
            stationName = newStationName;
            await RefreshEligibleStationDefects();
            InvokeAsync(StateHasChanged);
        }

        public async Task CreateDefect()
        {
            var request = new DefectViewModel
            {
                DefectiveSKU = DefectiveComponent.ComponentSku,
                DefectReasonId = defectReason.DefectReasonId,
                StationId = Station.Id,
                WorkOrderId = WorkOrder.WorkOrder,
                WaveId = WorkOrder.WaveId,
                UserId = CurrentUser.Id,
                SiteId = Station.Cell.Site.Id,
                ShipmentId = WorkOrder.Shipment,
                Quantity = quantityAffected,
                WriteOffType = WriteOffOrRepair,
                BullseyeX = 0,
                BullseyeY = 0
            };

            var result = await _httpClient.PostJsonAsync<bool>($"api/defect", request);

            await OnClose.InvokeAsync(null);
        }

        private async Task CloseWindow()
        {
            //bubble up events to HoldButton component
            await OnClose.InvokeAsync(null);
            await OnClickCancel.InvokeAsync(false);
        }
         
        public void IncrementQuantity() => quantityAffected++;

        public void DecrementQuantity() { if (quantityAffected > 1) quantityAffected--; } 
    }
}
