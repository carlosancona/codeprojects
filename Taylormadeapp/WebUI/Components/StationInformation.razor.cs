﻿using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class StationInformation : ComponentBase
    {
        [CascadingParameter] public StationViewModel Station { get; set; }

        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; } 

        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        protected override async Task OnInitializedAsync() => _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
    }
}
