﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class TemperatureGraphComponent
    {
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        public List<CuringTemperatureViewModel> CuringTemperatures { get; set; }
        private Timer RefreshTimer { get; set; }
        private int RefreshIntervalInMs { get; set; } = 5000;
          
        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };

        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);

            RefreshTimer = new Timer((_) =>
            {
                UpdateGraph();
            }, null, 0, RefreshIntervalInMs); 
        }

        public async Task UpdateGraph()
        {
            //api call to receive updated temperature for this station
            var result = await _httpClient.GetJsonAsync<CuringTemperatureViewModel>($"api/iot/temperature/{Station.Id}");
             
            //add to our list (CuringTemperatures)
            CuringTemperatures.Add(result);


            //if our list is too long (n records), truncate old
        }

        public class CuringTemperatureViewModel
        {
            public int Id { get; set; }
            public float Temperature { get; set; }
            public DateTime CreatedOn { get; set; }
        }
    }
}
