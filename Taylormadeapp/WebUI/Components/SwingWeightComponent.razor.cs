﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class SwingWeightComponent : ComponentBase
    {
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();
        [Inject] IConfiguration _configuration { get; set; }

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
        }

        public void CalculateSwingWeights()
        {
            var mapping = new Dictionary<string, int> {
                {"A", 10 },
                {"B", 20 },
                {"C", 30 },
                {"D", 40 },
                {"E", 50 },
                {"F", 60 },
                {"G", 70 },
            };

            var startLetter = WorkOrder.SKUs.FirstOrDefault().SubSKUs.FirstOrDefault().SwingweightCode.Substring(0, 1);
            var endLetter = WorkOrder.SKUs.FirstOrDefault().SubSKUs.FirstOrDefault().SwingweightCode.Substring(0, 1);
            var swingWeightValue = Convert.ToInt32(WorkOrder.SKUs.FirstOrDefault().SubSKUs.FirstOrDefault().SwingweightCode.Substring(1, 5).Trim());

            ////rafa's swing weight algo:

            //@*$sw_start_letter = substr($specs['SW'], 0, 1);

            //$sw_start_value = 0;

            //            if ($sw_start_letter == "A"){$sw_start_value = 10; }
            //            if ($sw_start_letter == "B"){$sw_start_value = 20; }
            //            if ($sw_start_letter == "C"){$sw_start_value = 30; }
            //            if ($sw_start_letter == "D"){$sw_start_value = 40; }
            //            if ($sw_start_letter == "E"){$sw_start_value = 50; }
            //            if ($sw_start_letter == "F"){$sw_start_value = 60; }
            //            if ($sw_start_letter == "G"){$sw_start_value = 70; }

            //$sw_start_value = $sw_start_value + substr($specs['SW'], 1, 1);

            //$sw_end_letter = substr($specs['SW'], 5, 1);

            //$sw_end_value = 0;

            //            if ($sw_end_letter == "A"){$sw_end_value = 10; }
            //            if ($sw_end_letter == "B"){$sw_end_value = 20; }
            //            if ($sw_end_letter == "C"){$sw_end_value = 30; }
            //            if ($sw_end_letter == "D"){$sw_end_value = 40; }
            //            if ($sw_end_letter == "E"){$sw_end_value = 50; }
            //            if ($sw_end_letter == "F"){$sw_end_value = 60; }
            //            if ($sw_end_letter == "G"){$sw_end_value = 70; }

            //$sw_end_value = $sw_end_value + substr($specs['SW'], 6, 1);

            //            if (preg_replace("/[^0-9-+,.]/", "", $specs['SHAFT_ADJUSTMENT']) > 0)
            //            {
            //    $difference = $sw_end_value - $sw_start_value;
            //    $sw_start_value = $sw_end_value;
            //    $sw_end_value = round($sw_end_value + (preg_replace("/[^0-9-+,.]/", "", $specs['SHAFT_ADJUSTMENT']) * 3)) + $difference;
            //            }

            //            if (preg_replace("/[^0-9-+,.]/", "", $specs['SHAFT_ADJUSTMENT']) < 0)
            //            {
            //    $difference = $sw_start_value - $sw_end_value;
            //    $sw_start_value = $sw_start_value + $difference;
            //    $sw_end_value = round($sw_end_value + (preg_replace("/[^0-9-+,.]/", "", $specs['SHAFT_ADJUSTMENT']) * 3)) + $difference;
            //            }

            //$median = round(($sw_start_value + $sw_end_value) / 2); 

            //$final_sw_start_value = substr($sw_start_value, 0, 1);

            //            if ($final_sw_start_value == "1"){$sw_start_value = "A".substr($sw_start_value, 1, 1); }
            //            if ($final_sw_start_value == "2"){$sw_start_value = "B".substr($sw_start_value, 1, 1); }
            //            if ($final_sw_start_value == "3"){$sw_start_value = "C".substr($sw_start_value, 1, 1); }
            //            if ($final_sw_start_value == "4"){$sw_start_value = "D".substr($sw_start_value, 1, 1); }
            //            if ($final_sw_start_value == "5"){$sw_start_value = "E".substr($sw_start_value, 1, 1); }
            //            if ($final_sw_start_value == "6"){$sw_start_value = "F".substr($sw_start_value, 1, 1); }
            //            if ($final_sw_start_value == "7"){$sw_start_value = "G".substr($sw_start_value, 1, 1); }

            //$final_sw_end_value = substr($sw_end_value, 0, 1);

            //            if ($final_sw_end_value == "1"){$sw_end_value = "A".substr($sw_end_value, 1, 1); }
            //            if ($final_sw_end_value == "2"){$sw_end_value = "B".substr($sw_end_value, 1, 1); }
            //            if ($final_sw_end_value == "3"){$sw_end_value = "C".substr($sw_end_value, 1, 1); }
            //            if ($final_sw_end_value == "4"){$sw_end_value = "D".substr($sw_end_value, 1, 1); }
            //            if ($final_sw_end_value == "5"){$sw_end_value = "E".substr($sw_end_value, 1, 1); }
            //            if ($final_sw_end_value == "6"){$sw_end_value = "F".substr($sw_end_value, 1, 1); }
            //            if ($final_sw_end_value == "7"){$sw_end_value = "G".substr($sw_end_value, 1, 1); }

            //$final_sw_target_value = substr($median, 0, 1);

            //$sw_target_value = "0";

            //            if ($final_sw_target_value == "1"){$sw_target_value = "A".substr($median, 1, 1); }
            //            if ($final_sw_target_value == "2"){$sw_target_value = "B".substr($median, 1, 1); }
            //            if ($final_sw_target_value == "3"){$sw_target_value = "C".substr($median, 1, 1); }
            //            if ($final_sw_target_value == "4"){$sw_target_value = "D".substr($median, 1, 1); }
            //            if ($final_sw_target_value == "5"){$sw_target_value = "E".substr($median, 1, 1); }
            //            if ($final_sw_target_value == "6"){$sw_target_value = "F".substr($median, 1, 1); }
            //            if ($final_sw_target_value == "7"){$sw_target_value = "G".substr($median, 1, 1); }

            //$final_sw_target_value = substr($median + 1, 0, 1);


            //$sw_target_value_2 = "0";

            //            if ($final_sw_target_value == "1"){$sw_target_value_2 = "A". (substr($median + 1, 1, 1)); }
            //            if ($final_sw_target_value == "2"){$sw_target_value_2 = "B". (substr($median + 1, 1, 1)); }
            //            if ($final_sw_target_value == "3"){$sw_target_value_2 = "C". (substr($median + 1, 1, 1)); }
            //            if ($final_sw_target_value == "4"){$sw_target_value_2 = "D". (substr($median + 1, 1, 1)); }
            //            if ($final_sw_target_value == "5"){$sw_target_value_2 = "E". (substr($median + 1, 1, 1)); }
            //            if ($final_sw_target_value == "6"){$sw_target_value_2 = "F". (substr($median + 1, 1, 1)); }
            //            if ($final_sw_target_value == "7"){$sw_target_value_2 = "G". (substr($median + 1, 1, 1)); }

            //$final_sw_target_value = substr($median+1,0,1);

            //$sw_target_value_3 = "0";

            //if($final_sw_target_value == "1"){$sw_target_value_3 = "A" . (substr($median+2,1,1));}
            //if($final_sw_target_value == "2"){$sw_target_value_3 = "B" . (substr($median+2,1,1));}
            //if($final_sw_target_value == "3"){$sw_target_value_3 = "C" . (substr($median+2,1,1));}
            //if($final_sw_target_value == "4"){$sw_target_value_3 = "D" . (substr($median+2,1,1));}
            //if($final_sw_target_value == "5"){$sw_target_value_3 = "E" . (substr($median+2,1,1));}
            //if($final_sw_target_value == "6"){$sw_target_value_3 = "F" . (substr($median+2,1,1));}
            //if($final_sw_target_value == "7"){$sw_target_value_3 = "G" . (substr($median+2,1,1));}
            //}

            //if(empty($specs)){

            //	$sw_target_value_2 = "0";
            //	$sw_target_value_3 = "0";

            //}*@
            //}

        }
    }
}
