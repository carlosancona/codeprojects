﻿
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Net.Http;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class StationQueueInformation : ComponentBase
    {
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };

        [CascadingParameter] public StationViewModel Station { get; set; }

        protected override async Task OnInitializedAsync() { 
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
        }

        public async Task Refresh()
        {
           //var x = await _httpClient.GetJsonAsync<StationQueueViewModel>("api/station/queue/{Station.Id}");
        }

    }
}
