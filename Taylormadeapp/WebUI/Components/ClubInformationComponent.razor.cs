﻿
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;
namespace WebUI.Components
{
    public partial class ClubInformationComponent
    {
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        [Inject] IConfiguration _configuration { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };

        [Parameter] public RenderFragment ChildContent { get; set; }
        [Parameter] public EventCallback<bool> OnClickCancel { get; set; }
        [Parameter] public EventCallback OnOpen { get; set; }
        [Parameter] public EventCallback OnClose { get; set; }

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }
         
        public bool IsClosing = false;

        protected async override Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
            await OnOpen.InvokeAsync(null);  
        }

        private async Task CloseWindow()
        {
            IsClosing = true; 
            await InvokeAsync(StateHasChanged); 
            await OnClose.InvokeAsync(null);
            await OnClickCancel.InvokeAsync(false);
            IsClosing = false;
        }


    }
}
