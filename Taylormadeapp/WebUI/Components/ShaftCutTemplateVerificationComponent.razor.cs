﻿using MediatR;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class ShaftCutTemplateVerificationComponent
    {
        [Inject] ShaftCutTemplateVerificationComponentState ShaftCutTemplateVerificationComponentState { get; set; }
        [Inject] ShopFloorState ShopFloorState { get; set; }
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        [Inject] ILogger<SerialCaptureComponent> _logger { get; set; }
        [Inject] IMediator _mediator { get; set; }

        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };
        private I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        private string _shaftCutTemplateTipInput;
        private string shaftCutTemplateTipInput
        {
            get => _shaftCutTemplateTipInput;
            set
            {
                _shaftCutTemplateTipInput = value;
                if(value != null && value?.Length > 0) VerifyTip();
            }
        }


        private string _shaftCutTemplateButtInput;
        private string shaftCutTemplateButtInput
        {
            get => _shaftCutTemplateButtInput;
            set
            {
                _shaftCutTemplateButtInput = value;
                if (value != null && value?.Length > 0) VerifyButt();
            }
        }
         
        public string shaftCutTemplateTipRequired;
        public string shaftCutTemplateButtRequired;

        public bool tipVerified = false;
        public bool buttVerified = false;

        public Timer TipForceFocusTimer;
        public Timer ButtForceFocusTimer;

        string TemplateVerificationMessage;

        protected override async Task OnInitializedAsync()
        {
            if (WorkOrder?.SKUs != null)
            {
                shaftCutTemplateTipRequired = WorkOrder.SKUs.FirstOrDefault().TipTemplate;
                shaftCutTemplateButtRequired = WorkOrder.SKUs.FirstOrDefault().ButtTemplate;
            }


            TipForceFocusTimer = new Timer((_) =>
            {
                ExecuteTipForceFocus();
            }, null, 0, 1000);

           ButtForceFocusTimer = new Timer((_) =>
            {
                ExecuteButtForceFocus();
            }, null, 0, 1000);
             
            ShopFloorState.EnableScanInputForceFocus = false; 
        }

        public async Task ExecuteTipForceFocus() => _javascript.InvokeAsync<object>("RefocusTipTemplateInput");
        public async Task ExecuteButtForceFocus() => _javascript.InvokeAsync<object>("RefocusButtTemplateInput");
         
        public async Task VerifyTip()
        {
            ShaftCutTemplateVerificationComponentState.ShaftCutTemplateTipVerifyBegin();

            try
            {
                if (shaftCutTemplateTipInput == shaftCutTemplateTipRequired)
                {
                    tipVerified = true;
                    TemplateVerificationMessage = $"";
                    ShaftCutTemplateVerificationComponentState.ShaftCutTemplateTipVerifySuccess();
                    ExecuteButtForceFocus();
                } else
                {
                    shaftCutTemplateTipInput = null;
                    TemplateVerificationMessage = $"Invalid tip template. Please scan tip template {WorkOrder.SKUs?.FirstOrDefault().TipTemplate}";
                }
            }

            catch
            { 
                //shaftCutTemplateTipInput = "";
                ShaftCutTemplateVerificationComponentState.ShaftCutTemplateTipVerifyError();
            }

            //shaftCutTemplateTipInput = "";
            ShaftCutTemplateVerificationComponentState.ShaftCutTemplateTipVerifyEnd();
        }

        public async Task VerifyButt()
        {
            ShaftCutTemplateVerificationComponentState.ShaftCutTemplateButtVerifyBegin();

            try
            {
                if (shaftCutTemplateButtInput == shaftCutTemplateButtRequired)
                {
                    TemplateVerificationMessage = "";
                    buttVerified = true;  
                    ShopFloorState.EnableScanInputForceFocus = true;
                    ShaftCutTemplateVerificationComponentState.ShaftCutTemplateButtVerifySuccess();
                } else
                {
                    shaftCutTemplateButtInput = null;
                    TemplateVerificationMessage = $"Invalid butt template. Please scan butt template {WorkOrder.SKUs?.FirstOrDefault().ButtTemplate}";
                }
            }

            catch
            {
                //shaftCutTemplateButtInput = "";
                ShaftCutTemplateVerificationComponentState.ShaftCutTemplateButtVerifyError();
            }

           // shaftCutTemplateButtInput = "";
            ShaftCutTemplateVerificationComponentState.ShaftCutTemplateButtVerifyEnd();
        }


        //broadcasts

        //receiving


        //public methods

        //public members

        //private methods and members
    }
}
