﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebUI.Components
{
    public partial class ZoomableImage : ComponentBase
    {
        [Inject] ShopFloorState AppState { get; set; }
        [Inject] IWebHostEnvironment _environment { get; set; }

        [Parameter] public string Sku { get; set; }
        [Parameter] public string SiteAbbreviation { get; set; }
        [Parameter] public string ComponentType { get; set; } 

        protected HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://imageserver") };

        private List<string> imageFilePaths; 
        public ImagePathViewModel imagePathViewModel;

        private bool isOpen { get; set; } = false;

        protected override async Task OnInitializedAsync()
        {
            AppState.OnWorkOrderNumberChanged += RefreshImage;
            RefreshImage();
        }

        public async void RefreshImage()
        {
            try
            {
                imagePathViewModel = await _httpClient.GetJsonAsync<ImagePathViewModel>($"images/get{ComponentType}image/?sku={Sku}&country={SiteAbbreviation}");
                var path = _environment.ContentRootPath;
            }
            catch
            {
                imagePathViewModel = new ImagePathViewModel();
                imagePathViewModel.image = "";
            }

            StateHasChanged();
        }

        public void ToggleWindow() => isOpen = !isOpen;
    }

}

public class ImagePathViewModel
{
    public string image { get; set; }
}
