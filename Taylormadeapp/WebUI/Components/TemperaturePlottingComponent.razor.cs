﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using Microsoft.AspNetCore.Components;
using System.Net.Http;
using TMGA.Shared.ViewModels;
//using System.Threading;
using System.Timers;

namespace WebUI.Components
{
    public partial class TemperaturePlottingComponent : ComponentBase, IDisposable
    {
        [Inject] IJSRuntime _javascript { get; set; }
        private HttpClient HttpClient;
        private List<TemperatureCuringViewModel> CuringTemperatures { get; set; }
        private bool IsFinished { get; set; } = false;
        private double CurrentTemperature { get; set; } = 0.00;
        private string TemperatureRange { get; set; } = "";
        private int LowerTemperatureBound { get; set; } = 38;
        private int UpperTemperatureBound { get; set; } = 42;

        private List<TemperatureCuringViewModel> temperaturevm { get; set; }
        //private Timer RefreshTimer { get; set; }

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }
        private Timer timer;

        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        private I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);


            HttpClient = new HttpClient
            {
                BaseAddress = new System.Uri("http://restfulapi")
            };

            timer = new Timer();

        }

        public void Dispose()
        {
            if (timer != null)
            {
                timer.Dispose();
            }
        }

        private void StartTimer()
        {
            timer.Interval = 2000;
            timer.Elapsed += TimerOnElapsed;
            timer.Start();
        }

        private void TimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            ShowChart().ConfigureAwait(false);
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                StartTimer();
            }
        }

        public async Task ShowChart()
        {
            Console.WriteLine($"The Elapsed time is: {DateTime.Now:HH:mm:ss.fff}: done.");
            Console.WriteLine($"The current station id is: {Station?.Id}");

            temperaturevm = await HttpClient.GetJsonAsync<List<TemperatureCuringViewModel>>($"api/iot/temperature/getrecenttemperaturefeedbystation/10/{Station.Id}");
            CurrentTemperature = Math.Round(temperaturevm.Last().Temperature, 2);
            await _javascript.InvokeVoidAsync("plotlyFunctions.generateTemperaturePlot", "tempchart", temperaturevm);

            if (temperaturevm.Last().Temperature < LowerTemperatureBound)
            {
                TemperatureRange = "Low";
            }
            else if (temperaturevm.Last().Temperature > UpperTemperatureBound)
            {
                TemperatureRange = "High";
            }
            else
            {
                TemperatureRange = "Nominal";
            }

        }

    }
}
