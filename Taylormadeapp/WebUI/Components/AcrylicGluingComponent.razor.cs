﻿using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;
using System.Net.Http;
using System.Threading.Tasks;
using TMGA.Domain.Models.Intercept;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class AcrylicGluingComponent : ComponentBase
    {
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        [Inject] IConfiguration _configuration { get; set; }
        public I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        public ScanInput ScanInputField { get; set; }
        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };
    
        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
           // ScanInputField.OnSuccess(WorkOrderViewModel vm) += AddToCuringQueue(vm);
        }

        public void AddToCuringQueue()
        {
            var x = new CuringRecordViewModel();
            x.WorkOrder = WorkOrder.WorkOrder;
            x.ClubDescription = WorkOrder.SKUs[0].SkuDescription;
            x.StationId = Station.Id + 1; //fix this hack
            x.Quantity = WorkOrder.ClubCount();
            
            _httpClient.PostAsJsonAsync<CuringRecordViewModel>("api/temperature/createcuringrecordbywo", x);
        }


        //on scan work order, post wo as FromBody string to /api/iot/temperature/createcuringrecordbywo
    }
}
