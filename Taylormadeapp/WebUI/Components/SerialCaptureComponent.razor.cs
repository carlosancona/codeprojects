﻿using MediatR;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Domain.Models.Intercept;
using TMGA.Shared.ViewModels;
using Microsoft.Extensions.Configuration;

namespace WebUI.Components
{
    public partial class SerialCaptureComponent
    {
        [Inject] ShopFloorState AppState { get; set; }
        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }
        [Inject] ILogger<SerialCaptureComponent> _logger { get; set; }
        [Inject] IMediator _mediator { get; set; }
        [Inject] IConfiguration _configuration { get; set; }

        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };
        private I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [CascadingParameter(Name = "WorkOrders")] public List<WorkOrderViewModel> WorkOrders { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        public ScanInput scanInputField { get; set; }
        public int numberOfSerialsRemainingToScan { get; set; }

        private string redOrGreen { get; set; } = "Green";

        private string _scannedSerialNumber;
        private string scannedSerialNumber
        {
            get => _scannedSerialNumber;
            set
            {
                _scannedSerialNumber = value;
                //intercept change event (for barcode scanning)
                if (_scannedSerialNumber?.Length > 1) AddToScannedSerialNumbers();
            }
        }

        public List<string> scannedSerialNumbers { get; set; } = new List<string>();

        private Timer ForceFocusTimer { get; set; }

        public string ScanSerialMessage = "Begin scanning heads";
         
        protected override async Task OnInitializedAsync()
        {
            _localizer = await localizer.GetTextTableAsync<I18nText.Resources>(this);
            ScanSerialMessage = _localizer.BeginScanningHeads;
            RefreshComponent();
            AppState.OnWorkOrderNumberChanged += RefreshComponent;
        }

        public async Task AddToScannedSerialNumbers()
        {
            ////send to api if not previously scanned 
            //ScanSerialMessage = "";
            ////check local cache first
            //if (!scannedSerialNumbers.Contains(_scannedSerialNumber))
            //{
            //    var serialCodeCaptureViewModel = new SerialCodeCaptureViewModel
            //    {
            //        SerialNumber = _scannedSerialNumber,
            //        StationId = Station.Id,
            //        WorkOrderNumber = WorkOrders.First().WorkOrder,
            //        User = User,
            //    };

            //    //then check database
            //    var previouslyExists = await _httpClient.PostJsonAsync<bool>("api/station/serial/check", serialCodeCaptureViewModel);

            //    if (previouslyExists  == false)
            //    {
            //        await _httpClient.PostJsonAsync<object>("api/station/serial", serialCodeCaptureViewModel); 
            //        scannedSerialNumbers.Add(_scannedSerialNumber);
            //        numberOfSerialsRemainingToScan--;
            //        ScanSerialMessage = "Serial number scanned succesfully";
            //    } else
            //    {
            //        ScanSerialMessage = "Serial number already scanned for another work order";
            //    }
            //} else
            //{
            //    ScanSerialMessage = "Serial number already scanned for this work order";
            //}

            //scannedSerialNumber = null;

             
            //new
            ScanSerialMessage = "";

            if (scannedSerialNumbers.Contains(scannedSerialNumber))
            {
                redOrGreen = "Red";
                ScanSerialMessage = "Already scanned";
            }
           else
            { 
                //determine if exists 
                var serialCodeCaptureViewModel = new SerialCodeCaptureViewModel
                {
                    SerialNumber = scannedSerialNumber,
                    StationId = Station.Id,
                    WorkOrderNumber = WorkOrders.First().WorkOrder,
                    User = User,
                }; 

                var previouslyExists = await _httpClient.PostJsonAsync<bool>("api/station/serial/check/any", serialCodeCaptureViewModel);

                if (previouslyExists == false)
                {
                    await _httpClient.PostJsonAsync<object>("api/station/serial", serialCodeCaptureViewModel);
                    scannedSerialNumbers.Add(_scannedSerialNumber);
                    numberOfSerialsRemainingToScan--;
                    redOrGreen = "Green";
                    ScanSerialMessage = _localizer.SerialNumberScannedSuccesfully;
                }
                else
                {
                    redOrGreen = "Red";
                    ScanSerialMessage = _localizer.SerialNumberAlreadyScannedOnAnotherOrder;
                }
            }

            scannedSerialNumber = null;

            if (numberOfSerialsRemainingToScan == 0) Cleanup();

        }


        public void RefreshComponent()
        {
            AppState.EnableScanInputForceFocus = false;

            ForceFocusTimer = new Timer((_) =>
            {
                ExecuteForceFocus();
            }, null, 0, 1000);

            numberOfSerialsRemainingToScan = WorkOrders.Sum(_ => _.ClubCount());
        }

        public async Task ExecuteForceFocus() => _javascript.InvokeAsync<object>("RefocusScannedSerialInput");

        public void Cleanup()
        {
            scannedSerialNumber = null;
            _scannedSerialNumber = null;
            scannedSerialNumbers = new List<string>();
            numberOfSerialsRemainingToScan  = 0;
            AppState.EnableScanInputForceFocus = true;
        }
    }
}
