﻿
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TMGA.Shared.ViewModels;

namespace WebUI.Components
{
    public partial class MaintenanceModal : ComponentBase
    {
        [Inject] ILogger<MaintenanceModal> _logger { get; set; }
        [Inject] ShopFloorState AppState { get; set; }

        [Inject] NavigationManager _navigationManager { get; set; }
        [Inject] Blazored.LocalStorage.ILocalStorageService _localStorage { get; set; }
        [Inject] Toolbelt.Blazor.I18nText.I18nText localizer { get; set; }
        [Inject] IJSRuntime _javascript { get; set; }

        private HttpClient _httpClient = new HttpClient() { BaseAddress = new System.Uri("http://restfulapi") };
        private I18nText.Resources _localizer { get; set; } = new I18nText.Resources();

        [Parameter] public EventCallback<bool> OnClickCancel { get; set; }
        [Parameter] public EventCallback OnOpen { get; set; }
        [Parameter] public EventCallback OnClose { get; set; }

        [CascadingParameter(Name = "WorkOrder")] public WorkOrderViewModel WorkOrder { get; set; }
        [CascadingParameter(Name = "Station")] public StationViewModel Station { get; set; }
        [CascadingParameter(Name = "User")] public UserViewModel User { get; set; }

        private TimeSpan elapsed = new TimeSpan();
        private Timer elapsedTimer;
        private bool isClosing = false;

        public List<AnsweredQuestion> AnsweredQuestions { get; set; } = new List<AnsweredQuestion>();

        public List<MaintenanceQuestionViewModel> MaintenanceQuestions { get; set; }

        protected async override Task OnInitializedAsync()
        {
            await OnOpen.InvokeAsync(null);
            MaintenanceQuestions = await _httpClient.GetJsonAsync<List<MaintenanceQuestionViewModel>>($"api/maintenance/questions");
        }

        public async Task AnswerQuestion(int questionId, bool answer)
        {
            var answeredQuestion = new AnsweredQuestion { QuestionId = questionId, Answer = answer, UserId = User.Id, StationId = Station.Id };

            AnsweredQuestions.Add(answeredQuestion);

            await _httpClient.PostJsonAsync<object>($"api/maintenance", answeredQuestion);

            if (AnsweredQuestions.Count == MaintenanceQuestions.Count) CloseWindow();
        }

        private async Task CloseWindow()
        {
            isClosing = true;

            await InvokeAsync(StateHasChanged);

            //give it a bit for the window to scale out
            //Thread.Sleep(500);

            //bubble up events to HoldButton component
            await OnClose.InvokeAsync(null);
            await OnClickCancel.InvokeAsync(false);
            isClosing = false;
        }


    }
}
