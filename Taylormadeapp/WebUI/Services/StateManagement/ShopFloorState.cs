﻿using System;

namespace WebUI
{
    public class ShopFloorState
    {
        public bool EnableScanInputForceFocus { get; set; } = true;

        public bool IsLoading { get; set; }

        public event EventHandler WorkOrderScanned;
        private string _currentWorkOrderNumber;

        public event Action OnWorkOrderNumberChanged;
        public void WorkOrderNumberChanged() => OnWorkOrderNumberChanged.Invoke(); 
    }

    public class ShaftCutTemplateVerificationComponentState
    {

        public event Action OnShaftCutTemplateTipVerifyBegin;
        public void ShaftCutTemplateTipVerifyBegin() => OnShaftCutTemplateTipVerifyBegin.Invoke();

        public event Action OnShaftCutTemplateTipVerifyEnd;
        public void ShaftCutTemplateTipVerifyEnd() => OnShaftCutTemplateTipVerifyEnd.Invoke();

        public event Action OnShaftCutTemplateTipVerifyMismatch;
        public void ShaftCutTemplateTipVerifyMismatch() => OnShaftCutTemplateTipVerifyMismatch.Invoke();

        public event Action OnShaftCutTemplateTipVerifyError;
        public void ShaftCutTemplateTipVerifyError() => OnShaftCutTemplateTipVerifyError.Invoke();

        public bool TipTemplateVerified = false;
        public event Action OnShaftCutTemplateTipVerifySuccess;
        public void ShaftCutTemplateTipVerifySuccess()
        {
            TipTemplateVerified = true;
            OnShaftCutTemplateTipVerifySuccess.Invoke();
        }


        public event Action OnShaftCutTemplateButtVerifyBegin;
        public void ShaftCutTemplateButtVerifyBegin() => OnShaftCutTemplateButtVerifyBegin.Invoke();

        public event Action OnShaftCutTemplateButtVerifyEnd;
        public void ShaftCutTemplateButtVerifyEnd() => OnShaftCutTemplateButtVerifyEnd.Invoke();

        public event Action OnShaftCutTemplateButtVerifyMismatch;
        public void ShaftCutTemplateButtVerifyMismatch() => OnShaftCutTemplateButtVerifyMismatch.Invoke();

        public event Action OnShaftCutTemplateButtVerifyError;
        public void ShaftCutTemplateButtVerifyError() => OnShaftCutTemplateButtVerifyError.Invoke();

        public bool ButtTemplateVerified = false;
        public event Action OnShaftCutTemplateButtVerifySuccess;

        public void ShaftCutTemplateButtVerifySuccess()
        {
            ButtTemplateVerified = true;
            OnShaftCutTemplateButtVerifySuccess.Invoke();
        } 
    }

}
