using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System.Globalization;

namespace WebUI
{
//test
//test2
    public static class Program
    {
        public static void Main(string[] args) => CreateHostBuilder(args).Build().Run();

        public static IHostBuilder CreateHostBuilder(string[] args)
        {  
            return Host.CreateDefaultBuilder(args)
                       .ConfigureWebHostDefaults(_ => _.UseStartup<Startup>().ConfigureAppConfiguration((hostingContext, x) => { x.AddEnvironmentVariables(); })); 
        }
    }
}