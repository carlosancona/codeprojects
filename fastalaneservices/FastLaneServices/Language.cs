﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FastLaneServices
{
    public class Language
    {
        public class English
        {
            public static string doesntexist = "Pass doesnt exist";
            public static string autorization = "Not authorized";
            public static string asigned = "Pass already assigned";
            public static string asignok = "Pass assigned";
            public static string userexist = "User already exists with this email";
            public static string signupok = "Signup ok";
            public static string signupfail = "Error trying to register";
            public static string loginok = "Login ok";
            public static string loginfail = "Unable to login, verify your credentials";
            public static string userdoesntexist = "User doesnt exist or not verified";
            public static string emailverified = "Email verified";
            public static string verifyproblem = "Verification problem";
            public static string recoverinstructions = "Email sent with the guide to recover your password";
            public static string verifycode = "Verification code";
            public static string newpass = "New Fastlane pass";
            public static string passsent = "Pass sent";
            public static string userNotAllowed = "User does not have access permissions to the application.";

        }
        public class Spanish
        {
            public static string noexiste = "El pase no existe";
            public static string autorizacion = "No autorizado";
            public static string asignado = "El pase fue asignado previamente";
            public static string asignadook = "Se ha asignado satisfactoriamente";
            public static string usuarioexistente = "El usuario ya existe con ese correo electrónico";
            public static string registrook = "Se ha registrado satisfactoriamente";
            public static string registrofail = "Problema con el registro";
            public static string sesionok = "Inicio de sesión correcto";
            public static string sesionfail = "No se pudo iniciar sesión, verifique las credenciales";
            public static string usuarionoexiste = "El usuario no existe o no esta verificado";
            public static string emailverificado = "Correo verificado";
            public static string problemaverificacion = "Problema con la verificación";
            public static string instructionesrecuperacion = "Se le ha enviado un correo con las instrucciones para recuperar su contraseña";
            public static string codigoverificacion = "Código de Verficación";
            public static string pasenuevo = "Pase nuevo FastLane";
            public static string paseenviado = "Pase enviado";
            public static string usuarioNoAutorizado = "Usuario no cuenta con permisos de acceso a la aplicación";
        }
    }
}