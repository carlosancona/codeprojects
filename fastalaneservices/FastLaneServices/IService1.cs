﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FastLaneServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        [WebInvoke(Method = "*", RequestFormat = WebMessageFormat.Json, UriTemplate = "/iniciarsesion", ResponseFormat = WebMessageFormat.Json)]
        Status IniciarSesion(DataSesion datasesion);

        [OperationContract]
        [WebInvoke(Method = "*", RequestFormat = WebMessageFormat.Json, UriTemplate = "/sesioncaseta", ResponseFormat = WebMessageFormat.Json)]
        Status SesionCaseta(DataSesionCaseta dataSesionCaseta);

        [OperationContract]
        [WebInvoke(Method = "*", RequestFormat = WebMessageFormat.Json, UriTemplate = "/registrarse", ResponseFormat = WebMessageFormat.Json)]
        Status RegistrarUsuario(DataRegistrarUsuario dataRegistrarUsuario);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "/recuperarclave/?correo={correo}&lang={lang}", ResponseFormat = WebMessageFormat.Json)]
        Status RecuperarClave(string correo, string lang);

        [OperationContract]
        [WebInvoke(Method = "*", RequestFormat = WebMessageFormat.Json, UriTemplate = "/verificarcorreo", ResponseFormat = WebMessageFormat.Json)]
        Status VerificarCorreo(DataCorreo datacorreo);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "/listapases/?idcliente={idcliente}", ResponseFormat = WebMessageFormat.Json)]
        ListaPases ListaDePases(string idcliente);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "/detallepase/?codigopase={codigopase}", ResponseFormat = WebMessageFormat.Json)]
        DataPase DetallePase(string codigopase);

        [OperationContract]
        [WebInvoke(Method = "*", RequestFormat = WebMessageFormat.Json, UriTemplate = "/asignarpase", ResponseFormat = WebMessageFormat.Json)]
        Status AsignarPase(ClientePaseAsignar clientepaseasignar);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "/listaempresas/?idtipo={idtipo}", ResponseFormat = WebMessageFormat.Json)]
        ListaEmpresas ListaDeEmpresas(int idtipo);

        [OperationContract]
        [WebInvoke(Method = "*", RequestFormat = WebMessageFormat.Json, UriTemplate = "/asignarpasesms", ResponseFormat = WebMessageFormat.Json)]
        Status AsignarPaseSMS(ClientePaseAsignarSMS clientepaseasignar);

        [OperationContract]
        [WebInvoke(Method = "*", UriTemplate = "/listapasesasignados/?idcliente={idcliente}", ResponseFormat = WebMessageFormat.Json)]
        ListaPases ListaDePasesAsignados(string idcliente);
    }

    [DataContract]
    public class Status
    {
        [DataMember]
        public int result;
        [DataMember]
        public string message;
        [DataMember]
        public string token;
        [DataMember]
        public string info;
    }

    [DataContract]
    public class DataSesion
    {
        [DataMember]
        public string correo { get; set; }
        [DataMember]
        public string clave { get; set; }
        [DataMember]
        public string lang { get; set; }
    }

    [DataContract]
    public class DataSesionCaseta
    {
        [DataMember]
        public string usuario { get; set; }
        [DataMember]
        public string clave { get; set; }
    }


    [DataContract]
    public class ClientePaseAsignar
    {
        [DataMember]
        public string codigopase { get; set; }
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public string correo { get; set; }
        [DataMember]
        public string direccion { get; set; }
        [DataMember]
        public string codigopostal { get; set; }
        [DataMember]
        public int idclienteresponsable { get; set; }
        [DataMember]
        public string placas { get; set; }
        [DataMember]
        public string foto { get; set; }
        [DataMember]
        public string fechadecruce { get; set; }
        [DataMember]
        public int numeropasajeros { get; set; }
        [DataMember]
        public int edad { get; set; }
        [DataMember]
        public string genero { get; set; }
        [DataMember]
        public string lang { get; set; }
        [DataMember]
        public string telefono { get; set; }
    }

    [DataContract]
    public class ClientePaseAsignarSMS
    {
        [DataMember]
        public string codigopase { get; set; }
        [DataMember]
        public string telefono { get; set; }
        [DataMember]
        public string lang { get; set; }
    }

    [DataContract]
    public class DataCorreo
    {
        [DataMember]
        public string correo { get; set; }
        [DataMember]
        public int codigo { get; set; }
        [DataMember]
        public string lang { get; set; }
    }

    [DataContract]
    public class DataRegistrarUsuario
    {
        [DataMember]
        public string nombre { get; set; }
        [DataMember]
        public string apellidopaterno { get; set; }
        [DataMember]
        public string apellidomaterno { get; set; }
        [DataMember]
        public string direccion { get; set; }
        [DataMember]
        public string correo { get; set; }
        [DataMember]
        public string clave { get; set; }
        [DataMember]
        public int idgarita { get; set; }
        [DataMember]
        public string razonsocial { get; set; }
        [DataMember]
        public string rfc { get; set; }
        [DataMember]
        public string puesto { get; set; }
        [DataMember]
        public string telefono { get; set; }
        [DataMember]
        public string celular { get; set; }
        [DataMember]
        public string lang { get; set; }
    }

    [DataContract]
    public class ListaEmpresas
    {
        [DataMember]
        public List<string> lista { get; set; }
    }

    [DataContract]
    public class ListaPases
    {
        [DataMember]
        public List<DataPase> lista { get; set; }
    }

        [DataContract]
    public class DataPase
    {
        [DataMember]
        public int idpase { get; set; }
        [DataMember]
        public string codigopase { get; set; }
        [DataMember]
        public string fechacreado { get; set; }
        [DataMember]
        public string estado { get; set; }
        [DataMember]
        public int idcliente { get; set; }
        [DataMember]
        public string nombrecompleto { get; set; }
        [DataMember]
        public string fechacomprado { get; set; }
        [DataMember]
        public string fechaexpiracion { get; set; }
        [DataMember]
        public string placas { get; set; }
        [DataMember]
        public string fechaasignado { get; set; }
        [DataMember]
        public string fechacruce { get; set; }
        [DataMember]
        public int numeropasajeros { get; set; }
        [DataMember]
        public string responsable { get; set; }
        [DataMember]
        public int idclientepase { get; set; }
        [DataMember]
        public string telefono { get; set; }
    }
}
