﻿using Amazon;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Threading;
using System.Net;

namespace FastLaneServices
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        //public string StringConexion = "Data Source=fastlane.db.9692238.6eb.hostedresource.net; Initial Catalog=fastlane; User ID=fastlane; Password=Saguchi1!;";
        //public string StringConexion = "Data Source=sql5050.site4now.net; Initial Catalog=DB_A47A3F_FastLaneDev; User ID=DB_A47A3F_FastLaneDev_admin; Password=p0rt@luser01;";
        public string StringConexion = "Data Source=medlanedb.db.9692238.929.hostedresource.net; Initial Catalog=medlanedb; User ID=medlanedb; Password=Saguchi1!;";

        string token = GetSHA1("carril123");

        public int ObtenerIdPase(string codigopase)
        {
            int idpase = 0;

            string query = "SELECT idpase FROM Pases WHERE codigopase= '" + codigopase + "'";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                idpase = Convert.ToInt16(ds.Tables[0].Rows[0]["idpase"]);
            }
            else
            {
                idpase = 0;
            }

            return idpase;
        }

        public int ObtenerIdClientePase(int idpase)
        {
            int idclientepase = 0;

            string query = "SELECT idclientepase FROM Clientepase WHERE idpase= " + idpase + "";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                idclientepase = Convert.ToInt16(ds.Tables[0].Rows[0]["idclientepase"]);
            }
            else
            {
                idclientepase = 0;
            }

            return idclientepase;
        }

        public bool ValidarPaseNoUsado(int idclientepase)
        {
            bool usado = false;

            string query = "SELECT Count(*) FROM ClientePaseClientes WHERE idclientepase= " + idclientepase + "";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            SqlCommand cmd = new SqlCommand(query, conn);
            int contador = Convert.ToInt32(cmd.ExecuteScalar());

            if (contador > 0)
            {
                usado = true;
            }
            return usado;
        }

        public Status AsignarPase(ClientePaseAsignar clientepaseasignar)
        {
            Status status = new Status();

            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;
            WebOperationContext ctx = WebOperationContext.Current;
            string tokenrecibido = ctx.IncomingRequest.Headers["token"].ToString();


            if (tokenrecibido == token)
            {
                int idpase = ObtenerIdPase(clientepaseasignar.codigopase);

                if (idpase == 0)
                {
                    status.result = 400;

                    if (clientepaseasignar.lang == "es") status.message = Language.Spanish.noexiste;
                    else status.message = Language.English.doesntexist;
                    status.info = "";
                    status.token = "";
                }
                else
                {
                    int idclientepase = ObtenerIdClientePase(idpase);

                    if (ValidarPaseNoUsado(idclientepase))
                    {
                        status.result = 400;
                        if (clientepaseasignar.lang == "es") status.message = Language.Spanish.asignado;
                        else status.message = Language.English.asigned;
                        status.info = "";
                        status.token = "";
                    }
                    else
                    {
                        SqlConnection conn = new SqlConnection();
                        conn.ConnectionString = StringConexion;
                        conn.Open();

                        using (SqlCommand command = conn.CreateCommand())
                        {
                            command.CommandText = "INSERT INTO ClientePaseClientes([idclientepase], [nombre], [placas], [fechadecruce], [fechaasignado], [numeropasajeros], [email], [direccion], [codigopostal], [foto], [edad], [genero],[telefono]) " +
                                "VALUES (" + idclientepase + ", '" + clientepaseasignar.nombre + "', '" + clientepaseasignar.placas + "', '" + clientepaseasignar.fechadecruce + "', '" + clientepaseasignar.fechadecruce + "', '" + clientepaseasignar.numeropasajeros + "', '" + clientepaseasignar.correo + "', '" + clientepaseasignar.direccion + "', '" + clientepaseasignar.codigopostal + "', '" + clientepaseasignar.foto + "', " + clientepaseasignar.edad + ", '" + clientepaseasignar.genero + "', '" + clientepaseasignar.telefono + "')";
                            command.ExecuteNonQuery();

                            status.result = 200;
                            if (clientepaseasignar.lang == "es") status.message = Language.Spanish.asignadook;
                            else status.message = Language.English.asignok;
                            status.info = "";
                            status.token = "";

                            enviarCorreoAsignarPase(clientepaseasignar.codigopase, clientepaseasignar.nombre, clientepaseasignar.correo, clientepaseasignar.fechadecruce, clientepaseasignar.fechadecruce, clientepaseasignar.numeropasajeros, clientepaseasignar.lang);

                        }
                        conn.Close();
                    }
                }
            }
            else
            {
                status.result = 400;
                if (clientepaseasignar.lang == "es") status.message = Language.Spanish.autorizacion;
                else status.message = Language.English.autorization;
                status.info = "";
                status.token = "";
            }

            return status;
        }

        public Status AsignarPaseSMS(ClientePaseAsignarSMS clientepaseasignar)
        {
            Status status = new Status();

            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;
            WebOperationContext ctx = WebOperationContext.Current;
            string tokenrecibido = ctx.IncomingRequest.Headers["token"].ToString();

            if (tokenrecibido == token)
            {
                int idpase = ObtenerIdPase(clientepaseasignar.codigopase);

                if (idpase == 0)
                {
                    status.result = 400;
                    if (clientepaseasignar.lang == "es") status.message = Language.Spanish.noexiste;
                    else status.message = Language.English.doesntexist;
                    status.info = "";
                    status.token = "";
                }
                else
                {
                    int idclientepase = ObtenerIdClientePase(idpase);

                    SqlConnection conn = new SqlConnection();
                    conn.ConnectionString = StringConexion;
                    conn.Open();

                    string query = "Select * from ClientePaseClientes where idclientepase=" + idclientepase + "";
                    DataSet ds = new DataSet();
                    SqlCommand cmd = new SqlCommand(query, conn);
                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(ds);
                    conn.Close();

                    string mensaje = "Haz recibido un nuevo pase FastLane: " + clientepaseasignar.codigopase + " a nombre de " + ds.Tables[0].Rows[0]["nombre"].ToString() + " - tu fecha de cruce es " + ds.Tables[0].Rows[0]["fechadecruce"];

                    status.result = 200;
                    if (clientepaseasignar.lang == "es") status.message = Language.Spanish.paseenviado;
                    else status.message = Language.English.passsent;
                    status.info = "";
                    status.token = "";

                    try
                    {
                        Thread myNewThread = new Thread(() => amazonSMS(mensaje, clientepaseasignar.telefono));
                        myNewThread.Start();
                    }
                    catch
                    {

                    }
                }
            }
            else
            {
                status.result = 400;
                if (clientepaseasignar.lang == "es") status.message = Language.Spanish.autorizacion;
                else status.message = Language.English.autorization;
                status.info = "";
                status.token = "";
            }

            return status;
        }

        public void amazonSMS(string mensaje, string phone)
        {

            AmazonSimpleNotificationServiceClient smsClient = new AmazonSimpleNotificationServiceClient("AKIAJOTSBGBXSMVEOUJQ", "r5NEER2MGrTAuSPOXh8EhiFbm8AFST6C9Z1mFpSK", RegionEndpoint.USWest2);

            MessageAttributeValue sMSType = new MessageAttributeValue();
            sMSType.DataType = "String";
            sMSType.StringValue = "Promotional";

            var smsAttributes = new Dictionary<string, MessageAttributeValue>();
            smsAttributes.Add("AWS.SNS.SMS.SMSType", sMSType);

            PublishRequest publishRequest = new PublishRequest();
            publishRequest.Message = mensaje;
            publishRequest.MessageAttributes = smsAttributes;
            publishRequest.PhoneNumber = phone;

            PublishResponse result = smsClient.Publish(publishRequest);
        }


        public DataPase DetallePase(string codigopase)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;

            DataPase dataPase = new DataPase();

            WebOperationContext ctx = WebOperationContext.Current;
            string tokenrecibido = ctx.IncomingRequest.Headers["token"].ToString();

            if (tokenrecibido == token)
            {
                //   string query = "SELECT Pases.idpase, Pases.codigopase, Pases.fechacreado, Estados.nombre AS estado FROM Estados INNER JOIN " +
                //"Pases ON Estados.idestado = Pases.idestado WHERE(Pases.codigopase = '" + codigopase + "')";

                string query =
                    @"SELECT 
	                    Pases.idpase, 
	                    Pases.codigopase, 
	                    Pases.fechacreado, 
	                    Estados.nombre AS estado 
                    FROM Estados 
	                    INNER JOIN Pases ON Estados.idestado = Pases.idestado 
                    WHERE 
	                    Pases.codigopase = '" + codigopase + "'";


                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = StringConexion;
                conn.Open();

                DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
                conn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    dataPase.idpase = Convert.ToInt16(ds.Tables[0].Rows[0]["idpase"]);
                    dataPase.fechacreado = ds.Tables[0].Rows[0]["fechacreado"].ToString();
                    dataPase.estado = ds.Tables[0].Rows[0]["estado"].ToString();
                    dataPase.codigopase = codigopase;

                    //query = "SELECT ClientePase.idclientepase, ClientePase.idpase, ClientePase.fechacomprado, ClientePase.fechaexpiracion, Clientes.nombre, Clientes.apellidopaterno, Clientes.apellidomaterno " +
                    //    "FROM ClientePase INNER JOIN " +
                    //    "Clientes ON ClientePase.idcliente = Clientes.idcliente INNER JOIN " +
                    //    "Pases ON ClientePase.idpase = Pases.idpase " +
                    //    "WHERE (Pases.codigopase = '" + codigopase + "')";

                    query =
                        @"SELECT 
	                        ClientePase.idclientepase, 
	                        ClientePase.idpase, 
	                        ClientePase.fechacomprado, 
	                        ClientePase.fechaexpiracion, 
	                        Clientes.nombre, 
	                        Clientes.apellidopaterno, 
	                        Clientes.apellidomaterno 
                        FROM ClientePase 
	                        INNER JOIN ClienteSucursal ON ClientePase.idclientesucursal = ClienteSucursal.idclientesucursal 
	                        INNER JOIN Clientes ON ClienteSucursal.idcliente = Clientes.idcliente 
	                        INNER JOIN Pases ON ClientePase.idpase = Pases.idpase 
                        WHERE 
	                        Pases.codigopase = '" + codigopase + "'";

                    conn.Open();
                    DataSet ds2 = new DataSet();
                    SqlCommand cmd2 = new SqlCommand(query, conn);
                    SqlDataAdapter adapter2 = new SqlDataAdapter(cmd2);
                    adapter2.Fill(ds2);
                    conn.Close();

                    if (ds2.Tables[0].Rows.Count > 0)
                    {

                        dataPase.responsable = ds2.Tables[0].Rows[0]["nombre"].ToString() + " " + ds2.Tables[0].Rows[0]["apellidopaterno"].ToString() + " " + ds2.Tables[0].Rows[0]["apellidomaterno"].ToString();
                        dataPase.idclientepase = Convert.ToInt16(ds2.Tables[0].Rows[0]["idclientepase"]);
                        dataPase.fechacomprado = ds2.Tables[0].Rows[0]["fechacomprado"].ToString();

                        //query = "SELECT ClientePaseClientes.telefono, ClientePaseClientes.fechadecruce, ClientePaseClientes.numeropasajeros, ClientePaseClientes.placas, " +
                        //    "ClientePaseClientes.fechaasignado, ClientePaseClientes.nombre " +
                        //    "FROM ClientePaseClientes INNER JOIN " +
                        //    "ClientePase ON ClientePaseClientes.idclientepase = ClientePase.idclientepase INNER JOIN " +
                        //    "Pases ON ClientePase.idpase = Pases.idpase WHERE Pases.codigopase = '" + codigopase + "'";

                        query =
                            @"SELECT 
	                            ClientePaseClientes.telefono, 
	                            ClientePaseClientes.fechadecruce, 
	                            ClientePaseClientes.numeropasajeros, 
	                            ClientePaseClientes.placas,
	                            ClientePaseClientes.fechaasignado, 
	                            ClientePaseClientes.nombre
                            FROM ClientePaseClientes 
	                            INNER JOIN ClientePase ON ClientePaseClientes.idclientepase = ClientePase.idclientepase 
	                            INNER JOIN Pases ON ClientePase.idpase = Pases.idpase 
                            WHERE Pases.codigopase = '" + codigopase + "'";

                        conn.Open();
                        DataSet ds3 = new DataSet();
                        SqlCommand cmd3 = new SqlCommand(query, conn);
                        SqlDataAdapter adapter3 = new SqlDataAdapter(cmd3);
                        adapter3.Fill(ds3);
                        conn.Close();

                        if (ds3.Tables[0].Rows.Count > 0)
                        {
                            dataPase.nombrecompleto = ds3.Tables[0].Rows[0]["nombre"].ToString();
                            dataPase.placas = ds3.Tables[0].Rows[0]["placas"].ToString();
                            dataPase.fechacruce = ds3.Tables[0].Rows[0]["fechadecruce"].ToString();
                            dataPase.fechaasignado = ds3.Tables[0].Rows[0]["fechaasignado"].ToString();
                            dataPase.telefono = ds3.Tables[0].Rows[0]["telefono"].ToString();
                            dataPase.numeropasajeros = Convert.ToInt16(ds3.Tables[0].Rows[0]["numeropasajeros"]);

                            dataPase.fechaexpiracion = "";

                            int totaldays = (Convert.ToDateTime(dataPase.fechaasignado) - DateTime.Today).Days;
                            if (totaldays <-14)
                            {
                                query = "UPDATE PASES SET IDESTADO=3 WHERE CODIGOPASE='" + dataPase.codigopase + "'";
                                conn.Open();
                                cmd = new SqlCommand(query, conn);
                                int row = cmd.ExecuteNonQuery();
                                conn.Close();
                                dataPase.estado="Expirado";
                            }
                        }
                        else
                        {
                            dataPase.idcliente = 0;
                            dataPase.nombrecompleto = "N/A";
                            dataPase.placas = "";
                            dataPase.fechacruce = "";
                            dataPase.fechaasignado = "";
                            dataPase.numeropasajeros = 0;
                        }
                    }
                    else
                    {
                        dataPase.responsable = "N/A";
                        dataPase.fechacomprado = "";
                        dataPase.fechaexpiracion = "";
                    }
                }
                else
                {
                    dataPase.idpase = 0;
                    dataPase.fechacreado = "";
                    dataPase.estado = "N/A";
                    dataPase.codigopase = "N/A";
                    dataPase.idcliente = 0;
                    dataPase.nombrecompleto = "";
                    dataPase.placas = "";
                    dataPase.fechacruce = "";
                    dataPase.fechaasignado = "";
                    dataPase.numeropasajeros = 0;
                    dataPase.responsable = "";
                    dataPase.fechacomprado = "";
                    dataPase.fechaexpiracion = "";
                }
            }

            return dataPase;
        }

        public ListaPases ListaDePasesAsignados(string idcliente)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;

            ListaPases listaDePases = new ListaPases();
            listaDePases.lista = new List<DataPase>();

            WebOperationContext ctx = WebOperationContext.Current;
            string tokenrecibido = ctx.IncomingRequest.Headers["token"].ToString();

            if (tokenrecibido == token)
            {
                //string query = "SELECT Pases.codigopase, Pases.fechacreado, Estados.nombre AS estado, Clientes.nombre, Clientes.apellidopaterno, Clientes.apellidomaterno, Clientes.idcliente, ClientePase.idpase, ClientePase.idclientepase, ClientePase.fechacomprado, " +
                //    "ClientePase.fechaexpiracion FROM ClientePase INNER JOIN Clientes ON ClientePase.idcliente = Clientes.idcliente INNER JOIN Pases ON ClientePase.idpase = Pases.idpase INNER JOIN " +
                //    "Estados ON Pases.idestado = Estados.idestado WHERE Clientes.idcliente = '" + idcliente + "' and Pases.idestado=1";

                string query =
                    @"SELECT 
	                    Pases.codigopase, 
	                    Pases.fechacreado, 
	                    Estados.nombre AS estado, 
	                    Clientes.nombre, 
	                    Clientes.apellidopaterno, 
	                    Clientes.apellidomaterno, 
	                    Clientes.idcliente, 
	                    ClientePase.idpase, 
	                    ClientePase.idclientepase, 
	                    ClientePase.fechacomprado, 
	                    ClientePase.fechaexpiracion 
                    FROM ClientePase 
	                    INNER JOIN ClienteSucursal ON ClienteSucursal.idclientesucursal = ClientePase.idclientesucursal
	                    INNER JOIN Clientes ON ClienteSucursal.idcliente = Clientes.idcliente 
	                    INNER JOIN Pases ON ClientePase.idpase = Pases.idpase 
	                    INNER JOIN Estados ON Pases.idestado = Estados.idestado 
                    WHERE 
	                    ClienteSucursal.idclientesucursal = '" + idcliente + "' AND " +
	                    "Pases.idestado='1'";


                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = StringConexion;
                conn.Open();

                DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
                conn.Close();

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataPase dataPase = new DataPase();
                    dataPase.idpase = Convert.ToInt16(ds.Tables[0].Rows[i]["idpase"]);
                    dataPase.idcliente = Convert.ToInt16(ds.Tables[0].Rows[i]["idcliente"]);
                    dataPase.codigopase = ds.Tables[0].Rows[i]["codigopase"].ToString();
                    dataPase.fechacreado = ds.Tables[0].Rows[i]["fechacreado"].ToString();
                    dataPase.estado = ds.Tables[0].Rows[i]["estado"].ToString();
                    dataPase.responsable = ds.Tables[0].Rows[i]["nombre"].ToString() + " " + ds.Tables[0].Rows[0]["apellidopaterno"].ToString() + " " + ds.Tables[0].Rows[0]["apellidomaterno"].ToString();
                    dataPase.fechacomprado = ds.Tables[0].Rows[i]["fechacomprado"].ToString();


                    int idclientepase = Convert.ToInt16(ds.Tables[0].Rows[i]["idclientepase"]);

                    try
                    {
                        conn.Open();
                        string query2 = "Select * from ClientePaseClientes where idclientepase=" + idclientepase + "";
                        DataSet ds2 = new DataSet();
                        SqlCommand cmd2 = new SqlCommand(query2, conn);
                        SqlDataAdapter adapter2 = new SqlDataAdapter(cmd2);
                        adapter2.Fill(ds2);
                        conn.Close();

                        DateTime expiracion = Convert.ToDateTime(ds2.Tables[0].Rows[0]["fechadecruce"]);
                        expiracion = expiracion.AddHours(48);
                        dataPase.fechaexpiracion = "" + expiracion;

                        dataPase.placas = ds2.Tables[0].Rows[0]["placas"].ToString();
                        dataPase.fechacruce = ds2.Tables[0].Rows[0]["fechadecruce"].ToString();
                        dataPase.fechaasignado = ds2.Tables[0].Rows[0]["fechaasignado"].ToString();
                        dataPase.numeropasajeros = Convert.ToInt16(ds2.Tables[0].Rows[0]["numeropasajeros"]);
                    }
                    catch
                    {

                    }

                    if (ValidarPaseAsignado(idclientepase))
                    {
                        listaDePases.lista.Add(dataPase);
                    }

                }
            }

            return listaDePases;
        }


        public ListaPases ListaDePases(string idcliente)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;

            ListaPases listaDePases = new ListaPases();
            listaDePases.lista = new List<DataPase>();

            WebOperationContext ctx = WebOperationContext.Current;
            string tokenrecibido = ctx.IncomingRequest.Headers["token"].ToString();

            if (tokenrecibido == token)
            {
                //string query = "SELECT Pases.codigopase, Pases.fechacreado, Estados.nombre AS estado, Clientes.nombre, Clientes.apellidopaterno, Clientes.apellidomaterno, Clientes.idcliente, ClientePase.idpase, ClientePase.idclientepase, ClientePase.fechacomprado, " +
                //    "ClientePase.fechaexpiracion FROM ClientePase INNER JOIN Clientes ON ClientePase.idcliente = Clientes.idcliente INNER JOIN Pases ON ClientePase.idpase = Pases.idpase INNER JOIN " +
                //    "Estados ON Pases.idestado = Estados.idestado WHERE Clientes.idcliente = '" + idcliente + "' and Pases.idestado=1";

                string query =
                        @"SELECT 
                            p.codigopase, 
	                        p.fechacreado, 
	                        e.nombre AS estado, 
	                        c.nombre, 
	                        c.apellidopaterno, 
	                        c.apellidomaterno, 
	                        cp.idclientesucursal, 
	                        cp.idpase, 
	                        cp.idclientepase, 
	                        cp.fechacomprado,
                            cp.fechaexpiracion
                        FROM
                            ClientePase cp
                            INNER JOIN ClienteSucursal cs ON cp.idclientesucursal = cs.idclientesucursal
                            INNER JOIN Clientes c ON cs.idcliente = c.idcliente
                            INNER JOIN Pases p ON cp.idpase = p.idpase
                            INNER JOIN Estados e ON p.idestado = e.idestado
                        WHERE
                            cs.idclientesucursal =" + idcliente +
                            "AND p.idestado = 1";

                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = StringConexion;
                conn.Open();

                DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
                conn.Close();

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    DataPase dataPase = new DataPase();
                    dataPase.idpase = Convert.ToInt16(ds.Tables[0].Rows[i]["idpase"]);
                    dataPase.idcliente = Convert.ToInt16(ds.Tables[0].Rows[i]["idclientesucursal"]);
                    dataPase.codigopase = ds.Tables[0].Rows[i]["codigopase"].ToString();
                    dataPase.fechacreado = ds.Tables[0].Rows[i]["fechacreado"].ToString();
                    dataPase.estado = ds.Tables[0].Rows[i]["estado"].ToString();
                    dataPase.responsable = ds.Tables[0].Rows[i]["nombre"].ToString() + " " + ds.Tables[0].Rows[0]["apellidopaterno"].ToString() + " " + ds.Tables[0].Rows[0]["apellidomaterno"].ToString();
                    dataPase.fechacomprado = ds.Tables[0].Rows[i]["fechacomprado"].ToString();
                    dataPase.fechaexpiracion = ds.Tables[0].Rows[i]["fechaexpiracion"].ToString();
                    dataPase.placas = "N/A";
                    dataPase.fechacruce = "N/A";
                    dataPase.fechaasignado = "N/A";
                    dataPase.responsable = "N/A";
                    dataPase.numeropasajeros = 0;

                    int idclientepase = Convert.ToInt16(ds.Tables[0].Rows[i]["idclientepase"]);
                    dataPase.idclientepase = idclientepase;
                    if (!ValidarPaseAsignado(idclientepase))
                    {
                        listaDePases.lista.Add(dataPase);
                    }

                }
            }

            return listaDePases;
        }

        public ListaEmpresas ListaDeEmpresas(int idtipo)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;

            ListaEmpresas listaEmpresas = new ListaEmpresas();
            listaEmpresas.lista = new List<string>();

            WebOperationContext ctx = WebOperationContext.Current;
            string tokenrecibido = ctx.IncomingRequest.Headers["token"].ToString();

            if (tokenrecibido == token)
            {
                string query = "SELECT nombrecomercial FROM Clientes WHERE idtipocliente =" + idtipo + "";
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = StringConexion;
                conn.Open();

                DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ds);
                conn.Close();

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    listaEmpresas.lista.Add(ds.Tables[0].Rows[i]["nombrecomercial"].ToString());
                }
            }

            return listaEmpresas;
        }

        public bool ValidarPaseAsignado(int idclientepase)
        {
            bool asignado = false;

            string query = "Select count(*) from ClientePaseClientes where idclientepase=" + idclientepase + "";

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            int contador = Convert.ToInt16(cmd.ExecuteScalar());
            conn.Close();

            if (contador > 0) asignado = true;

            return asignado;
        }

        //Metodo descartado
        public Status RegistrarUsuario(DataRegistrarUsuario dataRegistrarUsuario)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;

            Status status = new Status();

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();
            string query = "SELECT COUNT (*) FROM clientes WHERE correo='" + dataRegistrarUsuario.correo + "'";
            SqlCommand cmd = new SqlCommand(query, conn);
            int existe = Convert.ToInt16(cmd.ExecuteScalar());
            conn.Close();

            int codigo = GenerateRandomNo();

            if (existe > 0)
            {
                status.result = 400;
                if (dataRegistrarUsuario.lang == "es") status.message = Language.Spanish.usuarioexistente;
                else status.message = Language.English.userexist;
                status.info = "";
                status.token = "";
            }
            else
            {
                try
                {
                    conn.Open();
                    dataRegistrarUsuario.clave = GetSHA1(dataRegistrarUsuario.clave + "fastlane1258");
                    using (SqlCommand command = conn.CreateCommand())
                    {
                        command.CommandText = "INSERT INTO Clientes([nombre], [apellidopaterno], [apellidomaterno], [direccion], [correo]," +
                            " [clave], [idrol], [idgarita], [razonsocial], [rfc], [puesto], [telefono], [celular], [codigo]) " +
                            "VALUES ('" + dataRegistrarUsuario.nombre + "', '" + dataRegistrarUsuario.apellidopaterno + "', '" + dataRegistrarUsuario.apellidomaterno + "', '" + dataRegistrarUsuario.direccion + "', " +
                            "'" + dataRegistrarUsuario.correo + "','" + dataRegistrarUsuario.clave + "', 3, '" + dataRegistrarUsuario.idgarita + "', " +
                            "'" + dataRegistrarUsuario.razonsocial + "', '" + dataRegistrarUsuario.rfc + "', '" + dataRegistrarUsuario.puesto + "'," +
                            "'" + dataRegistrarUsuario.telefono + "', '" + dataRegistrarUsuario.celular + "', " + codigo + ")";
                        command.ExecuteNonQuery();
                        status.result = 200;
                        if (dataRegistrarUsuario.lang == "es") status.message = Language.Spanish.registrook;
                        else status.message = Language.English.signupok;
                        status.info = "";
                        status.token = "";

                        enviarCodigoVerificacion(dataRegistrarUsuario.correo, codigo, dataRegistrarUsuario.lang);
                    }
                    conn.Close();
                }
                catch
                {
                    status.result = 400;
                    if (dataRegistrarUsuario.lang == "es") status.message = Language.Spanish.registrofail;
                    else status.message = Language.English.signupfail;
                    status.info = JsonConvert.SerializeObject(dataRegistrarUsuario);
                    status.token = "";
                }
            }
            return status;
        }

        public Status IniciarSesion(DataSesion datasesion)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;
            // WebOperationContext ctx = WebOperationContext.Current;
            //  string tokenrecibido= ctx.IncomingRequest.Headers["bearer"].ToString();

            Status status = new Status();
            string query = "SELECT * FROM Usuarios WHERE correo='" + datasesion.correo + "' and activo=1";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                string claverecibida = GetSHA1(datasesion.clave + "fastlane1258");
                string clavebd = ds.Tables[0].Rows[0]["clave"].ToString();
                if (claverecibida == clavebd)
                {
                    status.result = 200;
                    status.token = token;
                    status.message = "Inicio de sesión correcto";
                    if (datasesion.lang == "es") status.message = Language.Spanish.sesionok;
                    else status.message = Language.English.loginok;
                    status.info = ds.Tables[0].Rows[0]["idclientesucursal"].ToString();
                    if(status.info == string.Empty)
                    {
                        status.result = 400;
                        status.token = "";
                        if (datasesion.lang == "es") status.message = Language.Spanish.usuarioNoAutorizado;
                        else status.message = Language.English.userNotAllowed;
                        status.info = "";
                    }
                }
                else
                {
                    status.result = 400;
                    status.token = "";
                    if (datasesion.lang == "es") status.message = Language.Spanish.sesionfail;
                    else status.message = Language.English.loginfail;
                    status.info = "";
                }
            }
            else
            {
                status.result = 400;
                status.token = "";
                if (datasesion.lang == "es") status.message = Language.Spanish.usuarionoexiste;
                else status.message = Language.English.userdoesntexist;
                status.info = "";
            }
            return status;
        }

        public Status SesionCaseta(DataSesionCaseta dataSesionCaseta)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;
            // WebOperationContext ctx = WebOperationContext.Current;
            //  string tokenrecibido= ctx.IncomingRequest.Headers["bearer"].ToString();

            Status status = new Status();
            string query = "SELECT * FROM Seguridad WHERE usuario='" + dataSesionCaseta.usuario + "' and clave='" + dataSesionCaseta.clave + "' and activo=1";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                status.result = 200;
                status.token = token;
                status.message = "Inicio de sesión correcto";
                status.info = ds.Tables[0].Rows[0]["idseguridad"].ToString();
            }
            else
            {
                status.result = 400;
                status.token = "";
                status.message = "No se pudo iniciar sesión, verifique las credenciales";
                status.info = "";
            }
            return status;
        }

        public Status VerificarCorreo(DataCorreo datacorreo)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;
            // WebOperationContext ctx = WebOperationContext.Current;
            // string tokenrecibido= ctx.IncomingRequest.Headers["bearer"].ToString();

            Status status = new Status();
            string query = "SELECT * FROM clientes WHERE correo='" + datacorreo.correo + "' and codigo=" + datacorreo.codigo + "";
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;
            conn.Open();

            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(ds);
            conn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                try
                {
                    conn.Open();
                    using (SqlCommand command = conn.CreateCommand())
                    {
                        command.CommandText = "UPDATE Clientes SET ACTIVO=1 WHERE CORREO='" + datacorreo.correo + "'";

                        command.ExecuteNonQuery();
                        status.result = 200;
                        status.token = "";
                        if (datacorreo.lang == "es") status.message = Language.Spanish.emailverificado;
                        else status.message = Language.English.emailverified;
                        status.info = "";

                    }
                    conn.Close();

                }
                catch
                {
                    status.result = 400;
                    if (datacorreo.lang == "es") status.message = Language.Spanish.problemaverificacion;
                    else status.message = Language.English.verifyproblem;
                    status.info = "";
                    status.token = "";
                }
            }
            else
            {
                status.result = 400;
                status.token = "";
                if (datacorreo.lang == "es") status.message = Language.Spanish.autorizacion;
                else status.message = Language.English.autorization;
                status.info = "";
            }
            return status;
        }

        public static string GetSHA1(string str)
        {
            SHA1 sha1 = SHA1Managed.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha1.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++)
                sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }

        public Status RecuperarClave(string email, string lang)
        {
            if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
                return null;
            Status status = new Status();

            Random rnd = new Random();
            int code = rnd.Next(0, 9999);
            string clave = GetSHA1(email + code);

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = StringConexion;

            conn.Open();
            string query = "SELECT COUNT(*) FROM Usuarios  WHERE correo='" + email + "' AND ACTIVO=1";
            SqlCommand command2 = new SqlCommand(query, conn);
            int existe = Convert.ToInt16(command2.ExecuteScalar());
            conn.Close();

            if (existe == 0)
            {
                status.result = 400;
                status.token = "";
                if (lang == "es") status.message = Language.Spanish.usuarionoexiste;
                else status.message = Language.English.userdoesntexist;
                status.info = "";
            }
            else
            {
                try
                {
                    conn.Open();
                    using (SqlCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "INSERT INTO recuperar ([codigo],[correo]) VALUES (@codigo,@correo)";

                        cmd.Parameters.AddRange(new SqlParameter[]
                                 {
                                  new  SqlParameter("@codigo", clave),
                                  new  SqlParameter("@correo", email)
                                 });
                        cmd.ExecuteNonQuery();
                        enviarCorreo(clave, email, lang);

                        status.result = 200;
                        status.token = "";
                        if (lang == "es") status.message = Language.Spanish.instructionesrecuperacion;
                        else status.message = Language.English.recoverinstructions;
                        status.info = "";
                    }
                    conn.Close();
                }
                catch
                { }
            }
            return status;
        }

        public void enviarCorreo(string clave, string email, string lang)
        {   //Aqui va el link de la pagina a la cual se va a redirigir para recuperar su contraseña
            string cadenaURL = "http://" + clave + "";
            string mensaje;

            if (lang == "es") mensaje = "Hola, \n\n Si deseas recuperar tu contraseña para Medlane, por favor sigue la siguiente liga:\n\n " + cadenaURL + " \n\n Si no realizaste la petición, puedes ignorar este mensaje, probablemente alguien escribió tu correo por accidente. \n\n Muchas gracias, \n El equipo de Medlane";
            else mensaje = "Hello, \n\n If you wish to recover your Medlane password, please clic in the next link:\n\n " + cadenaURL + " \n\n If you dont made this request, please ignore this email, probably someone write your email accidentally. \n\n Thank you, \n Medlane team";

            MailMessage message = new MailMessage();
            message.From = new MailAddress("contacto@medlane.mx", "Medlane");

            message.To.Add(new MailAddress(email, email));

            if (lang == "es") message.Subject = "Recuperación de Contraseña Medlane";
            else message.Subject = "Recover Medlane password";

            message.Body = mensaje;
            message.IsBodyHtml = false;

            SmtpClient sc = new SmtpClient();
            //sc.Host = "email-smtp.us-west-2.amazonaws.com";
            sc.Host = "relay-hosting.secureserver.net";
            sc.Port = 25;
            sc.UseDefaultCredentials = true;
            sc.EnableSsl = false; // runtime encrypt the SMTP communications using SSL
            sc.DeliveryMethod = SmtpDeliveryMethod.Network;
            sc.Send(message);
        }

        public void enviarCorreoAsignarPase(string pase, string nombre, string email, string fechacruce, string fechaasignado, int pasajeros, string lang)
        {   //Aqui va el link de la pagina a la cual se va a redirigir para recuperar su contraseña
            string mensaje;

            if (lang == "es")
                mensaje = "Hola, \n\n Haz recibido un nuevo pase Medlane: " +
                "\n\n Pase:" + pase + " \n\n  " +
                "\n\n Nombre:" + nombre + " \n\n  " +
                "\n\n Fecha de cruce:" + fechacruce + " \n\n  " +
                "\n\n Fecha asginado:" + fechaasignado + " \n\n  " +
                "\n\n Pasajeros:" + pasajeros + " \n\n  " +

                "\n\n Muchas gracias, \n El equipo de Medlane";
            else
                mensaje = "Hello, \n\n You received a new Medlane pass: " +
                "\n\n Pass:" + pase + " \n\n  " +
                "\n\n Name:" + nombre + " \n\n  " +
                "\n\n Cross date:" + fechacruce + " \n\n  " +
                "\n\n Assigned date:" + fechaasignado + " \n\n  " +
                "\n\n Passengers:" + pasajeros + " \n\n  " +

                "\n\n Thank you, \n Medlane team";


            MailMessage message = new MailMessage();
            message.From = new MailAddress("no-reply@lpcoders.com", "Medlane");
            message.To.Add(new MailAddress(email, email));

            if (lang == "es") message.Subject = Language.Spanish.pasenuevo;
            else message.Subject = Language.English.newpass;
            message.Body = mensaje;
            message.IsBodyHtml = false;

            SmtpClient sc = new SmtpClient();
            //sc.Host = "email-smtp.us-west-2.amazonaws.com";
            sc.Host = "mail.lpcoders.com";
            sc.Port = 8889;
            sc.UseDefaultCredentials = false;
            sc.EnableSsl = false; // runtime encrypt the SMTP communications using SSL
            sc.DeliveryMethod = SmtpDeliveryMethod.Network;
            sc.Credentials = new NetworkCredential("info@lpcoders.com", "lpcoders10.");
            sc.Send(message);
        }

        public void enviarCodigoVerificacion(string email, int codigo, string lang)
        {   //Aqui va el link de la pagina a la cual se va a redirigir para recuperar su contraseña
            string mensaje;

            if (lang == "es") mensaje = "Bienvenido a Medlane, \n\n Utiliza el siguiente código para confirmar tu correo y terminar tu registro:\n\n " + codigo + " \n\n Si no realizaste la petición, puedes ignorar este mensaje, probablemente alguien escribió tu correo por accidente. \n\n Muchas gracias, \n El equipo de Medlane";
            else mensaje = "Welcome to Medlane, \n\n Use the next code to verify your email and finish your register:\n\n " + codigo + " \n\n If you dont made this request, please ignore this email, probably someone write your email accidentally. \n\n Thank you, \n Medlane team";

            MailMessage message = new MailMessage();
            message.From = new MailAddress("contacto@medlane.mx", "Medlane");

            message.To.Add(new MailAddress(email, email));

            if (lang == "es") message.Subject = Language.Spanish.codigoverificacion;
            else message.Subject = Language.English.verifycode;
            message.Body = mensaje;
            message.IsBodyHtml = false;

            SmtpClient sc = new SmtpClient();
            //sc.Host = "email-smtp.us-west-2.amazonaws.com";
            sc.Host = "relay-hosting.secureserver.net";
            sc.Port = 25;
            sc.UseDefaultCredentials = true;
            sc.EnableSsl = false; // runtime encrypt the SMTP communications using SSL
            sc.DeliveryMethod = SmtpDeliveryMethod.Network;
            sc.Send(message);
        }

        public int GenerateRandomNo()
        {
            int _min = 100000;
            int _max = 999999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }
    }
}
