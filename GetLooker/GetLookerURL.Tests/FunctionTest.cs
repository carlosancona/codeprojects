using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using GetLookerURL;
using Amazon.Lambda.APIGatewayEvents;

namespace GetLookerURL.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void TestToUpperFunction()
        {

            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            var request = new APIGatewayProxyRequest();
            var headers = new Dictionary<string, string>();
            headers.Add("sapienstoken", "7ab7a39b878f470914b0c7321537eb025d22565d");
            request.Headers = headers;

            var queryStringParameters = new Dictionary<string, string>();
            queryStringParameters.Add("userId", "137");
            queryStringParameters.Add("first_name", "Even Sosa");
            queryStringParameters.Add("lookerType", "dashboards");
            queryStringParameters.Add("lookerId", "2");
            queryStringParameters.Add("email", "carlos.villarreal@sonatasmx.com");

            request.QueryStringParameters = queryStringParameters;
            request.HttpMethod = "GET";
            APIGatewayProxyResponse url = function.FunctionHandler(request, context);

            Function.Result r = new Function.Result();
            r = Newtonsoft.Json.JsonConvert.DeserializeObject<Function.Result>(url.Body);
            Uri resultUri;
            Assert.True(Uri.TryCreate(r.url, UriKind.Absolute, out resultUri));
        }
    }
}
