using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Amazon.Lambda.Core;
using Amazon.Lambda.APIGatewayEvents;
using System.Text;
using System.Security.Cryptography;
using Sapiens_Snowflake_SQLclasses;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace GetLookerURL
{
    public class Function
    {
        public int stage = 1;
        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                stage = ChangeStage(context.InvokedFunctionArn);
            }
            catch
            {
                stage = 1;
            }

            APIGatewayProxyResponse responseobj = new APIGatewayProxyResponse();
            responseobj.StatusCode = 200;
            responseobj.Headers = new Dictionary<string, string>();
            responseobj.Headers.Add("Access-Control-Allow-Origin", "*");

            //Validate sapiens token
            if (!isValidSapiensToken(request))
            {
                responseobj.StatusCode = 400;
                responseobj.Body = "Invalid user";
                return responseobj;
            }

            Params p = getRequestParams(request);

            var user_attributes = new Dictionary<string, string>();
            user_attributes["email"] = p.email;

            var config = new LookerEmbedConfiguration()
            {
                HostName = "sapiensdatascience.looker.com",
                Secret = "e9900234724747f222aea9357c05946862e2743cc9c60689b22d6e769ec71dfd",
                ExternalUserId = p.userId.ToString(),
                UserFirstName = p.first_name,
                UserLastName = "",
                Permissions = new string[] { "access_data", "see_looks", "see_user_dashboards" },
                Models = new string[] { "qa_internal_dashboard" },
                GroupIds = new int[] {},
                ExternalGroupId = "",
                UserAttributeMapping = user_attributes
            };

            string lookerType = p.lookerType;
            string lookerId = p.lookerId;

            var url = GetLookerEmbedUrl(string.Format(@"/embed/{0}/{1}", lookerType, lookerId), config);

            Result res = new Result();
            res.url = url;

            responseobj.Body = JsonConvert.SerializeObject(res);

            return responseobj;
        }

        public bool isValidSapiensToken(APIGatewayProxyRequest request)
        {
            //Get sapiens token
            string sapienstoken = getSapiensToken(request);

            //Validate sapiens token
            var SapiensLogin = new SapiensLogin_SQLclasses(Configuration.Connection(stage));

            if (SapiensLogin.ValidateUserExist(sapienstoken) == "")
            {
                Logger.ERROR("Invalid sapiens token");
                return false;
            }

            return true;
        }

        public Params getRequestParams(APIGatewayProxyRequest request)
        {
            //Get body
            try
            {

                Params p = new Params();
                p.email = request.QueryStringParameters["email"];
                p.userId = int.Parse(request.QueryStringParameters["userId"]);
                p.first_name = request.QueryStringParameters["first_name"];
                p.lookerType = request.QueryStringParameters["lookerType"];
                p.lookerId = request.QueryStringParameters["lookerId"];

                return p;
            }
            catch (Exception ex)
            {
                Logger.ERROR("Incorrect data provided");
                return null;
            }
        }

        public string getSapiensToken(APIGatewayProxyRequest request)
        {
            if (request.Headers != null && request.Headers.ContainsKey("sapienstoken"))
                return request.Headers["sapienstoken"];

            return "";
        }

        public int ChangeStage(string environment)
        {
            int result = 0;
            string[] e = environment.Split(":");

            if (e[7] == "QA")
                result = 0;
            if (e[7] == "DEV")
                result = 1;
            if (e[7] == "PROD")
                result = 2;
            return result;
        }

        public class Params
        {
            public string first_name;
            public string email;
            public string lookerType;
            public string lookerId;
            public int userId;
        }

        public class Result
        {
            public string url;
        }

        public class LookerEmbedConfiguration
        {
            // AccessFilters holds a JSON serialized object tree describing the access control filters
            // {"model_name":{"view_name.field_name": "'Your Value'"}}"
            public string AccessFilters { get; set; }
            public string ExternalUserId { get; set; }
            public string UserFirstName { get; set; }
            public string UserLastName { get; set; }
            public bool ForceLogoutLogin { get; set; }
            public string[] Models { get; set; }
            public int[] GroupIds { get; set; }
            public string ExternalGroupId { get; set; }
            public string[] Permissions { get; set; }
            public Dictionary<string, string> UserAttributeMapping { get; set; }
            public string Secret { get; set; }
            public TimeSpan SessionLength { get; set; }
            public string HostName { get; set; }
            public int HostPort { get; set; }
            public string Nonce { get; set; }

            public LookerEmbedConfiguration()
            {
                ForceLogoutLogin = true;
                SessionLength = TimeSpan.FromMinutes(15);
                Nonce = DateTime.Now.Ticks.ToString();
                AccessFilters = "{}";
            }
        }

        public static string GetLookerEmbedUrl(string targetPath, LookerEmbedConfiguration config)
        {
            /* var builder = new UriBuilder
             {
                 Scheme = "https",
                 Host = config.HostName,
                 Port = config.HostPort,
                 Path = "/login/embed/" + System.Net.WebUtility.UrlEncode(targetPath)
             };*/

            string path = "/login/embed/" + System.Net.WebUtility.UrlEncode(targetPath);


            var unixTime = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            var time = unixTime.ToString();

            var json_nonce = JsonConvert.SerializeObject(config.Nonce);
            var json_external_user_id = JsonConvert.SerializeObject(config.ExternalUserId);
            var json_permissions = JsonConvert.SerializeObject(config.Permissions);
            var json_group_ids = JsonConvert.SerializeObject(config.GroupIds);
            var json_external_group_id = JsonConvert.SerializeObject(config.ExternalGroupId);
            var json_user_attribute_values = JsonConvert.SerializeObject(config.UserAttributeMapping);
            var json_models = JsonConvert.SerializeObject(config.Models);
            var json_session_length = String.Format("{0:N0}", (long)config.SessionLength.TotalSeconds);

            // order of elements is important
            var stringToSign = String.Join("\n", new string[] {
                config.HostName,
                path,
                json_nonce,
                time,
                json_session_length,
                json_external_user_id,
                json_permissions,
                json_models,
                json_group_ids,
                json_external_group_id,
                json_user_attribute_values,
                config.AccessFilters
            });

            var signature = EncodeString(stringToSign, config.Secret);

            var json_first_name = JsonConvert.SerializeObject(config.UserFirstName);
            var json_last_name = JsonConvert.SerializeObject(config.UserLastName);
            var json_force_logout_login = JsonConvert.SerializeObject(config.ForceLogoutLogin);

            var qparams = new Dictionary<string, string>()
            {
                { "nonce", json_nonce },
                { "time", time },
                { "session_length", json_session_length },
                { "external_user_id", json_external_user_id },
                { "permissions", json_permissions },
                { "models", json_models },
                { "group_ids", json_group_ids },
                { "external_group_id", json_external_group_id },
                { "user_attributes", json_user_attribute_values },
                { "access_filters", config.AccessFilters},
                { "first_name", json_first_name },
                { "last_name", json_last_name },
                { "force_logout_login", json_force_logout_login },
                { "signature", signature }
            };

            string url = String.Join("&", qparams.Select(kvp => kvp.Key + "=" + System.Net.WebUtility.UrlEncode(kvp.Value)));

            return "https://"  + config.HostName + path + "?" + url;
        }

        private static string EncodeString(string urlToSign, string secret)
        {
            var bytes = Encoding.UTF8.GetBytes(secret);
            var stringToEncode = Encoding.UTF8.GetBytes(urlToSign);
            using (HMACSHA1 hmac = new HMACSHA1(bytes))
            {
                var rawHmac = hmac.ComputeHash(stringToEncode);
                return Convert.ToBase64String(rawHmac);
            }

        }

    }

}
