﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AddNewUser
{
    class Configuration
    {
        public static string ConnectionStringPROD = "host=cp92702.us-east-1.snowflakecomputing.com;account=cp92702;user=PROD_SERVICE_ACCOUNT;password=aNvB8hbaGLCd;db=SAPIENSDB_PROD;warehouse=DATAACQ_PROD";
        public static string ConnectionStringDEV = "host=cp92702.us-east-1.snowflakecomputing.com;account=cp92702;user=DEV_SERVICE_ACCOUNT;password=ctSGUWP4vab7;db=SAPIENSDB_DEV;warehouse=DATAACQ_DEV";
        public static string ConnectionStringQA = "host=cp92702.us-east-1.snowflakecomputing.com;account=cp92702;user=QA_SERVICE_ACCOUNT;password=BTQugpMFJzp8;db=SAPIENSDB_QA;warehouse=DATAACQ_QA";

        public static string Connection(int stage)
        {
            string connect = "";
            switch (stage)
            {
                case 0:
                    connect = ConnectionStringQA;
                    break;
                case 1:
                    connect = ConnectionStringDEV;
                    break;
                case 2:
                    connect = ConnectionStringPROD;
                    break;
            }
            return connect;
        }
    }
}
