using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Newtonsoft.Json;
using Sapiens_Snowflake_SQLclasses;
using Snowflake.Data.Client;

using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Runtime;
using Amazon.CognitoIdentityProvider.Model.Internal.MarshallTransformations;
using Amazon;
using System.Threading;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AddNewUser
{
    public class Function
    {
        public int stage = 1;
        public string userPoolId = "us-east-1_8FQlhYm01";

        public APIGatewayProxyResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            try
            {
                stage = ChangeStage(context.InvokedFunctionArn);
            }
            catch
            {
                stage = 1;
            }

            APIGatewayProxyResponse responseobj = new APIGatewayProxyResponse();
            responseobj.StatusCode = 404;
            responseobj.Body = "Failed to add user";
            responseobj.Headers = new Dictionary<string, string>();
            responseobj.Headers.Add("Access-Control-Allow-Origin", "*");

            //Validate sapiens token
            if (!isValidSapiensToken(request))
            {
                responseobj.StatusCode = 400;
                responseobj.Body = "Invalid user";
                return responseobj;
            }

            //Obtain request data
            User user = getRequestBody(request);
            var SapiensLogin = new SapiensLogin_SQLclasses(Configuration.Connection(stage));
            int? userId = SapiensLogin.getUserId(user.email);

            if (userId == null)
            {
                //Create user in Cognito
                if (addCognitoUser(user, userPoolId))
                {
                    Logger.DEBUG("DEBUG email: - " + user.email + " Added cognito user");
                    responseobj.StatusCode = 200;
                    responseobj.Body = "User created";

                    var SapiensUser = new SapiensUser_SQLclasses(Configuration.Connection(stage));
                    string userToken = SapiensLogin.getUserToken(user.email);
                    string sapienstoken = getSapiensToken(request);
                    foreach (int roleId in user.roles)
                    {
                        SapiensUser.AddUserRole(userToken, roleId, sapienstoken);
                    }
                    
                }
            } else
            {
                responseobj.StatusCode = 404;
                responseobj.Body = "User already exist";
            }

            //Finish
            return responseobj;
        }

        public bool addCognitoUser(User user, string poolId)
        {
            bool response = false;
            using (var client = new AmazonCognitoIdentityProviderClient(RegionEndpoint.USEast1))
            {
                AdminCreateUserRequest req = new AdminCreateUserRequest();

                req.UserPoolId = poolId;
                List<AttributeType> attrList = new List<AttributeType>();

                AttributeType attrEmail = new AttributeType();
                attrEmail.Name = "email";
                attrEmail.Value = user.email;
                attrList.Add(attrEmail);

                AttributeType attrName = new AttributeType();
                attrName.Name = "name";
                attrName.Value = user.username;
                attrList.Add(attrName);

                AttributeType attrNickname = new AttributeType();
                attrNickname.Name = "nickname";
                attrNickname.Value = user.invitationCode;
                attrList.Add(attrNickname);

                req.Username = user.email;
                req.UserAttributes = attrList;
                
                client.AdminCreateUserAsync(req).Wait();
                Logger.DEBUG("DEBUG email: - " + user.email + " Added cognito user");
                response = true;
            }

            return response;
        }

        public User getRequestBody(APIGatewayProxyRequest request)
        {
            //Get body
            try
            {
                var settings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    MissingMemberHandling = MissingMemberHandling.Ignore
                };
                User user = JsonConvert.DeserializeObject<User>(request.Body, settings);
                return user;
            }
            catch (Exception ex)
            {
                Logger.ERROR("Incorrect data provided");
                return null;
            }
        }


        public bool isValidSapiensToken(APIGatewayProxyRequest request)
        {
            //Get sapiens token
            string sapienstoken = getSapiensToken(request);
            Logger.DEBUG("Validating " + sapienstoken);
            //Validate sapiens token
            var SapiensLogin = new SapiensLogin_SQLclasses(Configuration.Connection(stage));

            if (SapiensLogin.ValidateUserExist(sapienstoken) == "")
            {
                Logger.ERROR("Invalid sapiens token");
                return false;
            }

            return true;
        }

        public string getSapiensToken(APIGatewayProxyRequest request)
        {
            if (request.Headers != null && request.Headers.ContainsKey("sapienstoken"))
                return request.Headers["sapienstoken"];

            return "";
        }

        public int ChangeStage(string environment)
        {
            int result = 0;
            string[] e = environment.Split(":");

            if (e[7] == "QA")
            {
                result = 0;
                userPoolId = "us-east-1_7LJHWRBHA";
            }

            if (e[7] == "DEV")
            {
                result = 1;
                userPoolId = "us-east-1_8FQlhYm01";
            }

            if (e[7] == "PROD")
            {
                result = 2;
                userPoolId = "us-east-1_tiWCJEJzo";
            }

            return result;
        }

        public class User
        {
            public string username;
            public string email;
            public string invitationCode;
            public string mobilePhone;
            public int[] roles;
        }
    }
}
