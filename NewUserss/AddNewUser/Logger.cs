﻿using System;
using System.Collections.Generic;
using System.Text;
using Amazon.Lambda.Core;

namespace AddNewUser
{
    public static class Logger
    {
        private static string IsMainThread()
        {
            int mainThreadId = System.Threading.Thread.CurrentThread.ManagedThreadId;
            string threadname;
            if (System.Threading.Thread.CurrentThread.ManagedThreadId == mainThreadId)
            {
                threadname = "main";
            }
            else
            {
                threadname = "subtask";
            }
            return threadname;
        }

        public static void ERROR(string message)
        {
            LambdaLogger.Log(DateTime.Now + " [" + IsMainThread() + "] ERROR - " + message);
        }

        public static void ERROR(string message, Exception e)
        {
            LambdaLogger.Log(DateTime.Now + " [" + IsMainThread() + "] ERROR - " + message + " - Exception " + e);
        }

        public static void WARN(string message)
        {
            LambdaLogger.Log(DateTime.Now + " [" + IsMainThread() + "] WARN - " + message);
        }

        public static void DEBUG(string message)
        {
            LambdaLogger.Log(DateTime.Now + " [" + IsMainThread() + "] DEBUG - " + message);
        }

        public static void FATAL(string message)
        {
            LambdaLogger.Log(DateTime.Now + " [" + IsMainThread() + "] FATAL - " + message);
        }

        public static void INFO(string message)
        {
            LambdaLogger.Log(DateTime.Now + " [" + IsMainThread() + "] INFO - " + message);
        }


        public static void JSON(string message)
        {
            LambdaLogger.Log(message);
        }
    }
}
