using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using AddNewUser;
using Amazon.Lambda.APIGatewayEvents;
using static AddNewUser.Function;
using System.IO;

namespace AddNewUser.Tests
{
    public class FunctionTest
    {
        [Fact]
        public void Test_UT_AddCognitoUser()
        {
            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            string envVar = Environment.GetEnvironmentVariable("SAPIENS_ENV");
            string userPoolId = "us-east-1_8FQlhYm01";
            string invitationCode = "DEVTEST1";
            switch (envVar)
            {
                case "QA":
                    userPoolId = "us-east-1_7LJHWRBHA";
                    invitationCode = "10X10";
                    break;
                case "DEV":
                    userPoolId = "us-east-1_8FQlhYm01";
                    break;
                case "PROD":
                    userPoolId = "us-east-1_tiWCJEJzo";
                    invitationCode = "DEMO1";
                    break;
                default:
                    userPoolId = "us-east-1_8FQlhYm01";
                    break;
            }


            string randomName = Path.GetRandomFileName();
            randomName = randomName.Replace(".", ""); // Remove period.
            User user = new User();
            user.username = "Test";
            user.email = randomName + "@testable.com";
            user.invitationCode = invitationCode;
            var upperCase = function.addCognitoUser(user, userPoolId);

            Assert.True(upperCase);
        }

        [Fact]
        public void Test_UT_AddUser()
        {
            var function = new Function();
            TestLambdaContext context = new TestLambdaContext();
            var request = new APIGatewayProxyRequest();
            var headers = new Dictionary<string, string>();

            string randomName = Path.GetRandomFileName();
            randomName = randomName.Replace(".", ""); // Remove period.
            string envVar = Environment.GetEnvironmentVariable("SAPIENS_ENV") ?? "DEV";
            string userPoolId = "us-east-1_8FQlhYm01";
            string invitationCode = "DEVTEST1";
            context.InvokedFunctionArn = "arn:aws:lambda:us-east-1:193030773614:function:AddNewUser:" + envVar;
            switch (envVar)
            {
                case "QA":
                    userPoolId = "us-east-1_7LJHWRBHA";
                    invitationCode = "10X10";
                    headers.Add("sapienstoken", "21c54436a12102e8e20aec00aedb740d3c91134d");
                    break;
                case "DEV":
                    userPoolId = "us-east-1_8FQlhYm01";
                    headers.Add("sapienstoken", "21c54436a12102e8e20aec00aedb740d3c91134d");
                    break;
                case "PROD":
                    userPoolId = "us-east-1_tiWCJEJzo";
                    invitationCode = "DEMO1";
                    headers.Add("sapienstoken", "1dd8a2dea9aeaf20ba899008ba6056212192e9bb");
                    break;
                default:
                    userPoolId = "us-east-1_8FQlhYm01";
                    headers.Add("sapienstoken", "21c54436a12102e8e20aec00aedb740d3c91134d");
                    break;
            }

            request.Headers = headers;

            request.Body = "{\"username\":\"Test\"," +
                        "\"email\":\"" + randomName + "@testable.com\"," +
                        "\"invitationCode\":\"" + invitationCode + "\"," +
                        "\"roles\":[19]" +
                        "}";

            var response = function.FunctionHandler(request, context);

            Assert.Equal(200, response.StatusCode);
        }

        [Fact]
        public void Test_UT_getRequestBody()
        {
            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            var request = new APIGatewayProxyRequest();
            var headers = new Dictionary<string, string>();
            string envVar = Environment.GetEnvironmentVariable("SAPIENS_ENV") ?? "DEV";

            switch (envVar)
            {
                case "QA":
                    headers.Add("sapienstoken", "21c54436a12102e8e20aec00aedb740d3c91134d");
                    break;
                case "DEV":
                    headers.Add("sapienstoken", "21c54436a12102e8e20aec00aedb740d3c91134d");
                    break;
                case "PROD":
                    headers.Add("sapienstoken", "1dd8a2dea9aeaf20ba899008ba6056212192e9bb");
                    break;
                default:
                    headers.Add("sapienstoken", "21c54436a12102e8e20aec00aedb740d3c91134d");
                    break;
            }

            request.Headers = headers;

            request.HttpMethod = "GET";
            var response = function.getRequestBody(request);

            Assert.Null(response);
        }
    }
}
