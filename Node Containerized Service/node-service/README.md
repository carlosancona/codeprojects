# R Model - Service Setup

Copy rmodel.service file to /etc/systemd/system/
```
cp rmodel.service /etc/systemd/system/
```

Enable service so it can run at system boot
```
systemctl enable rmodel
```

Start service
```
service rmodel start
```
