var { spawnSync } = require( 'child_process' );

test('Calling /scores endpoint should return a correct calculation', () => {
	let calculatedScore = spawnSync( 'Rscript', [
     '../../rmodel/SDS-calculateRiskFromJSON.R',
     '../../rmodel/Sources/',
     '../../examples/example-1.json',
     '../../examples/example-2.json',
     '../../output/', '--vanilla']);
	strScores = calculatedScore.stdout.toString();
  indexStart = strScores.indexOf(']') + 1;
  indexEnd = strScores.indexOf(']',indexStart);
	scoreSummary = 	strScores.substring(indexStart + 3, indexEnd);
	expect(JSON.parse(scoreSummary).Score).toBe(365.5609);
	expect(JSON.parse(scoreSummary).calibratedScore).toBe(382.9561);
});

test('Calling /riskc-demo-scores endpoint should return a correct calculation', () => {
	let calculatedScore = spawnSync( 'Rscript', [
     '../../rmodel-riskc-demo/SDS-calculateRiskFromJSON.R',
     '../../rmodel-riskc-demo/Sources/',
     '../../examples/example-1.json',
     '../../examples/example-2.json',
     '../../output/', '--vanilla']);
	strScores = calculatedScore.stdout.toString();
  indexStart = strScores.indexOf(']') + 1;
  indexEnd = strScores.indexOf(']',indexStart);
	scoreSummary = 	strScores.substring(indexStart + 3, indexEnd);
	expect(JSON.parse(scoreSummary).Score).toBe(409.6789);
	expect(JSON.parse(scoreSummary).calibratedScore).toBe(431.7095);
});
