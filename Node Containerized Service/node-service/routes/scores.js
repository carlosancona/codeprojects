var express = require('express');
var router = express.Router();

var fs = require('fs');
var { spawnSync } = require( 'child_process' );
const uuidv1 = require('uuid/v1');

const rmodelHome = '/app/rmodel';
const outputDir = '/app/output/';

function deleteFileCallBack(err) {
    if (err)
      console.log('Could not delete file - ' + err.toString());
}

router.post('/', function(req, res, next) {
  let jsonFileName = '/tmp/' + uuidv1();
  let formattedJson;

  try {
    formattedJson = JSON.stringify(req.body.latestData);
    fs.writeFileSync(jsonFileName + '-1.json', formattedJson, 'utf8');
    formattedJson = JSON.stringify(req.body.historicalData);
    fs.writeFileSync(jsonFileName + '-2.json', formattedJson, 'utf8');
  } catch (e) {
    return res.status(500).send(err);
  }

  let rscript = spawnSync( 'Rscript', [ rmodelHome + '/SDS-calculateRiskFromJSON.R',
                                        rmodelHome + '/Sources/',
                                        jsonFileName+'-1.json',
                                        jsonFileName+'-2.json', outputDir, '--vanilla' ] );

  if(process.env.NODE_ENV.toLowerCase() == 'production') {
    // Delete input files
    fs.unlink(jsonFileName + '-1.json', deleteFileCallBack);
    fs.unlink(jsonFileName + '-2.json', deleteFileCallBack);
  }

  if (rscript.status > 0)
    res.status(500).send(rscript.stderr.toString());
  else
    res.status(200).send(rscript.stdout.toString());
});

module.exports = router;
