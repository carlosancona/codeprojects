var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var scoresRouter = require('./routes/scores');
var riskcDemoScoresRouter = require('./routes/riskc-demo-scores');

var app = express();

app.use(logger('dev'));
app.use(express.json({limit: '1mb'}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/scores', scoresRouter);
app.use('/riskc-demo-scores', riskcDemoScoresRouter);

module.exports = app;
