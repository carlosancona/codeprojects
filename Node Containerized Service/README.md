# R Model Service
The container exposes 2 endpoints on port 443(https):

- /scores
- /riskc-demo-scores

## R Model Versions
**rmodel:** v0.2.2

**rmodel-riskc-demo:** v0.2.2 . *(This version uses MET minutes inputs in calculations)*

## Development
Sapiens Pipeline uses Gitflow so, in order to promote new changes you need to create a feature branch having **develop** as parent.

Example below shows a common workflow for promoting a feature branch for the first time:

```
git clone git@bitbucket.org:sapiensengineering/containerized-r-model-service.git
git checkout develop
git checkout -b feature/your-feature
git add -A
git commit -m "Add new changes"
git push origin feature/your-feature
```

If you already cloned the repository to your local machine, just pull and checkout:

```
git checkout develop
git pull
git checkout -b feature/your-feature
git add -A
git commit -m "Add new changes"
git push origin feature/your-feature
```
