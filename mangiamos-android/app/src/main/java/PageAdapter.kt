import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.dotstudio.mangiamos.TabPuntosFragment
import com.dotstudio.mangiamos.TabRecompensasFragment
import kotlinx.android.synthetic.main.fragment_recompensas.view.*
import kotlinx.android.synthetic.main.fragment_tab_puntos.*

class PageAdapter(
    private val myContext: Context,
    fm: FragmentManager,
    private var totalTabs: Int
) : FragmentPagerAdapter(fm) {

    private var _fragments: ArrayList<Fragment>? = null

    fun PagerAdapter(activity: FragmentManager?) {
        activity
        this._fragments = ArrayList<Fragment>()
    }


    fun add(fragment: Fragment?) {
        _fragments!!.add(fragment!!)
    }

    override fun getItem(position: Int): Fragment {
        var fragment = Fragment()
        when (position) {
            0 -> {
                fragment = TabPuntosFragment()
            }
            1 -> {
                fragment = TabRecompensasFragment()
            }
        }
        return fragment
    }

    override fun getCount(): Int {
        return totalTabs
    }
}