package com.dotstudio.mangiamos

import PageAdapter
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_elegir_producto.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_recompensas.view.*
import kotlinx.android.synthetic.main.fragment_tab_puntos.*
import kotlinx.android.synthetic.main.fragment_tab_puntos.view.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ImportFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ImportFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RecompensasFragment : Fragment() {
    private var recompensaView: View? = null
    private lateinit var viewPager: ViewPager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        recompensaView = inflater.inflate(R.layout.fragment_recompensas, container, false)

        //if session detected hides all elements from fragment
        val prefs: SharedPreferences =
            this.activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
        val userid = prefs.getInt("userid", 0) //getting userid

        if (userid != 0) {
            recompensaView?.LoggedOffView?.visibility = View.GONE
            recompensaView?.LoggedInView?.visibility = View.VISIBLE

            val tabLayout = recompensaView?.tabLayout
            tabLayout?.addTab(tabLayout.newTab().setText("Puntos"))
            tabLayout?.addTab(tabLayout.newTab().setText("Recompensas"))

            val pageAdapter =
                PageAdapter(this.activity!!, childFragmentManager, tabLayout!!.tabCount)
            viewPager = recompensaView!!.pager
            viewPager.adapter = pageAdapter

            viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

            //recompensaView?.pager!!.currentItem = 1
            //recompensaView?.pager!!.currentItem = 0

            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    viewPager.currentItem = tab.position
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {

                }

                override fun onTabReselected(tab: TabLayout.Tab) {

                }
            })

        } else {
            recompensaView?.LoggedOffView!!.visibility = View.VISIBLE
            recompensaView?.LoggedInView!!.visibility = View.GONE
        }


        recompensaView?.btnLogin!!.setOnClickListener {
            val intent = Intent(activity, IniciarSesionActivity::class.java)
            activity?.startActivity(intent)
            activity?.finish()
        }

        return recompensaView
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun onBackPressed() {
        activity!!.supportFragmentManager.popBackStack()
        Log.d("BackPRESSED","RECOMPENSA FR")
    }
}

