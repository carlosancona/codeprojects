package com.dotstudio.mangiamos

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.fragment_recompensas.view.*
import kotlinx.android.synthetic.main.fragment_tab_puntos.*
import kotlinx.android.synthetic.main.fragment_tab_puntos.view.*


class TabPuntosFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val tabPuntos = inflater.inflate(R.layout.fragment_tab_puntos, container, false)
        var recompensaView = inflater.inflate(R.layout.fragment_recompensas, container, false)

        tabPuntos.txtPuntos.text = obtenerPuntos()
        //TODO agregar click listener para el boton y cambiar tab

        tabPuntos.btnCanjear.setOnClickListener {
            val viewPager: ViewPager =
                parentFragment!!.view!!.findViewById(R.id.pager)
            viewPager.currentItem = 1
        }

        return tabPuntos
    }

    fun obtenerPuntos(): String? {
        val prefs: SharedPreferences =
            this.activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val puntos = prefs.getString("puntos", "")!!

        return puntos
    }
}