package com.dotstudio.mangiamos

import DatosOrden
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.android.synthetic.main.activity_tipo_pedido.*

class TipoPedidoActivity : AppCompatActivity() {

    var idusuario = 0
    var idsucursal: String? = ""
    var varNombre: String? = ""
    var varDireccion: String? = ""
    private var parcelableDatosOrden: DatosOrden? = null

    lateinit var mView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tipo_pedido)

        val prefs: SharedPreferences =
            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
        idusuario = prefs.getInt("userid", 0) //getting userid


        mView = layoutInflater.inflate(R.layout.custom_action_bar, null)

        val params = ActionBar.LayoutParams(
            ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.MATCH_PARENT,
            Gravity.CENTER
        )

        val b = intent.extras
        if (b != null) {
            obtenerVariables()
        }

        val title = mView.findViewById(R.id.actionbar_title) as TextView
        val subTitle = mView.findViewById(R.id.actionbar_subtitle) as TextView

        title.text = varNombre
        subTitle.text = varDireccion

        supportActionBar!!.setCustomView(mView, params)
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)


        btnDrive.setOnClickListener {
            val intent = Intent(this, MenuCategoriaProductosActivity::class.java)
            parcelableDatosOrden?.tipo = "drivethru"
            val variables = guardarVariables()
            intent.putExtras(variables)

            setExtraParcelable(parcelableDatosOrden)
            intent.putExtras(variables)
            startActivity(intent)
        }

        btnRecoger.setOnClickListener {
            val intent = Intent(this, MenuCategoriaProductosActivity::class.java)
            parcelableDatosOrden?.tipo = "recoger"
            val variables = guardarVariables()
            intent.putExtras(variables)

            setExtraParcelable(parcelableDatosOrden)

            intent.putExtras(variables)
            startActivity(intent)
        }

        btnComer.setOnClickListener {
            val intent = Intent(this, MenuCategoriaProductosActivity::class.java)
            parcelableDatosOrden?.tipo = "comerahi"
            val variables = guardarVariables()
            intent.putExtras(variables)

            setExtraParcelable(parcelableDatosOrden)

            intent.putExtras(variables)
            startActivity(intent)
        }

        btnEnvio.setOnClickListener {
            if (parcelableDatosOrden?.idusuario != 0) {
                val intent = Intent(this, ConfirmarDireccionEnvioActivity::class.java)
                parcelableDatosOrden?.tipo = "servicioadomicilio"
                val variables = guardarVariables()
                intent.putExtras(variables)

                setExtraParcelable(parcelableDatosOrden)

                startActivity(intent)
            } else {
                Toast.makeText(
                    applicationContext,
                    "Debe iniciar sesión primero",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent =
            Intent(this@TipoPedidoActivity, DetalleRestauranteActivity::class.java)
        var variables = guardarVariables()
        finish()
        setExtraParcelable(parcelableDatosOrden)
        intent.putExtras(variables)
        startActivity(intent)
    }

    private fun guardarVariables(): Bundle {
        val bundle = Bundle()
        bundle.putString("id", idsucursal)
        bundle.putString("nombre", varNombre)
        bundle.putString("direccion", varDireccion)

        return bundle
    }

    private fun obtenerVariables() {
        val bundle = intent.extras

        if (bundle != null) {
            parcelableDatosOrden = if (getExtraParcelable() == null) parcelableDatosOrden else getExtraParcelable()

            varNombre = bundle.getString("nombre")
            varDireccion = bundle.getString("direccion")
            idsucursal = bundle.getString("id")
        }
    }

    private fun setExtraParcelable(datosOrden: DatosOrden?) {
        val prefs: SharedPreferences =
            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        if (datosOrden == null) {
            prefs.edit().putString("MiOrden", "").apply()
        } else {
            prefs.edit().putString(
                "MiOrden",
                jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)
            ).apply()
        }
    }

    private fun getExtraParcelable(): DatosOrden? {
        val prefs: SharedPreferences =
            getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val ordenString = prefs.getString("MiOrden", "")!!
        if (ordenString != "") {
            parcelableDatosOrden = jacksonObjectMapper().readValue(ordenString)
        }

        return parcelableDatosOrden
    }
}
