package com.dotstudio.mangiamos

import CallHandler
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.content.Intent
import android.content.SharedPreferences
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_cuenta.view.*


// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ImportFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ImportFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CuentaFragment : Fragment() {

    private lateinit var mView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_cuenta, container, false)

        //if session detected hides all elements from fragment
        var prefs: SharedPreferences =
            this.activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
        val userid = prefs.getInt("userid", 0) //getting userid
        val username = prefs.getString("username", "") //getting userid
        val useremail = prefs.getString("useremail", "") //getting userid

        if (userid != 0) { //if userid contains an id redirects to menu
            mView.LoggedOffView.visibility = View.GONE
            mView.LoggedInView.visibility = View.VISIBLE
            mView.txtNombreUsuario.text = username
            mView.txtCorreoUsuario.text = useremail
        } else {
            mView.LoggedOffView.visibility = View.VISIBLE
            mView.LoggedInView.visibility = View.GONE
        }

        mView.btnEditarDireccion.setOnClickListener {
            val intent = Intent(activity, EditarDireccionActivity::class.java)
            activity?.startActivity(intent)
        }

        mView.btnEditarMetodos.setOnClickListener {
            val intent = Intent(activity, EditarMetodosActivity::class.java)
            activity?.startActivity(intent)
        }

        mView.btnHistorial.setOnClickListener {
            val intent = Intent(activity, HistorialActivity::class.java)
            activity?.startActivity(intent)
        }

        mView.btnTerminos.setOnClickListener {
            val intent = Intent(activity, TerminosActivity::class.java)
            activity?.startActivity(intent)
        }

        mView.btnCerrarSesion.setOnClickListener {
            val editor = prefs.edit()
            editor.clear()
            editor.apply()
            Toast.makeText(
                this.context,
                "Su sesion ha finalizado",
                Toast.LENGTH_LONG
            ).show()

            activity?.finish()
            val intent = Intent(activity, MainActivity::class.java)
            activity?.startActivity(intent)
        }

        mView.btnLogin.setOnClickListener {
            val intent = Intent(activity, IniciarSesionActivity::class.java)
            activity?.startActivity(intent)
            activity?.finish()
        }

        return mView
    }
}
