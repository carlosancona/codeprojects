package com.dotstudio.mangiamos

import CallHandler
import Complementos
import DatosOrden
import DetallesProducto
import GridHandler
import ProductoOrdenes
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.KeyEvent
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.ActionBar
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detalle_producto.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception
import android.widget.ArrayAdapter
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.view.View
import kotlinx.android.synthetic.main.extras_gridview.view.*

class DetalleProductoActivity : AppCompatActivity() {
    private var varNombre: String? = ""
    private var idsucursal: String? = ""
    private var varDireccion: String? = ""
    private var idcategoria: Int = Int.MIN_VALUE
    private var idproducto: Int = Int.MIN_VALUE
    private var parcelableDatosOrden: DatosOrden? = DatosOrden(
        0,
        0,
        "",
        "",
        "",
        "",
        0,
        "",
        0.0,
        "",
        "",
        "",
        "",
        0.0,
        0.0,
        ArrayList(),
        ArrayList()
    )
    var grid: GridHandler? = null
    var total = 0.0

    var precioComplementos = 0.0
    var precioInicial = 0.0
    var precio = ""

    var selectedComplementos = ArrayList<Int>()

    private var txtCantidad: Int = Int.MIN_VALUE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_producto)

        val view = layoutInflater.inflate(R.layout.custom_action_bar, null)

        val b = intent.extras
        if (b != null) {
            obtenerVariables()

            val prefs: SharedPreferences =
                this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
            val userid = prefs.getInt("userid", 0) //getting userid
            parcelableDatosOrden?.idusuario = userid

            // Get an instance of base adapter
            doAsync {
                val handler = CallHandler()
                val response = handler.DetallesProducto(idproducto)
                val producto: DetallesProducto =
                    jacksonObjectMapper().readValue(response)
                uiThread {
                    try {
                        if (producto.status == 200) {
                            if (producto.imagen != "") {
                                Picasso.get().load(producto.imagen).into(imgProducto)
                            }
                            nombre.text = producto.nombre
                            descripcion.text = producto.ingredientes
                            val listaVariacionesNombre: ArrayList<String>? = null
                            for (x in producto.variaciones) {
                                listaVariacionesNombre?.add(x.nombre)
                            }

                            /**codigo para spinner**/
                            val spinnerArray = arrayOfNulls<String>(producto.variaciones.count())
                            val spinnerMap = arrayOfNulls<Int>(producto.variaciones.count())
                            val precios = arrayOfNulls<Float>(producto.variaciones.count())
                            grid = GridHandler(
                                this@DetalleProductoActivity,
                                producto.complementos as ArrayList<Complementos>
                            )
                            for (i in 0 until producto.variaciones.count()) {
                                spinnerMap[i] = producto.variaciones[i].idvariacion
                                spinnerArray[i] = producto.variaciones[i].nombre
                                precios[i] = producto.variaciones[i].precio.toFloat()
                            }
                            val adapter = ArrayAdapter<String>(
                                this@DetalleProductoActivity,
                                android.R.layout.simple_spinner_item,
                                spinnerArray
                            )

                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                            variacion.adapter = adapter

                            variacion.onItemSelectedListener = object : OnItemSelectedListener {
                                override fun onItemSelected(
                                    parentView: AdapterView<*>,
                                    selectedItemView: View,
                                    position: Int,
                                    id: Long
                                ) {
                                    precio_complementos.text = precios[position].toString()
                                    precioInicial = precios[position].toString().toDouble()
                                    cambiarTotalText()
                                }

                                override fun onNothingSelected(parentView: AdapterView<*>) {
                                }

                            }
                            variacion.setSelection(0)
                            /**codigo para spinner**/
                            gridExtras.adapter = grid
                        }
                    } catch (e: Exception) {
                        Toast.makeText(
                            applicationContext,
                            e.message,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }

        val params = ActionBar.LayoutParams(
            ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.MATCH_PARENT,
            Gravity.CENTER
        )

        val title = view.findViewById(R.id.actionbar_title) as TextView
        val subTitle = view.findViewById(R.id.actionbar_subtitle) as TextView

        title.text = varNombre
        subTitle.text = varDireccion

        supportActionBar!!.setCustomView(view, params)
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        btnAgregar.setOnClickListener {
            var complementos = ""
            if (parcelableDatosOrden?.idusuario != 0) {
                if (cantidad.text.toString().toDouble() >= 1.0) {
                    for ((index, value) in selectedComplementos.withIndex()) {
                        complementos += if (index == 0) "$value" else ",$value"
                    }

                    val productoOrden = ProductoOrdenes(
                        idproducto = idproducto,
                        cantidad = Integer.parseInt(cantidad.text.toString()),
                        idvariacion = variacion.selectedItemId.toInt(),
                        total = "%.2f".format(precio_complementos.text.toString().toDouble()).toDouble(),
                        complementos = complementos,
                        ingredientesextras = "",
                        comentario = ""
                    )

                    val elem =
                        parcelableDatosOrden?.productoOrdenes?.filter { e -> e.idproducto == idproducto && e.idvariacion == variacion.selectedItemId.toInt() }

                    if (!elem.isNullOrEmpty()) {
                        val index =
                            parcelableDatosOrden?.productoOrdenes!!.indexOfFirst { x -> x.idproducto == idproducto && x.idvariacion == variacion.selectedItemId.toInt() }
                        parcelableDatosOrden?.productoOrdenes!![index] = productoOrden
                    } else {
                        parcelableDatosOrden?.productoOrdenes?.add(productoOrden)
                    }
                    Toast.makeText(
                        applicationContext,
                        "Producto agregado a tu orden",
                        Toast.LENGTH_LONG
                    ).show()

                    /**Finish activity y regresar a menu de categorias**/
                    val intent = Intent(
                        applicationContext,
                        MenuCategoriaProductosActivity::class.java
                    )

                    val variables = guardarVariables()
                    finish()
                    intent.putExtras(variables)
                    guardarVariables()
                    startActivity(intent)
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Selecciona una cantidad para agregar a tu orden.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    applicationContext,
                    "Necesitas iniciar sesión para poder crear una orden.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        btnMas.setOnClickListener {
            txtCantidad = Integer.parseInt(cantidad.text.toString())
            txtCantidad += 1
            cantidad.tag = txtCantidad
            cantidad.text = txtCantidad.toString()

            if (grid?.selectedComplementosPrecio?.value != null) {
                total =
                    precioInicial * cantidad.text.toString().toDouble() + grid?.selectedComplementosPrecio?.value!! * cantidad.text.toString().toDouble()
            } else {
                total = precioInicial * cantidad.text.toString().toDouble()
            }
            cambiarTotalText()
        }

        btnMenos.setOnClickListener {
            txtCantidad = Integer.parseInt(cantidad.text.toString())
            if (txtCantidad > 1) {
                txtCantidad -= 1
                cantidad.tag = txtCantidad
                cantidad.text = txtCantidad.toString()

                if (grid?.selectedComplementosPrecio?.value != null) {
                    total =
                        (precioInicial * cantidad.text.toString().toDouble()) + (grid?.selectedComplementosPrecio?.value!! * cantidad.text.toString().toDouble())
                } else {
                    total = precioInicial * cantidad.text.toString().toDouble()
                }

                cambiarTotalText()
            }
        }

        gridExtras.onItemClickListener =
            AdapterView.OnItemClickListener { _, view, _, _ ->

                if (view.check.tag.toString() == "icon_check_off") {
                    selectedComplementos.add(view.imgExtra.tag.toString().toInt())

                    precio = view.txtIngredientes.tag.toString()
                    precioComplementos += precio.toDouble()

                    Log.d("", precioInicial.toString())
                    cambiarTotalText()

                    view.check.setImageResource(R.drawable.icon_check_on)
                    view.check.tag = "icon_check_on"
                } else {
                    selectedComplementos.remove(view.imgExtra.tag.toString().toInt())

                    precio = view.txtIngredientes.tag.toString()
                    precioComplementos -= precio.toDouble()

                    Log.d("", precioInicial.toString())
                    cambiarTotalText()

                    view.check.setImageResource(R.drawable.icon_check_off)
                    view.check.tag = "icon_check_off"
                }

            }
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent = Intent(this@DetalleProductoActivity, ElegirProductoActivity::class.java)
        val variables = guardarVariables()

        finish()
        intent.putExtras(variables)

        guardarVariables()
        startActivity(intent)
    }

    private fun guardarVariables(): Bundle {
        val bundle = Bundle()

        bundle.putString("id", idsucursal)
        bundle.putString("nombre", varNombre)
        bundle.putString("direccion", varDireccion)
        bundle.putInt("idCategoria", idcategoria)
        setExtraParcelable(parcelableDatosOrden)

        return bundle
    }

    private fun obtenerVariables() {
        val bundle = intent.extras

        if (bundle != null) {
            varNombre = bundle.getString("nombre")
            varDireccion = bundle.getString("direccion")

            idcategoria = bundle.getInt("idCategoria")
            idproducto = bundle.getInt("idproducto")

            parcelableDatosOrden =
                if (getExtraParcelable() == null) parcelableDatosOrden else getExtraParcelable()!!
            idsucursal = parcelableDatosOrden?.idsucursal.toString()
        }
    }

    private fun setExtraParcelable(datosOrden: DatosOrden?) {
        val prefs: SharedPreferences =
            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        if (datosOrden == null) {
            prefs.edit().putString("MiOrden", "").apply()
        } else {
            prefs.edit().putString(
                "MiOrden",
                jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)
            ).apply()
            prefs.edit().putString("direccionSuc", varDireccion).apply()
        }
    }

    private fun getExtraParcelable(): DatosOrden? {
        val prefs: SharedPreferences =
            getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val ordenString = prefs.getString("MiOrden", "")!!
        if (ordenString != "") {
            parcelableDatosOrden = jacksonObjectMapper().readValue(ordenString)
        }

        return parcelableDatosOrden
    }

    private fun cambiarTotalText() {
        val cantidad = cantidad.text.toString().toDouble()
        total = (precioInicial * cantidad) + (precioComplementos * cantidad)

        precio_complementos.text = total.toString()
    }
}
