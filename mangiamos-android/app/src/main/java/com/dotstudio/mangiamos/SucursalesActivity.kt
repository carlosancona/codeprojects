package com.dotstudio.mangiamos

import BusquedaSucursales
import CallHandler
import ListaSucursales
import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.KeyEvent
import android.view.MenuItem
import android.widget.*
import androidx.core.view.get
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_menu_categoria_productos.*
import kotlinx.android.synthetic.main.activity_sucursales.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception

class SucursalesActivity : AppCompatActivity() {
    private var idsucursal: String? = ""
    private var varNombre: String? = ""
    private var varDireccion: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sucursales)
        getSucursales("");
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent = Intent(this@SucursalesActivity, MainActivity::class.java)
        startActivity(intent)
    }


    private fun getSucursales(query: String) {

        doAsync {
            val handler = CallHandler()
            val response = handler.ListaSucursales(query)

            val sucursales: BusquedaSucursales =
                jacksonObjectMapper().readValue(response)
            uiThread {
                if (sucursales.status == 200) {

                    try {
                        var listView = findViewById<ListView>(R.id.listasucursales)
                        val adapter =
                            SucursalesAdapter(this@SucursalesActivity, sucursales.listasucursales)
                        listView.adapter = adapter

                        listView.setOnItemClickListener { _, _, position, _ ->
                            val intent = Intent(
                                this@SucursalesActivity,
                                DetalleRestauranteActivity::class.java
                            )
                            val bundle = Bundle()
                            bundle.putString("id","" + sucursales.listasucursales[position].idsucursal)
                            intent.putExtras(bundle)
                            startActivity(intent)

                        }


                    } catch (e: Exception) {
                        Toast.makeText(
                            applicationContext,
                            e.message,
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
                else{
                    Toast.makeText(
                        applicationContext,
                        "Ocurrio un error, intente de nuevo",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }


}
