package com.dotstudio.mangiamos

import CallHandler
import CategoriasSucursal
import android.content.Intent
import android.graphics.Typeface.BOLD
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.ActionBar
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.android.synthetic.main.activity_menu_categoria_productos.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import android.widget.TextView
import android.view.*
import android.widget.LinearLayout
import com.squareup.picasso.Picasso
import org.jetbrains.anko.matchParent
import java.lang.Exception


class MenuCategoriaProductosActivity : AppCompatActivity() {

    private var idsucursal: String? = ""
    private var varNombre: String? = ""
    private var varDireccion: String? = ""
    private var idcategoria: Int = Int.MIN_VALUE
    //private var parcelableDatosOrden:DatosOrden? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu_categoria_productos)

        val view = layoutInflater.inflate(R.layout.custom_action_bar, null)

        val params = ActionBar.LayoutParams(
            ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.MATCH_PARENT,
            Gravity.CENTER
        )

        val b = intent.extras
        if (b != null) {
            obtenerVariables()

            doAsync {
                val handler = CallHandler()
                val response = handler.ListaCategorias(idsucursal!!.toInt())
                val categorias: CategoriasSucursal =
                    jacksonObjectMapper().readValue(response)

                uiThread {
                    if (categorias.status == 200) {
                        if (categorias.promocionimagen != ""){
                            Picasso.get().load(categorias.promocionimagen).into(imgBanner)
                        }else{
                            imgBanner.visibility = View.GONE
                            scrollView.layoutParams.height = matchParent
                        }

                        try {
                            for (categoria in categorias.listacategorias!!) {
                                AddRows(
                                    categoria.nombre!!,
                                    categoria.imagen!!,
                                    categoria.id!!
                                )
                            }
                        }catch (e:Exception){}
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "Ocurrio un error, ${categorias.mensaje}",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }

        val title = view.findViewById(R.id.actionbar_title) as TextView
        val subTitle = view.findViewById(R.id.actionbar_subtitle) as TextView

        title.text = varNombre
        subTitle.text = varDireccion

        supportActionBar!!.setCustomView(view, params)
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()

        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent =
            Intent(this@MenuCategoriaProductosActivity, MainActivity::class.java)
        val variables = guardarVariables()

        intent.putExtras(variables)
        //setExtraParcelable(intent, parcelableDatosOrden)
        startActivity(intent)
        finish()
    }

    private fun guardarVariables(): Bundle {
        val bundle = Bundle()
        bundle.putString("id", idsucursal)
        bundle.putString("nombre", varNombre)
        bundle.putString("direccion", varDireccion)
        bundle.putInt("idCategoria", idcategoria)

        return bundle
    }

    private fun obtenerVariables() {
        val bundle = intent.extras

        if (bundle != null) {
            idsucursal = bundle.getString("id")
            varNombre = bundle.getString("nombre")
            varDireccion = bundle.getString("direccion")
            //parcelableDatosOrden = if (getExtraParcelable() == null) parcelableDatosOrden else getExtraParcelable()!!

            //idsucursal = parcelableDatosOrden?.idsucursal.toString()
        }
    }

    fun AddRows(rowText: String, imagenUrl: String, id: Int) {
        var layout = linearLayout

        val row = TableRow(this)
        val imagenCategoria = ImageView(this)
        val arrow = ImageView(this)
        val texto = TextView(this)
        val horizontalLayout1 = LinearLayout(this)
        val horizontalLayout2 = LinearLayout(this)
        val horizontalLayout3 = LinearLayout(this)
        val space = LinearLayout(this)

        space.setBackgroundColor(resources.getColor(R.color.colorBlack))
        space.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1)

        horizontalLayout1.orientation = LinearLayout.VERTICAL
        horizontalLayout1.layoutParams = TableRow.LayoutParams(180, 215)
        horizontalLayout1.gravity = Gravity.CENTER_VERTICAL

        horizontalLayout2.orientation = LinearLayout.VERTICAL
        horizontalLayout2.layoutParams = TableRow.LayoutParams(650, 215)
        horizontalLayout2.gravity = Gravity.CENTER_VERTICAL

        horizontalLayout3.orientation = LinearLayout.VERTICAL
        horizontalLayout3.layoutParams = TableRow.LayoutParams(155, 215)
        horizontalLayout3.gravity = Gravity.CENTER_VERTICAL

        row.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 215)
        row.gravity = Gravity.CENTER_VERTICAL
        row.setBackgroundColor(resources.getColor(R.color.colorWhite))
        row.setPadding(100, 0, 0, 0)
        row.isClickable = true
        row.tag = id

        imagenCategoria.layoutParams = TableRow.LayoutParams(180, 180)
        imagenCategoria.setPadding(5, 0, 0, 0)
        Picasso.get().load(imagenUrl).into(imagenCategoria)

        texto.text = rowText
        texto.setPadding(170, 0, 0, 0)
        texto.textSize = 15F
        texto.setTypeface(null, BOLD);
        texto.setTextColor(resources.getColor(R.color.colorBlue))

        arrow.layoutParams = TableRow.LayoutParams(60, 60)
        arrow.setPadding(20, 0, 0, 0)
        arrow.setImageResource(R.drawable.btn_backgrey)
        arrow.rotation = 180F

        horizontalLayout2.addView(texto)
        horizontalLayout1.addView(imagenCategoria)
        horizontalLayout3.addView(arrow)

        row.addView(horizontalLayout1)
        row.addView(horizontalLayout2)
        row.addView(horizontalLayout3)

        layout.addView(row)
        layout.addView(space)

        row.setOnClickListener(onClickListener)
    }

    private var onClickListener: View.OnClickListener = View.OnClickListener { v ->
        idcategoria = v.tag as Int
        val intent =
            Intent(this@MenuCategoriaProductosActivity, ElegirProductoActivity::class.java)
        var variables = guardarVariables()
        finish()
        intent.putExtras(variables)
        //setExtraParcelable(intent, parcelableDatosOrden)
        startActivity(intent)
    }

//    private fun setExtraParcelable(intent: Intent, datosOrden: DatosOrden?) {
//        intent.putExtra("datosOrden", datosOrden)
//
//        val prefs: SharedPreferences =
//            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
//
//        if (datosOrden == null) {
//            prefs.edit().putString("MiOrden", "").apply()
//        } else {
//            prefs.edit().putString(
//                "MiOrden",
//                jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)
//            ).apply()
//            prefs.edit().putString("direccionSuc", varDireccion).apply()
//        }
//    }

//    private fun getExtraParcelable(): DatosOrden? {
//        val prefs: SharedPreferences =
//            getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
//
//        val ordenString = prefs.getString("MiOrden", "")!!
//        if (ordenString != "") {
//            parcelableDatosOrden = jacksonObjectMapper().readValue(ordenString)
//        }
//
//        return parcelableDatosOrden
//    }
}

