package com.dotstudio.mangiamos

import Recompensa
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recompensas_gridview.view.*
import kotlinx.android.synthetic.main.recompensas_gridview.view.imgExtra
import kotlinx.android.synthetic.main.recompensas_gridview.view.txtNombre

class ListaRecompensasHandler(context: Context, var extra: ArrayList<Recompensa>) : BaseAdapter() {
    var context: Context? = context

    override fun getCount(): Int {
        return extra.size
    }

    override fun getItem(position: Int): Any {
        return extra[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val recompensa = this.extra[position]

        val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val extrasLayout = inflator.inflate(R.layout.recompensas_gridview, null)

        extrasLayout.txtPts?.text = recompensa.puntoscosto.toString()
        extrasLayout.txtPts.tag = recompensa.tipo //tipo de recompensa

        extrasLayout.txtNombre?.text = recompensa.descripcion
        extrasLayout.txtNombre?.tag = recompensa.idrecompensa //idrecompensa

        extrasLayout.imgExtra.tag = recompensa.imagen //imagen de recomepnsa

        extrasLayout.textView36.tag = recompensa.cantidad //cantidad de recompensa

        if (recompensa.imagen != "") {
            Picasso.get().load(recompensa.imagen).into(extrasLayout.imgExtra)
        }

        return extrasLayout
    }
}