package com.dotstudio.mangiamos

import CallHandler
import DatosOrden
import ObtenerRecompensas
import RecompensaOrdenes
import android.app.AlertDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.android.synthetic.main.activity_detalle_producto.view.*
import kotlinx.android.synthetic.main.fragment_miorden.*
import kotlinx.android.synthetic.main.fragment_miorden.view.*
import kotlinx.android.synthetic.main.fragment_tab_recompensas.view.*
import kotlinx.android.synthetic.main.fragment_tab_recompensas.view.gridView
import kotlinx.android.synthetic.main.recompensas_gridview.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TabRecompensasFragment : Fragment() {
    private lateinit var tabRecompensas: View

    var grid: ListaRecompensasHandler? = null
    var puntos: String? = ""
    private var parcelableDatosOrden: DatosOrden? = DatosOrden(
        0,
        0,
        "",
        "",
        "",
        "",
        0,
        "",
        0.0,
        "",
        "",
        "",
        "",
        0.0,
        0.0,
        ArrayList(),
        ArrayList()
    )

    var imagen: String? = ""
    var tipo: String? = ""
    var descripcion: String? = ""
    var cantidad: Int? = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        tabRecompensas = inflater.inflate(R.layout.fragment_tab_recompensas, container, false)

        val prefs: SharedPreferences =
            activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val userid = prefs.getInt("userid", 0) //getting userid

        doAsync {
            val handler = CallHandler()
            val response = handler.ListaRecompensas()
            val recompensas: ObtenerRecompensas = jacksonObjectMapper().readValue(response)

            puntos = obtenerPuntos()
            uiThread {
                if (recompensas.status == 200) {
                    grid = ListaRecompensasHandler(requireContext(), recompensas.listarecompensa)
                    tabRecompensas.gridView.adapter = grid
                    tabRecompensas.txtPuntos.text = puntos
                }
            }
        }

        doAsync { obtenerVariables() }

        tabRecompensas.gridView.onItemClickListener =
            AdapterView.OnItemClickListener { _, view, _, _ ->
                if (puntos!!.toInt() > view?.txtPts?.text.toString().toInt()) {
                    imagen = view.imgExtra.tag.toString()
                    tipo = view.txtPts.tag.toString()
                    descripcion = view.txtNombre.text.toString()
                    cantidad = view.textView36.tag.toString().toInt()

                    val recompensas = RecompensaOrdenes(view?.txtNombre?.tag.toString().toInt())
                    val puntos = obtenerPuntos().toString().toInt()

                    PopUpConfirmar(context, recompensas, puntos)
                }
            }

        return tabRecompensas
    }

    fun obtenerPuntos(): String? {
        val prefs: SharedPreferences =
            this.activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val puntos = prefs.getString("puntos", "")!!
        Log.d("puntos", puntos)
        return puntos
    }

    private fun guardarVariables(datosOrden: DatosOrden?) {
        val prefs: SharedPreferences =
            activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        prefs.edit().putString("tipoRecompensa", tipo).apply()
        prefs.edit().putString("descRecompensa", descripcion).apply()
        prefs.edit().putString("imagenRecompensa", imagen).apply()
        prefs.edit().putInt("cantidadRecompensa", cantidad!!).apply()

        if (datosOrden == null) {
            prefs.edit().putString("MiOrden", "").apply()
        } else {
            prefs.edit().putString(
                "MiOrden",
                jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)
            ).apply()
        }
    }

    private fun obtenerVariables() {
        val prefs: SharedPreferences =
            activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val ordenString = prefs.getString("MiOrden", "")!!
        if (ordenString != "") {
            parcelableDatosOrden = jacksonObjectMapper().readValue(ordenString)
        }
    }

    fun PopUpConfirmar(
        context: Context?,
        recompensas: RecompensaOrdenes,
        puntos: Int
    ) {
        val alert = AlertDialog.Builder(context)
            .setTitle("¿Desea canjear la recompensa?")
            .setMessage("se perderán los puntos utilizados")
            .setPositiveButton("Aceptar") { dialog, _ ->

                parcelableDatosOrden?.recompensaOrdenes?.clear()
                parcelableDatosOrden?.recompensaOrdenes?.add(recompensas)

                Log.d("puntos totales", puntos.toString())
                Log.d("costo puntos", view?.txtPts?.text.toString())
                val calculo = puntos - view?.txtPts?.text.toString().toInt()

                Log.d("Calculo de puntos", calculo.toString())



                guardarVariables(parcelableDatosOrden)

                //Cambiando de fragment
                val fragment = MiordenFragment()
                val fragmentManager = activity!!.supportFragmentManager
                fragmentManager.beginTransaction()
                    .setCustomAnimations(
                        R.anim.nav_default_pop_enter_anim,
                        R.anim.nav_default_pop_exit_anim
                    )
                    .replace(R.id.constraint, fragment)
                    .addToBackStack(null)
                    .commit()

                dialog.cancel()
            }
            .setNegativeButton("Cancelar") { dialog, _ ->
                dialog.cancel()
            }.create()

        alert.show()
    }

}