package com.dotstudio.mangiamos

import DatosOrden
import ListaPedidosHandler
import ProductoOrdenes
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.android.synthetic.main.fragment_miorden.*
import kotlinx.android.synthetic.main.fragment_miorden.view.*
import kotlinx.android.synthetic.main.miorden_gridview.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class MiordenFragment : Fragment() {
    private lateinit var mi_ordenView: View
    lateinit var grid: ListaPedidosHandler
    private var varDireccion: String? = ""
    private var parcelableDatosOrden: DatosOrden? = DatosOrden(
        0,
        0,
        "",
        "",
        "",
        "",
        0,
        "",
        0.0,
        "",
        "",
        "",
        "",
        0.0,
        0.0,
        ArrayList(),
        ArrayList()
    )
    var bundle = activity?.intent?.extras
    var total = 0.00
    var imagen: String? = ""
    var tipo: String? = ""
    var descripcion: String? = ""
    var cantidad: Int? = 0
    var descuento = 0.00
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mi_ordenView = inflater.inflate(R.layout.fragment_miorden, container, false)

        val prefs: SharedPreferences =
            activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        //if session detected hides all elements from fragment

        val userid = prefs.getInt("userid", 0) //getting userid
        val nombreSucursal = prefs.getString("nombreSucursal", "")
        varDireccion = prefs.getString("direccionSuc", "")

        if (userid != 0) { //if userid contains an id show logged in view
            doAsync {
                obtenerVariables()
                uiThread {
                    Log.d("cuenta", parcelableDatosOrden?.productoOrdenes!!.count().toString())
                    var productosEnOrden = parcelableDatosOrden?.productoOrdenes!!.filter { x -> x.idproducto != 0 }

                    if (productosEnOrden.count() > 0 ){
                        cargarLista()

                        mi_ordenView.txtSucursal.text = "${nombreSucursal}, $varDireccion"
                        mi_ordenView.txtTipoPedido.text = parcelableDatosOrden?.tipo
                        mi_ordenView.txtDireccionEnvio.text =
                            if (parcelableDatosOrden?.enviocalle.toString().isNotEmpty()) {
                                "${parcelableDatosOrden?.enviocalle} ${parcelableDatosOrden?.envionumero}, ${parcelableDatosOrden?.enviociudad}"
                            } else {
                                "N/A"
                            }
                        for (p in parcelableDatosOrden?.productoOrdenes!!) {
                            total += p.total
                        }
                        total -= (total * descuento)
                        mi_ordenView.txtTotal.text = ("%.2f".format(total))
                    } else {
                        mi_ordenView.imgOrdenVacia.visibility = View.VISIBLE
                        mi_ordenView.LoggedInView.visibility = View.GONE
                        mi_ordenView.LoggedOffView.visibility = View.GONE
                    }
                }
            }

        } else {
            mi_ordenView.LoggedOffView.visibility = View.VISIBLE
            mi_ordenView.LoggedInView.visibility = View.GONE
        }

        mi_ordenView.btnLogin.setOnClickListener {
            val intent = Intent(activity, IniciarSesionActivity::class.java)

            activity?.startActivity(intent)
            activity?.finish()
        }

        mi_ordenView.btnPedido.setOnClickListener {
            parcelableDatosOrden?.productoOrdenes?.removeIf { x -> x.idproducto == 0 }

            if (parcelableDatosOrden?.productoOrdenes!!.count() == 0) {
                Toast.makeText(
                    context,
                    "Debes agregar productos a tu orden para proceder",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                parcelableDatosOrden?.productoOrdenes?.removeIf { x -> x.idproducto == 0 }

                parcelableDatosOrden?.observaciones = txtObservaciones.text.toString()
                guardarVariables(parcelableDatosOrden)

                val intent =
                    Intent(activity, RealizarPago::class.java)
                startActivity(intent)
            }
        }

        return mi_ordenView
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }

        }
        return super.onOptionsItemSelected(item)
    }

    fun onBackPressed() {
        activity!!.supportFragmentManager.popBackStack()
    }

    private fun borrarProductoPopUp(
        context: Context?,
        idproducto: Int,
        idvariacion: Int,
        nombre: String
    ) {
        val alert = AlertDialog.Builder(context)
            .setTitle("¿Desea eleminar este pedido?")
            .setMessage(nombre)
            .setPositiveButton("Aceptar") { dialog, _ ->
                val eliminarProducto =
                    parcelableDatosOrden?.productoOrdenes?.find { x ->
                        x.idproducto == idproducto &&
                                x.idvariacion == idvariacion
                    }
                parcelableDatosOrden?.productoOrdenes?.remove(eliminarProducto)
                Toast.makeText(
                    context,
                    "Se elimino el producto $nombre de su orden",
                    Toast.LENGTH_LONG
                ).show()

                gridView.adapter = null
                cargarLista()
                guardarVariables(parcelableDatosOrden)
                total = 0.0
                for (p in parcelableDatosOrden?.productoOrdenes!!) {
                    total += p.total
                }
                mi_ordenView.txtTotal.text = ("%.2f".format(total))

                dialog.cancel()
            }
            .setNegativeButton("Cancelar") { dialog, _ ->
                dialog.cancel()
            }.create()

        alert.show()
    }

    private fun obtenerVariables() {
        val prefs: SharedPreferences =
            activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val ordenString = prefs.getString("MiOrden", "")!!
        if (ordenString != "") {
            parcelableDatosOrden = jacksonObjectMapper().readValue(ordenString)
        }

        imagen = prefs.getString("imagenRecompensa", "")
        descripcion = prefs.getString("descRecompensa", "")
        tipo = prefs.getString("tipoRecompensa", "")
        cantidad = prefs.getInt("cantidadRecompensa", 0)

        if (tipo == "descuento") {
            descuento = cantidad!!.toDouble() / 100
        } else {
            parcelableDatosOrden?.productoOrdenes?.add(
                ProductoOrdenes(
                    0,
                    0,
                    0,
                    0.0,
                    imagen,
                    "",
                    ""
                )
            )
        }
    }

    private fun guardarVariables(datosOrden: DatosOrden?) {
        val prefs: SharedPreferences =
            activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        if (datosOrden == null) {
            prefs.edit().putString("MiOrden", "").apply()
        } else {
            prefs.edit().putString(
                "MiOrden",
                jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)
            ).apply()
            prefs.edit().putString("direccionSuc", varDireccion).apply()
        }
    }

    private fun cargarLista() {
        grid =
            ListaPedidosHandler(requireContext(), parcelableDatosOrden?.productoOrdenes!!)

        if (parcelableDatosOrden?.productoOrdenes!!.count() == 0) {
            mi_ordenView.imgOrdenVacia.visibility = View.VISIBLE
            mi_ordenView.LoggedInView.visibility = View.GONE
            mi_ordenView.LoggedOffView.visibility = View.GONE
        } else {
            mi_ordenView.LoggedInView.visibility = View.VISIBLE

            if (grid.extra.size != 0)
                gridView.adapter = grid

            gridView.onItemClickListener =
                AdapterView.OnItemClickListener { _, view, _, _ ->
                    run {
                        borrarProductoPopUp(
                            context,
                            view.txtNombre.tag as Int,
                            view.imgExtra.tag as Int,
                            view.txtNombre.text.toString()
                        )
                    }
                }
        }
    }
}
