import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.squareup.picasso.Picasso
import com.dotstudio.mangiamos.R
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.android.synthetic.main.extras_gridview.view.imgExtra
import kotlinx.android.synthetic.main.extras_gridview.view.txtIngredientes
import kotlinx.android.synthetic.main.extras_gridview.view.txtNombre
import kotlinx.android.synthetic.main.miorden_gridview.view.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class ListaPedidosHandler(context: Context, var extra: ArrayList<ProductoOrdenes>) : BaseAdapter() {
    var context: Context? = context

    override fun getCount(): Int {
        return extra.size
    }

    override fun getItem(position: Int): Any {
        return extra[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val productoOrdenes = this.extra[position]

        val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val extrasLayout = inflator.inflate(R.layout.miorden_gridview, null)

        if (productoOrdenes.idproducto == 0) {
            val prefs: SharedPreferences =
                context!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
            var imagen = prefs.getString("imagenRecompensa", "")
            var descripcion = prefs.getString("descRecompensa", "")
            var tipo = prefs.getString("tipoRecompensa", "")
            var cantidad = prefs.getInt("cantidadRecompensa", 0)

            Log.d(
                "Mi Orden Fragment",
                "Mi Orden Fragment*********************************************"
            )
            Log.d("imagen", imagen)
            Log.d("descripcion", descripcion)
            Log.d("tipo", tipo)
            Log.d("cantidad", cantidad.toString())

            if (imagen != "") {
                Picasso.get().load(imagen).into(extrasLayout.imgExtra)
            }
            extrasLayout.txtNombre.tag = 0
            extrasLayout.imgExtra.tag = 0
            extrasLayout.txtPrecio.tag = ""

            extrasLayout.txtNombre.text = descripcion
            extrasLayout.txtSign.text = ""
            //extrasLayout.txtCantidad.text = "(x${productoOrdenes.cantidad})"
//            extrasLayout.txtPrecio.text = productoOrdenes.total.toString()
            //extrasLayout.txtIngredientes.text = detalleProducto.ingredientes
        } else {
            doAsync {
                val handler = CallHandler()
                val producto = handler.DetallesProducto(productoOrdenes.idproducto)
                val detalleProducto: DetallesProducto = jacksonObjectMapper().readValue(producto)

                uiThread {
                    if (detalleProducto.imagen != "") {
                        Picasso.get().load(detalleProducto.imagen).into(extrasLayout.imgExtra)
                    }

                    extrasLayout.txtNombre.tag = detalleProducto.idproducto
                    extrasLayout.imgExtra.tag = productoOrdenes.idvariacion
                    extrasLayout.txtPrecio.tag = extrasLayout.id //id del elemento gridView

                    extrasLayout.txtNombre.text = detalleProducto.nombre
                    extrasLayout.txtCantidad.text = "(x${productoOrdenes.cantidad})"
                    extrasLayout.txtPrecio.text = productoOrdenes.total.toString()
                    extrasLayout.txtIngredientes.text = detalleProducto.ingredientes

                }
            }
        }

        return extrasLayout
    }
}