package com.dotstudio.mangiamos.ui.login

import CallHandler
import PopUp
import SesionObject
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.*
import com.dotstudio.mangiamos.MainActivity
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread
import java.lang.Exception
import android.view.KeyEvent
import android.view.MenuItem
import com.dotstudio.mangiamos.IniciarSesionActivity
import com.dotstudio.mangiamos.R


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        val username = findViewById<EditText>(R.id.email)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.btnLogin)
        val loading = findViewById<ProgressBar>(R.id.loading)

        login.setOnClickListener {
            val handler = CallHandler()
            loading.visibility = View.VISIBLE

            doAsync {
                try {
                    val response = handler.IniciarSesion(
                        username.text.toString(),
                        password.text.toString()
                    )
                    val mensaje: SesionObject =
                        jacksonObjectMapper().readValue(response)
                    uiThread {
                        if (mensaje.status == 200) {
                            loading.visibility = View.INVISIBLE
                            longToast("${getString(com.dotstudio.mangiamos.R.string.welcome)} ${mensaje.nombre}")
                            //adding values to shared preferences
                            val prefs: SharedPreferences =
                                getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
                            val editor: SharedPreferences.Editor =
                                prefs.edit() //opening sharedprefs editor
                            //Saving user data in variables
                            mensaje.idusuario?.let { it1 ->
                                editor.putInt(
                                    "userid",
                                    it1
                                )
                            }//registering userid as new value
                            mensaje.codigoqr?.let { qrcode ->
                                editor.putString(
                                    "userqr",
                                    qrcode
                                )
                            }//registering userid as new value
                            mensaje.nombre?.let { nombre -> editor.putString("username", nombre) }
                            mensaje.email?.let { email -> editor.putString("useremail", email) }
                            editor.apply() //committing changes

                            //redirect to menu fragment
                            finish()
                            //closing IniciarSesionActivity
                            IniciarSesionActivity().finish()
                            val intent = Intent(baseContext, MainActivity::class.java)
                            startActivity(intent)
                        } else {
                            loading.visibility = View.INVISIBLE
                            longToast("Usuario o contraseña incorrectos")
                        }
                    }
                } catch (e: Exception) {
                    longToast("error: ${e.message}")
                    loading.visibility = View.INVISIBLE
                }
            }
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun recoverOnClick(view: View) {
        val popUp = PopUp()
        popUp.CreateRecoverPopUp(
            this,
            resources
        )
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            return true
        }
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
