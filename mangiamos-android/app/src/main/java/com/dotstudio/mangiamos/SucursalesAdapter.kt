package com.dotstudio.mangiamos

import BusquedaSucursales
import ListaSucursales
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class SucursalesAdapter(private val context: Context,
                        private val dataSource: List<ListaSucursales>) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return dataSource.size
    }

    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.sucursales_row, parent, false)

        val titleTextView = rowView.findViewById(R.id.titulotxt) as TextView
        val subtitleTextView = rowView.findViewById(R.id.subtitulotxt) as TextView

        val sucursales = getItem(position) as ListaSucursales

        titleTextView.text = sucursales.nombre
        subtitleTextView.text = sucursales.direccion

        return rowView
    }
}

