package com.dotstudio.mangiamos

import CallHandler
import Puntos
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.jetbrains.anko.doAsync

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    var userid = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_menu, R.id.nav_recompensas, R.id.nav_cuenta,
                R.id.nav_miorden, R.id.nav_escanear
            ), drawerLayout
        )

        /***************************************/
        val prefs: SharedPreferences =
            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
        userid = prefs.getInt("userid", 0)

        if (userid != 0) {
            cargarPuntos()
        }

        /***************************************/
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    private fun cargarPuntos() {
        doAsync {
            val handler = CallHandler()

            val responsePuntos = handler.ObtenerPuntos(userid)
            val puntos: Puntos = jacksonObjectMapper().readValue(responsePuntos)

            if (puntos.status == 200) {
                val prefs: SharedPreferences =
                    this@MainActivity.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
                Log.d("puntos:", puntos.mensaje)
                prefs.edit().putString("puntos", puntos.mensaje).apply()
            }

        }
    }
}
