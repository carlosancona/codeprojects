package com.dotstudio.mangiamos

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_menu.view.*


// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ImportFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ImportFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MenuFragment : Fragment() {
    lateinit var menuView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        menuView = inflater.inflate(R.layout.fragment_menu, container, false)

        menuView.btnBuscar.setOnClickListener {
            val intent = Intent(activity, MapsActivity::class.java)
            activity?.startActivity(intent)
            activity?.overridePendingTransition(
                R.transition.slide_out_bottom,
                R.transition.slide_in_bottom
            )

            /*******ABRIR FRAGMENT DE FRAGMENT********/
//            val fragment = EscanearFragment()
//            val fragmentManager = activity!!.supportFragmentManager
//            fragmentManager.beginTransaction()
//                .setCustomAnimations(
//                    R.anim.nav_default_pop_enter_anim,
//                    R.anim.nav_default_pop_exit_anim
//                )
//                .add(R.id.drawer_layout, fragment)
//                .addToBackStack(null)
//                .commit()
        }

        return menuView
    }
}
