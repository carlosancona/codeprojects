package com.dotstudio.mangiamos

import CallHandler
import DatosOrden
import DireccionObject
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import android.widget.Toast
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.android.synthetic.main.activity_confirmar_direccion_envio.*
import kotlinx.android.synthetic.main.activity_editar_direccion.calle
import kotlinx.android.synthetic.main.activity_editar_direccion.ciudad
import kotlinx.android.synthetic.main.activity_editar_direccion.colonia
import kotlinx.android.synthetic.main.activity_editar_direccion.numero
import kotlinx.android.synthetic.main.activity_editar_direccion.telefono
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class ConfirmarDireccionEnvioActivity : AppCompatActivity() {

    private var idsucursal: String? = ""
    private var varNombre: String? = ""
    private var varDireccion: String? = ""

    lateinit var parcelableDatosOrden: DatosOrden

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmar_direccion_envio)
        obtenerVariables()

        doAsync {
            val handler = CallHandler()
            val response = handler.ObtenerDireccion(parcelableDatosOrden.idusuario)
            val direccion: DireccionObject =
                jacksonObjectMapper().readValue(response)
            uiThread {
                if (direccion.status == 200) {
                    calle.setText(direccion.calle)
                    numero.setText(direccion.numero)
                    colonia.setText(direccion.colonia)
                    ciudad.setText(direccion.ciudad)
                    telefono.setText(direccion.telefono)
                } else {
                    Toast.makeText(
                        applicationContext,
                        direccion.mensaje,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }

        btnConfirmar.setOnClickListener {
            val intent =
                Intent(
                    this@ConfirmarDireccionEnvioActivity,
                    MenuCategoriaProductosActivity::class.java
                )
            var variables = guardarVariables()

            parcelableDatosOrden.enviocalle = calle.text.toString()
            parcelableDatosOrden.envionumero = numero.text.toString()
            parcelableDatosOrden.enviocolonia = colonia.text.toString()
            parcelableDatosOrden.enviociudad = ciudad.text.toString()
            parcelableDatosOrden.enviotelefono = Integer.parseInt(telefono.text.toString())
            parcelableDatosOrden.enviocalle = calle.text.toString()

            setExtraParcelable(parcelableDatosOrden)
            intent.putExtras(variables)
            startActivity(intent)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent =
            Intent(this@ConfirmarDireccionEnvioActivity, TipoPedidoActivity::class.java)
        val variables = guardarVariables()

        intent.putExtras(variables)
        setExtraParcelable(parcelableDatosOrden)
        startActivity(intent)
        finish()
    }

    private fun guardarVariables(): Bundle {
        val bundle = Bundle()
        bundle.putString("id", idsucursal)
        bundle.putString("nombre", varNombre)
        bundle.putString("direccion", varDireccion)

        return bundle
    }

    private fun obtenerVariables() {
        val bundle = intent.extras

        if (bundle != null) {
            varNombre = bundle.getString("nombre")
            varDireccion = bundle.getString("direccion")
            parcelableDatosOrden = if (getExtraParcelable() == null) parcelableDatosOrden else getExtraParcelable()!!
            idsucursal = parcelableDatosOrden.idsucursal.toString()
        }
    }

    private fun setExtraParcelable(datosOrden: DatosOrden?) {
        val prefs: SharedPreferences =
            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        if (datosOrden == null) {
            prefs.edit().putString("MiOrden", "").apply()
        } else {
            prefs.edit().putString(
                "MiOrden",
                jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)
            ).apply()
            prefs.edit().putString("direccionSuc", varDireccion).apply()
        }
    }

    private fun getExtraParcelable(): DatosOrden? {
        val prefs: SharedPreferences =
            getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val ordenString = prefs.getString("MiOrden", "")!!
        if (ordenString != "") {
            parcelableDatosOrden = jacksonObjectMapper().readValue(ordenString)
        }

        return parcelableDatosOrden
    }
}
