package com.dotstudio.mangiamos

import Ordenes
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.historial_gridview.view.*

class HistorialHandler(context: Context, var extra: ArrayList<Ordenes>) : BaseAdapter() {
    var context: Context? = context

    override fun getCount(): Int {
        return extra.size
    }

    override fun getItem(position: Int): Any {
        return extra[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val ordenes = this.extra[position]

        val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val extrasLayout = inflator.inflate(R.layout.historial_gridview, null)

        extrasLayout.txtFecha.text = ordenes.fecha
        extrasLayout.txtFolio.text = ordenes.numorden
        extrasLayout.txtHora.text = ordenes.hora
        extrasLayout.txtPrecio.text = "$ ${"%.2f".format(ordenes.total)}"

        return extrasLayout
    }
}