import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.lifecycle.MutableLiveData
import com.squareup.picasso.Picasso
import com.dotstudio.mangiamos.R
import kotlinx.android.synthetic.main.extras_gridview.view.*


class GridHandler(context: Context, var extra: ArrayList<Complementos>) : BaseAdapter() {
    var context: Context? = context
    var selectedComplementosPrecio: MutableLiveData<Double> = MutableLiveData()

    override fun getCount(): Int {
        return extra.size
    }

    override fun getItem(position: Int): Any {
        return extra[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val complemento = this.extra[position]

        val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val extrasLayout = inflator.inflate(R.layout.extras_gridview, null)

        if (complemento.imagen != "") {
            Picasso.get().load(complemento.imagen).into(extrasLayout.imgExtra)
        }
        extrasLayout.imgExtra.tag = complemento.idcomplemento
        extrasLayout.txtIngredientes.tag = complemento.precio
        extrasLayout.txtIngredientes.text = complemento.precio.toString()
        extrasLayout.txtNombre.text = complemento.nombre

        return extrasLayout
    }
}