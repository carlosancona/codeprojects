package com.dotstudio.mangiamos

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_respuesta_pago.*
import java.lang.Exception

class RespuestaPago : AppCompatActivity() {
    var respuesta: String? = ""
    var status: Int? = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_respuesta_pago)

        val bundle = intent.extras

        try {
            if (bundle != null) {
                respuesta = bundle.getString("responsePedido")
                status = bundle.getInt("statusPedido")
            }

            if (status == 200) {
                imgIcon.setImageResource(R.drawable.icon_success)
                txtPedido.text = "Pedido realizado con éxito"

                txtMensaje.text = respuesta
                limpiarLista()
            } else {
                imgIcon.setImageResource(R.drawable.icon_wrong)
                txtPedido.text = "Ups! Algo salió mal"
                txtMensaje.text = respuesta
            }
        } catch (e: Exception) {

        }


    }

    private fun limpiarLista() {
        val prefs: SharedPreferences =
            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        prefs.edit().remove("MiOrden").apply()
        prefs.edit().remove("imagenRecompensa").apply()
        prefs.edit().remove("descRecompensa").apply()
        prefs.edit().remove("tipoRecompensa").apply()
        prefs.edit().remove("cantidadRecompensa").apply()
    }

}
