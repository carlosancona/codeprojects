package com.dotstudio.mangiamos

import CallHandler
import Response
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_crear_cuenta.*
import android.widget.*
import com.dotstudio.mangiamos.ui.login.LoginActivity
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.lang.Exception


class CrearCuentaActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crear_cuenta)

        buttonRegistrar.setOnClickListener {
            var nombres = nombres.text
            var apellido = apellidos.text
            var calle = calle.text
            var numero = numero.text
            var colonia = colonia.text
            var ciudad = ciudad.text
            var telefono = telefono.text
            var email = correo.text
            var clave = passw.text
            var claveConf = passwConf.text

            if (nombres.isNullOrBlank() ||
                apellido.isNullOrBlank() ||
                calle.isNullOrBlank() ||
                numero.isNullOrBlank() ||
                colonia.isNullOrBlank() ||
                ciudad.isNullOrBlank() ||
                telefono.isNullOrBlank() ||
                email.isNullOrBlank() ||
                clave.isNullOrBlank()
            ) {
                val toast = Toast.makeText(
                    applicationContext,
                    "No puede haber campos vacios",
                    Toast.LENGTH_LONG
                )
                toast.show()
            }else if(clave.toString() != claveConf.toString()){
                val toast = Toast.makeText(
                    applicationContext,
                    "Las contraseñas no coinciden",
                    Toast.LENGTH_LONG
                )
                toast.show()
            }else if(!checkTerminos.isChecked){
                val toast = Toast.makeText(
                    applicationContext,
                    "Debe aceptar los terminos y condiciones para crear una cuenta",
                    Toast.LENGTH_LONG
                )
                toast.show()
            }
            else {
                doAsync {
                    var handler = CallHandler()
                    var jsonResponse = handler.CrearCuenta(
                        nombres.toString(),
                        apellido.toString(),
                        calle.toString(),
                        numero.toString(),
                        colonia.toString(),
                        ciudad.toString(),
                        telefono.toString(),
                        email.toString(),
                        clave.toString()
                    )
                    var mensaje: Response =
                    jacksonObjectMapper().readValue(jsonResponse)
                    uiThread {
                        try {
                            if (mensaje.status == 200) {
                                val toast = Toast.makeText(
                                    applicationContext,
                                    mensaje.mensaje,
                                    Toast.LENGTH_LONG
                                )
                                toast.show()
                                val intent =
                                    Intent(this@CrearCuentaActivity, LoginActivity::class.java)
                                startActivity(intent)
                            } else {
                                val toast = Toast.makeText(
                                    applicationContext,
                                    mensaje.mensaje,
                                    Toast.LENGTH_LONG
                                )
                                toast.show()
                            }
                        } catch (e: Exception) {
                            val toast = Toast.makeText(
                                applicationContext,
                                e.message,
                                Toast.LENGTH_LONG
                            )
                            toast.show()
                        }
                    }
                }
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            return true
        }
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        finish()
    }
}

