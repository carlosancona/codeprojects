package com.dotstudio.mangiamos

import BusquedaSucursales
import CallHandler
import DatosOrden
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.util.Log
import android.view.View
import android.widget.SearchView
import android.widget.SearchView.OnQueryTextListener
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_maps.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.IOException
import java.util.ArrayList

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private var idsucursal: String? = ""
    private var varNombre: String? = ""
    private var varDireccion: String? = ""
    private var parcelableDatosOrden: DatosOrden? = DatosOrden(
        0,
        0,
        "",
        "",
        "",
        "",
        0,
        "",
        0.0,
        "",
        "",
        "",
        "",
        0.0,
        0.0,
        ArrayList(),
        ArrayList()
    )

    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        getMarkers("")
        obtenerVariables()

        val listamenubtn= findViewById<FloatingActionButton>(R.id.floatingMenuButton)
        listamenubtn.setOnClickListener { view ->
            val intent = Intent(applicationContext,SucursalesActivity::class.java)
            startActivity(intent)
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        mapFinder.setOnQueryTextListener(object : OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchLocation()
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        val intent =
            Intent(this@MapsActivity, MainActivity::class.java)
        guardarVariables()
        startActivity(intent)
        finish()
    }

    override fun finish() {
        super.finish()
        val activity = this@MapsActivity as Activity
        activity.overridePendingTransition(R.transition.slide_in_up, R.transition.slide_out_up)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        setUpMap()
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        mMap.isMyLocationEnabled = true

        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            if (location != null) {
                lastLocation = location
                val currentLatLong = LatLng(location.latitude, location.longitude)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLong, 15f))
            }
        }
    }

    fun searchLocation() {
        val locationSearch: SearchView = findViewById(R.id.mapFinder)
        lateinit var location: String
        location = locationSearch.query.toString()
        var addressList: List<Address>? = null

        if (location == "") {
            Toast.makeText(applicationContext, "Escriba un lugar", Toast.LENGTH_SHORT).show()
        } else {
            val geoCoder = Geocoder(this)
            try {
                addressList = geoCoder.getFromLocationName(location, 1)
                getMarkers(location)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            val address = addressList!![0]
            val latLng = LatLng(address.latitude, address.longitude)
            mMap.addMarker(MarkerOptions().position(latLng).title(location))
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng))
        }
    }

    private fun getMarkers(query: String) {

        doAsync {
            val handler = CallHandler()
            val response = handler.ListaSucursales(query)

            val mensaje: BusquedaSucursales =
                jacksonObjectMapper().readValue(response)
            uiThread {
                if (mensaje.status == 200) {
                    mensaje.listasucursales.forEach {
                        val marker = mMap.addMarker(
                            MarkerOptions()
                                .position(LatLng(it.latitud, it.longitud))
                                .title(it.nombre)
                                .snippet(it.direccion)
                                .rotation((33.5).toFloat())
                                .icon(
                                    bitmapDescriptorFromVector(
                                        applicationContext,
                                        R.drawable.pinmap
                                    )
                                )
                        )
                        marker.tag = it.idsucursal

                        mMap.setOnMarkerClickListener { marker ->
                            if (marker.isInfoWindowShown) {
                                marker.hideInfoWindow()
                                val intent = Intent(
                                    applicationContext,
                                    DetalleRestauranteActivity::class.java
                                )

                                val bundle = Bundle()
                                bundle.putString("id", marker.tag.toString())
                                intent.putExtras(bundle)
                                startActivity(intent)

                            } else {
                                marker.showInfoWindow()
                                val intent = Intent(
                                    this@MapsActivity,
                                    DetalleRestauranteActivity::class.java
                                )
                                val bundle = Bundle()
                                bundle.putString("id", marker.tag.toString())
                                intent.putExtras(bundle)
                                startActivity(intent)

                            }
                            true
                        }
                    }
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Ocurrio un error, intente de nuevo",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    private fun guardarVariables(): Bundle {
        val bundle = Bundle()
        bundle.putString("nombre", varNombre)
        bundle.putString("direccion", varDireccion)
        setExtraParcelable(intent, parcelableDatosOrden)

        return bundle
    }

    private fun obtenerVariables() {
        val bundle = intent.extras

        if (bundle != null) {
            varNombre = bundle.getString("nombre")
            varDireccion = bundle.getString("direccion")
            idsucursal = bundle.getString("id")
        }
        parcelableDatosOrden = getExtraParcelable()
        //if (getExtraParcelable() == null) parcelableDatosOrden else getExtraParcelable()!!
    }

    private fun setExtraParcelable(intent: Intent, datosOrden: DatosOrden?) {
        intent.putExtra("datosOrden", datosOrden)

        val prefs: SharedPreferences =
            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

//        if (datosOrden == null) {
//            prefs.edit().putString("MiOrden", "").apply()
//        } else {
//            prefs.edit().putString(
//                "MiOrden",
//                jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)
//            ).apply()
//            prefs.edit().putString("direccionSuc", varDireccion).apply()
//        }
        prefs.edit().putString(
            "MiOrden",
            jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)
        ).apply()
        prefs.edit().putString("direccionSuc", varDireccion).apply()

        Log.d("setExtra", jacksonObjectMapper().writeValueAsString(parcelableDatosOrden))
    }

    private fun getExtraParcelable(): DatosOrden? {
        val prefs: SharedPreferences =
            getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val ordenString = prefs.getString("MiOrden", "")!!
        if (ordenString != "") {
            parcelableDatosOrden = jacksonObjectMapper().readValue(ordenString)
        }
        Log.d("getExtra", ordenString)
        return parcelableDatosOrden
    }
}
