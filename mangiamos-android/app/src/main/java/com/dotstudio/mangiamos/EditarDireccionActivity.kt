package com.dotstudio.mangiamos

import CallHandler
import DireccionObject
import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import android.widget.Toast
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.android.synthetic.main.activity_editar_direccion.*
import org.jetbrains.anko.custom.async
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class EditarDireccionActivity : AppCompatActivity() {

    var userid: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editar_direccion)

        var prefs: SharedPreferences =
            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
        userid = prefs.getInt("userid", 0) //getting userid

        doAsync {
            var handler = CallHandler()
            var response = handler.ObtenerDireccion(userid)
            var direccion: DireccionObject =
                jacksonObjectMapper().readValue(response)
            uiThread {
                if (direccion.status == 200) {
                    calle.setText(direccion.calle)
                    numero.setText(direccion.numero)
                    colonia.setText(direccion.colonia)
                    ciudad.setText(direccion.ciudad)
                    telefono.setText(direccion.telefono)
                } else {
                    Toast.makeText(
                        applicationContext,
                        direccion.mensaje,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        doAsync {
            var handler = CallHandler()
            var response = handler.ActualizarDireccion(
                userid,
                calle.text.toString(),
                numero.text.toString(),
                colonia.text.toString(),
                ciudad.text.toString(),
                telefono.text.toString()
            )
            var mensaje: DireccionObject =
                jacksonObjectMapper().readValue(response)
            uiThread {
                if (mensaje.status == 200) {
                    Toast.makeText(
                        applicationContext,
                        mensaje.mensaje,
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    Toast.makeText(
                        applicationContext,
                        mensaje.mensaje,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
        finish()
    }
}
