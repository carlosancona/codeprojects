package com.dotstudio.mangiamos

import CallHandler
import DatosOrden
import DetallesSucursal
import Puntos
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.ActionBar
import android.view.Gravity
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detalle_restaurante.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class DetalleRestauranteActivity : AppCompatActivity() {

    private var idsucursal: String? = ""
    private var varNombre: String? = ""
    private var varDireccion: String? = ""
    private var stripeSucursal: String? = ""
    private var parcelableDatosOrden: DatosOrden? = DatosOrden(
        0,
        0,
        "",
        "",
        "",
        "",
        0,
        "",
        0.0,
        "",
        "",
        "",
        "",
        0.0,
        0.0,
        ArrayList(),
        ArrayList()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_restaurante)

        val view = layoutInflater.inflate(R.layout.custom_action_bar, null)

        val params = ActionBar.LayoutParams(
            ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.MATCH_PARENT,
            Gravity.CENTER
        )

        supportActionBar!!.setCustomView(view, params)
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        val b = intent.extras

        if (b != null) {
            obtenerVariables()
            setDatosSucursal(view)
        }

        btnPedido.setOnClickListener {
            val intent =
                Intent(this@DetalleRestauranteActivity, TipoPedidoActivity::class.java)
            val variables = guardarVariables()
            intent.putExtras(variables)

            if (idsucursal!!.toInt() == parcelableDatosOrden?.idsucursal) {
                parcelableDatosOrden?.idsucursal = idsucursal!!.toInt()
            } else {
                cargarPuntos()

                parcelableDatosOrden = DatosOrden(
                    idsucursal!!.toInt(),
                    0,
                    "",
                    "",
                    "",
                    "",
                    0,
                    "",
                    0.0,
                    "",
                    "",
                    "",
                    "",
                    0.0,
                    0.0,
                    ArrayList(),
                    ArrayList()
                )
            }

            setExtraParcelable(parcelableDatosOrden)
            startActivity(intent)
            finish()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent =
            Intent(this@DetalleRestauranteActivity, MapsActivity::class.java)
        val variables = guardarVariables()

        intent.putExtras(variables)
        setExtraParcelable(parcelableDatosOrden)
        startActivity(intent)
        finish()
    }

    private fun setDatosSucursal(view: View) {
        doAsync {
            val handler = CallHandler()
            val response = handler.DetalleSucursal(idsucursal!!.toInt())
            uiThread {
                val sucursal: DetallesSucursal = jacksonObjectMapper().readValue(response)
                if (sucursal.status == 200) {
                    val title = view.findViewById(R.id.actionbar_title) as TextView
                    val subTitle = view.findViewById(R.id.actionbar_subtitle) as TextView

                    //setting variable values
                    varNombre = sucursal.nombre
                    varDireccion = sucursal.direccion
                    stripeSucursal = sucursal.stripe

                    title.text = sucursal.nombre
                    subTitle.text = sucursal.direccion
                    ciudad.text = sucursal.ciudad

                    Picasso.get().load(sucursal.imagen).into(imgSucursal)

                    hrLunes.text = sucursal.horariolunes
                    hrMartes.text = sucursal.horariomartes
                    hrMiercoles.text = sucursal.horariomiercoles
                    hrJueves.text = sucursal.horariojueves
                    hrViernes.text = sucursal.horarioviernes
                    hrSabado.text = sucursal.horariosabado
                    hrDomingo.text = sucursal.horariodomingo

                    if (sucursal.drivethru!!)
                        checkDrive.setImageResource(R.drawable.icon_check_on)
                    else
                        checkDrive.setImageResource(R.drawable.icon_check_off)

                    if (sucursal.recoger!!)
                        checkRecoger.setImageResource(R.drawable.icon_check_on)
                    else
                        checkRecoger.setImageResource(R.drawable.icon_check_off)

                    if (sucursal.comerahi!!)
                        checkComer.setImageResource(R.drawable.icon_check_on)
                    else
                        checkComer.setBackgroundResource(R.drawable.icon_check_off)

                    if (sucursal.abierto!!) {
                        statusSucursal.text = "ABIERTO"
                        statusSucursal.setTextColor(getColor(R.color.colorGreen))
                    } else {
                        statusSucursal.text = "CERRADO"
                        statusSucursal.setTextColor(getColor(R.color.colorGray))
                    }
                    //if both vars are true, enables button
                    btnPedido.isEnabled = sucursal.ordernarporinternet!! && sucursal.abierto

                    btnLlamar.setOnClickListener {
                        var intent = Intent(Intent.ACTION_DIAL)
                        intent.data = Uri.parse("tel:${sucursal.telefono}")
                        this@DetalleRestauranteActivity.startActivity(intent)
                    }

                    btnLlegar.setOnClickListener {
                        val intent = Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("geo:${sucursal.latitud},${sucursal.longitud}?q=${sucursal.latitud},${sucursal.longitud}(${sucursal.nombre})")
                        )
                        startActivity(intent)
                    }
                }
            }
        }
    }

    private fun guardarVariables(): Bundle {
        val bundle = Bundle()
        bundle.putString("nombre", varNombre)
        bundle.putString("direccion", varDireccion)
        bundle.putString("id", idsucursal)

        return bundle
    }

    private fun obtenerVariables() {
        val bundle = intent.extras

        if (bundle != null) {
            idsucursal = bundle.getString("id")
            varNombre = bundle.getString("nombre")
            varDireccion = bundle.getString("direccion")

            parcelableDatosOrden =
                if (getExtraParcelable() == null) {
                    parcelableDatosOrden
                } else {
                    getExtraParcelable()
                }
        }
    }

    private fun setExtraParcelable(datosOrden: DatosOrden?) {
        val prefs: SharedPreferences =
            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        if (datosOrden == null) {
            prefs.edit().putString("MiOrden", "").apply()
        } else {
            prefs.edit().putString(
                "MiOrden",
                jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)
            ).apply()
            prefs.edit().putString("direccionSuc", varDireccion).apply()
        }
        prefs.edit().putString("nombreSucursal", varNombre).apply()
        prefs.edit().putString("stripeSucursal", stripeSucursal).apply()

    }

    private fun getExtraParcelable(): DatosOrden? {
        val prefs: SharedPreferences =
            getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val ordenString = prefs.getString("MiOrden", "")!!
        if (ordenString != "") {
            parcelableDatosOrden = jacksonObjectMapper().readValue(ordenString)
        }

        return parcelableDatosOrden
    }

    private fun cargarPuntos() {
        val prefs: SharedPreferences =
            this@DetalleRestauranteActivity.getSharedPreferences(
                "SessionInfo",
                Context.MODE_PRIVATE
            )

        val userid = prefs.getInt("userid", 0)
        if (userid != 0) {
            doAsync {
                val handler = CallHandler()

                val responsePuntos = handler.ObtenerPuntos(userid)
                val puntos: Puntos = jacksonObjectMapper().readValue(responsePuntos)

                if (puntos.status == 200) {
                    Log.d("puntos:", puntos.mensaje)
                    prefs.edit().putString("puntos", puntos.mensaje).apply()
                }
            }
        }
    }
}
