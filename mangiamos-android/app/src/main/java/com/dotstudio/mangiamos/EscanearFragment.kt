package com.dotstudio.mangiamos

import QrEncoder
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import kotlinx.android.synthetic.main.fragment_escanear.view.*

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ImportFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ImportFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EscanearFragment : Fragment() {


    lateinit var escanearView: View
    internal var bitmap: Bitmap? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        escanearView = inflater.inflate(R.layout.fragment_escanear, container, false)

        //if session detected hides all elements from fragment
        var prefs: SharedPreferences =
            this.activity!!.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
        var userid = prefs.getInt("userid", 0) //getting userid
        var qrcode = prefs.getString("userqr", "") //getting userid

        if (userid != 0) { //if userid contains an id redirects to menu
            escanearView.LoggedInView?.visibility = View.VISIBLE
            escanearView.LoggedOffView?.visibility = View.GONE

            var encoder = QrEncoder()
            val bitmap = encoder.TextToImageEncode(qrcode!!)
            escanearView.ivQrCode.setImageBitmap(bitmap)
        }else{
            escanearView.LoggedInView?.visibility = View.GONE
            escanearView.LoggedOffView?.visibility = View.VISIBLE
        }

        escanearView.btnLogin.setOnClickListener {
            val intent = Intent(activity, IniciarSesionActivity::class.java)
            activity?.startActivity(intent)
            activity?.finish()
        }

        return escanearView
    }

}
