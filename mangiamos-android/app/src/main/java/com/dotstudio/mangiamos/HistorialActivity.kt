package com.dotstudio.mangiamos

import CallHandler
import Historial
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import android.widget.Toast
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import kotlinx.android.synthetic.main.activity_historial.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class HistorialActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_historial)

        val prefs: SharedPreferences = getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
        val userid = prefs.getInt("userid", 0)

        doAsync {
            val handler = CallHandler()
            val producto = handler.ObtenerHistorial(userid)
            val historial: Historial = jacksonObjectMapper().readValue(producto)

            uiThread {
                if (historial.status == 200) {
                    val historialGrid =
                        HistorialHandler(this@HistorialActivity, historial.listaordenes)
                    gridView.adapter = historialGrid
                } else {
                    Toast.makeText(
                        applicationContext,
                        historial.mensaje,
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent = Intent(this@HistorialActivity, MainActivity::class.java)
        startActivity(intent)
    }
}

