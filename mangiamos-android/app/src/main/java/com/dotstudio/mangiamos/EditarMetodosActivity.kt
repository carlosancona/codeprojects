package com.dotstudio.mangiamos

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_editar_metodos.*
import java.lang.Exception

class EditarMetodosActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editar_metodos)

        try {
            txtTarjeta.addTextChangedListener(CardFormat())

            val text =
                applicationContext.openFileInput("card_data").bufferedReader().useLines { lines ->
                    lines.fold("") { titular, tarjeta ->
                        "$titular\n$tarjeta"
                    }
                }

            var card_values = text.split(",").toTypedArray()

            txtTitular.setText(card_values[0])
            txtTarjeta.setText(card_values[1])
            txtMes.setText(card_values[2])
            txtYear.setText(card_values[3])
        } catch (e: Exception) {
            Log.d("error", e.message)
        }


        btnGuardarTarjeta.setOnClickListener {
            try {
                val fileContents = "${txtTitular.text}," +
                        "${txtTarjeta.text}," +
                        "${txtMes.text}," +
                        "${txtYear.text}"

                applicationContext.openFileOutput("card_data", Context.MODE_PRIVATE).use {
                    it.write(fileContents.toByteArray())
                }

                Toast.makeText(
                    applicationContext,
                    "Tarjeta guardada",
                    Toast.LENGTH_LONG
                ).show()
            } catch (e: Exception) {
                Toast.makeText(
                    applicationContext,
                    "Ocurrio un error ${e.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        finish()
    }

    class CardFormat : TextWatcher {
        private val space = ' '

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun afterTextChanged(s: Editable) {
            // Remove all spacing char
            var pos = 0
            while (true) {
                if (pos >= s.length) break
                if (space == s[pos] && ((pos + 1) % 5 != 0 || pos + 1 == s.length)) {
                    s.delete(pos, pos + 1)
                } else {
                    pos++
                }
            }

            pos = 4

            while (true) {
                if (pos >= s.length) break
                val c = s[pos]
                // Only if its a digit where there should be a space we insert a space
                if ("0123456789".indexOf(c) >= 0) {
                    s.insert(pos, "" + space)
                }
                pos += 5
            }
        }
    }

}

