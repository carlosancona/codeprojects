package com.dotstudio.mangiamos

import CallHandler
import DatosOrden
import ProductosCategoria
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.core.view.marginEnd
import androidx.core.view.marginStart
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_menu_categoria_productos.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

import java.lang.Exception

class ElegirProductoActivity : AppCompatActivity() {

    private var varNombre: String? = ""
    private var idsucursal: String? = ""
    private var varDireccion: String? = ""
    private var idcategoria: Int = Int.MIN_VALUE
    private var idproducto: Int = Int.MIN_VALUE
    //lateinit var parcelableDatosOrden: DatosOrden

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_elegir_producto)

        val view = layoutInflater.inflate(R.layout.custom_action_bar, null)

        val b = intent.extras
        if (b != null) {
            obtenerVariables()

            doAsync {
                val handler = CallHandler()
                val response = handler.ListaProductos(idcategoria)
                val categoria: ProductosCategoria =
                    jacksonObjectMapper().readValue(response)
                uiThread {
                    if (categoria.status == 200) {
                        try {
                            for (producto in categoria.listaproductos!!) {
                                AddRows(
                                    producto.nombre!!,
                                    producto.imagen!!,
                                    producto.id
                                )
                            }
                        } catch (e: Exception) {
                            Toast.makeText(
                                applicationContext,
                                e.message,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "Ocurrio un error, ${categoria.mensaje}",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
            }
        }

        val params = ActionBar.LayoutParams(
            ActionBar.LayoutParams.WRAP_CONTENT,
            ActionBar.LayoutParams.MATCH_PARENT,
            Gravity.CENTER
        )

        val title = view.findViewById(R.id.actionbar_title) as TextView
        val subTitle = view.findViewById(R.id.actionbar_subtitle) as TextView

        title.text = varNombre
        subTitle.text = varDireccion

        supportActionBar!!.setCustomView(view, params)
        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed()
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent =
            Intent(this@ElegirProductoActivity, MenuCategoriaProductosActivity::class.java)
        val variables = guardarVariables()

        intent.putExtras(variables)
        //setExtraParcelable(intent, parcelableDatosOrden)
        startActivity(intent)
        finish()
    }

    private fun guardarVariables(): Bundle {
        var bundle = Bundle()
        bundle.putString("id", idsucursal)
        bundle.putString("nombre", varNombre)
        bundle.putString("direccion", varDireccion)
        bundle.putInt("idproducto", idproducto)
        bundle.putInt("idCategoria", idcategoria)

        return bundle
    }

    private fun obtenerVariables() {
        val bundle = intent.extras

        if (bundle != null) {
            varNombre = bundle.getString("nombre")
            varDireccion = bundle.getString("direccion")
            //parcelableDatosOrden = if (getExtraParcelable() == null) parcelableDatosOrden else getExtraParcelable()!!
            idsucursal = bundle.getString("id")

            //idsucursal = parcelableDatosOrden.idsucursal.toString()
            idcategoria = bundle.getInt("idCategoria")
        }
    }

    fun AddRows(rowText: String, imagenUrl: String, id: Int) {
        val layout = linearLayout

        val row = TableRow(this)
        val imagenCategoria = ImageView(this)
        val arrow = ImageView(this)
        val texto = TextView(this)
        val horizontalLayout1 = LinearLayout(this)
        val horizontalLayout2 = LinearLayout(this)
        val horizontalLayout3 = LinearLayout(this)
        val space = LinearLayout(this)

        space.setBackgroundColor(resources.getColor(R.color.colorBlack))
        space.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 1)

        horizontalLayout1.orientation = LinearLayout.VERTICAL
        horizontalLayout1.layoutParams = TableRow.LayoutParams(180, 215)
        horizontalLayout1.gravity = Gravity.CENTER_VERTICAL

        horizontalLayout2.orientation = LinearLayout.VERTICAL
        horizontalLayout2.layoutParams = TableRow.LayoutParams(570, 215)
        horizontalLayout2.gravity = Gravity.CENTER_VERTICAL

        horizontalLayout3.orientation = LinearLayout.VERTICAL
        horizontalLayout3.layoutParams = TableRow.LayoutParams(155, 215)
        horizontalLayout3.gravity = Gravity.CENTER_VERTICAL

        row.layoutParams = TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, 215)
        row.gravity = Gravity.CENTER_VERTICAL
        row.setBackgroundColor(resources.getColor(R.color.colorWhite))
        row.setPadding(70, 0, 0, 0)
        row.isClickable = true
        row.tag = id

        imagenCategoria.layoutParams = TableRow.LayoutParams(200, 200)
        imagenCategoria.setPaddingRelative(5, 0, 0, 0)

        if (imagenUrl != "") {
            Picasso.get().load(imagenUrl).into(imagenCategoria)
        }

        texto.text = rowText
        texto.setPadding(80, 0, 0, 0)
        texto.textSize = 15F
        texto.setTypeface(null, Typeface.BOLD);
        texto.setTextColor(resources.getColor(R.color.colorBlue))

        arrow.layoutParams = TableRow.LayoutParams(50, 50)
        arrow.setPaddingRelative(20, 0, 0, 0)
        arrow.setImageResource(R.drawable.btn_backgrey)
        arrow.rotation = 180F

        horizontalLayout2.addView(texto)
        horizontalLayout1.addView(imagenCategoria)
        horizontalLayout3.addView(arrow)

        row.addView(horizontalLayout1)
        row.addView(horizontalLayout2)
        row.addView(horizontalLayout3)

        layout.addView(row)
        layout.addView(space)

        row.setOnClickListener(onClickListener)
    }

    var onClickListener: View.OnClickListener = View.OnClickListener { v ->
        idproducto = v.tag as Int

        val intent =
            Intent(this@ElegirProductoActivity, DetalleProductoActivity::class.java)
        val variables = guardarVariables()
        //setExtraParcelable(intent, parcelableDatosOrden)
        intent.putExtras(variables)
        startActivity(intent)
    }

//    private fun setExtraParcelable(intent: Intent, datosOrden: DatosOrden?) {
//        intent.putExtra("datosOrden", datosOrden)
//
//        val prefs: SharedPreferences =
//            this.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
//
//        if (datosOrden == null) {
//            prefs.edit().putString("MiOrden", "").apply()
//        } else {
//            prefs.edit().putString(
//                "MiOrden",
//                jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)
//            ).apply()
//            prefs.edit().putString("direccionSuc", varDireccion).apply()
//        }
//    }

//    private fun getExtraParcelable(): DatosOrden? {
//        val prefs: SharedPreferences =
//            getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
//
//        val ordenString = prefs.getString("MiOrden", "")!!
//        if (ordenString != "") {
//            parcelableDatosOrden = jacksonObjectMapper().readValue(ordenString)
//        }
//
//        return parcelableDatosOrden
//    }
}
