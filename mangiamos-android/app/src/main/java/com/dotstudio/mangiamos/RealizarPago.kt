package com.dotstudio.mangiamos

import CallHandler
import DatosOrden
import OrdenGenerada
import Puntos
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.textfield.TextInputEditText
import com.stripe.android.ApiResultCallback
import com.stripe.android.PaymentConfiguration
import com.stripe.android.Stripe
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import kotlinx.android.synthetic.main.activity_realizar_pago.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.text.SimpleDateFormat
import java.util.*


class RealizarPago : AppCompatActivity() {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var userId = 0
    private lateinit var stripe: Stripe
    private var publishKey: String = ""//"pk_test_6cTF4X2Wsb7jdl7lruh9Nnce00RrqAOEbv"
    var idempotency = ""
    lateinit var txtCardView: TextInputEditText
    private var parcelableDatosOrden: DatosOrden? = DatosOrden(
        0,
        0,
        "",
        "",
        "",
        "",
        0,
        "",
        0.0,
        "",
        "",
        "",
        "",
        0.0,
        0.0,
        ArrayList(),
        ArrayList()
    )
    private val space = ' '

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_realizar_pago)

        txtCardView = txtCard
        txtCardView.setCompoundDrawablesWithIntrinsicBounds(
            0,
            0,
            R.drawable.mastercard_card_on,
            0
        )
        doAsync {
            obtenerVariables()
        }

        var cardtype = ""

        val prefs: SharedPreferences =
            getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        userId = prefs.getInt("userid", 0)
        publishKey = prefs.getString("stripeSucursal", "")!!

        Log.d("token sucursal", publishKey)
        stripe = Stripe(this, publishKey)

        try {

            txtCard.addTextChangedListener(
                object : TextWatcher {
                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        val ccNum = s.toString()

                        if (ccNum.length >= 2) {

                            for (i in 0 until listOfPattern()!!.size) {
                                if (ccNum.substring(0, 2).matches(listOfPattern()!![i])) {
                                    txtCardView.setCompoundDrawablesWithIntrinsicBounds(
                                        0,
                                        0,
                                        imageArray[i],
                                        0
                                    )
                                    cardtype = i.toString()
                                }
                            }
                        }
                    }

                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun afterTextChanged(s: Editable) {
                        // Remove all spacing char
                        var pos = 0
                        while (true) {
                            if (pos >= s.length) break
                            if (space == s[pos] && ((pos + 1) % 5 != 0 || pos + 1 == s.length)) {
                                s.delete(pos, pos + 1)
                            } else {
                                pos++
                            }
                        }

                        pos = 4

                        while (true) {
                            if (pos >= s.length) break
                            val c = s[pos]
                            // Only if its a digit where there should be a space we insert a space
                            if ("0123456789".indexOf(c) >= 0) {
                                s.insert(pos, "" + space)
                            }
                            pos += 5
                        }
                    }
                }

            )

            val text =
                applicationContext.openFileInput("card_data").bufferedReader().useLines { lines ->
                    lines.fold("") { titular, tarjeta ->
                        "$titular\n$tarjeta"
                    }
                }
            val cardValues = text.split(",").toTypedArray()

            txtTitular.text = cardValues[0]
            txtCard.setText(cardValues[1])
            txtMes.setText(cardValues[2])
            txtYear.setText(cardValues[3])
        } catch (e: Exception) {
            Log.d("", e.message)
        }

        try {
            PaymentConfiguration.init(
                applicationContext,
                publishKey
            )

            btnPagar.setOnClickListener {
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

                fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                    if (location != null) {
                        parcelableDatosOrden?.latitud = location.latitude
                        parcelableDatosOrden?.longitud = location.longitude
                    }
                }

                val date = getCurrentDateTime()
                val dateInString = date.toString("yyyy-MM-ddHH:mm:ss")
                idempotency =
                    "$dateInString${parcelableDatosOrden?.idusuario}${parcelableDatosOrden?.idsucursal}"

                val card = Card
                val c = card.create(
                    number = txtCard.text.toString(),
                    expMonth = txtMes.text.toString().toInt(),
                    expYear = txtYear.text.toString().toInt(),
                    cvc = txtCvv.text.toString()
                )

                loading.visibility = View.VISIBLE
                c.let { card ->
                    stripe = Stripe(
                        applicationContext,
                        PaymentConfiguration.getInstance(applicationContext).publishableKey
                    )

                    stripe.createToken(c, idempotency, object : ApiResultCallback<Token> {
                        override fun onSuccess(result: Token) {
                            val tokenID = result.id//"tok_visa"//
                            parcelableDatosOrden?.tokenstripe = tokenID
                            parcelableDatosOrden?.lugarpago = "movil"
                            parcelableDatosOrden?.estatus = "pagado"
                            if (parcelableDatosOrden?.recompensaOrdenes?.count() != 0) {
                                cargarPuntos()
                            }

                            val handler = CallHandler()
                            var caller =
                                jacksonObjectMapper().writeValueAsString(parcelableDatosOrden)

                            doAsync {
                                val response = handler.GenerarOrden(caller)
                                val respuesta: OrdenGenerada =
                                    jacksonObjectMapper().readValue(response)
                                Log.d("", caller)
                                uiThread {
                                    loading.visibility = View.GONE

                                    val intent = Intent(
                                        applicationContext,
                                        RespuestaPago::class.java
                                    )
                                    val bundle = Bundle()
                                    bundle.putString("responsePedido", respuesta.mensaje)
                                    bundle.putInt("statusPedido", respuesta.status!!)

                                    intent.putExtras(bundle)
                                    startActivity(intent)
                                }
                            }
                        }

                        override fun onError(e: Exception) {
                            Log.d("error:", e.toString())
                            Log.d("idempotency", idempotency)

                            loading.visibility = View.GONE
                        }
                    })
                }
            }
        } catch (e: Exception) {
            Log.d("error", e.message)
        }
    }

    fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
        val formatter = SimpleDateFormat(format, locale)
        return formatter.format(this)
    }

    private fun getCurrentDateTime(): Date {
        return Calendar.getInstance().time
    }

    private fun obtenerVariables() {
        val prefs: SharedPreferences =
            getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)

        val ordenString = prefs.getString("MiOrden", "")!!
        if (ordenString != "") {
            parcelableDatosOrden = jacksonObjectMapper().readValue(ordenString)
        }

        var listaProductos = parcelableDatosOrden?.productoOrdenes
        for (p in listaProductos!!) {
            parcelableDatosOrden!!.totalpagado += p.total
        }
    }

    private fun cargarPuntos() {
        doAsync {
            val handler = CallHandler()

            val responsePuntos = handler.ObtenerPuntos(userId)
            val puntos: Puntos = jacksonObjectMapper().readValue(responsePuntos)

            if (puntos.status == 200) {
                val prefs: SharedPreferences =
                    this@RealizarPago.getSharedPreferences("SessionInfo", Context.MODE_PRIVATE)
                Log.d("puntos:", puntos.mensaje)
                prefs.edit().putString("puntos", puntos.mensaje).apply()
            }

        }
    }

    fun listOfPattern(): ArrayList<Regex>? {
        val listOfPattern = ArrayList<Regex>()

        val ptVisa = "^4[0-9]$".toRegex()
        listOfPattern.add(ptVisa)

        val ptMasterCard = "^5[1-5]$".toRegex()
        listOfPattern.add(ptMasterCard)

        val ptAmeExp = "^3[47]$".toRegex()
        listOfPattern.add(ptAmeExp)

        return listOfPattern
    }

    var imageArray =
        arrayOf<Int>(
            R.drawable.visa_card_on,
            R.drawable.mastercard_card_on,
            R.drawable.american_card_on
        )

}

