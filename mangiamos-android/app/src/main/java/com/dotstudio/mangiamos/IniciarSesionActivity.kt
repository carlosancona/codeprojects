package com.dotstudio.mangiamos

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dotstudio.mangiamos.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_iniciarsesion.*

class IniciarSesionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_iniciarsesion)

        btnCreateAcc.setOnClickListener {
            val intent = Intent(this, CrearCuentaActivity::class.java)
            startActivity(intent)
        }

        btnLogin.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    fun redirectOnClick() {
        val intent = Intent(this, TerminosActivity::class.java)
        startActivity(intent)
    }
}
