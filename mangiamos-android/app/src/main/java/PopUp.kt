import android.app.AlertDialog
import android.content.Context
import android.content.res.Resources
import android.os.Build
import android.transition.Slide
import android.transition.TransitionManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import com.dotstudio.mangiamos.R
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.android.material.textfield.TextInputLayout
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class PopUp {

    fun CreatePopUp(activity: ViewGroup, text: String, inflater: LayoutInflater): Unit {

        // Inflate a custom view using layout inflater
        val view = inflater.inflate(R.layout.activity_pop_up_window, null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
            view, // Custom view to show in popup window
            LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
            LinearLayout.LayoutParams.WRAP_CONTENT // Window height
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            popupWindow.elevation = 10.0F
        }
        val tv = view.findViewById<TextView>(R.id.text_view)
        val buttonPopup = view.findViewById<Button>(R.id.button_popup)
        tv.text = text

        // Set a click listener for popup's button widget
        buttonPopup.setOnClickListener {
            // Dismiss the popup window
            popupWindow.dismiss()
        }

        // Set a dismiss listener for popup window
        /*popupWindow.setOnDismissListener {
            Toast.makeText(appContext,"Popup closed", Toast.LENGTH_SHORT).show()
        }*/


        // Finally, show the popup window on app
        TransitionManager.beginDelayedTransition(activity)
        popupWindow.showAtLocation(
            activity, // Location to display popup window
            Gravity.CENTER, // Exact position of layout to display popup
            0, // X offset
            0 // Y offset
        )

        // If API level 23 or higher then execute the code
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.BOTTOM
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.BOTTOM
            popupWindow.exitTransition = slideOut
        }
    }

    fun CreateRecoverPopUp(context: Context, resources: Resources) {
        val textInputLayout = TextInputLayout(context)
        textInputLayout.setPadding(
            resources.getDimensionPixelOffset(R.dimen.activity_horizontal_margin),
            0,
            resources.getDimensionPixelOffset(R.dimen.activity_horizontal_margin),
            0
        )
        val input = EditText(context)
        textInputLayout.hint = "Email"
        textInputLayout.addView(input)

        val alert = AlertDialog.Builder(context)
            .setTitle("Recuperar contraseña")
            .setView(textInputLayout)
            .setMessage("Ingrese su correo")
            .setPositiveButton("Aceptar") { dialog, _ ->
                doAsync {
                    val handler = CallHandler()
                    val response = handler.RecuperarContraseña(input.text.toString())
                    val mensaje: Response =
                        jacksonObjectMapper().readValue(response)
                    uiThread {
                        val toast = Toast.makeText(
                            context,
                            mensaje.mensaje,
                            Toast.LENGTH_SHORT
                        )
                        toast.show()
                    }
                }
                dialog.cancel()
            }
            .setNegativeButton("Cancelar") { dialog, _ ->
                dialog.cancel()
            }.create()

        alert.show()
    }

}