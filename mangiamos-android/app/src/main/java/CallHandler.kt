import android.os.Parcel
import android.os.Parcelable
import okhttp3.*
import okhttp3.Response

class CallHandler {

    fun CrearCuenta(
        nombre: String,
        apellido: String,
        calle: String,
        numero: String,
        colonia: String,
        ciudad: String,
        telefono: String,
        correo: String,
        passw: String
    ): String {
        var c_nombres = nombre
        var c_apellidos = apellido
        var c_calle = calle
        var c_numero = numero
        var c_colonia = colonia
        var c_ciudad = ciudad
        var c_telefono = telefono
        var c_email = correo
        var c_clave = passw

        var bodyString = "{\r\n  \"nombre\": \"$c_nombres\"," +
                "\r\n  \"apellido\": \"$c_apellidos\"," +
                "\r\n  \"calle\": \"$c_calle\"," +
                "\r\n  \"numero\": \"$c_numero\"," +
                "\r\n  \"colonia\": \"$c_colonia\"," +
                "\r\n  \"ciudad\": \"$c_ciudad\"," +
                "\r\n  \"telefono\": \"$c_telefono\"," +
                "\r\n  \"email\": \"$c_email\"," +
                "\r\n  \"clave\": \"$c_clave\"\r\n}"

        val client = OkHttpClient().newBuilder()
            .build()
        val mediaType = MediaType.parse("application/json")
        val body = RequestBody.create(mediaType, bodyString)
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/registrarse")
            .method("POST", body)
            .addHeader("Content-Type", "application/json")
            .build()
        val response = client.newCall(request).execute()
        return response.body()!!.string()//se retorna la respuesta en JSON

    }

    fun IniciarSesion(
        email: String,
        passw: String
    ): String {
        val client = OkHttpClient().newBuilder()
            .build()
        val mediaType = MediaType.parse("application/json")
        val body = RequestBody.create(
            mediaType,
            "{\r\n    \"correo\": \"$email\"," +
                    "\r\n    \"clave\": \"$passw\"\r\n}"
        )
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/iniciarsesion")
            .method("POST", body)
            .addHeader("Content-Type", "application/json")
            .build()
        var response = client.newCall(request).execute()

        return response?.body()!!.string()

    }

    fun RecuperarContraseña(
        email: String
    ): String {
        val client = OkHttpClient().newBuilder()
            .build()
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/recuperarclave/?email=${email}")
            .method("GET", null)
            .build()
        val response = client.newCall(request).execute()
        return response.body()!!.string()//se retorna la respuesta en JSON
    }

    fun ActualizarDireccion(
        idusuario: Int,
        calle: String,
        numero: String,
        colonia: String,
        ciudad: String,
        telefono: String
    ): String {
        val client = OkHttpClient().newBuilder()
            .build()
        val mediaType = MediaType.parse("application/json")
        val body = RequestBody.create(
            mediaType,
            "{\r\n  \"idusuario\": $idusuario," +
                    "\r\n  \"calle\": \"$calle\"," +
                    "\r\n  \"numero\": \"$numero\"," +
                    "\r\n  \"colonia\": \"$colonia\"," +
                    "\r\n  \"ciudad\": \"$ciudad\"," +
                    "\r\n  \"telefono\": \"$telefono\",\r\n}"
        )
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/direccion")
            .method("PUT", body)
            .addHeader("Content-Type", "application/json")
            .addHeader("token", "6a99d2664218c0a23fa064b9debc2455469fdaf0")
            .build()
        val response = client.newCall(request).execute()
        return response.body()!!.string()//se retorna la respuesta en JSON

    }

    fun ObtenerDireccion(
        idusuario: Int
    ): String {
        var response: Response

        val client = OkHttpClient().newBuilder()
            .build()
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/direccion/?idusuario=$idusuario")
            .method("GET", null)
            .addHeader("token", "6a99d2664218c0a23fa064b9debc2455469fdaf0")
            .build()
        response = client.newCall(request).execute()

        return response.body()!!.string()//se retorna la respuesta en JSON
    }

    fun DetalleSucursal(
        idsucursal: Int
    ): String {
        var response: Response

        val client = OkHttpClient().newBuilder()
            .build()
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/versucursal/?idsucursal=$idsucursal")
            .method("GET", null)
            //.addHeader("token", "6a99d2664218c0a23fa064b9debc2455469fdaf0")
            .build()
        response = client.newCall(request).execute()

        return response.body()!!.string()//se retorna la respuesta en JSON
    }

    fun ListaCategorias(
        idsucursal: Int
    ): String {
        var response: Response

        val client = OkHttpClient().newBuilder()
            .build()
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/categorias/?idsucursal=$idsucursal")
            .method("GET", null)
            .build()
        response = client.newCall(request).execute()

        return response.body()!!.string()//se retorna la respuesta en JSON
    }

    fun ListaProductos(
        idcategoria: Int
    ): String {
        var response: Response
        val client = OkHttpClient().newBuilder()
            .build()
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/productos/?idcategoria=$idcategoria")
            .method("GET", null)
            //.addHeader("token", "6a99d2664218c0a23fa064b9debc2455469fdaf0")
            .build()
        response = client.newCall(request).execute()

        return response.body()!!.string()//se retorna la respuesta en JSON
    }

    fun ListaSucursales(
        busqueda: String?
    ): String {
        var response: Response
        val client = OkHttpClient().newBuilder()
            .build()
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/sucursal/?buscar=$busqueda")
            .method("GET", null)
            //.addHeader("token", "6a99d2664218c0a23fa064b9debc2455469fdaf0")
            .build()
        response = client.newCall(request).execute()

        return response.body()!!.string()//se retorna la respuesta en JSON
    }

    fun DetallesProducto(
        idproducto: Int
    ): String {
        val client = OkHttpClient().newBuilder()
            .build()
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/producto/?idproducto=${idproducto}")
            .method("GET", null)
            .build()
        val response = client.newCall(request).execute()

        return response.body()!!.string()//se retorna la respuesta en JSON
    }

    fun GenerarOrden(datosOrden: String): String {
        val client = OkHttpClient().newBuilder()
            .build()
        val mediaType = MediaType.parse("application/json")
        val body = RequestBody.create(
            mediaType, datosOrden
        )
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/generarorden")
            .method("POST", body)
            .addHeader("token", "6a99d2664218c0a23fa064b9debc2455469fdaf0")
            .addHeader("Content-Type", "application/json")
            .build()
        val response = client.newCall(request).execute()

        return response.body()!!.string()//se retorna la respuesta en JSON
    }

    fun ObtenerHistorial(id: Int): String {
        val client = OkHttpClient().newBuilder()
            .build()
        val request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/ordenes/?id=${id}&tipo=usuario")
            .method("GET", null)
            .addHeader("token", "6a99d2664218c0a23fa064b9debc2455469fdaf0")
            .build()
        val response = client.newCall(request).execute()
        return response.body()!!.string()//se retorna la respuesta en JSON
    }

    fun ObtenerPuntos(id: Int): String {
        val client = OkHttpClient().newBuilder()
            .build()
        val request: Request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/puntos/?id=${id}")
            .method("GET", null)
            .addHeader("token", "6a99d2664218c0a23fa064b9debc2455469fdaf0")
            .build()
        val response = client.newCall(request).execute()
        return response.body()!!.string()//se retorna la respuesta en JSON

    }

    fun ListaRecompensas(): String {
        val client = OkHttpClient().newBuilder()
            .build()
        val request: Request = Request.Builder()
            .url("http://dotstudioservices.com/mangiamosservices/Service1.svc/recompensas")
            .method("GET", null)
            .addHeader("token", "6a99d2664218c0a23fa064b9debc2455469fdaf0")
            .build()
        val response = client.newCall(request).execute()
        return response.body()!!.string()//se retorna la respuesta en JSON
    }
}


data class Response(
    val mensaje: String,
    val status: Int
)

data class SesionObject(
    val apellido: String? = "",
    val calle: String? = "",
    val ciudad: String? = "",
    val codigoqr: String? = "",
    val colonia: String? = "",
    val email: String? = "",
    val idusuario: Int? = Int.MIN_VALUE,
    val mensaje: String? = "",
    val nombre: String? = "",
    val numero: String? = "",
    val status: Int? = Int.MIN_VALUE,
    val telefono: String? = "",
    val token: String? = ""
)

data class Productos(
    val id: Int = Int.MIN_VALUE,
    val imagen: String? = "",
    val nombre: String? = ""
)

data class ProductosCategoria(
    val listaproductos: List<Productos>?,
    val mensaje: String? = "",
    val status: Int? = Int.MIN_VALUE
)

data class DireccionObject(
    val calle: String? = "",
    val ciudad: String? = "",
    val colonia: String? = "",
    val mensaje: String? = "",
    val numero: String? = "",
    val status: Int? = Int.MIN_VALUE,
    val telefono: String? = ""
)

data class CategoriasSucursal(
    val listacategorias: List<Categorias>?,
    val mensaje: String?,
    val promocionimagen: String?,
    val status: Int? = Int.MIN_VALUE
)

data class Categorias(
    val id: Int? = Int.MIN_VALUE,
    val imagen: String? = "",
    val nombre: String? = ""
)

data class ListaSucursales(
    val direccion: String,
    val idsucursal: Int,
    val latitud: Double,
    val longitud: Double,
    val nombre: String
)

data class BusquedaSucursales(

    val listasucursales: List<ListaSucursales>,
    val mensaje: String,
    val status: Int
)

data class DetallesSucursal(

    val abierto: Boolean? = false,
    val ciudad: String? = "",
    val comerahi: Boolean? = false,
    val direccion: String? = "",
    val drivethru: Boolean? = false,
    val email: String? = "",
    val horariodomingo: String? = "",
    val horariojueves: String? = "",
    val horariolunes: String? = "",
    val horariomartes: String? = "",
    val horariomiercoles: String? = "",
    val horariosabado: String? = "",
    val horarioviernes: String? = "",
    val idsucursal: Int? = Int.MIN_VALUE,
    val imagen: String? = "",
    val latitud: Double? = Double.MIN_VALUE,
    val longitud: Double? = Double.MIN_VALUE,
    val mensaje: String? = "",
    val nombre: String? = "",
    val ordernarporinternet: Boolean? = false,
    val recoger: Boolean? = false,
    val servicioadomicilio: Boolean? = false,
    val status: Int,
    val stripe: String? = "",
    val telefono: String? = ""
)

data class DatosOrden(
    var idsucursal: Int,
    var idusuario: Int,
    var enviocalle: String?,
    var envionumero: String?,
    var enviocolonia: String?,
    var enviociudad: String?,
    var enviotelefono: Int,
    var observaciones: String?,
    var totalpagado: Double = 0.00,
    var lugarpago: String?,
    var estatus: String?,
    var tipo: String?,
    var tokenstripe: String?,
    var latitud: Double?,
    var longitud: Double?,
    var productoOrdenes: ArrayList<ProductoOrdenes>?,
    var recompensaOrdenes: ArrayList<RecompensaOrdenes>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.createTypedArrayList(ProductoOrdenes.CREATOR),
        parcel.createTypedArrayList(RecompensaOrdenes.CREATOR)
    )


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idsucursal)
        parcel.writeInt(idusuario)
        parcel.writeString(enviocalle)
        parcel.writeString(envionumero)
        parcel.writeString(enviocolonia)
        parcel.writeString(enviociudad)
        parcel.writeInt(enviotelefono)
        parcel.writeString(observaciones)
        parcel.writeDouble(totalpagado)
        parcel.writeString(lugarpago)
        parcel.writeString(estatus)
        parcel.writeString(tipo)
        parcel.writeList(productoOrdenes as ArrayList<*>?)
        parcel.writeList(recompensaOrdenes as ArrayList<*>?)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DatosOrden> {
        override fun createFromParcel(parcel: Parcel): DatosOrden {
            return DatosOrden(parcel)
        }

        override fun newArray(size: Int): Array<DatosOrden?> {
            return arrayOfNulls(size)
        }
    }
}

data class ProductoOrdenes(
    var idproducto: Int,
    var cantidad: Int,
    var idvariacion: Int,
    var total: Double,
    var complementos: String?,
    var ingredientesextras: String?,
    var comentario: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readDouble(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idproducto)
        parcel.writeInt(cantidad)
        parcel.writeInt(idvariacion)
        parcel.writeDouble(total)
        parcel.writeString(complementos)
        parcel.writeString(ingredientesextras)
        parcel.writeString(comentario)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductoOrdenes> {
        override fun createFromParcel(parcel: Parcel): ProductoOrdenes {
            return ProductoOrdenes(parcel)
        }

        override fun newArray(size: Int): Array<ProductoOrdenes?> {
            return arrayOfNulls(size)
        }
    }


}

data class RecompensaOrdenes(
    var idrecompensa: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(parcel.readInt())


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(idrecompensa)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RecompensaOrdenes> {
        override fun createFromParcel(parcel: Parcel): RecompensaOrdenes {
            return RecompensaOrdenes(parcel)
        }

        override fun newArray(size: Int): Array<RecompensaOrdenes?> {
            return arrayOfNulls(size)
        }
    }
}

data class DetallesProducto(

    var complementos: List<Complementos>,
    var idproducto: Int? = Int.MIN_VALUE,
    var imagen: String? = "",
    var ingredientes: String? = "",
    var mensaje: String? = "",
    var nombre: String? = "",
    var status: Int? = Int.MIN_VALUE,
    var variaciones: List<Variaciones>
)

data class Variaciones(

    val idvariacion: Int,
    val nombre: String,
    val precio: Int
)

data class Complementos(

    val idcomplemento: Int,
    val imagen: String,
    val nombre: String,
    val precio: Int
)

data class Ordenes(

    val fecha: String,
    val hora: String,
    val numorden: String,
    val total: Double
)

data class Historial(

    val listaordenes: ArrayList<Ordenes>,
    val mensaje: String,
    val status: Int
)

data class OrdenGenerada(
    val mensaje: String?,
    val status: Int?
)


data class ObtenerRecompensas(

    val listarecompensa: ArrayList<Recompensa>,
    val mensaje: String,
    val status: Int
)

data class Recompensa(
    val cantidad: Int,
    val descripcion: String,
    val fechafin: String,
    val idrecompensa: Int,
    val imagen: String,
    val puntoscosto: Int,
    val tipo: String
)

data class Puntos(
    var mensaje: String?,
    var status: Int?
)